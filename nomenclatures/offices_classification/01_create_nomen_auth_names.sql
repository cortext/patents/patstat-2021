-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- Building the nomenclatures table for names of all patent offices in Patstat
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS nomen_appln_auth;

CREATE TABLE nomen_appln_auth (
    appln_auth varchar(2) COLLATE utf8_unicode_ci NOT NULL,
    auth_name varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    acronym varchar(100) CHARACTER SET utf8 DEFAULT NULL,
    `status` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (appln_auth),
    KEY idx_auth_name (auth_name ASC),
    KEY idx_appln_auth_name (appln_auth ASC, auth_name ASC)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

-- ---------------------------------------------------------------------------------
-- The data from https://wiki.epfl.ch/patstat/documents/country/country_codes.txt 
-- was transformed into csv and then loaded into `nomen_appln_auth` table
LOAD DATA LOCAL INFILE '/nomenclatures/offices_classification/nomen_appln_auth.csv'
INTO TABLE nomen_appln_auth
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
-- Query OK, 225 rows affected (0,01 sec)
-- Records: 225  Deleted: 0  Skipped: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Inserting the patent authority names that are missing by adding new appln_auth 
-- that exist in Patstat but not in the csv list 

INSERT INTO
    nomen_appln_auth (appln_auth)
SELECT
    a.appln_auth
FROM
    tls201_appln AS a
    LEFT JOIN nomen_appln_auth AS b ON a.appln_auth = b.appln_auth
WHERE
    b.appln_auth IS NULL
GROUP BY
    a.appln_auth;
-- Query OK, 6 rows affected (21 min 34.56 sec)
-- Records: 6  Duplicates: 0  Warnings: 0

-- Inserting deprecate codes

ALTER TABLE nomen_appln_auth DISABLE KEYS;

UPDATE `nomen_appln_auth` SET `auth_name`='Serbia and Montenegro', `status`='deprecate' WHERE `appln_auth` ='CS'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='unknown' WHERE `appln_auth` ='ZZ'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='11'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='AA'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='German Democratic Republic', `status`='deprecate' WHERE `appln_auth` ='DD'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='RE'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='RH'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='Soviet Union', `status`='deprecate' WHERE `appln_auth` ='SU';-- 1
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='TP'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='U9'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='W0'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='XH'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='XP'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Yugoslavia', `status`='deprecate' WHERE `appln_auth` ='YU'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='Unknown', `status`='deprecate' WHERE `appln_auth` ='ZR'; -- 0
UPDATE `nomen_appln_auth` SET `auth_name`='Falkland Islands Malvinas', `acronym`='' WHERE `appln_auth`='FK'; -- 1

-- Manual changes of specific information, acronyms and some cleaning

UPDATE `nomen_appln_auth` SET `auth_name`='' WHERE `appln_auth`='EH'; -- 1
UPDATE `nomen_appln_auth` SET `acronym`='' WHERE `appln_auth`='IR'; -- 1
UPDATE `nomen_appln_auth` SET `acronym`='' WHERE `appln_auth`='VG'; -- 1
UPDATE `nomen_appln_auth` SET `acronym`='INPI' WHERE `appln_auth`='FR'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='The China National Intellectual Property Administration', `acronym`='CNIPA' WHERE `appln_auth`='CN'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='Japan Patent Office', `acronym`='JPO' WHERE `appln_auth`='JP'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='World Intellectual Property Organization' WHERE `appln_auth`='WO'; -- 1
UPDATE `nomen_appln_auth` SET `acronym`='IPO' WHERE `appln_auth`='GB'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='German Patent and Trade Mark Office', `acronym`='DPMA' WHERE `appln_auth`='DE'; -- 1
UPDATE `nomen_appln_auth` SET `auth_name`='United States Patent and Trademark Office', `acronym`='USPTO' WHERE `appln_auth`='US'; -- 1

ALTER TABLE nomen_appln_auth ENABLE KEYS;