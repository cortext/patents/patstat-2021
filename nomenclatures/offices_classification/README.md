# Enriching Patstat: Patent Office names nomenclature

*Based on the previous work done for the 2017 version ([here](https://gitlab.com/cortext/patents/patstat/-/tree/dev/nomenclatures/offices_classification)).*

Full names and acronyms of all Patent Offices in Patstat. It is based on the names provided by [EPFL](https://wiki.epfl.ch/patstat/documents/country/country_codes.txt), already transformed into a csv file for the previous version update [here](https://gitlab.com/cortext/patents/patstat/-/blob/dev/nomenclatures/offices_classification/nomen_appln_auth.csv).

## Status
* **'in use'** all codes (i.e. country codes or patent office codes) that are in use, following the ISO Norm [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2);
* **'deprecate'** all `appln_auth` that are not any more in use (e.g. 'CS' for 'Serbia and Montenegro' or 'SU' for 'Soviet Union' or 'DD' for 'German Democratic Republic'). Depending on the time stamp you are looking for, some of these codes represent a large amount of patents (e.g. 'DD' or 'SU');
* **'unknown'** only for a few patents with 'ZZ' as `appln_auth`.

## Steps
- [01_create_nomen_auth_names](01_create_nomen_auth_names.sql)
Setup and update the patent offices nomenclature table.

- Queries: [02_nomen_auth_names_stat](02_nomen_auth_names_stat.sql) 
  
  Results: [02_nomen_auth_names_stat_report](02_nomen_auth_names_stat_report.sql)
  * 1.1 Comparison of the codes from nomen_appln_auth table and the Patstat table to find missing codes.
  * 1.2 Total number of patents for each appln_auth in order.
  * 2.1 Number of patents per year for each patent office (Pan-AFRICA offices) from the application year 2000 (inclusive).
  * 2.2 Based on 2.1, ipr_type = “PI” and appln_kind IN (“A”, “W”).