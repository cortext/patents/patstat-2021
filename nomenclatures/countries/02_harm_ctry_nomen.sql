USE patstat2021_staging;

DROP TABLE IF EXISTS ctry_lib_ifris;

CREATE TABLE ctry_lib_ifris (
    ctry_final varchar(2) COLLATE utf8_unicode_ci NOT NULL,
    lib_ctry_harm varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    ctry_harm varchar(2) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (ctry_final),
    KEY IX_lib_ctry (lib_ctry_harm),
    KEY IX_ctry_harm (ctry_harm)
);

INSERT INTO
    ctry_lib_ifris
SELECT
    *
FROM
    u001_nano.ctry_lib_ifris;
-- 349