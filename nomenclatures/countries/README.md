# Countries nomenclature

Running:
```
    chmod +x get_ctry_nomen_data.sh
    ./get_ctry_nomen_data.sh
```
The following will be done:
- [01_geographic_nomen.sql](01_geographic_nomen.sql): build the table nomen_geo_ifris, bringing the harmonized country name, the continent and the region. 195 rows.
- [02_harm_ctry_nomen.sql](02_harm_ctry_nomen.sql): build the table ctry_lib_ifris, bringing the country harmonized name and the iso2 code. 349 rows.
- [03_ctry_iso_nomen.sql](03_ctry_iso_nomen.sql): build the table nomen_ctry_iso, bringing the country harmonized name and the iso2 code. 385 rows.