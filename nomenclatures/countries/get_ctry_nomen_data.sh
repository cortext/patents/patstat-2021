#!/bin/sh\

mysql --defaults-file=~/mycnf.cnf -vvv < 01_geographic_nomen.sql  > 01_geographic_nomen_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_harm_ctry_nomen.sql  > 02_harm_ctry_nomen_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 03_ctry_iso_nomen.sql > 03_ctry_iso_nomen_report.txt 2>&1 &
