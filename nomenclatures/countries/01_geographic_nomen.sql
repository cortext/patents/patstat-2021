USE patstat2021_staging;

DROP TABLE IF EXISTS nomen_geo_ifris;

CREATE TABLE nomen_geo_ifris (
    lib_ctry_harm varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    ctry_harm varchar(2) COLLATE utf8_unicode_ci NOT NULL,
    continent varchar(40) COLLATE utf8_unicode_ci NOT NULL,
    region varchar(40) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (lib_ctry_harm),
    KEY IX_ctr (ctry_harm),
    KEY IX_cont (continent),
    KEY IX_reg (region)
);

INSERT INTO
    nomen_geo_ifris
SELECT
    *
FROM
    u001_nano.nomen_geo_ifris;
-- 195 