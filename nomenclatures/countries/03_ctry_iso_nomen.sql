USE patstat2021_staging;

DROP TABLE IF EXISTS nomen_ctry_iso;

CREATE TABLE nomen_ctry_iso LIKE patstatAvr2017.nomen_ctry_iso;

INSERT INTO
    nomen_ctry_iso
SELECT
    DISTINCT *
FROM
    patstatAvr2017.nomen_ctry_iso;
-- 385