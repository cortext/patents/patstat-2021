USE patstat2021_staging;

LOAD DATA LOCAL INFILE '/home/patstat-2021/nomenclatures/ipc_descriptions/get_missing_data/results/01_ipc_position.output.csv' 
INTO TABLE ipc_position FIELDS TERMINATED BY "\t" LINES TERMINATED BY "\n" IGNORE 1 LINES (
    ipc_position,
    section,
    class,
    subclass,
    full_subclass,
    ipc_version
)
SET
    ipc_position = NULLIF(ipc_position, ''),
    section = NULLIF(section, ''),
    class = NULLIF(class, ''),
    subclass = NULLIF(subclass, ''),
    full_subclass = NULLIF(full_subclass, ''),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 8 rows affected (0,65 sec)
-- Records: 3642  Deleted: 0  Skipped: 3634  Warnings: 0

LOAD DATA LOCAL INFILE '/home/patstat-2021/nomenclatures/ipc_descriptions/get_missing_data/results/02_ipc_description.output.csv' 
INTO TABLE ipc_description FIELDS TERMINATED BY "\t" LINES TERMINATED BY "\n" IGNORE 1 LINES (
    ipc_class_level,
    ipc_position,
    ipc_description,
    `level`,
    ipc_version
)
SET
    ipc_class_level = NULLIF(ipc_class_level, ''),
    ipc_position = NULLIF(ipc_position, ''),
    ipc_description = NULLIF(ipc_description, ''),
    `level` = IF(`level` = '', NULL, `level`),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 3657 rows affected, 15 warnings (0,57 sec)
-- Records: 3657  Deleted: 0  Skipped: 0  Warnings: 15

LOAD DATA LOCAL INFILE '/home/patstat-2021/nomenclatures/ipc_descriptions/get_missing_data/results/03_ipc_list.output.csv' 
INTO TABLE ipc_synom FIELDS TERMINATED BY "\t" LINES TERMINATED BY "\n" IGNORE 1 LINES (ipc_class_level, description, ipc_version)
SET
    ipc_class_level = NULLIF(ipc_class_level, ''),
    description = NULLIF(description, ''),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 3657 rows affected (0,38 sec)
-- Records: 3657  Deleted: 0  Skipped: 0  Warnings: 0

LOAD DATA LOCAL INFILE '/home/patstat-2021/nomenclatures/ipc_descriptions/get_missing_data/results/04_ipc_hierarchy.output.csv' 
INTO TABLE ipc_hierarchy FIELDS TERMINATED BY "\t" LINES TERMINATED BY "\n" IGNORE 1 LINES (ipc_class_level, ancestor, parent, ipc_version)
SET
    ipc_class_level = NULLIF(ipc_class_level, ''),
    ancestor = IF(ancestor IS NULL, '', ancestor),
    parent = IF(parent IS NULL, '', parent),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 3657 rows affected (0,37 sec)
-- Records: 3657  Deleted: 0  Skipped: 0  Warnings: 0