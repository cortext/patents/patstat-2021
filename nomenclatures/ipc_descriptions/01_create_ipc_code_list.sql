-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS `conso_ipc_codes`;

CREATE TABLE conso_ipc_codes AS
SELECT
    ipc_class_symbol,
    MAX(ipc_version) AS last_ipc_version,
    COUNT(appln_id) AS NbAppln
FROM
    tls209_appln_ipc
GROUP BY
    ipc_class_symbol
ORDER BY
    ipc_class_symbol ASC;

-- ---------------------------------------------------------------------------------
-- Count number of distinct IPC codes and number of patent applications, grouped by last_ipc_version field
SELECT
    last_ipc_version,
    COUNT(ipc_class_symbol) AS NbDistinctIPCcodes,
    SUM(NbAppln) AS NbAppln
FROM
    conso_ipc_codes
GROUP BY
    last_ipc_version;