-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS `ipc_position`;

CREATE TABLE `ipc_position` (
    `ipc_position` varchar(15) NOT NULL,
    `section` varchar(1000) DEFAULT NULL,
    `class` varchar(1000) DEFAULT NULL,
    `subclass` varchar(1000) DEFAULT NULL,
    `full_subclass` text DEFAULT NULL,
    `ipc_version` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`ipc_position`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/results/01_ipc_position.output.csv' 
INTO TABLE ipc_position 
FIELDS TERMINATED BY "\t" 
LINES TERMINATED BY "\n"
IGNORE 1 LINES 
(
    ipc_position,
    section,
    class,
    subclass,
    full_subclass,
    ipc_version
)
SET
    ipc_position = NULLIF(ipc_position, ''),
    section = NULLIF(section, ''),
    class = NULLIF(class, ''),
    subclass = NULLIF(subclass, ''),
    full_subclass = NULLIF(full_subclass, ''),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 639 rows affected (13,57 sec)
-- Records: 73181  Deleted: 0  Skipped: 72542  Warnings: 0

DROP TABLE IF EXISTS `ipc_description`;

CREATE TABLE `ipc_description` (
    `ipc_class_level` varchar(15) NOT NULL,
    `ipc_position` varchar(100) DEFAULT NULL,
    `ipc_description` varchar(10000) DEFAULT NULL,
    `level` int(11) DEFAULT NULL,
    `ipc_version` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`ipc_class_level`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/results/02_ipc_description.output.csv' INTO TABLE ipc_description FIELDS TERMINATED BY "\t" LINES TERMINATED BY "\n" 
IGNORE 1 LINES 
(
    ipc_class_level,
    ipc_position,
    ipc_description,
    `level`,
    ipc_version
)
SET
    ipc_class_level = NULLIF(ipc_class_level, ''),
    ipc_position = NULLIF(ipc_position, ''),
    ipc_description = NULLIF(ipc_description, ''),
    `level` = IF(`level`='', NULL, `level`),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 74071 rows affected, 890 warnings (17,51 sec)
-- Records: 74071  Deleted: 0  Skipped: 0  Warnings: 890

DROP TABLE IF EXISTS `ipc_synom`;

CREATE TABLE `ipc_synom` (
    `ipc_class_level` varchar(15) NOT NULL,
    `description` text DEFAULT NULL,
    `ipc_version` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`ipc_class_level`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/results/03_ipc_list.output.csv' INTO TABLE ipc_synom FIELDS TERMINATED BY "\t" LINES TERMINATED BY "\n" 
IGNORE 1 LINES 
(ipc_class_level, description, ipc_version)
SET
    ipc_class_level = NULLIF(ipc_class_level, ''),
    description = NULLIF(description, ''),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 74071 rows affected (4,87 sec)
-- Records: 74071  Deleted: 0  Skipped: 0  Warnings: 0

DROP TABLE IF EXISTS `ipc_hierarchy`;

CREATE TABLE `ipc_hierarchy` (
    `ipc_class_level` varchar(15) NOT NULL,
    `ancestor` varchar(15) NOT NULL,
    `parent` varchar(15) NOT NULL,
    `ipc_version` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`ipc_class_level`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/results/04_ipc_hierarchy.output.csv' INTO TABLE ipc_hierarchy FIELDS TERMINATED BY "\t" LINES TERMINATED BY "\n" 
IGNORE 1 LINES 
(ipc_class_level, ancestor, parent, ipc_version)
SET
    ipc_class_level = NULLIF(ipc_class_level, ''),
    ancestor = IF(ancestor IS NULL, '', ancestor),
    parent = IF(parent IS NULL, '', parent),
    ipc_version = NULLIF(ipc_version, '');
-- Query OK, 74071 rows affected (7,38 sec)
-- Records: 74071  Deleted: 0  Skipped: 0  Warnings: 0
