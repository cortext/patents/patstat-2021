# Nomenclatures: IPC descriptions

Enriching Patstat: building descriptions for the International Patent Classification as described [here](https://gitlab.com/cortext/patents/patstat/-/tree/dev/nomenclatures/ipc_descriptions).

## Steps
1. [Lists all the IPC values from the current version](01_create_ipc_code_list.sql) 

2. [Creates IPC tables and loads the data](02_create_ipc_tables_and_load_data.sql) 
   
   Loads into our MySql instance [these files](../../../nomenclatures/ipc_descriptions/patstat_2017/results/) with the IPC description data obtained for an older version of the IPC codes list.
  *Note: There are 890 rows without description*

3. [Lists codes missing their description](03_list_missing_codes.sql) 
  
    As some new IPC codes are included from one version to the next. Here a list is created to store the ones not in `ipc_description` table, which means their description and other info is missing.

    - Export the ipc codes to a file to be used as input in [the script to get the descriptions](filing_ipc_descriptions/abstract_from_ipc.py)
        ```shell
        mysql --defaults-file=~/mycnf.cnf -e 'SELECT * FROM patstat2021_staging.ipc_codes_missing_descr' > /home/patstat-2021/nomenclatures/ipc_descriptions/get_missing_data/ipc_codes_input_0.csv
        sed 's/\t/,/g' ipc_codes_input_0.csv > ipc_codes_input.csv
        rm ipc_codes_input_0.csv
        ```

4. [Gets missing descriptions](get_missing_data/)
  
    Gets missing IPC descriptions from the [patclass.api.lens](http://patclass.api.lens.org) API as it was done for [Patstat 2017](../../../nomenclatures/ipc_descriptions/patstat_2017/). The python script was slightly modified.

    ```shell
    cd get_missing_data
    pip install -r requirements.txt
    python3 abstract_from_ipc.py
    ```

5. [Inserts new description data obtained](04_insert_new_data.sql) 

    Once the script is finished, each data file from [results](get_missing_data/results/) will be loaded in the final tables.
    
    *Note: There are now 905 rows without description.*
    ```sql
    SELECT COUNT(*) FROM ipc_description WHERE ipc_description IS NULL; -- 905
    ```
    *In the future, they could eventually be obtained from https://ipcpub.wipo.int/*
