USE patstat2021_staging;

CREATE INDEX idx_ipc_class_symbol USING BTREE ON conso_ipc_codes (ipc_class_symbol);

-- Creates a table with the new IPC codes, which are missing their description
DROP TABLE IF EXISTS ipc_codes_missing_descr;

CREATE TABLE ipc_codes_missing_descr AS
SELECT
    *
FROM conso_ipc_codes a 
WHERE NOT EXISTS (
    SELECT 1
    FROM ipc_description c 
    WHERE a.ipc_class_symbol = c.ipc_class_level
);
-- 3.657

CREATE INDEX idx_ipc_class_symbol USING BTREE ON ipc_codes_missing_descr (ipc_class_symbol);

SELECT COUNT(*) FROM ipc_description WHERE ipc_description IS NULL;
-- 890