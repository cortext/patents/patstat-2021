#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < 01_applt_frac.sql > 01_applt_frac_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_invt_frac.sql > 02_invt_frac_report.txt 2>&1 &
