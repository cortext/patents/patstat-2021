--------------
DROP TABLE IF EXISTS applt_frac_tmp
--------------

Query OK, 0 rows affected, 1 warning (0.11 sec)

--------------
CREATE TABLE applt_frac_tmp AS
SELECT
    appln_id,
    count(*) AS nb
FROM
    applt_addr_ifris
GROUP BY
    appln_id
--------------

Query OK, 84464795 rows affected (58.80 sec)
Records: 84464795  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX idx_appln_id ON applt_frac_tmp(appln_id)
--------------

Query OK, 84464795 rows affected (1 min 0.53 sec)
Records: 84464795  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS applt_addr_ifris_frac
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE applt_addr_ifris_frac AS
SELECT
    a.*,
    1 / b.nb AS frac_applt
FROM
    applt_addr_ifris a,
    applt_frac_tmp b
WHERE
    a.appln_id = b.appln_id
--------------

Query OK, 104100624 rows affected, 65535 warnings (56 min 39.15 sec)
Records: 104100624  Duplicates: 0  Warnings: 9872550

--------------
CREATE INDEX idx_appln_id ON applt_addr_ifris_frac(appln_id)
--------------

Query OK, 104100624 rows affected (3 min 55.87 sec)
Records: 104100624  Duplicates: 0  Warnings: 0

Bye

