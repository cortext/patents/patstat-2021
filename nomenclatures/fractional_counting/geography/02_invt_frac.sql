-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

-- invt_frac
DROP TABLE IF EXISTS invt_frac_tmp;

CREATE TABLE invt_frac_tmp AS
SELECT
    appln_id,
    count(*) AS nb
FROM
    invt_addr_ifris
GROUP BY
    appln_id;

CREATE INDEX idx_appln_id ON invt_frac_tmp(appln_id);

DROP TABLE IF EXISTS invt_addr_ifris_frac;

CREATE TABLE invt_addr_ifris_frac AS
SELECT
    a.*,
    1 / b.nb AS frac_invt
FROM
    invt_addr_ifris a,
    invt_frac_tmp b
WHERE
    a.appln_id = b.appln_id;

CREATE INDEX idx_appln_id ON invt_addr_ifris_frac(appln_id);