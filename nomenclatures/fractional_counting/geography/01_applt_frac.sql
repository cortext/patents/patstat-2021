-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

-- applt_frac
DROP TABLE IF EXISTS applt_frac_tmp;

CREATE TABLE applt_frac_tmp AS
SELECT
    appln_id,
    count(*) AS nb
FROM
    applt_addr_ifris
GROUP BY
    appln_id;

CREATE INDEX idx_appln_id ON applt_frac_tmp(appln_id);

DROP TABLE IF EXISTS applt_addr_ifris_frac;

CREATE TABLE applt_addr_ifris_frac AS
SELECT
    a.*,
    1 / b.nb AS frac_applt
FROM
    applt_addr_ifris a,
    applt_frac_tmp b
WHERE
    a.appln_id = b.appln_id;

CREATE INDEX idx_appln_id ON applt_addr_ifris_frac(appln_id);