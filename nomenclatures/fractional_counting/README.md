# Fractional counting

## Technologies
[technologies/run_01-06_ipc_frac_tech.sh](technologies/run_01-06_ipc_frac_tech.sh)
- Checks for duplicates.
- Calcultes the fractional count by IPC.
- Adds some indexes.
- Calcultes the fractional count by IPC for the clusters nomenclature.
- Checks the integrity of the calculations.

## Geography
[geography/run_01_02_geo_frac.sh](geography/run_01_02_geo_frac.sh)
- Calculates the columns `frac_invt` and `frac_applt`. For example, if an application has 4 inventors then each inventor will have 1/4 (0.25) as `frac_invt` value.
