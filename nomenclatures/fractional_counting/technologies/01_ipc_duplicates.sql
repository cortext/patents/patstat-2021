-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

-- Checking duplicates
DROP TABLE IF EXISTS doub_ipc;

CREATE TABLE doub_ipc AS
SELECT
    appln_id,
    ipc_class_symbol,
    count(*) AS nb
FROM
    tls209_appln_ipc_ifris
GROUP BY
    appln_id,
    ipc_class_symbol
HAVING
    count(*) > 1;

CREATE INDEX idx_appln_id ON doub_ipc (appln_id);

DROP TABLE IF EXISTS doub_ipc2;

CREATE TABLE doub_ipc2 AS
SELECT
    b.*
FROM
    doub_ipc a,
    tls209_appln_ipc_ifris b
WHERE
    a.appln_id = b.appln_id
    AND a.ipc_class_symbol = b.ipc_class_symbol;

CREATE INDEX idx_appln_id ON doub_ipc2 (appln_id);

-- Remove duplicated lines which are level C
DELETE FROM
    tls209_appln_ipc_ifris
WHERE
    (appln_id, ipc_class_symbol, ipc_class_level) IN (
        SELECT
            appln_id,
            ipc_class_symbol,
            ipc_class_level
        FROM
            doub_ipc2
        WHERE
            ipc_class_level = 'C'
    );