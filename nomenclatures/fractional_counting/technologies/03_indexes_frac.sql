-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

ALTER TABLE
    ipc_technology_frac_ifris
ADD
    PRIMARY KEY (appln_id, ipc_class_symbol);

CREATE INDEX idx_domaines ON ipc_technology_frac_ifris (domaines);

CREATE INDEX idx_fields ON ipc_technology_frac_ifris (fields);

CREATE INDEX idx_sfields ON ipc_technology_frac_ifris (sfields);