-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

CREATE INDEX idx_sfields ON nomen_technology_cluster (sfields);

DROP TABLE IF EXISTS ipc_cluster_frac_ifris;

CREATE TABLE ipc_cluster_frac_ifris AS
SELECT
    a.*,
    b.cluster_index
FROM
    ipc_technology_frac_ifris a,
    nomen_technology_cluster b
WHERE
    a.sfields = b.sfields;

ALTER TABLE
    ipc_cluster_frac_ifris
ADD
    PRIMARY KEY (appln_id, ipc_class_symbol);

CREATE INDEX idx_appln_id ON ipc_cluster_frac_ifris(appln_id);

CREATE INDEX idx_domaines ON ipc_cluster_frac_ifris(domaines);

CREATE INDEX idx_fields ON ipc_cluster_frac_ifris(fields);

CREATE INDEX idx_sfields ON ipc_cluster_frac_ifris(sfields);

CREATE INDEX idx_cluster_index ON ipc_cluster_frac_ifris(cluster_index);