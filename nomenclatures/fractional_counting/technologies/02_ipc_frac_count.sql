-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS ipc_tmp;

CREATE TABLE ipc_tmp AS
SELECT
    appln_id,
    count(ipc_class_symbol) AS nb_ipc
FROM
    tls209_appln_ipc_ifris
GROUP BY
    appln_id;

CREATE INDEX idx_appln_id ON ipc_tmp (appln_id);

-- Fractioning by IPC
DROP TABLE IF EXISTS ipc_technology_frac_ifris_tmp1;

CREATE TABLE ipc_technology_frac_ifris_tmp1 AS
SELECT
    a.*,
    c.nb_ipc,
    1 / c.nb_ipc AS frac_ipc
FROM
    tls209_appln_ipc_ifris a,
    ipc_tmp c
WHERE
    a.appln_id = c.appln_id;

CREATE INDEX idx_appln_id ON ipc_technology_frac_ifris_tmp1 (appln_id);

-- Comparing with the nomenclatures table to add the domains and the fields
CREATE INDEX idx_ipc_class_symbol ON ipc_technology_frac_ifris_tmp1 (ipc_class_symbol);

DROP TABLE IF EXISTS ipc_technology_frac_ifris;

CREATE TABLE ipc_technology_frac_ifris AS
SELECT
    a.*,
    b.domaines,
    b.fields,
    b.sfields
FROM
    ipc_technology_frac_ifris_tmp1 a,
    patstat2017_ifris_tables.nomen_technology_ipc b
WHERE
    b.not_ipc = ''
    AND b.not_appln_id = ''
    AND a.ipc_class_symbol LIKE concat(b.ipc, '%');

-- Insertion of special cases NOT IPC
INSERT INTO
    ipc_technology_frac_ifris
SELECT
    a.*,
    b.domaines,
    b.fields,
    b.sfields
FROM
    ipc_technology_frac_ifris_tmp1 a,
    patstat2017_ifris_tables.nomen_technology_ipc b
WHERE
    b.not_ipc != ''
    AND a.ipc_class_symbol LIKE concat(b.ipc, '%')
    AND a.ipc_class_symbol NOT LIKE concat(b.not_ipc, '%');

CREATE INDEX idx_appln_id ON ipc_technology_frac_ifris (appln_id);

-- Insertion of special cases NOT APPLN_ID
DROP TABLE IF EXISTS ipc_tmp_not_appln;

CREATE TABLE ipc_tmp_not_appln AS
SELECT
    a.appln_id,
    a.ipc_class_symbol
FROM
    ipc_technology_frac_ifris_tmp1 a,
    patstat2017_ifris_tables.nomen_technology_ipc b
WHERE
    b.not_appln_id != ''
    AND a.ipc_class_symbol LIKE concat(b.ipc, '%');

CREATE INDEX idx_appln_id ON ipc_tmp_not_appln (appln_id);

DROP TABLE IF EXISTS ipc_tmp_not_appln_2;

CREATE TABLE ipc_tmp_not_appln_2 AS
SELECT
    DISTINCT a.appln_id
FROM
    ipc_tmp_not_appln a,
    ipc_technology_frac_ifris_tmp1 b,
    patstat2017_ifris_tables.nomen_technology_ipc c
WHERE
    c.not_appln_id != ''
    AND a.appln_id = b.appln_id
    AND b.ipc_class_symbol LIKE concat(c.not_appln_id, '%');

CREATE INDEX idx_appln_id ON ipc_tmp_not_appln_2 (appln_id);

DELETE FROM
    ipc_technology_frac_ifris
WHERE
    appln_id IN (
        SELECT
            appln_id
        FROM
            ipc_tmp_not_appln_2
    );

CREATE INDEX idx_appln_id_ipc_class_symbol ON ipc_technology_frac_ifris_tmp1 (appln_id, ipc_class_symbol);

CREATE INDEX idx_appln_id_ipc_class_symbol ON ipc_tmp_not_appln (appln_id, ipc_class_symbol);

-- Selecting patent applications and ipc for the cases not_appln_id
DROP TABLE IF EXISTS ipc_tmp2;

CREATE TABLE ipc_tmp2 AS
SELECT
    DISTINCT a.*,
    c.domaines,
    c.fields,
    c.sfields
FROM
    ipc_technology_frac_ifris_tmp1 a,
    ipc_tmp_not_appln_2 b,
    patstat2017_ifris_tables.nomen_technology_ipc c
WHERE
    a.appln_id = b.appln_id
    AND a.ipc_class_symbol LIKE concat(c.ipc, '%')
    AND c.not_ipc != ''
    AND a.ipc_class_symbol NOT LIKE concat(c.not_ipc, '%')
    AND NOT EXISTS (
		SELECT
			1
		FROM
			ipc_tmp_not_appln d
		WHERE
			a.appln_id = d.appln_id AND a.ipc_class_symbol = d.ipc_class_symbol
);

INSERT INTO
    ipc_tmp2
SELECT
    DISTINCT a.*,
    c.domaines,
    c.fields,
    c.sfields
FROM
    ipc_technology_frac_ifris_tmp1 a,
    ipc_tmp_not_appln_2 b,
    patstat2017_ifris_tables.nomen_technology_ipc c
WHERE
    a.appln_id = b.appln_id
    AND c.not_ipc = ''
    AND a.ipc_class_symbol LIKE concat(c.ipc, '%')
    AND NOT EXISTS (
		SELECT
			1
		FROM
			ipc_tmp_not_appln d
		WHERE
			a.appln_id = d.appln_id AND a.ipc_class_symbol = d.ipc_class_symbol
    );

CREATE INDEX idx_appln_id ON ipc_tmp2(appln_id);

-- Counting the number of IPC per application
DROP TABLE IF EXISTS ipc_tmp3;

CREATE TABLE ipc_tmp3 AS
SELECT
    appln_id,
    count(ipc_class_symbol) AS nb_ipc
FROM
    ipc_tmp2
GROUP BY
    appln_id;

-- Recalculating the fractional
DROP TABLE IF EXISTS ipc_technology_frac_ifris_tmp2;

CREATE TABLE ipc_technology_frac_ifris_tmp2
SELECT
    a.*,
    c.nb_ipc AS nb_ipc_recalcul,
    1 / c.nb_ipc AS frac_ipc_recalcul
FROM
    ipc_tmp2 a,
    ipc_tmp3 c
WHERE
    a.appln_id = c.appln_id;

-- Inserting in the final table
INSERT INTO
    ipc_technology_frac_ifris
SELECT
    appln_id,
    ipc_class_symbol,
    ipc_class_level,
    ipc_version,
    ipc_value,
    ipc_position,
    ipc_gener_auth,
    nb_ipc_recalcul,
    frac_ipc_recalcul,
    domaines,
    FIELDS,
    sfields
FROM
    ipc_technology_frac_ifris_tmp2;

DROP TABLE IF EXISTS ipc_tmp_not_appln_4;

CREATE TABLE ipc_tmp_not_appln_4 AS
SELECT
    appln_id,
    ipc_class_symbol
FROM
    ipc_tmp_not_appln a
WHERE
    NOT EXISTS (
        SELECT
            NULL
        FROM
            ipc_tmp_not_appln_2 b
        WHERE
            a.appln_id = b.appln_id
    );

CREATE INDEX idx_appln_id_ipc_class_symbol ON ipc_tmp_not_appln_4 (appln_id, ipc_class_symbol);

CREATE TABLE ipc_tech_in_table4 AS
SELECT DISTINCT a.*
FROM
    ipc_technology_frac_ifris_tmp1 a
    INNER JOIN ipc_tmp_not_appln_4 b
    ON a.appln_id = b.appln_id
    AND a.ipc_class_symbol = b.ipc_class_symbol;

CREATE TABLE nomen_ipc_tmp AS
SELECT c.domaines,
    c.fields,
    c.sfields,
    concat(c.ipc, '%') AS like_ipc
FROM patstat2017_ifris_tables.nomen_technology_ipc c;

CREATE TABLE test_tmp_result AS
SELECT DISTINCT a.*,
    c.domaines,
    c.fields,
    c.sfields
FROM ipc_tech_in_table4 a
    INNER JOIN nomen_ipc_tmp c
    ON a.ipc_class_symbol LIKE c.like_ipc;

INSERT INTO
    ipc_technology_frac_ifris
SELECT 
    *
FROM test_tmp_result;

DROP TABLE ipc_tech_in_table4;
DROP TABLE nomen_ipc_tmp;
DROP TABLE test_tmp_result;