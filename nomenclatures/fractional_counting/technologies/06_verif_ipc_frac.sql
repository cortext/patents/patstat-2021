﻿-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

SELECT
    count(*)
FROM
    tls209_appln_ipc_ifris;

SELECT
    count(*)
FROM
    ipc_technology_frac_ifris;

SELECT
    count(DISTINCT appln_id)
FROM
    ipc_technology_frac_ifris;

SELECT
    sum(frac_ipc)
FROM
    ipc_technology_frac_ifris;

DROP TABLE IF EXISTS verif_ipc;

CREATE TABLE verif_ipc AS
SELECT
    appln_id,
    sum(frac_ipc) AS nb
FROM
    ipc_technology_frac_ifris
GROUP BY
    appln_id;