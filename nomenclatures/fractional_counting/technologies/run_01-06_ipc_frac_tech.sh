#/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < 01_ipc_duplicates.sql > 01_ipc_duplicates_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_ipc_frac_count.sql > 02_ipc_frac_count_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 03_indexes_frac.sql > 03_indexes_frac_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 04_ipc_cluster.sql > 04_ipc_cluster_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 05_ipc_cluster_frac.sql > 05_ipc_cluster_frac_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 06_verif_ipc_frac.sql > 06_verif_ipc_frac_report.txt 2>&1 &
