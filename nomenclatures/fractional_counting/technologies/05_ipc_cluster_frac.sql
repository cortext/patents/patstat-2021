-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp AS
SELECT
    appln_id,
    count(*) AS nb
FROM
    ipc_cluster_frac_ifris
GROUP BY
    appln_id;

ALTER TABLE
    tmp
ADD
    PRIMARY KEY (appln_id);

UPDATE
    ipc_cluster_frac_ifris a,
    tmp b
SET
    a.nb_ipc = b.nb,
    a.frac_ipc = 1 / b.nb
WHERE
    a.appln_id = b.appln_id;