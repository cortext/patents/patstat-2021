USE patstat2021_staging;

DROP TABLE IF EXISTS nomen_technology_cluster;

CREATE TABLE nomen_technology_cluster LIKE patstatAvr2017.nomen_technology_cluster;

INSERT INTO
    nomen_technology_cluster
SELECT
    DISTINCT *
FROM
    patstatAvr2017.nomen_technology_cluster;
-- 401