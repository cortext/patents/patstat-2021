# IPC technology nomenclatures

The tables are brought from older Patstat versions.

- [01_ipc_technology.sql](01_ipc_technology.sql): The `nomen_technology_std` table stores the ipc technology nomenclatures standardized. 
  - The ipc codes for domains (`domaines`), fields and subfields (`sfields`).
  - Their respective description: `lib_domaines`, `lib_fields`, `lib_sfields_long` and `lib_sfields_short`.

- [02_nomen_tech_cluster.sql](02_nomen_tech_cluster.sql): The `nomen_technology_cluster` table stores the same data from the previous point along with the cluster classification: `cluster_index`, `cluster_label`, `lib_cluster_label`.