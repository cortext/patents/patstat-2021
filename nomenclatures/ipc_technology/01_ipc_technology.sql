USE patstat2021_staging;

DROP TABLE IF EXISTS nomen_technology_std;

CREATE TABLE nomen_technology_std LIKE patstatAvr2017.nomen_technology_std;

INSERT INTO
    nomen_technology_std
SELECT
    DISTINCT *
FROM
    patstatAvr2017.nomen_technology_std;
-- 401

ALTER TABLE nomen_technology_std ADD PRIMARY KEY (domaines,fields,sfields);

CREATE INDEX IX_domaines ON nomen_technology_std(domaines);
CREATE INDEX IX_fields ON nomen_technology_std(fields);
CREATE INDEX IX_sfields ON nomen_technology_std(sfields);