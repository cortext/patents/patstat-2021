ERROR 1136 (21S01) at line 31: Column count doesn't match value count at row 1
--------------
DROP TABLE IF EXISTS applt_adr_ifris_artif
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE `applt_adr_ifris_artif` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `pm_pp_comp` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `iapplt_epwo` (`appln_id`),
    KEY `isource` (`source`),
    KEY `imetho5` (`methode`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
--------------

Query OK, 0 rows affected (0.01 sec)

--------------
INSERT INTO
    applt_adr_ifris_artif
SELECT
    *
FROM
    applt_addr_ifris
--------------

Bye
ERROR 1064 (42000) at line 8: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'INSERT INTO
    applt_adr_ifris_artif
SELECT
    *
FROM
    applt_addr_ifris' at line 25
--------------
DROP TABLE IF EXISTS applt_adr_ifris_artif
--------------

Query OK, 0 rows affected (0.01 sec)

--------------
CREATE TABLE `applt_adr_ifris_artif` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `pm_pp_comp` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `invt_seq_nr` smallint(4) DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `idx_appln_id` (`appln_id`),
    KEY `idx_source` (`source`),
    KEY `idx_methode` (`methode`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci

INSERT INTO
    applt_adr_ifris_artif
SELECT
    *
FROM
    applt_addr_ifris
--------------

Bye
--------------
DROP TABLE IF EXISTS applt_adr_ifris_artif
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE `applt_adr_ifris_artif` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `pm_pp_comp` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `invt_seq_nr` smallint(4) DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `idx_appln_id` (`appln_id`),
    KEY `idx_source` (`source`),
    KEY `idx_methode` (`methode`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
--------------

Query OK, 0 rows affected (0.02 sec)

--------------
INSERT INTO
    applt_adr_ifris_artif
SELECT
    *
FROM
    applt_addr_ifris
--------------

Query OK, 107655504 rows affected (25 min 18.09 sec)
Records: 107655504  Duplicates: 0  Warnings: 0

--------------
INSERT INTO
    applt_adr_ifris_artif
SELECT
    DISTINCT *
FROM
    w004_applt_addr_ifris
WHERE
    (appln_id, person_id) NOT IN (
        SELECT
            appln_id,
            person_id
        FROM
            applt_addr_ifris
    )
--------------

Query OK, 9536918 rows affected (18 min 27.55 sec)
Records: 9536918  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS invt_adr_ifris_artif
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE `invt_adr_ifris_artif` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `invt_seq_nr` smallint(4) DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `idx_appln_id` (`appln_id`),
    KEY `idx_source` (`source`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
--------------

Query OK, 0 rows affected (0.01 sec)

--------------
INSERT INTO
    invt_adr_ifris_artif
SELECT
    *
FROM
    invt_addr_ifris
--------------

Query OK, 200755133 rows affected (58 min 16.14 sec)
Records: 200755133  Duplicates: 0  Warnings: 0

--------------
INSERT INTO
    invt_adr_ifris_artif
SELECT
    DISTINCT *
FROM
    w004_invt_addr_ifris
WHERE
    (appln_id, person_id) NOT IN (
        SELECT
            appln_id,
            person_id
        FROM
            invt_addr_ifris
    )
--------------

Query OK, 13760957 rows affected (18 min 39.66 sec)
Records: 13760957  Duplicates: 0  Warnings: 0

Bye
