-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

CREATE INDEX idx_inpadoc_family_id on tls201_appln (inpadoc_family_id);

DROP TABLE IF EXISTS w001_artif_01_complete_info;

CREATE TABLE w001_artif_01_complete_info(
	artif_appln_id int(10),
	artif_filing_year int(4),
	artif_filing_year_type varchar(15),
	artif_auth varchar(2),
	artif_type int(1),
	fam_inpadoc_id int(10),
	fam_inpa_type varchar(10),
	sib_appln_id int(10),
	sib_filing_year int(4),
	sib_auth varchar(2),
	sib_has_year varchar(4),
	sib_is_add varchar(4),
	sib_has_address varchar(4),
	diffYear int(2) DEFAULT -1
) ENGINE = MyISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO
	w001_artif_01_complete_info
SELECT
	a.appln_id as artif_appln_id,
	a.appln_filing_year as artif_filing_year,
	IF(
		a.appln_filing_year = 9999,
		'withoutYear',
		'withYear'
	) as artif_filing_year_type,
	a.appln_auth as artif_auth,
	a.artificial as artif_type,
	d.inpadoc_family_id as fam_inpadoc_id,
	IF(
		a.appln_id = e.appln_id,
		'Himself',
		IF(
			c.appln_id IS NOT NULL,
			'DirectPrior',
			IF(g.appln_id IS NOT NULL, 'DirectCont', 'Indirect')
		)
	) as fam_inpa_type,
	e.appln_id as sib_appln_id,
	e.appln_filing_year as sib_filing_year,
	e.appln_auth as sib_auth,
	IF(e.appln_filing_year = 9999, 'No', 'Yes') as sib_has_year,
	'' as sib_is_add,
	'' as sib_has_address,
	-1 as diff_dates
FROM
	tls201_appln a
	INNER JOIN tls201_appln b ON a.appln_id = b.appln_id
	INNER JOIN tls201_appln d ON b.inpadoc_family_id = d.inpadoc_family_id
	LEFT JOIN tls204_appln_prior c ON a.appln_id = c.prior_appln_id
	AND d.appln_id = c.appln_id
	LEFT JOIN tls216_appln_contn g ON a.appln_id = g.parent_appln_id
	AND d.appln_id = g.appln_id
	LEFT JOIN tls201_appln e ON e.appln_id = d.appln_id
WHERE
	a.artificial <> 0
	AND a.singleton = 0;

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(artif_appln_id);

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(artif_filing_year);

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(artif_auth);

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(fam_inpa_type);

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(artif_auth, fam_inpa_type);

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(sib_appln_id);

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(sib_auth);

UPDATE
	w001_artif_01_complete_info a
	INNER JOIN applt_addr_ifris b ON a.sib_appln_id = b.appln_id
SET
	a.sib_has_address = 'yes'
WHERE
	a.artif_filing_year <> 9999
	AND b.person_address <> "";

UPDATE
	w001_artif_01_complete_info a
	INNER JOIN invt_addr_ifris b ON a.sib_appln_id = b.appln_id
SET
	a.sib_has_address = 'yes'
WHERE
	a.artif_filing_year <> 9999
	AND b.person_address <> "";

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(sib_has_address);

ALTER TABLE
	w001_artif_01_complete_info
ADD
	INDEX(artif_filing_year_type);