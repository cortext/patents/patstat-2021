-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS tls202_appln_title_tmp;

CREATE TABLE `tls202_appln_title_tmp` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `appln_title_lg` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `appln_title` varchar(3500) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

DROP TABLE IF EXISTS tls203_appln_abstr_tmp;

CREATE TABLE `tls203_appln_abstr_tmp` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `appln_abstract_lg` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `appln_abstract` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO
    tls202_appln_title_tmp
SELECT
    artif_appln_id,
    b.appln_title_lg,
    b.appln_title
FROM
    w001_artif_01_complete_info a
    INNER JOIN tls202_appln_title b ON a.sib_appln_id = b.appln_id
WHERE
    a.sib_is_add = 'Yes';

-- Delete the possible duplicates in title
DELETE FROM
    tls202_appln_title_tmp
WHERE
    appln_id IN (
        SELECT
            appln_id
        FROM
            tls202_appln_title
    );

INSERT INTO
    tls203_appln_abstr_tmp
SELECT
    artif_appln_id,
    b.appln_abstract_lg,
    b.appln_abstract
FROM
    w001_artif_01_complete_info a
    INNER JOIN tls203_appln_abstr b ON a.sib_appln_id = b.appln_id
WHERE
    a.sib_is_add = 'Yes';

-- Delete possible duplicates if there are any
DELETE FROM
    tls203_appln_abstr_tmp
WHERE
    appln_id IN (
        SELECT
            appln_id
        FROM
            tls203_appln_abstr
    );

DROP TABLE IF EXISTS tls202_appln_title_ifris;

CREATE TABLE `tls202_appln_title_ifris` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `appln_title_lg` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `appln_title` varchar(3500) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

-- Add titles existing before propagation and artificials
ALTER TABLE
    `tls202_appln_title_ifris` DISABLE KEYS;

INSERT INTO
    tls202_appln_title_ifris
SELECT
    *
FROM
    tls202_appln_title;

-- Add titles of the artificial patents
INSERT INTO
    tls202_appln_title_ifris
SELECT
    *
FROM
    tls202_appln_title_tmp;

ALTER TABLE
    `tls202_appln_title_ifris` ENABLE KEYS;

-- Abstracts
DROP TABLE IF EXISTS tls203_appln_abstr_ifris;

CREATE TABLE `tls203_appln_abstr_ifris` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `appln_abstract_lg` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `appln_abstract` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

ALTER TABLE
    `tls203_appln_abstr_ifris` DISABLE KEYS;

-- Add abstracts before the propagation of artificials
INSERT INTO
    tls203_appln_abstr_ifris
SELECT
    *
FROM
    tls203_appln_abstr;

-- Add abstracts of the artificials
INSERT INTO
    tls203_appln_abstr_ifris
SELECT
    *
FROM
    tls203_appln_abstr_tmp;

ALTER TABLE
    `tls203_appln_abstr_ifris` ENABLE KEYS;

-- Selecting the distinct set of values to avoid duplicates
DROP TABLE IF EXISTS w004_applt_addr_ifris;

CREATE TABLE w004_applt_addr_ifris
SELECT
    DISTINCT a.artif_appln_id AS appln_id,
    b.person_id,
    b.doc_std_name_id,
    b.person_name,
    b.person_address,
    b.person_ctry_code,
    'ARTIF' AS source,
    b.pm_pp_comp,
    b.qualif_comp,
    b.name_comp,
    b.adr_comp,
    b.street_comp,
    b.zip_code_comp,
    b.city_comp,
    b.ctry_comp,
    'MET1' AS methode,
    b.invt_seq_nr
FROM
    w001_artif_01_complete_info a
    INNER JOIN applt_addr_ifris b ON a.sib_appln_id = b.appln_id
    INNER JOIN tls201_appln f ON a.artif_appln_id = f.appln_id
    INNER JOIN tls207_pers_appln c ON b.appln_id = c.appln_id
    AND b.person_id = c.person_id
WHERE
    a.sib_is_add = 'Yes';

DROP TABLE IF EXISTS w004_invt_addr_ifris;

CREATE TABLE w004_invt_addr_ifris
SELECT
    DISTINCT a.artif_appln_id AS appln_id,
    b.person_id,
    b.doc_std_name_id,
    b.person_name,
    b.person_address,
    b.person_ctry_code,
    'ARTIF' AS source,
    b.qualif_comp,
    b.name_comp,
    b.adr_comp,
    b.street_comp,
    b.zip_code_comp,
    b.city_comp,
    b.ctry_comp,
    'MET1' AS methode,
    b.invt_seq_nr
FROM
    w001_artif_01_complete_info a
    INNER JOIN invt_addr_ifris b ON a.sib_appln_id = b.appln_id
    INNER JOIN tls201_appln f ON a.artif_appln_id = f.appln_id
    INNER JOIN tls207_pers_appln c ON b.appln_id = c.appln_id
    AND b.person_id = c.person_id
WHERE
    a.sib_is_add = 'Yes';

ALTER TABLE
    w004_applt_addr_ifris
ADD
    INDEX(appln_id, person_id);

ALTER TABLE
    w004_invt_addr_ifris
ADD
    INDEX(appln_id, person_id);