-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

CREATE TABLE backup_04072022_tls201_appln AS
SELECT * FROM tls201_appln;

-- Update to change the nomenclature of artificials with two digits
UPDATE tls201_appln
SET artificial=IF(artificial=1,10,IF(artificial=2,20,If(artificial=3,30,40)))
WHERE artificial<>0;

-- Add the information concerning artificial patents enriched according to the type of artificial
UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.artificial=IF(a.appln_filing_year=b.artif_filing_year,11,12)
WHERE a.artificial=10 AND b.sib_is_add='Yes';

UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.artificial=IF(a.appln_filing_year=b.artif_filing_year,21,22)
WHERE a.artificial=20 AND b.sib_is_add='Yes';

UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.artificial=IF(a.appln_filing_year=b.artif_filing_year,31,32)
WHERE a.artificial=30 AND b.sib_is_add='Yes';

-- Add the new year for the artificials where the year was taken from their siblings
UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.appln_filing_year=b.artif_filing_year
WHERE a.artificial IN (12,22,32) AND b.sib_is_add='Yes';
