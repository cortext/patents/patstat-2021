# Artificial patents

*The scripts used were adapted from the MySQL scripts applied in previous versions*

Artificial patents correspond in certain cases to situations that we want to be able to follow. Therefore the goal is to complete the titles, abstract, addresses (for applicants and inventors) and technologies, from the Inpadoc families containing a date. Within these families, there are *siblings*, which are all the sibling patents of the family belonging to the artificial patent that is intended to be completed.

Therefore, there are three identified cases:
- Direct priority sibling: family patent mentioning the artificial by priority.
- Direct continuity sibling: patent of the family mentioning the artificial by a continuity.
- Indirect sibling: patent of the artificial family but not mentioning it either by continuity or priority.

In order to avoid adding a column, it was decided to modify the field `artificial` in the `TLS201_APPLN_IFRIS` table with the different cases mentioned above, as follows:
- Equal to 0: It does not change.
- Equal to 1: it becomes 10. 
  - If the patent is added or enriched with the artificial patent selection method, then it is 11. 
  - If the dates of the artificial patents are enriched (for those who had a year of 9999), then it is 12.
- Equal to 2: it becomes 20.
  - If it has been enriched it becomes 21. 
  - If the dates of the artificial patents are enriched (for those who had a year of 9999), then it is 22.
- Equal to 3: it becomes 30.
  - If it has been enriched, it becomes 31. 
  - If the dates of the artificial patents are enriched (for those who had a year of 9999), then it is 32.
- Equal to 4: it becomes 40.

With this system, the patents that have been completed with this method can be easily checked.

## Steps

- [01_create_table_artificials_with_family_siblings.sql](01_create_table_artificials_with_family_siblings.sql): Create table to store the artificials with their siblings info.
- [02_add_sibling_filing_year_to_artificials.sql](02_add_sibling_filing_year_to_artificials.sql): Add year information from siblings.
- [03_method_get_info_from_sibling.sql](03_method_get_info_from_sibling.sql): Complete from the sibling info.
- [04_create_patstat_tables_with_new_artificials_info.sql](04_create_patstat_tables_with_new_artificials_info.sql)
- [05_create_tables_applt_invt_ifris_uniques.sql](05_create_tables_applt_invt_ifris_uniques.sql)
- [06_create_table_ipc_techno_ifris_artif.sql](06_create_table_ipc_techno_ifris_artif.sql)
- [07_new_artificials_nomenclature.sql](07_new_artificials_nomenclature.sql)
