--------------
CREATE TABLE backup_04072022_tls201_appln AS
SELECT * FROM tls201_appln
--------------

Query OK, 111116943 rows affected (5 min 56.95 sec)
Records: 111116943  Duplicates: 0  Warnings: 0

--------------
UPDATE tls201_appln
SET artificial=IF(artificial=1,10,IF(artificial=2,20,If(artificial=3,30,40)))
WHERE artificial<>0
--------------

Query OK, 13439524 rows affected (9 min 54.34 sec)
Rows matched: 13439524  Changed: 13439524  Warnings: 0

--------------
UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.artificial=IF(a.appln_filing_year=b.artif_filing_year,11,12)
WHERE a.artificial=10 AND b.sib_is_add='Yes'
--------------

Query OK, 7210561 rows affected (6 min 42.62 sec)
Rows matched: 7210561  Changed: 7210561  Warnings: 0

--------------
UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.artificial=IF(a.appln_filing_year=b.artif_filing_year,21,22)
WHERE a.artificial=20 AND b.sib_is_add='Yes'
--------------

Query OK, 0 rows affected (28.55 sec)
Rows matched: 0  Changed: 0  Warnings: 0

--------------
UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.artificial=IF(a.appln_filing_year=b.artif_filing_year,31,32)
WHERE a.artificial=30 AND b.sib_is_add='Yes'
--------------

Query OK, 0 rows affected (0.48 sec)
Rows matched: 0  Changed: 0  Warnings: 0

--------------
UPDATE tls201_appln a
INNER JOIN w001_artif_01_complete_info b
ON a.appln_id=b.artif_appln_id
SET a.appln_filing_year=b.artif_filing_year
WHERE a.artificial IN (12,22,32) AND b.sib_is_add='Yes'
--------------

Query OK, 7012 rows affected (0.62 sec)
Rows matched: 7012  Changed: 7012  Warnings: 0

Bye
