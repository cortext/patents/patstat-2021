-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS w003_methode_selection_info_artif;

CREATE TABLE w003_methode_selection_info_artif (
    artif_appln_id int(10),
    sib_appln_id int(10),
    methode_selection char(2),
    KEY artif_appln_id(artif_appln_id),
    KEY sib_appln_id(sib_appln_id)
);

ALTER TABLE
    w003_methode_selection_info_artif DISABLE KEYS;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'A1'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND artif_auth <> 'US'
            AND sib_auth IN ('WO', 'EP', 'DE', 'FR', 'JP')
            AND fam_inpa_type = 'DirectPrio'
            AND diffYear < 6
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

ALTER TABLE
    w003_methode_selection_info_artif ENABLE KEYS;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'A2'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND artif_auth = 'US'
            AND sib_auth = 'US'
            AND fam_inpa_type = 'DirectPrio'
            AND diffYear < 6
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'A3'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND sib_auth NOT IN ('US', 'WO', 'EP', 'DE', 'FR', 'JP')
            AND fam_inpa_type = 'DirectPrio'
            AND diffYear < 6
            AND artif_appln_id NOT IN (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'A4'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND artif_auth <> 'US'
            AND sib_auth IN ('WO', 'EP', 'DE', 'FR', 'JP')
            AND fam_inpa_type = 'DirectCont'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'A5'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND artif_auth = 'US'
            AND sib_auth = 'US'
            AND fam_inpa_type = 'DirectCont'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'A6'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND sib_auth NOT IN ('US', 'WO', 'EP', 'DE', 'FR', 'JP')
            AND fam_inpa_type = 'DirectCont'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'B1'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND sib_auth IN ('WO', 'EP', 'DE', 'FR', 'JP')
            AND artif_auth <> 'US'
            AND fam_inpa_type = 'Indirect'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'B2'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND artif_auth = 'US'
            AND sib_auth = 'US'
            AND fam_inpa_type = 'Indirect'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'B3'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND sib_auth NOT IN ('US', 'WO', 'EP', 'DE', 'FR', 'JP')
            AND fam_inpa_type = 'Indirect'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'C1'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND sib_auth = 'US'
            AND fam_inpa_type = 'DirectPrio'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'C2'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND sib_auth = 'US'
            AND fam_inpa_type = 'DirectCont'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

INSERT INTO
    w003_methode_selection_info_artif
SELECT
    a.artif_appln_id,
    a.sib_appln_id,
    'C3'
FROM
    (
        SELECT
            *,
            IF(sib_has_address = 'yes', 0, diffYear + 10) AS score
        FROM
            w001_artif_01_complete_info
        WHERE
            artif_filing_year <> 9999
            AND sib_filing_year <> 9999
            AND sib_auth = 'US'
            AND fam_inpa_type = 'Indirect'
            AND diffYear < 6
            AND NOT EXISTS (
                SELECT
                    artif_appln_id
                FROM
                    w003_methode_selection_info_artif
                WHERE
                    w003_methode_selection_info_artif.artif_appln_id = w001_artif_01_complete_info.artif_appln_id
            )
        ORDER BY
            artif_appln_id,
            score ASC
    ) a
GROUP BY
    artif_appln_id;

-- Add the info to know which sibling was chosen
UPDATE
    w001_artif_01_complete_info a
    INNER JOIN w003_methode_selection_info_artif b ON a.artif_appln_id = b.artif_appln_id
    AND a.sib_appln_id = b.sib_appln_id
SET
    sib_is_add = 'yes';