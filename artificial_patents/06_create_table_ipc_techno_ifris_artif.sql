-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS tls209_appln_ipc_ifris_epwofr_artif;

CREATE TABLE `tls209_appln_ipc_ifris_epwofr_artif` (
  `appln_id` int(10) NOT NULL DEFAULT '0',
  `ipc_class_symbol` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ipc_class_level` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ipc_version` date DEFAULT NULL,
  `ipc_value` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipc_position` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipc_gener_auth` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`appln_id`,`ipc_class_symbol`,`ipc_class_level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- CREATION techno appln_id
DROP TABLE IF EXISTS w006_technologie_frac_ifris_artif;

CREATE TABLE w006_technologie_frac_ifris_artif
SELECT DISTINCT a.artif_appln_id,
	b.ipc_class_symbol, b.ipc_class_level, b.ipc_version,
	b.ipc_value, b.ipc_position,
	b.ipc_gener_auth
FROM (SELECT *
	FROM w001_artif_01_complete_info
	WHERE sib_is_add='YES' AND
	artif_appln_id NOT IN (SELECT distinct appln_id FROM tls209_appln_ipc_ifris_epwofr)
	) a
INNER JOIN tls209_appln_ipc_ifris_epwofr b
ON a.sib_appln_id=b.appln_id;

alter table w006_technologie_frac_ifris_artif add index(artif_appln_id);
alter table w006_technologie_frac_ifris_artif add index(ipc_class_symbol);

INSERT INTO tls209_appln_ipc_ifris_epwofr_artif
SELECT	*
FROM tls209_appln_ipc_ifris_epwofr;

INSERT INTO tls209_appln_ipc_ifris_epwofr_artif
select DISTINCT *
FROM w006_technologie_frac_ifris_artif;
