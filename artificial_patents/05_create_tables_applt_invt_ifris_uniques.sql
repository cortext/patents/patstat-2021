-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS applt_adr_ifris_artif;

CREATE TABLE `applt_adr_ifris_artif` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `pm_pp_comp` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `invt_seq_nr` smallint(4) DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `idx_appln_id` (`appln_id`),
    KEY `idx_source` (`source`),
    KEY `idx_methode` (`methode`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO
    applt_adr_ifris_artif
SELECT
    *
FROM
    applt_addr_ifris;

INSERT INTO
    applt_adr_ifris_artif
SELECT
    DISTINCT *
FROM
    w004_applt_addr_ifris
WHERE
    (appln_id, person_id) NOT IN (
        SELECT
            appln_id,
            person_id
        FROM
            applt_addr_ifris
    );

DROP TABLE IF EXISTS invt_adr_ifris_artif;

CREATE TABLE `invt_adr_ifris_artif` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `invt_seq_nr` smallint(4) DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `idx_appln_id` (`appln_id`),
    KEY `idx_source` (`source`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO
    invt_adr_ifris_artif
SELECT
    *
FROM
    invt_addr_ifris;

INSERT INTO
    invt_adr_ifris_artif
SELECT
    DISTINCT *
FROM
    w004_invt_addr_ifris
WHERE
    (appln_id, person_id) NOT IN (
        SELECT
            appln_id,
            person_id
        FROM
            invt_addr_ifris
    );