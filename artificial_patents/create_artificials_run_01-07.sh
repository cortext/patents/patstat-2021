#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < 01_create_table_artificials_with_family_siblings.sql > 01_create_table_artificials_with_family_siblings_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_add_sibling_filing_year_to_artificials.sql > 02_add_sibling_filing_year_to_artificials_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 03_method_get_info_from_sibling.sql > 03_method_get_info_from_sibling_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 04_create_patstat_tables_with_new_artificials_info.sql >> 04_create_patstat_tables_with_new_artificials_info_report.txt  2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 05_create_tables_applt_invt_ifris_uniques.sql >> 05_create_tables_applt_invt_ifris_uniques_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 06_create_table_ipc_techno_ifris_artif.sql > 06_create_table_ipc_techno_ifris_artif_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 07_new_artificials_nomenclature.sql > 07_new_artificials_nomenclature_report.txt 2>&1 &
