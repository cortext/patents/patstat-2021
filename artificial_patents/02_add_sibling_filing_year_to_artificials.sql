-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS w002_artif_add_filing_year;

CREATE TABLE w002_artif_add_filing_year(
	artif_appln_id INT(10),
	artif_filing_year int(4),
	fam_inpa_type varchar(10),
	KEY artif_appln_id(artif_appln_id)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

ALTER TABLE
	w002_artif_add_filing_year DISABLE KEYS;

-- For the artificials, looking for the siblings that are direct links and that have a year set
INSERT INTO
	w002_artif_add_filing_year
SELECT
	artif_appln_id,
	MIN(sib_filing_year) as artif_filing_year,
	'DirectPrio' as fam_inpa_type
FROM
	w001_artif_01_complete_info
WHERE
	artif_filing_year = 9999
	AND sib_filing_year <> 9999
	AND fam_inpa_type = 'directprio'
GROUP BY
	artif_appln_id;

ALTER TABLE
	w002_artif_add_filing_year ENABLE KEYS;

INSERT INTO
	w002_artif_add_filing_year
SELECT
	artif_appln_id,
	MIN(sib_filing_year) as artif_filing_year,
	'DirectCont' as fam_inpa_type
FROM
	w001_artif_01_complete_info
WHERE
	artif_filing_year = 9999
	AND sib_filing_year <> 9999
	AND fam_inpa_type = 'directcont'
	AND artif_appln_id NOT in (
		SELECT
			artif_appln_id
		from
			w002_artif_add_filing_year
	)
GROUP BY
	artif_appln_id;

INSERT INTO
	w002_artif_add_filing_year
SELECT
	artif_appln_id,
	MIN(sib_filing_year) as artif_filing_year,
	'Indirect' as fam_inpa_type
FROM
	w001_artif_01_complete_info
WHERE
	artif_filing_year = 9999
	AND sib_filing_year <> 9999
	AND fam_inpa_type = 'Indirect'
	AND artif_appln_id NOT in (
		select
			artif_appln_id
		from
			w002_artif_add_filing_year
	)
GROUP BY
	artif_appln_id;

-- Add year information found by siblings in artificial patents
UPDATE
	w001_artif_01_complete_info a
	INNER JOIN w002_artif_add_filing_year b ON a.artif_appln_id = b.artif_appln_id
SET
	a.artif_filing_year = b.artif_filing_year,
	a.artif_filing_year_type = 'NewYear'
WHERE
	a.artif_filing_year = 9999;

-- Add the difference of years: diffYear
UPDATE
	w001_artif_01_complete_info
SET
	diffYear = ABS(artif_filing_year - sib_filing_year)
WHERE
	artif_filing_year <> 9999
	AND sib_filing_year <> 9999;