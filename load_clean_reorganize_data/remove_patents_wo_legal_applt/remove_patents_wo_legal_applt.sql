USE patstat2021_staging;

-- Select all the appln_id with at least one legal applicant associated
CREATE TABLE patstat2021_lab.patents_with_legal_applt AS
SELECT
    DISTINCT appln_id
FROM
    applt_addr_ifris -- Applicants table, where all the legal entities are
WHERE
    invt_seq_nr = 0; -- It means it is not an inventor (natural person), so we are sure it is a legal entity
-- Query OK, 84464795 rows affected (3 min 35.51 sec)
-- Records: 84464795  Duplicates: 0  Warnings: 0

ALTER TABLE patstat2021_lab.patents_with_legal_applt ADD CONSTRAINT pk_appln_id PRIMARY KEY (appln_id);
CREATE INDEX idx_appln_id USING BTREE ON tls201_appln (appln_id);

-- Select all the appln_id of the patents that will be removed, just to keep track of them
CREATE TABLE patstat2021_lab.patents_without_legal_applt AS
SELECT
    DISTINCT a.appln_id
FROM
    tls201_appln a
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            patstat2021_lab.patents_with_legal_applt b
        WHERE
            a.appln_id = b.appln_id
    );
-- Query OK, 26652148 rows affected (5 min 37.96 sec)
-- Records: 26652148  Duplicates: 0  Warnings: 0

-- Add some missing primary keys and indexes
ALTER TABLE patstat2021_lab.patents_without_legal_applt ADD CONSTRAINT pk_appln_id PRIMARY KEY (appln_id);
ALTER TABLE invt_addr_ifris ADD CONSTRAINT pk_appln_person_id PRIMARY KEY (appln_id,person_id);
ALTER TABLE applt_addr_ifris ADD CONSTRAINT pk_appln_person_id PRIMARY KEY (appln_id,person_id);

CREATE INDEX idx_appln_id USING BTREE ON applt_addr_ifris (appln_id);
CREATE INDEX idx_appln_id USING BTREE ON invt_addr_ifris (appln_id);
CREATE INDEX idx_person_id USING BTREE ON applt_addr_ifris (person_id);
CREATE INDEX idx_person_id USING BTREE ON invt_addr_ifris (person_id);

-- Backup the tables that will be updated
ALTER TABLE applt_addr_ifris RENAME TO patstat2021_lab.backup120722_applt_addr_ifris;
ALTER TABLE invt_addr_ifris RENAME TO patstat2021_lab.backup120722_invt_addr_ifris;
ALTER TABLE tls201_appln RENAME TO patstat2021_lab.backup120722_tls201_appln;

-- Create new applt_addr_ifris table where the appln_id have at least one legal applicant associated
CREATE TABLE applt_addr_ifris LIKE patstat2021_lab.backup120722_applt_addr_ifris;
-- Query OK, 0 rows affected (0.12 sec)

INSERT INTO
    applt_addr_ifris
SELECT
    *
FROM
    patstat2021_lab.backup120722_applt_addr_ifris a
WHERE
    EXISTS (
        SELECT
            1
        FROM
            patstat2021_lab.patents_with_legal_applt b
        WHERE
            a.appln_id = b.appln_id
    );
-- Query OK, 104100624 rows affected (12 min 39.13 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

-- Create new invt_addr_ifris table where the appln_id have at least one legal applicant associated
CREATE TABLE invt_addr_ifris LIKE patstat2021_lab.backup120722_invt_addr_ifris;
-- Query OK, 0 rows affected (0.89 sec)

INSERT INTO
    invt_addr_ifris
SELECT
    *
FROM
    patstat2021_lab.backup120722_invt_addr_ifris a
WHERE
    EXISTS (
        SELECT
            1
        FROM
            patstat2021_lab.patents_with_legal_applt b
        WHERE
            a.appln_id = b.appln_id
    );
-- Query OK, 197449042 rows affected (50 min 23.89 sec)
-- Records: 197449042  Duplicates: 0  Warnings: 0

-- Create new tls201_appln table where the appln_id have at least one legal applicant associated
CREATE TABLE tls201_appln LIKE patstat2021_lab.backup120722_tls201_appln;

INSERT INTO
    tls201_appln
SELECT
    *
FROM
    patstat2021_lab.backup120722_tls201_appln a
WHERE
    EXISTS (
        SELECT
            1
        FROM
            patstat2021_lab.patents_with_legal_applt b
        WHERE
            a.appln_id = b.appln_id
    );
-- Query OK, 84464795 rows affected (1 hour 10 min 44.23 sec)
-- Records: 84464795  Duplicates: 0  Warnings: 0

-- Counting
select count(*) from patstat2021_lab.backup120722_tls201_appln; -- 111.116.943
select count(*) from patstat2021_lab.patents_without_legal_applt; -- 26.652.148
select count(*) from patstat2021_staging.tls201_appln; -- 84.464.795

select count(*) from patstat2021_lab.backup120722_applt_addr_ifris; -- 117.192.422
select count(*) from patstat2021_staging.applt_addr_ifris; -- 104.100.624

select count(*) from patstat2021_lab.backup120722_invt_addr_ifris; -- 214.516.090
select count(*) from patstat2021_staging.invt_addr_ifris; -- 197.449.042