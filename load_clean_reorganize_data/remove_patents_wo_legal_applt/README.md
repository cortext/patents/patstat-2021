# Remove patents without legal entities associated

- Table `tls201_appln`

Total number of patents: 111.116.943 (100%)
Number of patents **without** legal applicant associated: **26.652.148** (24%)
Total number of patents after removing the ones **without** legal applicant associated: 84.464.795 (76%)

- Table `applt_addr_ifris`

Total number of applicants: 117.192.422
Total number of applicants after removing the patents **without** legal applicant associated: 104.100.624 (88,82%)

- Table `invt_addr_ifris`

Total number of inventors: 214.516.090
Total number of inventors after removing the patents **without** legal inventor associated: 197.449.042 (92.04%)
