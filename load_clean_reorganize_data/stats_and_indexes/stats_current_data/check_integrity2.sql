USE patstat2021_raw_data;

-- Application without legal applicant (actor) but at least one inventor
 SELECT 'Without actor but 1 inventor: ', COUNT(DISTINCT appln_id) FROM tls201_appln a
 WHERE NOT EXISTS
 (
	SELECT NULL
	FROM tls207_pers_appln b
	WHERE a.appln_id = b.appln_id
	AND	b.applt_seq_nr > 0
 )
 AND EXISTS
 (
	SELECT NULL
	FROM tls207_pers_appln b
	WHERE a.appln_id = b.appln_id
	AND	b.invt_seq_nr > 0
 );

-- Application without inventor but at least one legal applicant (actor)
 SELECT 'Without inventor but 1 actor: ', COUNT(DISTINCT appln_id) FROM tls201_appln a
 WHERE NOT EXISTS
 (
	SELECT NULL
	FROM tls207_pers_appln b
	WHERE a.appln_id = b.appln_id
	AND	b.invt_seq_nr > 0
 )
 AND EXISTS
 (
	SELECT NULL
	FROM tls207_pers_appln b
	WHERE a.appln_id = b.appln_id
	AND	b.applt_seq_nr > 0
 ); 

-- Missing adresses
SELECT 'Missing addresses: ', COUNT(DISTINCT a.person_id)
FROM tls206_person a
WHERE a.person_address IS NULL;

-- Missing countries
SELECT 'Missing countries: ' , COUNT(DISTINCT a.person_id)
FROM tls206_person a
WHERE a.person_ctry_code IS NULL;
