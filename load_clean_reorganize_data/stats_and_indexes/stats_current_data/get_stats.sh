#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < check_integrity.sql > check_integrity_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < check_integrity2.sql > check_integrity2_report.txt 2>&1 &
