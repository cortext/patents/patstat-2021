USE patstat2021_raw_data;

-- Total number of applicants
SELECT COUNT(DISTINCT appln_id) FROM tls201_appln ;

-- Patent application without applicant and without inventor
SELECT count(DISTINCT appln_id) FROM tls201_appln a
WHERE NOT EXISTS
(
	SELECT NULL
	FROM tls207_pers_appln b
	WHERE a.appln_id = b.appln_id
);

-- Application without applicant (actor)
SELECT COUNT(DISTINCT appln_id) FROM tls201_appln a
WHERE NOT EXISTS
(
	SELECT NULL
	FROM tls207_pers_appln b
	WHERE a.appln_id = b.appln_id
	AND	b.applt_seq_nr > 0
);

-- Application without inventor
SELECT COUNT(DISTINCT appln_id) FROM tls201_appln a
WHERE NOT EXISTS
(
	SELECT NULL
	FROM tls207_pers_appln b
	WHERE a.appln_id = b.appln_id
	AND	b.invt_seq_nr > 0
);

-- Application without IPC
SELECT COUNT(DISTINCT appln_id) FROM tls201_appln a
WHERE NOT EXISTS
(
	SELECT NULL
	FROM tls209_appln_ipc b
	WHERE a.appln_id = b.appln_id
);

-- Application without CPC
SELECT COUNT(DISTINCT appln_id) FROM tls201_appln a
WHERE NOT EXISTS
(
        SELECT NULL
        FROM tls224_appln_cpc b
        WHERE a.appln_id = b.appln_id
);


-- Total number of actors
SELECT COUNT(DISTINCT person_id)
FROM tls206_person;


-- Missing adresses 
SELECT COUNT(DISTINCT a.person_id)
FROM tls206_person a, tls207_pers_appln b
WHERE a.person_id = b.person_id
AND a.person_address IS NULL;


-- Missing countries
SELECT COUNT(DISTINCT a.person_id)
FROM tls206_person a, tls207_pers_appln b
WHERE a.person_id = b.person_id
AND a.person_ctry_code IS NULL;