-- INDEXING PATSTAT
USE patstat2021_staging;

-- --------------------------------------------------------
--
-- Indexes for table `tls201_appln`
-- 
ALTER TABLE tls201_appln
ADD INDEX IX_appln_nr (appln_nr);

-- --------------------------------------------------------
--
-- Indexes for table `tls204_appln_prior`
ALTER TABLE tls204_appln_prior
ADD INDEX IX_ppat_seq_nr (prior_appln_seq_nr);

-- --------------------------------------------------------
--
-- Indexes for table `tls206_person`
ALTER TABLE tls206_person
ADD INDEX IX_name (person_name(50)),
ADD INDEX IX_address (person_address(100)),
ADD INDEX IX_doc_std_name (doc_std_name(50));

-- --------------------------------------------------------
--
-- Indexes for table `tls207_pers_appln`
ALTER TABLE tls207_pers_appln
ADD INDEX IX_applt_seq_nr (applt_seq_nr),
ADD INDEX IX_invt_seq_nr (invt_seq_nr),
ADD INDEX IX_appln_person (appln_id, person_id);

-- --------------------------------------------------------
--
-- Indexes for table `tls209_appln_ipc`
ALTER TABLE tls209_appln_ipc
ADD INDEX IX_id (appln_id);

-- --------------------------------------------------------
--
-- Indexes for table `tls210_appln_n_cls`
ALTER TABLE tls210_appln_n_cls
ADD INDEX IX_appln_id (appln_id),
ADD INDEX IX_nat (nat_class_symbol(15));

-- --------------------------------------------------------
--
-- Indexes for table `tls211_pat_publn`
ALTER TABLE tls211_pat_publn
ADD INDEX IX_publn_nr (publn_nr),
ADD INDEX IX_publn_kind (publn_kind),
ADD INDEX IX_publn_lg (publn_lg);

-- --------------------------------------------------------
--
-- Indexes for table `tls212_citation`
ALTER TABLE tls212_citation
ADD INDEX IX_publn_id (pat_publn_id),
ADD INDEX IX_citn_id (citn_id),
ADD INDEX IX_npl_id (cited_npl_publn_id),
ADD INDEX IX_pat_seq_nr (pat_citn_seq_nr),
ADD INDEX IX_npl_seq_nr (npl_citn_seq_nr),
ADD INDEX IX_origin (citn_origin);

-- --------------------------------------------------------
--
-- Indexes for table `tls215_citn_categ`
ALTER TABLE tls215_citn_categ
ADD INDEX IX_pat_publn_id (pat_publn_id),
ADD INDEX IX_citn_id (citn_id),
ADD INDEX IX_citn_categ (citn_categ);

-- --------------------------------------------------------
--
-- Indexes for table `tls216_appln_contn`
ALTER TABLE tls216_appln_contn
ADD INDEX IX_id (appln_id),
ADD INDEX IX_parent (parent_appln_id),
ADD INDEX IX_type (contn_type);

-- --------------------------------------------------------
--
-- Indexes for table `tls222_appln_jp_class`
ALTER TABLE tls222_appln_jp_class
ADD INDEX IX_appln_jp_class (jp_class_symbol,jp_class_scheme),
ADD INDEX IX_appln_id (appln_id);
