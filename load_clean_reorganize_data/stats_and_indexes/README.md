# Stats and indexes

Some numbers will be obtained from the raw data of Patstat 2021, and also some more indexes will be added to the database that will be modified and enriched, as they might be useful when running some queries.

The file that is being used as parameters for `--defaults-file` has to have this information:
```toml
[client]
port=3307
host=127.0.0.1
user=your_user
password=your_password
```
Or you could instead provide it directly in the command, modifying the sh file.
The host and port variables as set this way when there is an open tunnel 

## Stats

|            Table           |                   Number of rows                  |
|:--------------------------:|:-------------------------------------------------:|
| tls201_appln               | 111.116.943                                       |
| tls202_appln_title         | 91.265.855                                        |
| tls203_appln_abstr         | 69.292.783                                        |
| tls204_appln_prior         | 45.081.379                                        |
| tls205_tech_rel            | 4.150.394                                         |
| tls206_person              | 79.291.634                                        |
| tls207_pers_appln          | 285.543.732                                       |
| tls209_appln_ipc           | 281.862.225                                       |
| tls210_appln_n_cls         | 25.977.190                                        |
| tls211_pat_publn           | 130.921.310                                       |
| tls212_citation            | 404.844.984                                       |
| tls214_npl_publn           | 29.762.512                                        |
| tls215_citn_categ          | 666.584.180                                       |
| tls216_appln_contn         | 4.128.229                                         |
| tls222_appln_jp_class      | 360.735.009                                       |
| tls223_appln_docus         | 39.008.349                                        |
| tls224_appln_cpc           | 307.662.539                                       |
| tls225_docdb_fam_cpc       | 132.558.564                                       |
| tls226_person_orig         | 96.111.920                                        |
| tls227_pers_publn          | 370.323.515                                       |
| tls228_docdb_fam_citn      | 209.295.728                                       |
| tls229_appln_nace2         | 128.931.138                                       |
| tls230_appln_techn_field   | 129.105.923                                       |
| tls231_inpadoc_legal_event | 330.235.243                                       |
| tls801_country             | 242 -> **241** actually in the file and uploaded. |
| tls803_legal_event_code    | 3.921                                             |
| tls901_techn_field_ipc     | 764                                               |
| tls902_ipc_nace2           | 850                                               |
| tls904_nuts                | 2.056                                             |
| eee_ppat                   | 79.291.634                                        |

Executing [stats_current_data/get_stats.sh](stats_current_data/get_stats.sh) will generate some report files, the results are summarized below.

Number of:
- Distinct patent applications: 111.116.943
- Distinct patent applications without applicant and without inventor associated: 21.928.007
- Distinct patent applications without applicant associated: 22.832.627
- Distinct patent applications without inventor associated: 29.028.346
- Distinct patent applications without International Patent Classification (IPC) associated: 17.753.502
- Distinct patent applications without Cooperative Patent Classification (CPC) associated: 49.928.572
- Actors: 79.291.634
- Distinct person applicants and inventors without address associated: 0
- Distinct person applicants and inventors without country associated: 0
- Distinct patent applications without legal applicant (actor) but at least one inventor: 904.620
- Distinct patent applications without inventor but at least one legal applicant (actor): 7.100.339

## Indexes

Executing [add_indexes/add_indexes.sh](add_indexes/add_indexes.sh) will run the sql file to add some more indexes that will be useful for the upcoming queries, and a report file will be generated.