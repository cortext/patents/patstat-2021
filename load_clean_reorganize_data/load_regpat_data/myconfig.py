# Tunnel connection
ssh_host = 'ssh_host'
ssh_username = 'ssh_username'
host = 'host'
port = 3307
# DB connection
database_username = 'database_username'
localhost = '127.0.0.1'
# Info to load data
tablenames = ['regpat_regions', 'regpat_regions_us', 'cpc_classes', 'epo_app_reg', 'epo_inv_reg', 'epo_ipc', 'epo_pct', 'pct_app_reg', 'pct_inv_reg', 'pct_ipc']
filenames = ['REGPAT_REGIONS', 'REGPAT_REGIONS_US', '202107_CPC_Classes', '202107_EPO_App_reg', '202107_EPO_Inv_reg', '202107_EPO_IPC', '202107_EPO_PCT', '202107_PCT_App_reg', '202107_PCT_Inv_reg', '202107_PCT_IPC']
               # Set an empty list if you will not use it, otherwise list the filenames 
               # corresponding to each tablename provided in the tablenames variable, 
               # it does not necessarily have to be the exact same name.
               # Its value is taken into account while csv_already_unzipped is set to True
delete_rows_if_any = True
path_to_file = 'tables_data/'
database_name = 'regpatJul2021'
sep = ',' # csv -> , tsv -> /t 
enclosed_by = '"'
file_extension = 'csv' # csv or txt
csv_already_unzipped = True
rebuild_csv = False
chunks = 5 # Its value is taken while the variable `rebuild_csv` is set True. Otherwise only 1 chunk will be used
           # Each csv file will be divided in this amount of files, to decrease the memory usage,
           # when rebuilding a csv file 
