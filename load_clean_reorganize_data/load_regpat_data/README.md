# Create and load REGPAT data
Once we are provided with the REGPAT data, we can create the schema that will store it, this process is in [create_regpat_schema.sql](create_regpat_schema.sql). 

The data was obtained from OECD directly and stored in our server.

Then to load the data we will reuse the script: [unzip_and_load_data/](../load_new_raw_data/unzip_and_load_data/), for which we create a file called [myconfig.py](myconfig.py) here, that will be later copied there in the unzip_and_load_data script folder (so you need to make sure there is not a file with the same name there), this file contains the configuration to load the REGPAT data properly.

Note: You need to modify the [myconfig.py](myconfig.py) file providing your database credentials or change the configuration if needed. 

## Run
You can modify the bash file (create_and_load_REGPAT.sh)[create_and_load_REGPAT.sh], as you might have located the data files or the unzip_and_load_data script elsewhere.

```sh
chmod +x create_and_load_REGPAT.sh
./create_and_load_REGPAT.sh
```

## Results

Database name: regpatJul2021

| Table             | Number of rows |
|-------------------|---------------:|
| regpat_regions_us |          3.145 |
| regpat_regions    |          5.520 |
| cpc_classes       |     51.169.333 |
| epo_app_reg       |      4.060.210 |
| epo_inv_reg       |      9.967.689 |
| epo_ipc           |     15.414.790 |
| epo_pct           |      3.928.525 |
| pct_app_reg       |      4.365.343 |
| pct_inv_reg       |     10.701.720 |
| pct_ipc           |     14.050.714 |
