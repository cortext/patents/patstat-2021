-- Create REGPAT database, version 2021-07
DROP DATABASE IF EXISTS regpatJul2021;
CREATE DATABASE regpatJul2021;

USE regpatJul2021;

CREATE TABLE `cpc_classes` (
  `appln_id` int(11) DEFAULT NULL,
  `CPC_Class` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `epo_app_reg` (
  `app_nbr` text,
  `appln_id` int(11) DEFAULT NULL,
  `pub_nbr` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `app_name` text,
  `address` text,
  `city` text,
  `postal_code` text,
  `reg_code` text,
  `ctry_code` text,
  `reg_share` int(11) DEFAULT NULL,
  `app_share` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `epo_inv_reg` (
  `app_nbr` text,
  `appln_id` int(11) DEFAULT NULL,
  `pub_nbr` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `inv_name` text,
  `address` text,
  `city` text,
  `postal_code` text,
  `reg_code` text,
  `ctry_code` text,
  `reg_share` int(11) DEFAULT NULL,
  `inv_share` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `epo_ipc` (
  `appln_id` int(11) DEFAULT NULL,
  `prio_year` int(11) DEFAULT NULL,
  `app_year` int(11) DEFAULT NULL,
  `IPC` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `epo_pct` (
  `app_nbr` text,
  `pct_nbr` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `pct_app_reg` (
  `pct_nbr` text,
  `internat_appln_nr` text,
  `appln_id` int(11) DEFAULT NULL,
  `app_name` text,
  `address` text,
  `city` text,
  `postal_code` text,
  `reg_code` text,
  `ctry_code` text,
  `reg_share` int(11) DEFAULT NULL,
  `app_share` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `pct_inv_reg` (
  `pct_nbr` text,
  `internat_appln_nr` text,
  `appln_id` int(11) DEFAULT NULL,
  `inv_name` text,
  `address` text,
  `city` text,
  `postal_code` text,
  `reg_code` text,
  `ctry_code` text,
  `reg_share` int(11) DEFAULT NULL,
  `inv_share` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `pct_ipc` (
  `pct_nbr` text,
  `prio_year` int(11) DEFAULT NULL,
  `app_year` int(11) DEFAULT NULL,
  `IPC` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `regpat_regions` (
  `Ctry_code` text,
  `reg_code` text,
  `reg_label` text,
  `Up_reg_code` text,
  `Up_reg_label` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `regpat_regions_us` (
  `Ctry_code` text,
  `reg_code` text,
  `reg_tl3` text,
  `reg_tl3_label` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
