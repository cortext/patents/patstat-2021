#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < create_regpat_schema.sql 2>&1 &

cp myconfig.py ../load_new_raw_data/unzip_and_load_data/
cd ../load_new_raw_data/unzip_and_load_data/
docker build -t load_regpat_data .
docker run -v /srv/local/REGPAT_202107:/tables_data -it load_regpat_data