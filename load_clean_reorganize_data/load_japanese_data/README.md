# Create tables and load JP new data

The raw data files were stored in our server.

Run the sql file [load_jp_newdata.sql](load_jp_newdata.sql).

## Results 

Database name: patstat2021_lab

| Table                                            | Number of rows |
| ------------------------------------------------ | -------------: |
| JPnew_applicant_ad8_tc5                          |        985.169 |
| JPnew_applicant_ad8_tc5 (distinct appln_id, seq) |        985.169 |
| JPnew_applicant_ad8_tc5 (distinct appln_id)      |        903.541 |
| JPnew_inventor_ad8_tc5                           |      2.352.536 |
| JPnew_inventor_ad8_tc5 (distinct appln_id, seq)  |      2.352.536 |
| JPnew_inventor_ad8_tc5 (distinct appln_id)       |        903.541 |
