CREATE DATABASE patstat2021_lab CHARACTER SET utf8mb4;
USE patstat2021_lab;

DROP TABLE IF EXISTS JPnew_applicant_ad8_tc5;
CREATE TABLE `JPnew_applicant_ad8_tc5` (
  `appln_id` int(11) DEFAULT NULL,
  `appln_nr` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_filing_date` date DEFAULT NULL,
  `appln_filing_year` smallint(6) DEFAULT NULL,
  `seq` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address8` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `towncode5` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOAD DATA LOCAL INFILE 'JPnew_applicant_ad8_tc5.txt'
INTO TABLE patstat2021_lab.JPnew_applicant_ad8_tc5
CHARACTER SET utf8mb4 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n'
(appln_id,appln_nr,appln_filing_date,appln_filing_year,seq,`name`,`address`,address8,towncode5);

CREATE INDEX pk_appln_id_seq USING BTREE ON JPnew_applicant_ad8_tc5 (appln_id,seq);

SELECT count(*) FROM JPnew_applicant_ad8_tc5; -- 985.169
SELECT count(DISTINCT appln_id, seq) FROM JPnew_applicant_ad8_tc5; -- 985.169
SELECT count(DISTINCT appln_id) FROM JPnew_applicant_ad8_tc5; -- 903.541

DROP TABLE IF EXISTS JPnew_inventor_ad8_tc5;
CREATE TABLE `JPnew_inventor_ad8_tc5` (
  `appln_id` int(11) DEFAULT NULL,
  `appln_nr` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_filing_date` date DEFAULT NULL,
  `appln_filing_year` smallint(6) DEFAULT NULL,
  `seq` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address8` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `towncode5` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOAD DATA LOCAL INFILE 'JPnew_inventor_ad8_tc5.txt'
INTO TABLE patstat2021_lab.JPnew_inventor_ad8_tc5
CHARACTER SET utf8mb4 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(appln_id,appln_nr,appln_filing_date,appln_filing_year,seq,`name`,`address`,address8,towncode5);

SELECT count(*) FROM JPnew_inventor_ad8_tc5; -- 2.352.536
SELECT count(DISTINCT appln_id, seq) FROM JPnew_inventor_ad8_tc5; -- 2.352.536
SELECT count(DISTINCT appln_id) FROM JPnew_inventor_ad8_tc5; -- 903.541

CREATE INDEX pk_appln_id_seq USING BTREE ON JPnew_inventor_ad8_tc5 (appln_id,seq);
