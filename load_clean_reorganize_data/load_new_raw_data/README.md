# Load raw data of Patstat (version April 2021)

## Steps

1. Create database
  ```sql
    CREATE DATABASE patstat2021_raw_data DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
  ```
  Make sure when creating this database (or any other that will contain Patstat data) to include the default charset utf8mb4, in this way you won't need to include it each time you create a table and it will deal properly with the different characters of the languages included in Patstat.

2. Create tables
  - The table creation sql files were provided only in a T-SQL syntax. They are in: [tsql_createtable_files](tsql_createtable_files/). 
  - A python script ([tsql_to_mysql_create_table.py](tsql_to_mysql_create_table.py)) was created and used to convert **these specific files** to MySQL syntax.
    - You can change directly in the script: the path where the original tsql files are placed, the path where the new mysql files will be placed, and the matching string (by default: create*.sql) to select the files in the indicated folder.

    After the files are placed in the mentioned folder you just need to run:
    ```
    python tsql_to_mysql_create_table.py
    ```
    Then, the result files will be placed in the folder [mysql_createtable_files](mysql_createtable_files/)

    - The EEE_PAT table was provided later, so a file was created by us in MySQL syntax (based on the info they provided) and placed in this folder.
  
  - Run all the table creation sql files.
    Once all the MySQL table creation files are placed in the above mentioned directory, run the sh file to execute them and the tables will be loaded in the database.
    ```
    chmod +x run_multiple_sql_files.sh
    ./run_multiple_sql_files.sh
    ```
    It generates a report in this folder [createtable_reports](createtable_reports/) for each sql file executed.

3. Load the data into the tables
  - Place all the zip files containing the data in a directory. 
  - Unzip and load the data: a script was created to do so, it is containerized, you can find it in [unzip_and_load_data](unzip_and_load_data/)

4. Add indexes
  - Files provided with the indexes creation: [tsql_create_indexes/](tsql_create_indexes/). As they were provided in T-SQL syntax, the files were taken as reference and the indexes were added with slightly different names.
    
## Additional steps

- The eee_ppat data could not be loaded correctly, an error in the csv file provided was found. Each line had this characters at the end `,"`, corrupting the data, as it did not even correspond to another field value. It was solved rebuilding the file this way:
  ```sh
  sed 's/\,\".$//' eee_ppat_2021a.csv >> eee_ppat_2021a_rebuilt.csv
  ```
