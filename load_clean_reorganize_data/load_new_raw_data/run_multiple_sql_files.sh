#!/bin/sh

FILES="mysql_createtable_files/*"
for FILE in $FILES; do
    filename=$(basename "$FILE" .sql)
    mysql --defaults-file=~/mycnf.cnf -vvv test_raw_data < $FILE > create_table_reports/report_${filename}.txt  2>&1 
done