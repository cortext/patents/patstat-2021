import glob, os, re

path_new_mysql_files = 'mysql_createtable_files'
path_original_tsql_files = './tsql_createtable_files'
files_match = 'create*.sql'
# A list of files is provided, in this case the ones matching the `files_match` value
tsql_files_list = glob.glob(f'{path_original_tsql_files}/{files_match}')


def change_tsql_to_mysql(line):
    if line.startswith(("USE", "GO", "/*", "SET", ") ON", ")ON")):
        return ''
    if line.startswith("CREATE TABLE"):
        return line.replace("[dbo].", "").replace("[","`").replace("]","`")
    if line.startswith((")WITH", ") WITH")):
        return ')'
    
    if "[nvarchar](max)" in line:
        line = line.replace("[nvarchar](max)", "text").replace("NOT NULL", "")
        line = re.sub("DEFAULT.*,", ",", line)
    line = line.replace("[int]", "int").replace("[real]", "float").replace("[char]", "char").replace("[nvarchar]", "varchar").replace("[varchar]", "varchar").replace("[smallint]", "smallint").replace("[tinyint]", "tinyint").replace("[date]", "date")
    line = line.replace("('", "'").replace("')", "'")
    line = line.replace("[", "`").replace("]", "`")
    line = line.replace(")WITH", "`").replace("]", "`")
    line = line.replace("DEFAULT (", "DEFAULT ").replace(") ,", " ,").replace("),", " ,")
    return line

def change_tsql_to_mysql_pk(line):
    if line.startswith("PRIMARY KEY"):
        return "PRIMARY KEY ("
    elif line.startswith((")WITH", ") WITH", ")ON", ") ON", "(", "GO", "/*")):
        return ''
    else:
        return line.replace("[", "`").replace("]", "`").replace("ASC", "").replace("DESC", "")

def split_in_two(original_list):
    size = len(original_list)
    # idx_list: list with indexes where the new list must start
    idx_list = [idx for idx, val in
                enumerate(original_list) if val.startswith("PRIMARY KEY")] 

    res = [original_list[i: j] for i, j in
            zip([0] + idx_list, idx_list + 
            ([size] if idx_list[-1] != size else []))]
            
    return res[0], res[1]


for file in tsql_files_list:

    file_tsql = open(file, "r")
    lines_file_tsql = file_tsql.readlines()

    lines_tsql_fields, lines_tsql_pk = split_in_two(lines_file_tsql)

    lines_mysql_fields = [change_tsql_to_mysql(line) for line in lines_tsql_fields]    
    lines_mysql_pk = [change_tsql_to_mysql_pk(line) for line in lines_tsql_pk]

    lines_file_mysql = lines_mysql_fields + lines_mysql_pk 
    lines_file_mysql.append(")) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;")

    filename = os.path.basename(file)
    result_file = f'{path_new_mysql_files}/mysql_{filename}'
    if os.path.exists(result_file):
        os.remove(result_file)
    file_mysql = open(result_file, "w") 
    file_mysql.writelines(line for line in lines_file_mysql if line.strip())
    file_mysql.close()
