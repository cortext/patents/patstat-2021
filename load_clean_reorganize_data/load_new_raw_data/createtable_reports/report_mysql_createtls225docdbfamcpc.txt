--------------
CREATE TABLE `tls225_docdb_fam_cpc`(
	`docdb_family_id` int NOT NULL DEFAULT '0',
	`cpc_class_symbol` varchar(19) NOT NULL DEFAULT '',
	`cpc_gener_auth` char(2) NOT NULL DEFAULT '',
	`cpc_version` date NOT NULL DEFAULT '9999-12-31',
	`cpc_position` char(1) NOT NULL DEFAULT '',
	`cpc_value` char(1) NOT NULL DEFAULT '',
	`cpc_action_date` date NOT NULL DEFAULT '9999-12-31',
	`cpc_status` char(1) NOT NULL DEFAULT '',
	`cpc_data_source` char(1) NOT NULL DEFAULT '',
PRIMARY KEY (	`docdb_family_id` ,
	`cpc_class_symbol` ,
	`cpc_gener_auth` ,
	`cpc_version` 
)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
--------------

Query OK, 0 rows affected (0.02 sec)

Bye
