--------------
CREATE TABLE `tls206_person`(
	`person_id` int NOT NULL DEFAULT '0',
	`person_name` varchar(500) NOT NULL DEFAULT '',
	`person_name_orig_lg` varchar(500) NOT NULL DEFAULT '',
	`person_address` varchar(1000) NOT NULL DEFAULT '',
	`person_ctry_code` char(2) NOT NULL DEFAULT '',
	`nuts` varchar(5) NOT NULL DEFAULT '',
	`nuts_level` tinyint NOT NULL DEFAULT '9',
	`doc_std_name_id` int NOT NULL DEFAULT '0',
	`doc_std_name` varchar(500) NOT NULL DEFAULT '',
	`psn_id` int NOT NULL DEFAULT '0',
	`psn_name` varchar(500) NOT NULL DEFAULT '',
	`psn_level` tinyint NOT NULL DEFAULT '0',
	`psn_sector` varchar(50) NOT NULL DEFAULT '',
	`han_id` int NOT NULL DEFAULT '0',
	`han_name` varchar(500) NOT NULL DEFAULT '',
	`han_harmonized` int NOT NULL DEFAULT '0',
PRIMARY KEY (	`person_id` 
)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
--------------

Query OK, 0 rows affected (0.01 sec)

Bye
