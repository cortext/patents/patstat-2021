import mysql.connector
import time, getpass, os, glob, logging
from zipfile import ZipFile
import pandas as pd
import myconfig
from sshtunnel import SSHTunnelForwarder

ssh_host = myconfig.ssh_host
ssh_username = myconfig.ssh_username
database_username = myconfig.database_username
host = myconfig.host
port = myconfig.port
localhost = myconfig.localhost
database_name = myconfig.database_name
tablenames = myconfig.tablenames
filenames = myconfig.filenames
delete_rows_if_any = myconfig.delete_rows_if_any
path_to_file = myconfig.path_to_file
rebuild_csv = myconfig.rebuild_csv
csv_already_unzipped = myconfig.csv_already_unzipped
chunks = myconfig.chunks
sep = myconfig.sep
enclosed_by = myconfig.enclosed_by
file_extension = myconfig.file_extension


def open_ssh_tunnel(verbose=False):
    """Open an SSH tunnel and connect using a username and password.
    
    :param verbose: Set to True to show logging
    :return tunnel: Global SSH tunnel connection
    """

    try:
        ssh_password = getpass.getpass(prompt=f'password ssh user "{ssh_username}": ')
    except Exception as error:
        logging.warning('ERROR', error)
    
    global tunnel
    tunnel = SSHTunnelForwarder(
        (ssh_host, 22),
        ssh_username = ssh_username,
        ssh_password = ssh_password,
        remote_bind_address = (host, port)
    )
    
    tunnel.start()


def mysql_connect():
    """Connect to a MySQL server using the SSH tunnel connection
    
    :return db_connection: Global MySQL database connection
    :return db_cursor: Global MySQL database connection cursor
    """

    try:
        database_password = getpass.getpass(prompt=f'password DB user "{database_username}": ')
    except Exception as error:
        logging.warning('ERROR', error)

    params = {
        'user': database_username,
        'password': database_password,
        'host': localhost,
        'port': tunnel.local_bind_port,
        'allow_local_infile': True   
    }

    global db_connection, db_cursor

    db_connection = mysql.connector.connect(**params) # DB connection
    db_cursor = db_connection.cursor()
    print("Database connected")


def unzip_files(list_files):
    for file in list_files:
        try:
            print(f'unzipping {file}')
            zf = ZipFile(file, 'r')
            zf.extractall(path_to_file)
            zf.close()
        except Exception:
            logging.warning(f'Error unzipping file: {file}')
            return False
    return True # to indicate all the files were unzipped successfully
            

def load_csv_into_table(tablename, tablename_prefix, csv_files):
    if delete_rows_if_any:
        print(f'======================================================= Deleting existing data from table: {tablename}')

        query0 = f'delete from {database_name}.{tablename};'
        print(query0)
        tr = db_connection._execute_query(query0)
        print(tr)
    print(f'======================================================= Loading table: {tablename}')
    global chunks
    
    for csv_file in csv_files:
        number_lines = sum(1 for row in (open(csv_file)))
        print(f'number of lines: {number_lines}')
        if not rebuild_csv: chunks = 1
        rowsize = int(number_lines/chunks) + 1
        print(f'=========================== Loading {file_extension} file: {csv_file}')
        print(f'row size: {rowsize}')
        
        for i in range(1,number_lines,rowsize):
            if rebuild_csv:
                df = pd.read_csv(csv_file,
                    sep=f'{sep}',
                    header=None,
                    nrows = rowsize, #number of rows to read at each loop
                    skiprows = i, #skip rows that have been read
                    engine = 'python',
                    quotechar = f'{enclosed_by}',
                    quoting = 2
                )
                
                out_csv = f'{path_to_file}{tablename_prefix}_{i}.{file_extension}'
                
                df.to_csv(out_csv,
                    index=False,
                    header=False,
                    mode='a',#append data to csv or txt file
                    chunksize=rowsize)#size of data to append for each loop
            else:
                out_csv = csv_file

            query1 = (
                f"LOAD DATA LOCAL INFILE '{out_csv}'\n" +
                f"INTO TABLE {database_name}.{tablename}\n" +
                f"CHARACTER SET utf8mb4 \n" +
                f"FIELDS TERMINATED BY '{sep}' \n" +
                f"ENCLOSED BY '{enclosed_by}' \n" +
                f"LINES TERMINATED BY " + 
                repr('\n') + 
                f" ;"
            )
            print(query1)
            tr = db_connection._execute_query(query1)
            print(tr)
            print(f'Warnings: {db_cursor.fetchwarnings()}')
            if rebuild_csv: os.remove(out_csv) 
            print(f"--- {time.time() - start_time} seconds ---")
        if not csv_already_unzipped: os.remove(csv_file)


open_ssh_tunnel()
mysql_connect()

start_time = time.time()
print(f'Start time: {time.ctime()}')
i = 0

for tablename in tablenames:
    tablename_prefix = tablename.split('_', 1)[0]
    unzipped_correctly = False

    if not csv_already_unzipped:
        zip_files = glob.glob(f"{path_to_file}{tablename_prefix}*.zip") + glob.glob(f"{path_to_file}{tablename_prefix}*.7z") # get the list of all the zip and 7z files related to this table
        unzipped_correctly = unzip_files(zip_files)

    if (unzipped_correctly or csv_already_unzipped) and not filenames:
        print(f"{path_to_file}{tablename_prefix}*.{file_extension}")
        csv_files = glob.glob(f"{path_to_file}{tablename_prefix}*.{file_extension}") # get the list of all the files related to this table with the specified extension
        csv_files.sort()
        print(f'{file_extension} files')
        print(csv_files)
        load_csv_into_table(tablename, tablename_prefix, csv_files)

    if filenames:
        load_csv_into_table(tablename, filenames[i], [filenames[i]])
        i += 1

print(f"Finished --- {time.time() - start_time} seconds ---")