# Unzip and load data to a MySQL database

When being provided with new Patstat versions, we need to load them into some database to be able to perform some procedures in order to enrich the data.
This script allows you to load data which is given to you in **csv** or **txt** files zipped (or not) either with **zip** or **7z** extension, into a database connecting through an SSH tunnel. You will only have to provide some configuration parameters, run the docker container, wait for the data to be loaded, and that's it.

## Configure

You need to create the file `myconfig.py` for which an example is provided called `myconfig.example.py`.

The first part is for tunnel connection parameters: ssh host, ssh username, host and port. Second part, for the database connection: database username and localhost.
The third and last part contains the following variables:
- tablenames: list of the tables already created in the database, where we want to load the data.
- delete_rows_if_any: if set to True it will delete all the rows in the tables provided, before it starts loading the data.
- path_to_file: path of the folder where all the csv files zipped or unzipped are. (Do not forget to put `/` at the end of the string).
- database_name: where the tables are.
- sep: separator of the csv file.
- enclosed_by: enclosing character used in the csv file.
- file_extension: defines whether the extension of the files is csv or txt. 
  - **Note: In the other variables names explained here, that contain csv, it is not a problem if you set file_extension as txt, it is just to simplify the variable names**.
- csv_already_unzipped: in case the files were already unzipped or provided directly as csv you just have to set this as `True`. Otherwise the files with **zip** and **7z** extensions will be unzipped.
- rebuild_csv: in some cases the loading process might have some issues, which sometimes can be solved re-building the csv. Set it to `True` if you want to re-build it.
- chunks: when rebuilding the csv file, it may take too much RAM memory if the file is big, so it is good to split it. The number you provide here will be the number of files in which the original file will be split into, as long as you have set `rebuild_csv` to `True`.

In the case of Patstat, the names of the files containing the data do not usually correspond exactly to the tablenames created in the database, but they do have the same prefix (before the first underscore `_`), for example: for the table `tls201_appln` the data file is called `tls201_part01.csv` which is zipped as `tls201_part01.zip`. 

Then, the script was built to take the prefix of each table in `tablenames` (which is the list we provide with the tables where we want to load the data), and search for files with this prefix in the `path_to_file`.

### Constraints
- If you want to **unzip** and load your files, provide `csv_already_unzipped = False` in config.py, if they are not zipped but directly available as csv (or txt) set it as True.
- Each tablename (`tablenames=["table1_person", "table2_library"]`]) needs to have the same prefix on the name as the file intended to be unziped and then upload. 
  - Prefix: part of the name before the first underscore `_`, if there is no underscore, the whole name will be taken.
  - So, having `table1_person.zip` then unzipped as `table1_part1_basic_info.csv`, `table1_part2_address.csv`, `table1_part3.csv`, ... They will be loaded with no problem as they share the same prefix before the first underscore `_`.

## Run script

Run the following inside the folder containing the Dockerfile. 
  - Replace `container_name` as you want to name the container.
  - Provide the path where the files are in you local machine `local_path_to_files` and make sure `container_path_to_files` has the value which is being used in [myconfig.py](myconfig.example.py) for the variable `path_to_file`.

```shell=
docker build -t container_name .
docker run -v local_path_to_files:container_path_to_files -it container_name
```

Example:
```shell=
docker build -t unzip_and_load .
docker run -v /srv/local/patstat_2021/tables_zip_data:/tables_data -it unzip_and_load
```