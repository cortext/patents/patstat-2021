CREATE TABLE `tls209_appln_ipc`(
	`appln_id` int NOT NULL DEFAULT '0',
	`ipc_class_symbol` varchar(15) NOT NULL DEFAULT '',
	`ipc_class_level` char(1) NOT NULL DEFAULT '',
	`ipc_version` date NOT NULL DEFAULT '9999-12-31',
	`ipc_value` char(1) NOT NULL DEFAULT '',
	`ipc_position` char(1) NOT NULL DEFAULT '',
	`ipc_gener_auth` char(2) NOT NULL DEFAULT '',
PRIMARY KEY (	`appln_id` ,
	`ipc_class_symbol` 
)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;