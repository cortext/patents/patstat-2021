CREATE TABLE `tls801_country`(
	`ctry_code` char(2) NOT NULL DEFAULT '',
	`iso_alpha3` char(3) NOT NULL DEFAULT '',
	`st3_name` varchar(100) NOT NULL DEFAULT '',
	`organisation_flag` char(1) NOT NULL DEFAULT '',
	`continent` varchar(25) NOT NULL DEFAULT '',
	`eu_member` char(1) NOT NULL DEFAULT '',
	`epo_member` char(1) NOT NULL DEFAULT '',
	`oecd_member` char(1) NOT NULL DEFAULT '',
	`discontinued` char(1) NOT NULL DEFAULT '',
PRIMARY KEY (	`ctry_code` 
)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;