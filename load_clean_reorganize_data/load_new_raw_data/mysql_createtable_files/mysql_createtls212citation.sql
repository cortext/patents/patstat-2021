CREATE TABLE `tls212_citation`(
	`pat_publn_id` int NOT NULL DEFAULT '0',
	`citn_replenished` int NOT NULL DEFAULT '0',
	`citn_id` smallint NOT NULL DEFAULT '0',
	`citn_origin` char(3) NOT NULL DEFAULT '',
	`cited_pat_publn_id` int NOT NULL DEFAULT '0',
	`cited_appln_id` int NOT NULL DEFAULT '0',
	`pat_citn_seq_nr` smallint NOT NULL DEFAULT '0',
	`cited_npl_publn_id` varchar(32) NOT NULL DEFAULT '0',
	`npl_citn_seq_nr` smallint NOT NULL DEFAULT '0',
	`citn_gener_auth` char(2) NOT NULL DEFAULT '',	
PRIMARY KEY (	`pat_publn_id` ,
	`citn_replenished` ,
	`citn_id` 
)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;