CREATE TABLE `tls902_ipc_nace2`(
	`ipc` varchar(8) NOT NULL DEFAULT '',
	`not_with_ipc` varchar(8) NOT NULL DEFAULT '',
	`unless_with_ipc` varchar(8) NOT NULL DEFAULT '',
	`nace2_code` varchar(5) NOT NULL DEFAULT '',
	`nace2_weight` tinyint NOT NULL DEFAULT 1 ,
	`nace2_descr` varchar(150) NOT NULL DEFAULT '',
PRIMARY KEY (	`ipc` ,
	`not_with_ipc` ,
	`unless_with_ipc` ,
	`nace2_code` 
)) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;