#!/bin/sh


mysql --defaults-file=~/mycnf.cnf -vvv < 02_ajout_ctry_harm.sql > 02_ajout_ctry_harm_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_extrac_colonne.sql > 02_extrac_colonne_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_ajout_stdname.sql > 02_ajout_stdname_report.txt 2>&1 &
