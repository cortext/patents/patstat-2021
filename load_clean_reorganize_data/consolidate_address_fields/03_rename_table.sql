-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

RENAME TABLE tls209_appln_ipc_ifris_epwofr_artif TO tls209_appln_ipc_ifris;
