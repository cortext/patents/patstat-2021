--------------
DROP TABLE IF EXISTS extract_applt_comp
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE extract_applt_comp AS
SELECT
    appln_id,
    person_id,
    person_address,
    pm_pp_comp,
    qualif_comp,
    name_comp,
    adr_comp,
    street_comp,
    zip_code_comp,
    city_comp,
    ctry_comp,
    ctry_final
FROM
    applt_addr_ifris
--------------

Query OK, 117192422 rows affected (5 min 17.15 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    extract_applt_comp
ADD
    PRIMARY KEY PK_ext (appln_id, person_id)
--------------

Query OK, 117192422 rows affected (9 min 10.27 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS extract_invt_comp
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE extract_invt_comp AS
SELECT
    appln_id,
    person_id,
    person_address,
    qualif_comp,
    name_comp,
    adr_comp,
    street_comp,
    zip_code_comp,
    city_comp,
    ctry_comp,
    ctry_final
FROM
    invt_addr_ifris
--------------

Query OK, 214516090 rows affected (9 min 14.75 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    extract_invt_comp
ADD
    PRIMARY KEY PK_ext (appln_id, person_id)
--------------

Query OK, 214516090 rows affected (25 min 41.62 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN person_address
--------------

Query OK, 117192422 rows affected (5 min 8.07 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN pm_pp_comp
--------------

Query OK, 117192422 rows affected (2 min 17.39 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN qualif_comp
--------------

Query OK, 117192422 rows affected (2 min 18.42 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN name_comp
--------------

Query OK, 117192422 rows affected (2 min 9.50 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN adr_comp
--------------

Query OK, 117192422 rows affected (2 min 14.04 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN street_comp
--------------

Query OK, 117192422 rows affected (2 min 2.66 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN zip_code_comp
--------------

Query OK, 117192422 rows affected (2 min 1.19 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN city_comp
--------------

Query OK, 117192422 rows affected (2 min 3.50 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN ctry_comp
--------------

Query OK, 117192422 rows affected (1 min 55.63 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    applt_addr_ifris DROP COLUMN ctry_final
--------------

Query OK, 117192422 rows affected (1 min 56.07 sec)
Records: 117192422  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN person_address
--------------

Query OK, 214516090 rows affected (8 min 25.31 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN qualif_comp
--------------

Query OK, 214516090 rows affected (3 min 47.17 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN name_comp
--------------

Query OK, 214516090 rows affected (3 min 41.52 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN adr_comp
--------------

Query OK, 214516090 rows affected (3 min 27.80 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN street_comp
--------------

Query OK, 214516090 rows affected (3 min 22.12 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN zip_code_comp
--------------

Query OK, 214516090 rows affected (3 min 21.28 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN city_comp
--------------

Query OK, 214516090 rows affected (3 min 11.68 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN ctry_comp
--------------

Query OK, 214516090 rows affected (3 min 10.04 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    invt_addr_ifris DROP COLUMN ctry_final
--------------

Query OK, 214516090 rows affected (2 min 58.02 sec)
Records: 214516090  Duplicates: 0  Warnings: 0

Bye
