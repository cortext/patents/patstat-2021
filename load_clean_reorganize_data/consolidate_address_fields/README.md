# Consolidate address fields

- [Consolidate address info: name, address, country](01_consolidate_address_info.sh)

After building the artificial patents, in the *applicants* and *inventors* tables there are two fields referring to each of these: *name* (person_name, name_comp), *address* (person_address, adr_comp), and *country* (person_ctry_code, ctry_comp). So now, each of them will be consolidated into one.

Rename the applicants and inventors tables respectively to: `applt_adr_ifris_conso` and `invt_adr_ifris_conso`.

- [Organize tables](02_finalisation.sh)

Setting up the necessary indexes, renaming the tables, extracting certain columns from INVT and APPLT to lighten the tables (Fields contained in EXTRACT_APPLT_COMP and EXTRACT_INVT_COMP) and finally adding other fields such as the harmonized country or the doc_std_name.

- [Rename IPC table](03_menage.sh)