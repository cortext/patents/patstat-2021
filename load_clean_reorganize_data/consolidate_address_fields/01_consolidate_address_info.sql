-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS `applt_adr_ifris_conso`;

CREATE TABLE `applt_adr_ifris_conso` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `pm_pp_comp` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `invt_seq_nr` smallint(4) DEFAULT NULL,
    name_final varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    adr_final varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    ctry_final varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    KEY `idx_appln_id` (`appln_id`),
    KEY (person_id),
    KEY (doc_std_name_id),
    KEY (adr_final(100)),
    KEY (ctry_final(2)),
    KEY (source),
    KEY (methode)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO
    applt_adr_ifris_conso
SELECT
    a.*,
    IF(a.name_comp != '', a.name_comp, a.person_name) AS name_final,
    IF(a.adr_comp != '', a.adr_comp, a.person_address) AS adr_final,
    IF(a.ctry_comp != '', a.ctry_comp, a.person_ctry_code) AS ctry_final
FROM
    applt_adr_ifris_artif a;

DROP TABLE IF EXISTS `invt_adr_ifris_conso`;

CREATE TABLE `invt_adr_ifris_conso` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `invt_seq_nr` smallint(4) DEFAULT NULL,
    name_final varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    adr_final varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    ctry_final varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    KEY `idx_appln_id` (`appln_id`),
    KEY (adr_final(100)),
    KEY (person_id),
    KEY (doc_std_name_id),
    KEY (ctry_final(2)),
    KEY (source),
    KEY (methode)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

INSERT INTO
    invt_adr_ifris_conso
SELECT
    a.*,
    IF(a.name_comp != '', a.name_comp, a.person_name) AS name_final,
    IF(a.adr_comp != '', a.adr_comp, a.person_address) AS adr_final,
    IF(a.ctry_comp != '', a.ctry_comp, a.person_ctry_code) AS ctry_final
FROM
    invt_adr_ifris_artif a;

ALTER TABLE
    invt_adr_ifris_conso
ADD
    PRIMARY KEY (appln_id, person_id);

ALTER TABLE
    applt_adr_ifris_conso
ADD
    PRIMARY KEY (appln_id, person_id);

-- Backup the old applicants and inventors tables, as those table names will be used
ALTER TABLE applt_addr_ifris
RENAME TO backup_07072022_applt_addr_ifris;

ALTER TABLE invt_addr_ifris
RENAME TO backup_07072022_invt_addr_ifris;

-- Change name for the current applicants and inventors tables, to continue using applt_addr_ifris and invt_addr_ifris
CREATE TABLE applt_addr_ifris AS
SELECT * FROM applt_adr_ifris_conso;

CREATE TABLE invt_addr_ifris AS
SELECT * FROM invt_adr_ifris_conso;