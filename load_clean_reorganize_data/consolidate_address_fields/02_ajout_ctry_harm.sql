-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

ALTER TABLE applt_addr_ifris ADD COLUMN iso_ctry VARCHAR(2);
ALTER TABLE invt_addr_ifris ADD COLUMN iso_ctry VARCHAR(2);

UPDATE 	applt_addr_ifris a, nomen_ctry_iso b
SET 	a.iso_ctry = b.ctry_harm
WHERE	a.ctry_final=b.ctry_final
;

UPDATE 	invt_addr_ifris a, nomen_ctry_iso b
SET 	a.iso_ctry = b.ctry_harm
WHERE	a.ctry_final=b.ctry_final
;
