-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

ALTER TABLE
    applt_addr_ifris
ADD
    COLUMN doc_std_name VARCHAR(500);

UPDATE
    applt_addr_ifris a,
    tls206_person b
SET
    a.doc_std_name = b.doc_std_name
WHERE
    a.doc_std_name_id = b.doc_std_name_id;