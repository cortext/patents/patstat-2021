-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

DROP TABLE IF EXISTS extract_applt_comp;

CREATE TABLE extract_applt_comp AS
SELECT
    appln_id,
    person_id,
    person_address,
    pm_pp_comp,
    qualif_comp,
    name_comp,
    adr_comp,
    street_comp,
    zip_code_comp,
    city_comp,
    ctry_comp,
    ctry_final
FROM
    applt_addr_ifris;

ALTER TABLE
    extract_applt_comp
ADD
    PRIMARY KEY PK_ext (appln_id, person_id);

DROP TABLE IF EXISTS extract_invt_comp;

CREATE TABLE extract_invt_comp AS
SELECT
    appln_id,
    person_id,
    person_address,
    qualif_comp,
    name_comp,
    adr_comp,
    street_comp,
    zip_code_comp,
    city_comp,
    ctry_comp,
    ctry_final
FROM
    invt_addr_ifris;

ALTER TABLE
    extract_invt_comp
ADD
    PRIMARY KEY PK_ext (appln_id, person_id);

/**************suppression dans les tables*****************/
ALTER TABLE
    applt_addr_ifris DROP COLUMN person_address;

ALTER TABLE
    applt_addr_ifris DROP COLUMN pm_pp_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN qualif_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN name_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN adr_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN street_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN zip_code_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN city_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN ctry_comp;

ALTER TABLE
    applt_addr_ifris DROP COLUMN ctry_final;

ALTER TABLE
    invt_addr_ifris DROP COLUMN person_address;

ALTER TABLE
    invt_addr_ifris DROP COLUMN qualif_comp;

ALTER TABLE
    invt_addr_ifris DROP COLUMN name_comp;

ALTER TABLE
    invt_addr_ifris DROP COLUMN adr_comp;

ALTER TABLE
    invt_addr_ifris DROP COLUMN street_comp;

ALTER TABLE
    invt_addr_ifris DROP COLUMN zip_code_comp;

ALTER TABLE
    invt_addr_ifris DROP COLUMN city_comp;

ALTER TABLE
    invt_addr_ifris DROP COLUMN ctry_comp;

ALTER TABLE
    invt_addr_ifris DROP COLUMN ctry_final;