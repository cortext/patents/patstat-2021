-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE transnat_patents_2021;

CREATE INDEX IX_mention ON couple_unique (appln_id_mention);

CREATE INDEX IX_appln_id ON couple_unique (appln_id);

DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1;

CREATE INDEX IX_tmp ON tmp (appln_id_mention);

DROP TABLE IF EXISTS duplicates;

CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id;

-- We keep those with an IPC
DROP TABLE IF EXISTS tmp_ipc;

CREATE TABLE tmp_ipc AS
SELECT
        appln_id_mention
FROM
        duplicates
GROUP BY
        appln_id_mention
HAVING
        count(DISTINCT no_ipc) > 1;

CREATE INDEX IX_tmp_ipc ON tmp_ipc (appln_id_mention);

DROP TABLE IF EXISTS tmp_ipc_2;

CREATE TABLE tmp_ipc_2 AS
SELECT
        a.appln_id_mention,
        b.appln_id
FROM
        tmp_ipc a,
        duplicates b
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.no_ipc = 0;

DELETE a.*
FROM
        couple_unique a,
        tmp_ipc b
WHERE
        a.appln_id_mention = b.appln_id_mention;

INSERT INTO
        couple_unique
SELECT
        appln_id,
        appln_id_mention
FROM
        tmp_ipc_2;

DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1;

CREATE INDEX IX_tmp ON tmp (appln_id_mention);

DROP TABLE IF EXISTS duplicates;

CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id;

-- We keep those who have an applicant and an inventor
DROP TABLE IF EXISTS tmp_appt_invt;

CREATE TABLE tmp_appt_invt AS
SELECT
        appln_id_mention
FROM
        duplicates
GROUP BY
        appln_id_mention
HAVING
        count(DISTINCT no_appt_invt) > 1;

CREATE INDEX IX_tmp_appt_invt ON tmp_appt_invt (appln_id_mention);

DROP TABLE IF EXISTS tmp_appt_invt_2;

CREATE TABLE tmp_appt_invt_2 AS
SELECT
        a.appln_id_mention,
        b.appln_id
FROM
        tmp_appt_invt a,
        duplicates b
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.no_appt_invt = 0;

DELETE a.*
FROM
        couple_unique a,
        tmp_appt_invt b
WHERE
        a.appln_id_mention = b.appln_id_mention;

INSERT INTO
        couple_unique
SELECT
        appln_id,
        appln_id_mention
FROM
        tmp_appt_invt_2;

DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1;

CREATE INDEX IX_tmp ON tmp (appln_id_mention);

DROP TABLE IF EXISTS duplicates;

CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id;

-- We keep the smallest appln_nr (which is supposed to be the first one)
CREATE INDEX IX_appln_id_mention ON duplicates (appln_id_mention);

CREATE INDEX IX_appln_id_mention ON couple_unique (appln_id_mention);

DROP TABLE IF EXISTS tmp_appln_nr_2;

CREATE TABLE tmp_appln_nr_2 AS
SELECT
        appln_id_mention,
        min(appln_nr) AS appln_nr
FROM
        duplicates
GROUP BY
        appln_id_mention;

CREATE INDEX IX_tmp_appln_nr_2 ON tmp_appln_nr_2 (appln_id_mention);

DELETE a.*
FROM
        couple_unique a,
        tmp_appln_nr_2 b
WHERE
        a.appln_id_mention = b.appln_id_mention;

INSERT INTO
        couple_unique
SELECT
        DISTINCT b.appln_id,
        a.appln_id_mention
FROM
        tmp_appln_nr_2 a,
        duplicates b
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND a.appln_nr = b.appln_nr;

DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1;

CREATE INDEX IX_tmp ON tmp (appln_id_mention);

DROP TABLE IF EXISTS duplicates;

CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id;

-- We keep the smallest appln_id (which is supposed to be the first one)
CREATE INDEX IX_appln_id_mention ON duplicates (appln_id_mention);

DROP TABLE IF EXISTS tmp_appln_id_2;

CREATE TABLE tmp_appln_id_2 AS
SELECT
        appln_id_mention,
        min(appln_id) AS appln_id
FROM
        duplicates
GROUP BY
        appln_id_mention;

CREATE INDEX IX_tmp_appln_id_2 ON tmp_appln_id_2 (appln_id_mention);

DELETE a.*
FROM
        couple_unique a,
        tmp_appln_id_2 b
WHERE
        a.appln_id_mention = b.appln_id_mention;

INSERT INTO
        couple_unique
SELECT
        DISTINCT b.appln_id,
        a.appln_id_mention
FROM
        tmp_appln_id_2 a,
        duplicates b
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND a.appln_id = b.appln_id;

DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1;

CREATE INDEX IX_tmp ON tmp (appln_id_mention);

DROP TABLE IF EXISTS duplicates;

CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id;