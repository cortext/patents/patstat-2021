-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

ALTER TABLE
    tls201_appln
ADD
    COLUMN transnat int(1) NOT NULL DEFAULT '0';

UPDATE
    tls201_appln a,
    transnat_patents_2021.unique_transnat_patents b
SET
    a.transnat = 1
WHERE
    a.appln_id = b.appln_id;

CREATE INDEX IX_transnat ON tls201_appln (transnat);