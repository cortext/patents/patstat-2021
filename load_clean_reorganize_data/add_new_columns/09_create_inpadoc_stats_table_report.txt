--------------
DROP TABLE IF EXISTS inpadoc_stats_ifris
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE inpadoc_stats_ifris (
    inpadoc_family_id int(10) DEFAULT '0',
    nb_patents int DEFAULT '0',
    nb_prio int DEFAULT '0',
    nb_auth int DEFAULT '0',
    nb_fam_docDB int DEFAULT '0',
    auth_1er_prio VARCHAR(2) DEFAULT NULL
)
--------------

Query OK, 0 rows affected (0.01 sec)

--------------
ALTER TABLE
    inpadoc_stats_ifris
ADD
    PRIMARY KEY PK_stats (inpadoc_family_id)
--------------

Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0

--------------
INSERT INTO
    inpadoc_stats_ifris
SELECT
    DISTINCT inpadoc_family_id,
    0,
    0,
    0,
    0,
    NULL
FROM
    tls201_appln
--------------

Query OK, 70115230 rows affected (7 min 41.22 sec)
Records: 70115230  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS nb_patents
--------------

Query OK, 0 rows affected, 1 warning (0.01 sec)

--------------
CREATE TABLE nb_patents AS
SELECT
    inpadoc_family_id,
    count(DISTINCT appln_id) AS nb_patents
FROM
    tls201_appln
GROUP BY
    inpadoc_family_id
--------------

Query OK, 70115230 rows affected (1 min 48.63 sec)
Records: 70115230  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    nb_patents
ADD
    PRIMARY KEY PK_nb_patents (inpadoc_family_id)
--------------

Query OK, 70115230 rows affected (3 min 4.04 sec)
Records: 70115230  Duplicates: 0  Warnings: 0

--------------
UPDATE
    inpadoc_stats_ifris a,
    nb_patents b
SET
    a.nb_patents = b.nb_patents
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id
--------------

Query OK, 70115230 rows affected (32 min 28.12 sec)
Rows matched: 70115230  Changed: 70115230  Warnings: 0

--------------
DROP TABLE IF EXISTS nb_prio
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE nb_prio AS
SELECT
    inpadoc_family_id,
    count(DISTINCT appln_id) AS nb_prio
FROM
    tls201_appln
WHERE
    appln_first_priority_year = 0
GROUP BY
    inpadoc_family_id
--------------

Query OK, 70106647 rows affected (13 min 3.39 sec)
Records: 70106647  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE   
    nb_prio
ADD   
    PRIMARY KEY PK_nb_prio (inpadoc_family_id)
--------------

Query OK, 70106647 rows affected (3 min 3.42 sec)
Records: 70106647  Duplicates: 0  Warnings: 0

--------------
UPDATE
    inpadoc_stats_ifris a,
    nb_prio b
SET
    a.nb_prio = b.nb_prio
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id
--------------

Query OK, 70106647 rows affected (32 min 21.95 sec)
Rows matched: 70106647  Changed: 70106647  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_inpadoc_auth
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp_inpadoc_auth AS
SELECT
    inpadoc_family_id,
    appln_auth
FROM
    tls201_appln
GROUP BY
    inpadoc_family_id, appln_auth
--------------

Query OK, 98591376 rows affected (9 min 35.85 sec)
Records: 98591376  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS nb_auth
--------------

Query OK, 0 rows affected, 1 warning (0.03 sec)

--------------
CREATE TABLE nb_auth AS
SELECT
    inpadoc_family_id,
    count(*) AS nb_auth
FROM
    tmp_inpadoc_auth
GROUP BY
    inpadoc_family_id
--------------

Query OK, 70115230 rows affected (3 min 14.12 sec)
Records: 70115230  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    nb_auth
ADD
    PRIMARY KEY PK_nb_auth (inpadoc_family_id)
--------------

Query OK, 70115230 rows affected (3 min 15.06 sec)
Records: 70115230  Duplicates: 0  Warnings: 0

--------------
UPDATE
    inpadoc_stats_ifris a,
    nb_auth b
SET
    a.nb_auth = b.nb_auth
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id
--------------

Query OK, 70115230 rows affected (38 min 41.09 sec)
Rows matched: 70115230  Changed: 70115230  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_auth
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp_auth AS
SELECT
    DISTINCT inpadoc_family_id,
    appln_auth,
    appln_id
FROM
    tls201_appln a
WHERE
    appln_first_priority_year = 0
    AND appln_filing_date IN (
        SELECT
            min(appln_filing_date)
        FROM
            tls201_appln d
        WHERE
            a.appln_id = d.appln_id
    )
--------------

Query OK, 75277537 rows affected (34 min 2.42 sec)
Records: 75277537  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_inpadoc_id ON tmp_auth (inpadoc_family_id)
--------------

Query OK, 75277537 rows affected (1 min 5.09 sec)
Records: 75277537  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_appln_id ON tmp_auth (appln_id)
--------------

Query OK, 75277537 rows affected (1 min 36.88 sec)
Records: 75277537  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_auth2
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)
--------------
CREATE TABLE tmp_auth2 AS
SELECT
    inpadoc_family_id,
    min(appln_id) AS appln_id
FROM
    tmp_auth
GROUP BY
    inpadoc_family_id
--------------

Query OK, 70106647 rows affected (3 min 18.22 sec)
Records: 70106647  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_inpadoc_id ON tmp_auth2 (inpadoc_family_id)
--------------

Query OK, 70106647 rows affected (40.04 sec)
Records: 70106647  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_appln_id ON tmp_auth2 (appln_id)
--------------

Query OK, 70106647 rows affected (1 min 25.53 sec)
Records: 70106647  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS appln_auth_1er_prio
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE appln_auth_1er_prio AS
SELECT
    DISTINCT a.inpadoc_family_id,
    b.appln_auth AS appln_auth_1er_prio
FROM
    tmp_auth2 a,
    tmp_auth b
WHERE
    a.appln_id = b.appln_id
    AND a.inpadoc_family_id = b.inpadoc_family_id
--------------

Query OK, 70106647 rows affected (17 min 20.47 sec)
Records: 70106647  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    appln_auth_1er_prio
ADD
    PRIMARY KEY PK_appln_auth_1er_prio (inpadoc_family_id)

--------------

Query OK, 70106647 rows affected (3 min 8.33 sec)
Records: 70106647  Duplicates: 0  Warnings: 0

--------------
UPDATE
    inpadoc_stats_ifris a,
    appln_auth_1er_prio b
SET
    a.auth_1er_prio = b.appln_auth_1er_prio
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id
--------------

Query OK, 70106647 rows affected (1 hour 1 min 30.42 sec)
Rows matched: 70106647  Changed: 70106647  Warnings: 0

--------------
DROP TABLE IF EXISTS nb_fam_docDB
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE nb_fam_docDB AS
SELECT
    inpadoc_family_id,
    count(DISTINCT docdb_family_id) AS nb_fam_docDB
FROM
    tls201_appln
GROUP BY
    inpadoc_family_id
--------------

Query OK, 70115230 rows affected (1 min 58.84 sec)
Records: 70115230  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE
    nb_fam_docDB
ADD
    PRIMARY KEY PK_fam_docDB (inpadoc_family_id)
--------------

Query OK, 70115230 rows affected (3 min 8.66 sec)
Records: 70115230  Duplicates: 0  Warnings: 0

--------------
UPDATE
    inpadoc_stats_ifris a,
    nb_fam_docDB b
SET
    a.nb_fam_docDB = b.nb_fam_docDB
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id
--------------

Query OK, 70115230 rows affected (1 hour 5 min 44.73 sec)
Rows matched: 70115230  Changed: 70115230  Warnings: 0

Bye