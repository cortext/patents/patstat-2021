#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < 07_1_create_transnational_pat_db.sql > 07_1_create_transnational_pat_db_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 07_2_create_duplicates_table.sql > 07_2_create_duplicates_table_report.txt  2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 07_3_create_unique_patents_table.sql > 07_3_create_unique_patents_table_report.txt  2>&1 &
