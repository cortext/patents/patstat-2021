-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

-- Addition of the first priority year
-- Adding the appln year to the priority table
ALTER TABLE tls204_appln_prior
ADD COLUMN appln_priority_year INT(4);

UPDATE tls204_appln_prior a,
	tls201_appln b
SET
    a.appln_priority_year = b.appln_filing_year
WHERE
	a.prior_appln_id = b.appln_id;

CREATE INDEX IX_appln_id
ON tls204_appln_prior (appln_id);

-- Adding indexes
ALTER TABLE tls201_appln
ADD INDEX IX_earliest_filing_date (earliest_filing_date),
ADD INDEX IX_earliest_filing_year (earliest_filing_year);

ALTER TABLE tls204_appln_prior
ADD INDEX IX_appln_priority_year (appln_priority_year);
