-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

CREATE DATABASE IF NOT EXISTS transnat_patents_2021 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

USE transnat_patents_2021;

DROP TABLE IF EXISTS selection_201;

CREATE TABLE selection_201 AS
SELECT
	DISTINCT appln_id,
	appln_auth,
	appln_filing_date
FROM
	patstat2021_staging.tls201_appln
WHERE
	appln_first_priority_year = 0;

ALTER TABLE
	selection_201
ADD
	PRIMARY KEY PK_sel_201 (appln_id);

DROP TABLE IF EXISTS selection_204;

CREATE TABLE selection_204 AS
SELECT
	DISTINCT a.appln_id,
	b.appln_id AS appln_id_mention,
	c.appln_auth,
	c.appln_filing_date
FROM
	selection_201 a,
	patstat2021_staging.tls204_appln_prior b,
	patstat2021_staging.tls201_appln c
WHERE
	a.appln_id = b.prior_appln_id
	AND b.appln_id = c.appln_id;

ALTER TABLE
	selection_204
ADD
	PRIMARY KEY PK_sel_204 (appln_id, appln_id_mention);

DROP TABLE IF EXISTS couple;

CREATE TABLE couple AS
SELECT
	a.appln_id,
	a.appln_filing_date,
	a.appln_auth,
	b.appln_id_mention,
	b.appln_filing_date AS appln_filing_date_mention,
	b.appln_auth AS appln_auth_mention
FROM
	selection_201 a,
	selection_204 b
WHERE
	a.appln_id = b.appln_id
	AND a.appln_auth != b.appln_auth;

ALTER TABLE
	couple
ADD
	PRIMARY KEY PK_couple (appln_id, appln_id_mention);

CREATE INDEX IX_mention ON couple (appln_id_mention);

CREATE INDEX IX_date ON couple (appln_filing_date);

DROP TABLE IF EXISTS couple_unique;

CREATE TABLE couple_unique AS
SELECT
	a.appln_id,
	a.appln_id_mention
FROM
	couple a
WHERE
	a.appln_filing_date = (
		SELECT
			min(appln_filing_date)
		FROM
			couple b
		WHERE
			a.appln_id_mention = b.appln_id_mention
		GROUP BY
			a.appln_id_mention
	);

ALTER TABLE
	couple_unique
ADD
	PRIMARY KEY PK_couple_unique (appln_id, appln_id_mention);