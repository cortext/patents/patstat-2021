-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

DROP TABLE IF EXISTS inpadoc_stats_ifris;

CREATE TABLE inpadoc_stats_ifris (
    inpadoc_family_id int(10) DEFAULT '0',
    nb_patents int DEFAULT '0',
    nb_prio int DEFAULT '0',
    nb_auth int DEFAULT '0',
    nb_fam_docDB int DEFAULT '0',
    auth_1er_prio VARCHAR(2) DEFAULT NULL
);

ALTER TABLE
    inpadoc_stats_ifris
ADD
    PRIMARY KEY PK_stats (inpadoc_family_id);

INSERT INTO
    inpadoc_stats_ifris
SELECT
    DISTINCT inpadoc_family_id,
    0,
    0,
    0,
    0,
    NULL
FROM
    tls201_appln;

-- Number of patents
DROP TABLE IF EXISTS nb_patents;

CREATE TABLE nb_patents AS
SELECT
    inpadoc_family_id,
    count(DISTINCT appln_id) AS nb_patents
FROM
    tls201_appln
GROUP BY
    inpadoc_family_id;

ALTER TABLE
    nb_patents
ADD
    PRIMARY KEY PK_nb_patents (inpadoc_family_id);

UPDATE
    inpadoc_stats_ifris a,
    nb_patents b
SET
    a.nb_patents = b.nb_patents
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id;

-- Nb prio
DROP TABLE IF EXISTS nb_prio;

CREATE TABLE nb_prio AS
SELECT
    inpadoc_family_id,
    count(DISTINCT appln_id) AS nb_prio
FROM
    tls201_appln
WHERE
    appln_first_priority_year = 0
GROUP BY
    inpadoc_family_id;

ALTER TABLE
    nb_prio
ADD
    PRIMARY KEY PK_nb_prio (inpadoc_family_id);

UPDATE
    inpadoc_stats_ifris a,
    nb_prio b
SET
    a.nb_prio = b.nb_prio
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id;

-- Number of distinct authorization offices per inpadoc family

DROP TABLE IF EXISTS tmp_inpadoc_auth;

CREATE TABLE tmp_inpadoc_auth AS
SELECT 
    inpadoc_family_id, 
    appln_auth
FROM 
    tls201_appln
GROUP BY 
    inpadoc_family_id, appln_auth;

DROP TABLE IF EXISTS nb_auth;

CREATE TABLE nb_auth AS
SELECT 
    inpadoc_family_id, 
    count(*) AS nb_auth
FROM 
    tmp_inpadoc_auth
GROUP BY 
    inpadoc_family_id;

ALTER TABLE
    nb_auth
ADD
    PRIMARY KEY PK_nb_auth (inpadoc_family_id);

UPDATE
    inpadoc_stats_ifris a,
    nb_auth b
SET
    a.nb_auth = b.nb_auth
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id;

-- Office of first priority patent
DROP TABLE IF EXISTS tmp_auth;

CREATE TABLE tmp_auth AS
SELECT
    DISTINCT inpadoc_family_id,
    appln_auth,
    appln_id
FROM
    tls201_appln a
WHERE
    appln_first_priority_year = 0
    AND appln_filing_date IN (
        SELECT
            min(appln_filing_date)
        FROM
            tls201_appln d
        WHERE
            a.appln_id = d.appln_id
    );

CREATE INDEX IX_inpadoc_id ON tmp_auth (inpadoc_family_id);

CREATE INDEX IX_appln_id ON tmp_auth (appln_id);

DROP TABLE IF EXISTS tmp_auth2;

CREATE TABLE tmp_auth2 AS
SELECT
    inpadoc_family_id,
    min(appln_id) AS appln_id
FROM
    tmp_auth
GROUP BY
    inpadoc_family_id;

CREATE INDEX IX_inpadoc_id ON tmp_auth2 (inpadoc_family_id);

CREATE INDEX IX_appln_id ON tmp_auth2 (appln_id);

DROP TABLE IF EXISTS appln_auth_1er_prio;

CREATE TABLE appln_auth_1er_prio AS
SELECT
    DISTINCT a.inpadoc_family_id,
    b.appln_auth AS appln_auth_1er_prio
FROM
    tmp_auth2 a,
    tmp_auth b
WHERE
    a.appln_id = b.appln_id
    AND a.inpadoc_family_id = b.inpadoc_family_id;

ALTER TABLE
    appln_auth_1er_prio
ADD
    PRIMARY KEY PK_appln_auth_1er_prio (inpadoc_family_id);

UPDATE
    inpadoc_stats_ifris a,
    appln_auth_1er_prio b
SET
    a.auth_1er_prio = b.appln_auth_1er_prio
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id;

-- Number of doc db families
DROP TABLE IF EXISTS nb_fam_docDB;

CREATE TABLE nb_fam_docDB AS
SELECT
    inpadoc_family_id,
    count(DISTINCT docdb_family_id) AS nb_fam_docDB
FROM
    tls201_appln
GROUP BY
    inpadoc_family_id;

ALTER TABLE
    nb_fam_docDB
ADD
    PRIMARY KEY PK_fam_docDB (inpadoc_family_id);

UPDATE
    inpadoc_stats_ifris a,
    nb_fam_docDB b
SET
    a.nb_fam_docDB = b.nb_fam_docDB
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id;