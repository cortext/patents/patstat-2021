-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

-- Addition of the first priority year

-- First priority year per appln_id

DROP TABLE IF EXISTS first_priority;

CREATE TABLE first_priority AS
SELECT
	appln_id,
	MIN(appln_priority_year) AS min_year
FROM
	tls204_appln_prior
GROUP BY
	appln_id;

CREATE INDEX IX_appln_id ON first_priority (appln_id);

-- Set a value for appln_first_priority_year in the applications table
ALTER TABLE tls201_appln
ADD COLUMN appln_first_priority_year int(4);

UPDATE tls201_appln a,
    first_priority b
SET
    a.appln_first_priority_year = b.min_year
WHERE
	a.appln_id = b.appln_id;

-- Set the remaining null values to '0'
UPDATE
	tls201_appln
SET
	appln_first_priority_year = '0'
WHERE
	appln_first_priority_year IS NULL;

CREATE INDEX IX_first_priority_year ON tls201_appln (appln_first_priority_year);