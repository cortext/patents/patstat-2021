#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < 08_1_add_transnat.sql  > 08_1_add_transnat_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 08_2_add_nb_citation.sql  > 08_2_add_nb_citation_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 08_3_add_nb_person.sql  > 08_3_add_nb_person_report.txt 2>&1 &

