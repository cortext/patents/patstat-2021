-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

-- Number of applicants and inventors
-- Done already by patstat since 2017, but we do it anyway because after the
-- recovery of the missing numbers
ALTER TABLE
	tls201_appln
ADD
	COLUMN nb_applt int NOT NULL DEFAULT '0';

ALTER TABLE
	tls201_appln
ADD
	COLUMN nb_invt int NOT NULL DEFAULT '0';

DROP TABLE IF EXISTS tmp_applt;

CREATE TABLE tmp_applt AS
SELECT
	a.appln_id,
	count(DISTINCT b.person_id) AS nb_applt
FROM
	tls201_appln a,
	applt_ifris b
WHERE
	a.appln_id = b.appln_id
GROUP BY
	a.appln_id;

ALTER TABLE
	tmp_applt
ADD
	PRIMARY KEY PK_appln_id (appln_id);

UPDATE
	tls201_appln a,
	tmp_applt b
SET
	a.nb_applt = b.nb_applt
WHERE
	a.appln_id = b.appln_id;

DROP TABLE IF EXISTS tmp_invt;

CREATE TABLE tmp_invt AS
SELECT
	a.appln_id,
	count(DISTINCT b.person_id) AS nb_invt
FROM
	tls201_appln a,
	invt_ifris b
WHERE
	a.appln_id = b.appln_id
GROUP BY
	a.appln_id;

ALTER TABLE
	tmp_invt
ADD
	PRIMARY KEY PK_appln_id (appln_id);

UPDATE
	tls201_appln a,
	tmp_invt b
SET
	a.nb_invt = b.nb_invt
WHERE
	a.appln_id = b.appln_id;

CREATE INDEX nb_applt ON tls201_appln (nb_applt);

CREATE INDEX nb_invt ON tls201_appln (nb_invt);