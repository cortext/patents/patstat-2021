-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

-- Create tables for APPLT and INVT based on the tables 206 and 207

-- APPLT
DROP TABLE IF EXISTS applt_ifris;

CREATE TABLE applt_ifris (
    appln_id int(11) DEFAULT NULL,
    person_id int(11) NOT NULL DEFAULT '0',
    person_name varchar(500) DEFAULT NULL,
    person_address varchar(1000) DEFAULT NULL,
    person_ctry_code char(2) DEFAULT NULL,
    doc_std_name_id int(11) NOT NULL DEFAULT '0',
    doc_std_name varchar(500) DEFAULT NULL,
    psn_id int(11) DEFAULT NULL,
    han_id int(11) DEFAULT NULL
);

INSERT INTO
    applt_ifris
SELECT
    DISTINCT a.appln_id,
    b.person_id,
    b.person_name,
    b.person_address,
    b.person_ctry_code,
    b.doc_std_name_id,
    b.doc_std_name,
    b.psn_id,
    b.han_id
FROM
    tls207_pers_appln a,
    tls206_person b
WHERE
    a.applt_seq_nr != 0
    AND a.person_id < 30000000
    AND a.person_id = b.person_id;

INSERT INTO
    applt_ifris
SELECT
    DISTINCT a.appln_id,
    b.person_id,
    b.person_name,
    b.person_address,
    b.person_ctry_code,
    b.doc_std_name_id,
    b.doc_std_name,
    b.psn_id,
    b.han_id
FROM
    tls207_pers_appln a,
    tls206_person b
WHERE
    a.applt_seq_nr != 0
    AND a.person_id >= 30000000
    AND a.person_id = b.person_id;

-- INVT 
DROP TABLE IF EXISTS invt_ifris;

CREATE TABLE invt_ifris (
    appln_id int(11) DEFAULT NULL,
    person_id int(11) NOT NULL DEFAULT '0',
    person_name varchar(500) DEFAULT NULL,
    person_address varchar(1000) DEFAULT NULL,
    person_ctry_code char(2) DEFAULT NULL,
    doc_std_name_id int(11) NOT NULL DEFAULT '0',
    doc_std_name varchar(500) DEFAULT NULL,
    psn_id int(11) DEFAULT NULL,
    han_id int(11) DEFAULT NULL
);

INSERT INTO
    invt_ifris
SELECT
    DISTINCT a.appln_id,
    b.person_id,
    b.person_name,
    b.person_address,
    b.person_ctry_code,
    b.doc_std_name_id,
    b.doc_std_name,
    b.psn_id,
    b.han_id
FROM
    tls207_pers_appln a,
    tls206_person b
WHERE
    a.invt_seq_nr != 0
    AND a.person_id < 25000000
    AND a.person_id = b.person_id;

INSERT INTO
    invt_ifris
SELECT
    DISTINCT a.appln_id,
    b.person_id,
    b.person_name,
    b.person_address,
    b.person_ctry_code,
    b.doc_std_name_id,
    b.doc_std_name,
    b.psn_id,
    b.han_id
FROM
    tls207_pers_appln a,
    tls206_person b
WHERE
    a.invt_seq_nr != 0
    AND a.person_id >= 25000000
    AND a.person_id = b.person_id;

-- Add primary key
ALTER TABLE applt_ifris
ADD CONSTRAINT PK_applt_ifris PRIMARY KEY (appln_id, person_id);

ALTER TABLE invt_ifris
ADD CONSTRAINT PK_invt_ifris PRIMARY KEY (appln_id, person_id);