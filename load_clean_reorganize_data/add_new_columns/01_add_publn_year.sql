-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

-- Add a new column for the year to the tls211 table

ALTER TABLE tls211_pat_publn
ADD COLUMN publn_year INT(4) AFTER publn_date;

UPDATE tls211_pat_publn
SET
    publn_year = year(publn_date);
