-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE transnat_patents_2021;

DROP TABLE IF EXISTS unique_transnat_patents;

CREATE TABLE unique_transnat_patents AS
SELECT
	appln_id,
	count(*) AS nb_cit
FROM
	couple_unique
GROUP BY
	appln_id;

ALTER TABLE
	unique_transnat_patents
ADD
	PRIMARY KEY PK_appln_id (appln_id);

DROP TABLE IF EXISTS unique_transnat_patents_no_singleton;

CREATE TABLE unique_transnat_patents_no_singleton AS
SELECT
	*
FROM
	unique_transnat_patents
WHERE
	appln_id NOT IN (
		SELECT
			appln_id
		FROM
			patstat2021_staging.tls201_appln
        WHERE 
            singleton = 1
	);

ALTER TABLE
	unique_transnat_patents_no_singleton
ADD
	PRIMARY KEY PK_appln_id (appln_id);