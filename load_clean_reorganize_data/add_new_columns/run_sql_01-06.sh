#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < 01_add_publn_year.sql  > 01_add_publn_year_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_add_prior_year.sql  > 02_add_prior_year_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 03_add_appln_type.sql  > 03_add_appln_type_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 04_add_singleton.sql  > 04_add_singleton_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 05_create_invt_applt_tables.sql  > 05_create_invt_applt_tables_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 06_add_first_prior_year.sql  > 06_add_first_prior_year_report.txt 2>&1 &
