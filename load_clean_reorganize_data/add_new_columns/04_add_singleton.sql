-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

-- Create table tmp_singleton to count the number of distinct patents belonging to 
-- each inpadoc_family_id
DROP TABLE IF EXISTS tmp_singleton;

CREATE TABLE tmp_singleton AS
SELECT
	inpadoc_family_id,
	count(DISTINCT appln_id) AS nb
FROM
	tls201_appln
GROUP BY
	inpadoc_family_id;

ALTER TABLE tmp_singleton
ADD	PRIMARY KEY PK_tmp_fam_id (inpadoc_family_id);

CREATE INDEX IX_singl ON tmp_singleton (nb);

-- Add singleton column to tls201_appln table
ALTER TABLE tls201_appln 
ADD COLUMN singleton int(1) NOT NULL DEFAULT '0';

UPDATE
    tls201_appln a,
    tmp_singleton b
SET
    a.singleton = 1
WHERE
    a.inpadoc_family_id = b.inpadoc_family_id
	AND b.nb = 1;

CREATE INDEX IX_singleton ON tls201_appln (singleton);
