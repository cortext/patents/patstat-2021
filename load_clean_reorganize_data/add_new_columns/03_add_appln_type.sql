-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

-- Addition of a column allowing to identify the applications without applicant and without inventor
ALTER TABLE tls201_appln
ADD COLUMN no_appt_invt INT(1);

UPDATE tls201_appln a
SET 
    no_appt_invt = 1
WHERE NOT EXISTS (
    SELECT
        1
    FROM
        tls207_pers_appln b
    WHERE
        a.appln_id = b.appln_id
);

-- Set the column for the rest of the rows that do have an applicant and/or inventor
UPDATE tls201_appln a
SET 
    no_appt_invt = 0
WHERE no_appt_invt IS NULL;

-- Add column to identify the applications without ipc technology
ALTER TABLE tls201_appln
ADD no_ipc int(1);

UPDATE tls201_appln a
SET
    no_ipc = 1
WHERE NOT EXISTS (
    SELECT
        NULL
    FROM
        tls209_appln_ipc b
    WHERE
        a.appln_id = b.appln_id
);

-- Add the rest of the rows that do have ipc technology related
UPDATE tls201_appln a
SET 
    no_ipc = 0
WHERE no_ipc IS NULL;

-- Add a column to identify the artificial applications ranges
ALTER TABLE tls201_appln
ADD COLUMN artificial INT(1); 

-- 1: range 900000001 - 930000000
UPDATE tls201_appln
SET 
    artificial = 1
WHERE
	appln_id > 900000000
	AND appln_id <= 930000000;

-- 2: range 930000001 - 960000000
UPDATE tls201_appln
SET 
    artificial = 2
WHERE
	appln_id > 930000000
	AND appln_id <= 960000000;

-- 3: range 960000001 - 999999999
UPDATE tls201_appln
SET 
    artificial = 3
WHERE
	appln_id > 960000000
	AND appln_id <= 999999999;

-- 4: patents with unknown date (9999)
UPDATE tls201_appln
SET 
    artificial = 4
WHERE
	appln_filing_year = 9999
	AND artificial IS NULL;

-- 0 - Not artificial patents with known filing year
UPDATE tls201_appln
SET 
    artificial = 0
WHERE
	artificial IS NULL;

-- Add indexes
ALTER TABLE tls201_appln
ADD INDEX IX_no_ipc (no_ipc),
ADD INDEX IX_no_appt_invt (no_appt_invt),
ADD INDEX IX_artificial (artificial);
