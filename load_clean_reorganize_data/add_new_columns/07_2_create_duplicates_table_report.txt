--------------
CREATE INDEX IX_mention ON couple_unique (appln_id_mention)
--------------

Query OK, 30138984 rows affected (2 min 20.33 sec)
Records: 30138984  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_appln_id ON couple_unique (appln_id)
--------------

Query OK, 30138984 rows affected (2 min 58.04 sec)
Records: 30138984  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1
--------------

Query OK, 764465 rows affected (9.11 sec)
Records: 764465  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp ON tmp (appln_id_mention)
--------------

Query OK, 764465 rows affected (0.42 sec)
Records: 764465  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS duplicates
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id
--------------

Query OK, 1932026 rows affected (1 min 0.62 sec)
Records: 1932026  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_ipc
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp_ipc AS
SELECT
        appln_id_mention
FROM
        duplicates
GROUP BY
        appln_id_mention
HAVING
        count(DISTINCT no_ipc) > 1
--------------

Query OK, 90238 rows affected (1.39 sec)
Records: 90238  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp_ipc ON tmp_ipc (appln_id_mention)
--------------

Query OK, 90238 rows affected (0.15 sec)
Records: 90238  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_ipc_2
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp_ipc_2 AS
SELECT
        a.appln_id_mention,
        b.appln_id
FROM
        tmp_ipc a,
        duplicates b
WHERE 
        a.appln_id_mention = b.appln_id_mention
        AND b.no_ipc = 0
--------------

Query OK, 115932 rows affected (2.44 sec)
Records: 115932  Duplicates: 0  Warnings: 0

--------------
DELETE a.*
FROM
        couple_unique a,
        tmp_ipc b
WHERE
        a.appln_id_mention = b.appln_id_mention
--------------

Query OK, 228790 rows affected (24.88 sec)

--------------
INSERT INTO
        couple_unique
SELECT
        appln_id,
        appln_id_mention
FROM
        tmp_ipc_2
--------------

Query OK, 115932 rows affected (3.48 sec)
Records: 115932  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp
--------------

Query OK, 0 rows affected (0.02 sec)

--------------
CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1
--------------

Query OK, 687794 rows affected (7.08 sec)
Records: 687794  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp ON tmp (appln_id_mention)
--------------

Query OK, 687794 rows affected (0.36 sec)
Records: 687794  Duplicates: 0  Warnings: 0
--------------
DROP TABLE IF EXISTS duplicates
--------------

Query OK, 0 rows affected (0.07 sec)

--------------
CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id
--------------

Query OK, 1742497 rows affected (43.17 sec)
Records: 1742497  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_appt_invt
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)
--------------
CREATE TABLE tmp_appt_invt AS
SELECT
        appln_id_mention
FROM
        duplicates
GROUP BY
        appln_id_mention
HAVING
        count(DISTINCT no_appt_invt) > 1
--------------

Query OK, 14234 rows affected (1.27 sec)
Records: 14234  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp_appt_invt ON tmp_appt_invt (appln_id_mention)
--------------

Query OK, 14234 rows affected (1.88 sec)
Records: 14234  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_appt_invt_2
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp_appt_invt_2 AS
SELECT
        a.appln_id_mention,
        b.appln_id
FROM
        tmp_appt_invt a,
        duplicates b
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.no_appt_invt = 0
--------------

Query OK, 17392 rows affected (2.13 sec)
Records: 17392  Duplicates: 0  Warnings: 0

--------------
DELETE a.*
FROM
        couple_unique a,
        tmp_appt_invt b
WHERE
        a.appln_id_mention = b.appln_id_mention
--------------

Query OK, 34771 rows affected (2.56 sec)

--------------
INSERT INTO
        couple_unique
SELECT
        appln_id,
        appln_id_mention
FROM
        tmp_appt_invt_2
--------------

Query OK, 17392 rows affected (0.52 sec)
Records: 17392  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp
--------------

Query OK, 0 rows affected (0.01 sec)

--------------
CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1
--------------

Query OK, 675455 rows affected (7.11 sec)
Records: 675455  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp ON tmp (appln_id_mention)
--------------

Query OK, 675455 rows affected (0.36 sec)
Records: 675455  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS duplicates
--------------

Query OK, 0 rows affected (0.07 sec)

--------------
CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id
--------------

Query OK, 1712779 rows affected (42.08 sec)
Records: 1712779  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_appln_id_mention ON duplicates (appln_id_mention)
--------------

Query OK, 1712779 rows affected (4.42 sec)
Records: 1712779  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_appln_id_mention ON couple_unique (appln_id_mention)
--------------

Query OK, 30008747 rows affected, 1 warning (5 min 51.70 sec)
Records: 30008747  Duplicates: 0  Warnings: 1

--------------
DROP TABLE IF EXISTS tmp_appln_nr_2
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp_appln_nr_2 AS
SELECT
        appln_id_mention,
        min(appln_nr) AS appln_nr
FROM
        duplicates
GROUP BY
        appln_id_mention
--------------

Query OK, 675455 rows affected (2.50 sec)
Records: 675455  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp_appln_nr_2 ON tmp_appln_nr_2 (appln_id_mention)
--------------

Query OK, 675455 rows affected (0.60 sec)
Records: 675455  Duplicates: 0  Warnings: 0

--------------
DELETE a.*
FROM
        couple_unique a,
        tmp_appln_nr_2 b
WHERE
        a.appln_id_mention = b.appln_id_mention
--------------

Query OK, 1712779 rows affected (1 min 27.36 sec)

--------------
INSERT INTO
        couple_unique
SELECT
        DISTINCT b.appln_id,
        a.appln_id_mention
FROM
        tmp_appln_nr_2 a,
        duplicates b
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND a.appln_nr = b.appln_nr
--------------

Query OK, 675616 rows affected (42.32 sec)
Records: 675616  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp
--------------

Query OK, 0 rows affected (0.01 sec)

--------------
CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1
--------------

Query OK, 161 rows affected (7.23 sec)
Records: 161  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp ON tmp (appln_id_mention)
--------------

Query OK, 161 rows affected (0.06 sec)
Records: 161  Duplicates: 0  Warnings: 0
--------------
DROP TABLE IF EXISTS duplicates
--------------

Query OK, 0 rows affected (0.05 sec)

--------------
CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id
--------------

Query OK, 322 rows affected (0.02 sec)
Records: 322  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_appln_id_mention ON duplicates (appln_id_mention)
--------------

Query OK, 322 rows affected (0.06 sec)
Records: 322  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_appln_id_2
--------------

Query OK, 0 rows affected, 1 warning (0.00 sec)

--------------
CREATE TABLE tmp_appln_id_2 AS
SELECT
        appln_id_mention,
        min(appln_id) AS appln_id
FROM
        duplicates
GROUP BY
        appln_id_mention
--------------

Query OK, 161 rows affected (0.01 sec)
Records: 161  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp_appln_id_2 ON tmp_appln_id_2 (appln_id_mention)
--------------

Query OK, 161 rows affected (0.05 sec)
Records: 161  Duplicates: 0  Warnings: 0

--------------
DELETE a.*
FROM
        couple_unique a,
        tmp_appln_id_2 b
WHERE
        a.appln_id_mention = b.appln_id_mention
--------------

Query OK, 322 rows affected (0.02 sec)

--------------
INSERT INTO
        couple_unique
SELECT
        DISTINCT b.appln_id,
        a.appln_id_mention
FROM
        tmp_appln_id_2 a,
        duplicates b
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND a.appln_id = b.appln_id
--------------

Query OK, 161 rows affected (0.02 sec)
Records: 161  Duplicates: 0  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp
--------------

Query OK, 0 rows affected (0.00 sec)

--------------
CREATE TABLE tmp AS
SELECT
        appln_id_mention
FROM
        couple_unique
GROUP BY
        appln_id_mention
HAVING
        count(*) > 1
--------------

Query OK, 0 rows affected (6.76 sec)
Records: 0  Duplicates: 0  Warnings: 0

--------------
CREATE INDEX IX_tmp ON tmp (appln_id_mention)
--------------

Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0
--------------
DROP TABLE IF EXISTS duplicates
--------------

Query OK, 0 rows affected (0.00 sec)

--------------
CREATE TABLE duplicates AS
SELECT
        a.appln_id_mention,
        c.*
FROM
        tmp a,
        couple_unique b,
        patstat2021_staging.tls201_appln c
WHERE
        a.appln_id_mention = b.appln_id_mention
        AND b.appln_id = c.appln_id
--------------

Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0

Bye
