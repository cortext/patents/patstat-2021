-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------

USE patstat2021_staging;

ALTER TABLE
	tls201_appln
ADD
	COLUMN nb_cit int NOT NULL DEFAULT '0';

DROP TABLE IF EXISTS tmp_cit;

CREATE TABLE tmp_cit AS
SELECT
	a.appln_id,
	count(DISTINCT c.citn_id) AS nb_cit
FROM
	tls211_pat_publn b,
	tls212_citation c,
	tls201_appln a
WHERE
	a.appln_id = b.appln_id
	AND b.pat_publn_id = c.pat_publn_id
GROUP BY
	a.appln_id;

ALTER TABLE
	tmp_cit
ADD
	PRIMARY KEY PK_tmp_cit (appln_id);

UPDATE
	tls201_appln a,
	tmp_cit b
SET
	a.nb_cit = b.nb_cit
WHERE
	a.appln_id = b.appln_id;

CREATE INDEX nb_cit ON tls201_appln (nb_cit);