CREATE DATABASE inpi2017_2020 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

USE inpi2017_2020;

DROP TABLE IF EXISTS patents;

CREATE TABLE patents (
    appln_nr INT(11) NOT NULL,
    pub_nr INT(11) NOT NULL,
    appln_date VARCHAR(15) NULL,
    pub_date VARCHAR(15) NULL,
    country CHAR(2) NULL,
    nature VARCHAR(40) NULL,
    kind VARCHAR(10) NULL,
    title VARCHAR(300) NULL,
    title_lang CHAR(2) NULL,
    abstract TEXT NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,pub_nr) USING BTREE,
    KEY IX_appln_nr (appln_nr),
    KEY pub_nr (pub_nr)
);

DROP TABLE IF EXISTS applicants;

CREATE TABLE applicants (
    appln_nr INT(11) NOT NULL,
    `sequence` INT(2) NOT NULL,
    id_applt INT(11) NULL,
    orgname VARCHAR(150) NULL,
    first_name VARCHAR(100) NULL,
    last_name VARCHAR(100) NULL,
    middle_name VARCHAR(100) NULL,
    address VARCHAR(400) NULL,
    city VARCHAR(100) NULL,
    postcode VARCHAR(10) NULL,
    country VARCHAR(100) NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,`sequence`) USING BTREE,
    KEY IX_appln_nr (appln_nr)
);

DROP TABLE IF EXISTS inventors;

CREATE TABLE inventors (
    appln_nr INT(11) NOT NULL,
    `sequence` INT(2) NOT NULL,
    id_invt INT(11) NULL,
    orgname VARCHAR(150) NULL,
    first_name VARCHAR(100) NULL,
    last_name VARCHAR(100) NULL,
    middle_name VARCHAR(100) NULL,
    address VARCHAR(400) NULL,
    city VARCHAR(100) NULL,
    postcode VARCHAR(10) NULL,
    country VARCHAR(100) NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,`sequence`) USING BTREE,
    KEY IX_appln_nr (appln_nr)
);

DROP TABLE IF EXISTS agents;

CREATE TABLE agents (
    appln_nr INT(11) NOT NULL,
    `sequence` INT(2) NOT NULL,
    id_agent INT(11) NULL,
    orgname VARCHAR(150) NULL,
    first_name VARCHAR(100) NULL,
    last_name VARCHAR(100) NULL,
    middle_name VARCHAR(100) NULL,
    address VARCHAR(400) NULL,
    city VARCHAR(100) NULL,
    postcode VARCHAR(10) NULL,
    country VARCHAR(100) NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,`sequence`) USING BTREE,
    KEY IX_appln_nr (appln_nr)
);

DROP TABLE IF EXISTS owners;

CREATE TABLE owners (
    appln_nr INT(11) NOT NULL,
    `sequence` INT(2) NOT NULL,
    id_owner INT(11) NULL,
    orgname VARCHAR(150) NULL,
    first_name VARCHAR(100) NULL,
    last_name VARCHAR(100) NULL,
    middle_name VARCHAR(100) NULL,
    address VARCHAR(400) NULL,
    city VARCHAR(100) NULL,
    postcode VARCHAR(10) NULL,
    country VARCHAR(100) NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,`sequence`) USING BTREE,
    KEY IX_appln_nr (appln_nr)
);

DROP TABLE IF EXISTS ipcr_classes;

CREATE TABLE ipcr_classes (
    appln_nr INT(11) NOT NULL,
    `sequence` INT(2) NOT NULL,
    text VARCHAR(70) NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,`sequence`) USING BTREE,
    KEY IX_appln_nr (appln_nr)
);

DROP TABLE IF EXISTS tech_classifications;

CREATE TABLE tech_classifications (
    appln_nr INT(11) NOT NULL,
    `sequence` INT(2) NOT NULL,
    classification_scheme VARCHAR(70) NULL,
    classification_scheme_office VARCHAR(70) NULL,
    classification_scheme_date VARCHAR(70) NULL,
    classification_symbol VARCHAR(70) NULL,
    symbol_position VARCHAR(70) NULL,
    classification_value VARCHAR(70) NULL,
    classification_status VARCHAR(70) NULL,
    classification_data_source VARCHAR(70) NULL,
    action_date VARCHAR(70) NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,`sequence`) USING BTREE,
    KEY IX_appln_nr (appln_nr)
);

DROP TABLE IF EXISTS priority_applications;

CREATE TABLE priority_applications (
    appln_nr INT(11) NOT NULL,
    `sequence` INT(2) NOT NULL,
    doc_number VARCHAR(70) NULL,
    `date` VARCHAR(15) NULL,
    kind VARCHAR(10) NULL,
    country CHAR(2) NULL,
    UNIQUE KEY IX_appln_nr_seq (appln_nr,`sequence`) USING BTREE,
    KEY IX_appln_nr (appln_nr)
);