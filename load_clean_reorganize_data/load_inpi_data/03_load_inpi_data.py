#!/usr/bin/env python3
# File name: 03_load_inpi_data.py
# Description: Load INPI data
# Author: @tatiana_s
# Date: 05-04-2022

import sys
import argparse
import logging
from numpy import full
from simplejson import OrderedDict
import myconfig
import xmltodict
import glob, os
from zipfile import ZipFile
import shutil
import getpass
from sshtunnel import SSHTunnelForwarder
import mysql.connector


def parse_arguments():
    """Read arguments from a command line."""
    parser = argparse.ArgumentParser(description='Arguments get parsed via --commands')
    parser.add_argument('-v', metavar='verbosity', type=int, default=2,
        help='Verbosity of logging: 0 -critical, 1- error, 2 -warning, 3 -info, 4 -debug')

    args = parser.parse_args()
    verbose = {0: logging.CRITICAL, 1: logging.ERROR, 2: logging.WARNING, 3: logging.INFO, 4: logging.DEBUG}
    logging.basicConfig(format='%(message)s', level=verbose[args.v], stream=sys.stdout)
    
    return args

def get_config_variables():
    global folder_path, unzip, delete_unzipped_items, ssh_host, ssh_username, database_username, host, port, localhost, database_name
    folder_path = myconfig.folder_path
    unzip = myconfig.unzip
    delete_unzipped_items = myconfig.delete_unzipped_items
    ssh_host = myconfig.ssh_host
    ssh_username = myconfig.ssh_username
    database_username = myconfig.database_username
    host = myconfig.host
    port = myconfig.port
    localhost = myconfig.localhost
    database_name = myconfig.database_name

def open_file(file):
    """Open file and return its string value"""
    with open(file, 'r') as f:
        return f.read()
        
def unzip_files(list_files, data_dir):
    unzipped_items = []
    for file in list_files:
        try:
            print(f'unzipping {file}')
            zf = ZipFile(file, 'r')
            zf.extractall(data_dir)
            unzipped_items = unzipped_items + zf.namelist()
            zf.close()
        except Exception:
            logging.warning(f'Error unzipping file: {file}')
            return False
    top_unzipped_items = {item.split('/')[0] for item in unzipped_items}
    return top_unzipped_items # returns the list of zip files unzipped successfully

def rename_keys(in_dict):
    new = {}
    for k, v in in_dict.items():
        if isinstance(v, dict):
            v = rename_keys(v)
        elif isinstance(v, list):
            new_v = []
            for item in v:
                if type(item) not in (dict, OrderedDict):
                    new_item = item
                else:
                    new_item = rename_keys(item)
                new_v.append(new_item)
            v = new_v
        new[k.replace('@', '').replace('#', '')] = v
    return new

def remove_quotation(text):
    if text and type(text) == str:
        return text.replace('"',"'")
    elif type(text) == dict:
        return ''
    return text

def join_abstract_parts(abstract_text_parts):
    if type(abstract_text_parts) == list:
        text_str_parts = [text_part for text_part in abstract_text_parts if type(text_part) == str]
        return ' '.join(text_str_parts)  
    return abstract_text_parts

def get_patent_info():
    global appln_nr

    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})

    # publication data
    # appln_nr, appln_date
    # In this case, it takes the first value as we don't care about the format (that is the reason why in some cases it has several values)
    fr_pub_ref = biblio_data.get("fr-publication-data",{}).get("fr-publication-reference",{})
    pub_info = fr_pub_ref[0] if isinstance(fr_pub_ref, list) else fr_pub_ref

    pub_nr = pub_info.get("document-id",{}).get("doc-number")
    pub_date = pub_info.get("document-id",{}).get("date")
    kind = pub_info.get("document-id",{}).get("kind")
    nature = pub_info.get("fr-nature")

    # pub_nr, pub_date, kind
    fr_appln_ref = biblio_data.get("fr-application-reference",{})
    appln_info = fr_appln_ref[0] if isinstance(fr_appln_ref, list) else fr_appln_ref

    appln_nr = appln_info.get("document-id",{}).get("doc-number")
    appln_date = appln_info.get("document-id",{}).get("date")
    country = appln_info.get("document-id",{}).get("country")

    # title, title_lang
    title_lang = biblio_data.get("invention-title",{}).get("lang")
    title = biblio_data.get("invention-title",{}).get("text")

    # abstract
    abstract = patent_data_dict.get("fr-patent-document",{}).get("abstract")
    abstract_text_parts = abstract.get("p") if abstract else None
    try:
        abstract_text = join_abstract_parts(abstract_text_parts)
    except Exception as err:
        print(err)

    return {
        "appln_nr": appln_nr,
        "appln_date": appln_date,
        "country": country,
        "pub_nr": pub_nr,
        "pub_date": pub_date,
        "nature": remove_quotation(nature),
        "kind": kind,
        "title": remove_quotation(title),
        "title_lang": title_lang,
        "abstract": remove_quotation(abstract_text)
    }

def get_patent_applicants():
    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})

    applicants = biblio_data.get("parties",{}).get("applicants",{})
    applicants_data = []
    if applicants:
        applicants_value = applicants.get("applicant",{})
        applicants_info = applicants_value if isinstance(applicants_value, list) else [applicants_value]

        for applicant in applicants_info:
            applicants_data.append({
                "appln_nr": appln_nr,
                "sequence": applicant.get("sequence"),
                "id_applt": applicant.get("addressbook",{}).get("iid"),
                "orgname": remove_quotation(applicant.get("addressbook",{}).get("orgname")),
                "first_name": remove_quotation(applicant.get("addressbook",{}).get("first-name")),
                "last_name": remove_quotation(applicant.get("addressbook",{}).get("last-name")),
                "middle_name": remove_quotation(applicant.get("addressbook",{}).get("middle-name")),
                "address": remove_quotation(applicant.get("addressbook",{}).get("address",{}).get("address-1")),
                "city": remove_quotation(applicant.get("addressbook",{}).get("address",{}).get("city")),
                "postcode": applicant.get("addressbook",{}).get("address",{}).get("postcode"),
                "country": applicant.get("addressbook",{}).get("address",{}).get("country")
            })
    return applicants_data

def get_patent_inventors():
    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})

    inventors = biblio_data.get("parties",{}).get("inventors",{})
    inventors_data = []
    if inventors:
        inventors_value = inventors.get("inventor",{})
        inventors_info = inventors_value if isinstance(inventors_value, list) else [inventors_value]

        for inventor in inventors_info:
            inventors_data.append({
                "appln_nr": appln_nr,
                "sequence": inventor.get("sequence"),
                "id_invt": inventor.get("addressbook",{}).get("iid"),
                "first_name": remove_quotation(inventor.get("addressbook",{}).get("first-name")),
                "last_name": remove_quotation(inventor.get("addressbook",{}).get("last-name")),
                "middle_name": remove_quotation(inventor.get("addressbook",{}).get("middle-name")),
                "address": remove_quotation(inventor.get("addressbook",{}).get("address",{}).get("address-1")),
                "city": remove_quotation(inventor.get("addressbook",{}).get("address",{}).get("city")),
                "postcode": inventor.get("addressbook",{}).get("address",{}).get("postcode"),
                "country": inventor.get("addressbook",{}).get("address",{}).get("country")
            })
    return inventors_data

def get_patent_agents():
    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})

    agents = biblio_data.get("parties",{}).get("agents",{})
    agents_data = []
    if agents:
        agents_value = agents.get("agent",{})
        agents_info = agents_value if isinstance(agents_value, list) else [agents_value]

        for agent in agents_info:
            agents_data.append({
                "appln_nr": appln_nr,
                "sequence": agent.get("sequence"),
                "id_agent": agent.get("addressbook",{}).get("iid"),
                "orgname": remove_quotation(agent.get("addressbook",{}).get("orgname")),
                "first_name": remove_quotation(agent.get("addressbook",{}).get("first-name")),
                "last_name": remove_quotation(agent.get("addressbook",{}).get("last-name")),
                "middle_name": remove_quotation(agent.get("addressbook",{}).get("middle-name")),
                "address": remove_quotation(agent.get("addressbook",{}).get("address",{}).get("address-1")),
                "city": remove_quotation(agent.get("addressbook",{}).get("address",{}).get("city")),
                "postcode": agent.get("addressbook",{}).get("address",{}).get("postcode"),
                "country": agent.get("addressbook",{}).get("address",{}).get("country")
            })
    return agents_data

def get_patent_owners():
    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})

    owners = biblio_data.get("fr-owners",{})
    owners_data = []
    if owners:
        owners_value = owners.get("fr-owner",{})
        owners_info = owners_value if isinstance(owners_value, list) else [owners_value]

        for owner in owners_info:
            owners_data.append({
                "appln_nr": appln_nr,
                "sequence": owner.get("sequence"),
                "id_owner": owner.get("addressbook",{}).get("iid"),
                "first_name": remove_quotation(owner.get("addressbook",{}).get("first-name")),
                "last_name": remove_quotation(owner.get("addressbook",{}).get("last-name")),
                "middle_name": remove_quotation(owner.get("addressbook",{}).get("middle-name")),
                "address": remove_quotation(owner.get("addressbook",{}).get("address",{}).get("address-1")),
                "city": remove_quotation(owner.get("addressbook",{}).get("address",{}).get("city")),
                "postcode": owner.get("addressbook",{}).get("address",{}).get("postcode"),
                "country": owner.get("addressbook",{}).get("address",{}).get("country")
            })
    return owners_data

def get_patent_ipcr_classes():
    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})

    ipcr_classes = biblio_data.get("classifications-ipcr",{})
    ipcr_classes_data = []
    if ipcr_classes:
        ipcr_classes_value = ipcr_classes.get("classification-ipcr",{})
        ipcr_classes_info = ipcr_classes_value if isinstance(ipcr_classes_value, list) else [ipcr_classes_value]

        for ipcr_class in ipcr_classes_info:
            ipcr_classes_data.append({
                "appln_nr": appln_nr,
                "sequence": ipcr_class.get("sequence"),
                "text": remove_quotation(ipcr_class.get("text"))
            })
    return ipcr_classes_data

def get_patent_classifications():
    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})

    patent_classes = biblio_data.get("patent-classifications",{})
    patent_classes_data = []
    if patent_classes:
        patent_classes_value = patent_classes.get("patent-classification",{})
        patent_classes_info = patent_classes_value if isinstance(patent_classes_value, list) else [patent_classes_value]

        for patent_class in patent_classes_info:
            patent_classes_data.append({
                "appln_nr": appln_nr,
                "sequence": patent_class.get("sequence"),
                "classification_scheme": patent_class.get("classification-scheme",{}).get("scheme"),
                "classification_scheme_office": patent_class.get("classification-scheme",{}).get("office"),
                "classification_scheme_date": patent_class.get("classification-scheme",{}).get("date"),
                "classification_symbol": patent_class.get("classification-symbol"),
                "symbol_position": patent_class.get("symbol-position"),
                "classification_value": patent_class.get("classification-value"),
                "classification_status": patent_class.get("classification-status"),
                "classification_data_source": patent_class.get("classification-data-source"),
                "action_date": patent_class.get("action-date",{}).get("date")
            })
    return patent_classes_data

def get_prioriry_applications():
    biblio_data = patent_data_dict.get("fr-patent-document",{}).get("fr-bibliographic-data",{})
    
    priority_claims = biblio_data.get("fr-priority-claims",{})
    priority_claims_data = []
    if priority_claims:
        priority_claims_value = priority_claims.get("fr-priority-claim",{})
        priority_claims_info = priority_claims_value if isinstance(priority_claims_value, list) else [priority_claims_value]

        for priority_claim in priority_claims_info:
            priority_claims_data.append({
                "appln_nr": appln_nr,
                "sequence": priority_claim.get("sequence"),
                "doc_number": priority_claim.get("doc-number"),
                "date": priority_claim.get("date"),
                "kind": priority_claim.get("kind"),
                "country": priority_claim.get("country")
            })
    return priority_claims_data

def parse_data_to_insert_query(list_data, db_name, tablename):
    query = f"""
        INSERT IGNORE INTO {db_name}.{tablename} 
        ({', '.join(list_data[0].keys())})
        VALUES 
    """

    data_string_sets = []
    for dict_item in list_data:
        data_values = dict_item.values()
        data_values_wo_none = ['NULL' if v is None else v for v in data_values]
        data_string_set = ','.join(f'{item}' if item == 'NULL' else f'"{item}"' for item in data_values_wo_none) # data string separated by comma and enclosed in quotation marks
        data_string_sets.append(data_string_set)

    data_string_sets_all = [f'({item})\n' for item in data_string_sets]
    query += f"{', '.join(data_string_sets_all)};"
    return query

def open_ssh_tunnel(verbose=False):
    """Open an SSH tunnel and connect using a username and password.
    
    :param verbose: Set to True to show logging
    :return tunnel: Global SSH tunnel connection
    """

    try:
        ssh_password = getpass.getpass(prompt=f'password ssh user "{ssh_username}": ')
    except Exception as error:
        logging.warning('ERROR', error)
    
    global tunnel
    tunnel = SSHTunnelForwarder(
        (ssh_host, 22),
        ssh_username = ssh_username,
        ssh_password = ssh_password,
        remote_bind_address = (host, port)
    )
    
    tunnel.start()


def mysql_connect():
    """Connect to a MySQL server using the SSH tunnel connection
    
    :return db_connection: Global MySQL database connection
    :return db_cursor: Global MySQL database connection cursor
    """

    try:
        database_password = getpass.getpass(prompt=f'password DB user "{database_username}": ')
    except Exception as error:
        logging.warning('ERROR', error)

    params = {
        'user': database_username,
        'password': database_password,
        'host': localhost,
        'port': tunnel.local_bind_port,
        'allow_local_infile': True   
    }

    global db_connection, db_cursor

    db_connection = mysql.connector.connect(**params) # DB connection
    db_cursor = db_connection.cursor()
    print("Database connected")

def divide_chunks(list_to_divide, n):
    # looping till list_to_divide length 
    for i in range(0, len(list_to_divide), n): 
        yield list_to_divide[i:i + n]

def load_data_into_db_table(list_data, db_name, tablename):
    try:
        if len(list_data) > 0:
            if len(list_data) > 10000:
                sublists_data = list(divide_chunks(list_data, 10000))
            else:
                sublists_data = [list_data]

            for sublist in sublists_data:
                insert_query = parse_data_to_insert_query(sublist, db_name, tablename)
                tr = db_connection._execute_query(insert_query)
                print(f"data inserted in {tablename}")
    except Exception as err:
        print(type(err))    # the exception errance
        print(err.args)     # arguments stored in .args
        print(err)
        print("Error inserting into MySQL")
        raise err

def main():
    DATA_DIR = os.path.join(folder_path)
    os.chdir(DATA_DIR)

    if unzip:
        zip_files = glob.glob("*.zip")
        unzipped_items = unzip_files(zip_files, DATA_DIR)
        print(unzipped_items)
    else:
        unzipped_items = next(os.walk(DATA_DIR))[1]
        print('folders already unzipped: ')
        print(unzipped_items)

    open_ssh_tunnel()
    mysql_connect()

    for item_week in unzipped_items:
        dir_publn = os.path.join(folder_path, item_week, "doc/") # directory path where the xml files containing the patent publications are
        os.chdir(dir_publn) # change current directory to the one specified so then glob can be used as follows

        print(f"opening data files from: {item_week}")
        patents_info_set,applicants_set,inventors_set,agents_set,owners_set,ipcr_classes_set,classifications_set,priority_applications_set=([] for i in range(8))
        for publn_xml_file in glob.glob("*.xml"):
            full_xml_path = os.path.join(dir_publn, publn_xml_file)

            xml_data = open_file(full_xml_path)
            if not xml_data: continue
            dict_data = xmltodict.parse(xml_data) # xml to dict
            global patent_data_dict
            patent_data_dict = rename_keys(dict_data)

            patents_info_set.append(get_patent_info())
            applicants_set = applicants_set + get_patent_applicants()
            inventors_set = inventors_set + get_patent_inventors()
            agents_set = agents_set + get_patent_agents()
            owners_set = owners_set + get_patent_owners()
            ipcr_classes_set = ipcr_classes_set + get_patent_ipcr_classes()
            classifications_set = classifications_set + get_patent_classifications()
            priority_applications_set = priority_applications_set + get_prioriry_applications()

        print(f"loading patent data from: {item_week}")

        try:
            load_data_into_db_table(patents_info_set, database_name, 'patents')
            load_data_into_db_table(applicants_set, database_name, 'applicants')
            load_data_into_db_table(inventors_set, database_name, 'inventors')
            load_data_into_db_table(agents_set, database_name, 'agents')
            load_data_into_db_table(owners_set, database_name, 'owners')
            load_data_into_db_table(ipcr_classes_set, database_name, 'ipcr_classes')
            load_data_into_db_table(classifications_set, database_name, 'tech_classifications')
            load_data_into_db_table(priority_applications_set, database_name, 'priority_applications')
        except Exception as error:
            break

        if delete_unzipped_items:
            dir_item = os.path.join(folder_path, item_week)
            os.remove(dir_item) if os.path.isfile(dir_item) else shutil.rmtree(dir_item)


if __name__ == '__main__':
    args = parse_arguments()
    get_config_variables()
    main()