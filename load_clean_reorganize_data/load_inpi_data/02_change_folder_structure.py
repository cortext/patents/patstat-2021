import sys
import pathlib

CURRENT_DIR = pathlib.Path(sys.argv[1])

print(CURRENT_DIR)

from zipfile import ZipFile
import logging
def unzip_files(list_files, data_dir):
    unzipped_items = []
    for file in list_files:
        try:
            zf = ZipFile(file, 'r')
            zf.extractall(data_dir)
            unzipped_items = unzipped_items + zf.namelist()
            zf.close()
        except Exception:
            logging.warning(f'Error unzipping file: {file}')
            return False
    top_unzipped_items = {item.split('/')[0] for item in unzipped_items}
    return top_unzipped_items # returns the list of zip files unzipped successfully

import os, glob
unzip = True
for item in list(CURRENT_DIR.glob("*")):
    if item.is_dir():
        print(item)
        new_doc = os.path.join(item, "doc")
        if os.path.exists(new_doc): continue
        os.mkdir(new_doc)
        if unzip:
            zip_files = glob.glob("*.zip")
            unzipped_items = unzip_files(list(item.glob("*.zip")), item)
        else:
            unzipped_items = next(os.walk(item))[1]

        for patent_folder in unzipped_items:
            patent_filename = str(patent_folder).split('_')[0]
            file_to_move = item.joinpath(item, patent_folder)
            file_destination = item.joinpath(new_doc, f"{patent_filename}")
            os.rename(file_to_move, file_destination)
