# Load INPI data

The data was obtained from www.inpi.net server by FTP with the credentials provided by them, and stored in our server.

1. Create INPI data schema.
    ```shell=
    mysql --defaults-file=~/mycnf.cnf -vvv < 01_create_inpi_schema.sql 2>&1 &
    ```

2. Change files distribution to make it consistent.

    There are 4 folders corresponding to the years: 2017, 2018, 2019, 2020. The xml files for each patent are distributed in a different way in the folder 2017 and in the folder 2018 is mixed (not consistent), so to make all the folders consistent in terms of files distribution, a python file was created and run as follows: 

    ```shell=
    python3 02_change_folder_structure.py /srv/local/inpi_data_2017-2020/2017
    python3 02_change_folder_structure.py /srv/local/inpi_data_2017-2020/2018
    ```

3. Run scripts to load data for each year.
    - Make sure you create a file named `myconfig.py` where all the configuration variables will be placed.

    ```shell=
    $ docker build -t load_inpi_data .
    $ docker run -v /srv/local/inpi_data_2017-2020/2017:/folder_data_year -it load_inpi_data
    $ docker run -v /srv/local/inpi_data_2017-2020/2018:/folder_data_year -it load_inpi_data
    $ docker run -v /srv/local/inpi_data_2017-2020/2019:/folder_data_year -it load_inpi_data
    $ docker run -v /srv/local/inpi_data_2017-2020/2020:/folder_data_year -it load_inpi_data
    ```
