folder_path = "/folder_data_year"
unzip = False
delete_unzipped_items = True # It is better to set it True, in terms of time, because in case the script 
                             # is stopped at some point, the folders whose data were already loaded, will not 
                             # be loaded again.
# Tunnel connection
ssh_host = 'ssh_host'
ssh_username = 'ssh_username'
host = 'host'
port = 3307
# DB connection
database_username = 'your_db_user'
localhost = '127.0.0.1'
database_name = 'inpi2017_2020'