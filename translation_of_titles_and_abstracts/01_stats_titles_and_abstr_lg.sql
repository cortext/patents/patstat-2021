USE patstat2021_staging;

SELECT appln_title_lg, COUNT(*)
FROM tls202_appln_title_ifris
GROUP BY appln_title_lg
ORDER BY COUNT(*) DESC; 

SELECT appln_abstract_lg, COUNT(*)
FROM tls203_appln_abstr_ifris 
GROUP BY appln_abstract_lg
ORDER BY COUNT(*) DESC; 
