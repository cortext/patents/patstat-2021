USE patstat2021_staging;

CREATE INDEX idx_appln_title_lg USING BTREE 
  ON tls202_appln_title_ifris (appln_title_lg);
-- Query OK, 98397238 rows affected (13 min 30,04 sec)
-- Records: 98397238  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id_title_lg USING BTREE 
  ON tls202_appln_title_ifris (appln_id, appln_title_lg);
-- Query OK, 98397238 rows affected (15 min 28,44 sec)
-- Records: 98397238  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_abstract_lg USING BTREE 
  ON tls203_appln_abstr_ifris (appln_abstract_lg);
-- Query OK, 73546933 rows affected (19 min 15,61 sec)
-- Records: 73546933  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id_abstract_lg USING BTREE 
  ON tls203_appln_abstr_ifris (appln_id, appln_abstract_lg);
-- Query OK, 73546933 rows affected (19 min 1,81 sec)
-- Records: 73546933  Duplicates: 0  Warnings: 0

-- Fill these new columns with the data already in english
UPDATE tls202_appln_title_ifris
SET appln_title_en = appln_title,
  appln_title_en_source = 'patstat'
WHERE appln_title_lg = 'en';
-- Query OK, 81111528 rows affected (1 hour 26 min 3,86 sec)
-- Rows matched: 81111528  Changed: 81111528  Warnings: 0

UPDATE tls203_appln_abstr_ifris
SET appln_abstract_en = appln_abstract,
  appln_abstract_en_source = 'patstat'
WHERE appln_abstract_lg = 'en';
-- Query OK, 66668613 rows affected (1 hour 21 min 38,27 sec)
-- Rows matched: 66668613  Changed: 66668613  Warnings: 0

-- Bring from the previous Patstat version all the info already translated, 
-- when the appln_id and appln_title_lg (appln_abstr_lg for tls203) is the same

UPDATE tls202_appln_title_ifris a 
  INNER JOIN patstatAvr2017_lab.tls202_appln_title_ifris b
  ON a.appln_id = b.appln_id AND a.appln_title_lg = b.appln_title_lg
SET a.appln_title_en = b.appln_title_en,
  a.appln_title_en_source = b.appln_title_en_source
WHERE a.appln_title_en IS NULL;
-- Query OK, 6914212 rows affected (48 min 30.69 sec)
-- Rows matched: 12324298  Changed: 6914212  Warnings: 0

UPDATE tls203_appln_abstr_ifris a 
  INNER JOIN patstatAvr2017_lab.tls203_appln_abstr_ifris b
  ON a.appln_id = b.appln_id AND a.appln_abstract_lg = b.appln_abstract_lg
SET a.appln_abstract_en = b.appln_abstract_en,
  a.appln_abstract_en_source = b.appln_abstract_en_source
WHERE a.appln_abstract_en IS NULL;
-- Query OK, 2422268 rows affected (32 min 51.48 sec)
-- Rows matched: 2861425  Changed: 2422268  Warnings: 0
