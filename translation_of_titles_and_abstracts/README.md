# Translation of titles and abstracts

1. Number of patents by language code  
  [01_stats_titles_and_abstr_lg.sql](01_stats_titles_and_abstr_lg.sql)
    <details>
    <summary>Titles: tls202_appln_title_ifris</summary>

    | appln_title_lg | count(\*) |
    | -------------- | --------- |
    | en             | 81111528  |
    | de             | 6979648   |
    | fr             | 3135475   |
    | zh             | 1147728   |
    | es             | 1090276   |
    | pt             | 889559    |
    | ja             | 718495    |
    | it             | 690311    |
    | ko             | 583562    |
    | ru             | 505134    |
    | da             | 491795    |
    | fi             | 226311    |
    | sv             | 204351    |
    | nl             | 197588    |
    | no             | 188981    |
    | tr             | 89106     |
    | uk             | 46156     |
    | el             | 39098     |
    | pl             | 19336     |
    | id             | 14840     |
    | is             | 7495      |
    | et             | 6759      |
    | ar             | 5142      |
    | sh             | 4552      |
    | lv             | 1256      |
    | lt             | 940       |
    | cs             | 853       |
    | bg             | 322       |
    | ro             | 234       |
    | bs             | 218       |
    | sr             | 95        |
    | hr             | 55        |
    | hu             | 23        |
    | sk             | 7         |
    | be             | 3         |
    | sl             | 2         |
    | he             | 2         |
    | vi             | 1         |
    | me             | 1         |

    </details>

    <details>
    <summary>Abstracts: tls202_appln_title_ifris</summary>

    | appln_abstract_lg | COUNT(\*) |
    | ----------------- | --------- |
    | en                | 66668613  |
    | ko                | 1411644   |
    | zh                | 1228004   |
    | de                | 1031832   |
    | es                | 794810    |
    | ja                | 657236    |
    | fr                | 642525    |
    | pt                | 465233    |
    | ru                | 248320    |
    | tr                | 74658     |
    | no                | 62999     |
    | uk                | 53721     |
    | it                | 39088     |
    | pl                | 37941     |
    | hu                | 28342     |
    | el                | 22654     |
    | ro                | 16895     |
    | nl                | 13246     |
    | cs                | 10779     |
    | sh                | 9686      |
    | hr                | 7047      |
    | ar                | 5622      |
    | sr                | 3678      |
    | da                | 2825      |
    | sv                | 2538      |
    | sl                | 2224      |
    | fi                | 1425      |
    | bg                | 1052      |
    | sk                | 1001      |
    | bs                | 474       |
    | me                | 469       |
    | lv                | 274       |
    | lt                | 77        |
    | et                | 1         |

    </details>

2. Add extra columns to store the title and abstract in english
  [02_add_column_title_and_abstract_in_english.sql](02_add_column_title_and_abstract_in_english.sql)

3. Bring titles and abstracts previously translated
  
    [03_bring_data_already_translated.sql](03_bring_data_already_translated.sql)
  - Fill the new columns with the titles and abstracts already in english.
  - Bring from the previous Patstat version all the values already translated. If the title/abstract language did not change we assume its content netiher, which means we can assign the same old translation.

4. Number of titles and abstracts missing translation to english
  [04_count_rows_missing_translation.sql](04_count_rows_missing_translation.sql)

5. Translate
  We do not translate for now, as it is not an important step to publish the first version of RPD 2021.
