USE patstat2021_staging;

SELECT COUNT(*) 
FROM tls202_appln_title_ifris WHERE appln_title_en IS NULL;
-- 10.371.498

SELECT appln_title_lg, COUNT(*)
FROM tls202_appln_title_ifris
WHERE appln_title_en IS NULL
GROUP BY appln_title_lg
ORDER BY COUNT(*) DESC; 
-- +----------------+----------+
-- | appln_title_lg | COUNT(*) |
-- +----------------+----------+
-- | de             |  3702565 |
-- | fr             |  2294767 |
-- | zh             |  1146909 |
-- | ko             |   577672 |x -> Language not supported by DeepL
-- | ja             |   514169 |
-- | ru             |   452180 |
-- | es             |   438313 |
-- | pt             |   339133 |
-- | it             |   261978 |
-- | da             |   241690 |
-- | fi             |   116818 |
-- | tr             |    60492 |
-- | sv             |    59152 |
-- | uk             |    44664 |
-- | no             |    34522 |x
-- | el             |    32809 |
-- | nl             |    24976 |
-- | pl             |    14224 |
-- | ar             |     2654 |x
-- | sh             |     2605 |x
-- | is             |     2217 |x
-- | id             |     2130 |
-- | et             |     1565 |
-- | lt             |      899 |
-- | lv             |      844 |
-- | cs             |      798 |
-- | bg             |      275 |
-- | ro             |      229 |
-- | bs             |       95 |x
-- | sr             |       78 |x
-- | hr             |       52 |x
-- | hu             |       13 |
-- | be             |        3 |x
-- | he             |        2 |x
-- | sk             |        2 |
-- | sl             |        2 |
-- | vi             |        1 |x
-- | me             |        1 |x
-- +----------------+----------+
-- 38 rows in set (14 min 42.30 sec)

SELECT COUNT(*) 
FROM tls203_appln_abstr_ifris WHERE appln_abstract_en IS NULL;
-- 4.456.052

SELECT appln_abstract_lg, COUNT(*)
FROM tls203_appln_abstr_ifris 
WHERE appln_abstract_en IS NULL
GROUP BY appln_abstract_lg
ORDER BY COUNT(*) DESC; 
-- +-------------------+----------+
-- | appln_abstract_lg | COUNT(*) |
-- +-------------------+----------+
-- | ko                |  1298031 |x -> Language not supported by DeepL
-- | zh                |  1227102 |
-- | ja                |   527166 |
-- | de                |   406155 |
-- | ru                |   233877 |
-- | es                |   213204 |
-- | pt                |   195713 |
-- | fr                |   109212 |
-- | tr                |    57283 |
-- | uk                |    51559 |
-- | it                |    20999 |
-- | pl                |    19805 |
-- | no                |    18713 |x
-- | el                |    16409 |
-- | ro                |    15029 |
-- | nl                |     7681 |
-- | hr                |     7002 |x
-- | sh                |     5508 |x
-- | cs                |     4390 |
-- | ar                |     4387 |x
-- | hu                |     3287 |
-- | sr                |     3129 |x
-- | sv                |     2537 |
-- | da                |     1976 |
-- | sl                |     1833 |
-- | fi                |     1425 |
-- | sk                |      945 |
-- | bg                |      479 |
-- | bs                |      474 |x
-- | me                |      469 |x
-- | lv                |      261 |
-- | lt                |       11 |
-- | et                |        1 |
-- +-------------------+----------+
-- 33 rows in set (18 min 57.93 sec)
