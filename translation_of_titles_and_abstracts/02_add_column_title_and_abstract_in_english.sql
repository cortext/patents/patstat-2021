USE patstat2021_staging;

ALTER TABLE tls202_appln_title_ifris 
ADD COLUMN appln_title_en VARCHAR(3500) NULL;
-- Query OK, 98397238 rows affected (10 min 35,76 sec)
-- Records: 98397238  Duplicates: 0  Warnings: 0

ALTER TABLE tls202_appln_title_ifris 
ADD COLUMN appln_title_en_source VARCHAR(15) NULL;
-- Query OK, 98397238 rows affected (10 min 26,56 sec)
-- Records: 98397238  Duplicates: 0  Warnings: 0

ALTER TABLE tls203_appln_abstr_ifris 
ADD COLUMN appln_abstract_en VARCHAR(10000) NULL;
-- Query OK, 73546933 rows affected (15 min 17,49 sec)
-- Records: 73546933  Duplicates: 0  Warnings: 0

ALTER TABLE tls203_appln_abstr_ifris 
ADD COLUMN appln_abstract_en_source VARCHAR(15) NULL;
-- Query OK, 73546933 rows affected (8 min 54,03 sec)
-- Records: 73546933  Duplicates: 0  Warnings: 0
