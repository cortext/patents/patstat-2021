-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Get FR addresses from INPI if not in PATSTAT
USE patstat2021_staging;

-- ---------------------------------------------------------------------------------
-- FR applicants
DROP TABLE IF EXISTS applt_adr_ifris_epwofr;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE applt_adr_ifris_epwofr (
    appln_id int(10) NOT NULL DEFAULT '0',
    person_id int(10) NOT NULL DEFAULT '0',
    doc_std_name_id int(10) NOT NULL DEFAULT '0',
    person_name varchar(300) NOT NULL,
    person_address varchar(500) NOT NULL,
    person_ctry_code varchar(3) NOT NULL DEFAULT '',
    source varchar(7) NULL,
    pm_pp_comp varchar(1) NULL,
    qualif_comp varchar(4) NULL,
    name_comp varchar(300) NULL,
    adr_comp varchar(500) NULL,
    street_comp varchar(400) NULL,
    zip_code_comp varchar(10) NULL,
    city_comp varchar(50) NULL,
    ctry_comp varchar(2) NULL,
    methode varchar(4) NULL,
    KEY iapplt_epwo (appln_id),
    KEY isource (source)
);
-- Query OK, 0 rows affected (0.01 sec)

-- ---------------------------------------------------------------------------------
-- Insert REGPAT names and addresses previously obtained

INSERT INTO
    applt_adr_ifris_epwofr
SELECT
    appln_id,
    person_id,
    doc_std_name_id,
    person_name,
    person_address,
    person_ctry_code,
    'REGPAT',
    '',
    '',
    name_regpat,
    adr_regpat,
    '',
    '',
    '',
    ctry_regpat,
    methode
FROM
    applt_adr_ifris_epwo
WHERE
    name_regpat != '';
-- Query OK, 3261994 rows affected (1 min 17.57 sec)
-- Records: 3261994  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Insert the rest of the lines

INSERT INTO
    applt_adr_ifris_epwofr
SELECT
    appln_id,
    person_id,
    doc_std_name_id,
    person_name,
    person_address,
    person_ctry_code,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
FROM
    applt_adr_ifris_epwo
WHERE
    name_regpat = '';
-- Query OK, 104197081 rows affected, 36 warnings (1 hour 2 min 5.20 sec)
-- Records: 104197081  Duplicates: 0  Warnings: 36

-- ---------------------------------------------------------------------------------
-- Addition of missing FR rows as they were built after REGPAT addresses processing 

INSERT INTO
    applt_adr_ifris_epwofr
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    0,
    a.name_inpi,
    concat(
        a.street_inpi,
        " ; ",
        a.zip_code_inpi,
        " ; ",
        a.city_inpi
    ),
    a.ctry_inpi,
    'INPI',
    a.pm_pp_inpi,
    a.qualification_inpi,
    a.name_inpi,
    concat(
        a.street_inpi,
        " ; ",
        a.zip_code_inpi,
        " ; ",
        a.city_inpi
    ),
    a.street_inpi,
    a.zip_code_inpi,
    a.city_inpi,
    a.ctry_inpi,
    ''
FROM
    applt_fr_inpi a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 9536 rows affected, 5631 warnings (1.34 sec)
-- Records: 9536  Duplicates: 0  Warnings: 5631  

ALTER TABLE
    applt_adr_ifris_epwofr
ADD
    PRIMARY KEY PK_appkt_fr (appln_id, person_id);
-- Query OK, 107468611 rows affected (22 min 28.11 sec)

-- ---------------------------------------------------------------------------------
-- Method 1
-- MET1: where person_name and orgname match strictly
UPDATE
    applt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.applicants b ON (
        a.appln_id = b.appln_nr
        AND a.person_name = IFNULL(b.orgname, CONCAT(b.last_name, ", ", b.first_name))
    )
SET
    a.source = 'INPI',
    a.pm_pp_comp = '', -- b.pm_pp,
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = IFNULL(b.orgname, CONCAT(b.last_name, ", ", b.first_name)),
    a.adr_comp = concat(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET1'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 3343 rows affected (13 min 20.49 sec)
-- Rows matched: 3343  Changed: 3343  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 2
-- MET2: PATSTAT name like INPI name
UPDATE
    applt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.applicants b ON (
        a.methode = ''
        AND a.appln_id = b.appln_nr
        AND a.person_name LIKE IFNULL(b.orgname, CONCAT(b.last_name, ", ", b.first_name))
    )
SET
    a.source = 'INPI',
    a.pm_pp_comp = '', -- b.pm_pp,
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = IFNULL(b.orgname, CONCAT(b.last_name, ", ", b.first_name)),
    a.adr_comp = CONCAT(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET2'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 0 rows affected (5.77 sec)
-- Rows matched: 0  Changed: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 3
-- MET3: INPI name like PATSTAT name 
UPDATE
    applt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.applicants b ON (
        a.methode = ''
        AND a.appln_id = b.appln_nr
        AND IFNULL(b.orgname, CONCAT(b.last_name, ", ", b.first_name)) LIKE concat('%', a.person_name, '%')
    )
SET
    a.source = 'INPI',
    a.pm_pp_comp = '', -- b.pm_pp,
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = IFNULL(b.orgname, CONCAT(b.last_name, ", ", b.first_name)),
    a.adr_comp = concat(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET3'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 31 rows affected (16 min 35.84 sec)
-- Rows matched: 31  Changed: 31  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 4
-- MET4: PATSTAT name like INPI name 
UPDATE
    applt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.applicants b ON (
        a.methode = ''
        AND a.appln_id = b.appln_nr
        AND 
            (
                a.person_name LIKE CONCAT('%', b.orgname, '%') OR 
                a.person_name LIKE CONCAT('%', b.last_name, "%", b.first_name, '%')
        )
    )
SET
    a.source = 'INPI',
    a.pm_pp_comp = '', -- b.pm_pp,
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = IFNULL(b.orgname, CONCAT(b.last_name, ", ", b.first_name)),
    a.adr_comp = concat(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET4'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 12 rows affected (5.28 sec)
-- Rows matched: 12  Changed: 12  Warnings: 0

CREATE INDEX idx_methode ON applt_adr_ifris_epwofr (methode);
-- Query OK, 107468611 rows affected (16 min 12.25 sec)
-- Records: 107468611  Duplicates: 0  Warnings: 0
