# Integrate INPI data

[01_missing_FR.sq](01_missing_FR.sql)

In the previous built tables: `applt_comp_ifris_epwo` and `invt_comp_ifris_epwo` are all the applicants and inventors respectively, including the ones obtained from EP and WO offices. Now, from FR office, which is the INPI data, we add more missing applicants and inventors.

[02_adresses_appt_FR.sql](02_adresses_appt_FR.sql) 

First matching by patent application id (appln_id), the missing applicants are included in a new table. And after the matching is done also by names strictly and using the SQL 'like' operator.

[03_adresses_invt_FR.sql](03_adresses_invt_FR.sql)

The same is done for the inventors.

[04_stats_EP_WO_FR.sql](04_stats_EP_WO_FR.sql)

Make some stats on the tables built, the number of rows coming from REGPAT and INPI, and the methods used to match and bring some addresses.