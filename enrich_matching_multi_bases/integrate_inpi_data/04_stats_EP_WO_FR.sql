-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Stats on the data obtained from REGPAT and INPI
USE patstat2021_staging;

SELECT count(*) from applt_fr_inpi; -- 50353
SELECT count(distinct appln_id)  from applt_fr_inpi; -- 44990

SELECT count(*) from invt_fr_inpi; -- 170021
SELECT count(distinct appln_id)  from invt_fr_inpi; -- 80815

SELECT count(*) from ipc_fr_inpi; -- 186152
SELECT count(distinct appln_id)  from ipc_fr_inpi; -- 68885

SELECT count(*) from applt_ep_regpat; -- 0
SELECT count(distinct appln_id)  from applt_ep_regpat; -- 0

SELECT count(*) from invt_ep_regpat; -- 0
SELECT count(distinct appln_id)  from invt_ep_regpat; -- 0

SELECT count(*) from ipc_ep_regpat; -- 0
SELECT count(distinct appln_id)  from ipc_ep_regpat; -- 0

SELECT count(*) from applt_wo_regpat; -- 25
SELECT count(distinct appln_id)  from applt_wo_regpat; -- 25

SELECT count(*) from invt_wo_regpat; -- 5714
SELECT count(distinct appln_id)  from invt_wo_regpat; -- 2271

SELECT count(*) from ipc_wo_regpat; -- 43134
SELECT count(distinct appln_id)  from ipc_wo_regpat; -- 16346

SELECT 		source,methode,count(*)
FROM		applt_adr_ifris_epwofr
WHERE 		source!=''
GROUP BY	source,methode;
-- +--------+---------+----------+
-- | source | methode | count(*) |
-- +--------+---------+----------+
-- | INPI   |         |     6197 |
-- | INPI   | MET1    |     3343 |
-- | INPI   | MET3    |       31 |
-- | INPI   | MET4    |       12 |
-- | REGPAT | MET1    |  3120578 |
-- | REGPAT | MET2    |    67356 |
-- | REGPAT | MET3    |    74052 |
-- +--------+---------+----------+

SELECT          source,count(*)
FROM            applt_adr_ifris_epwofr
WHERE           source!=''
AND		        person_address=''
GROUP BY        source;
-- +--------+----------+
-- | source | count(*) |
-- +--------+----------+
-- | INPI   |     3756 |
-- | REGPAT |  3261986 |
-- +--------+----------+

SELECT          source,count(*)
FROM            applt_adr_ifris_epwofr
WHERE           source!=''
AND             person_ctry_code !=''
AND		        person_ctry_code!=ctry_comp
GROUP BY        source;
-- +--------+----------+
-- | source | count(*) |
-- +--------+----------+
-- | INPI   |        2 |
-- | REGPAT |    28845 |
-- +--------+----------+

SELECT          source,methode,count(*)
FROM            invt_adr_ifris_epwofr
WHERE           source!=''
GROUP BY        source,methode;
-- +--------+---------+----------+
-- | source | methode | count(*) |
-- +--------+---------+----------+
-- | INPI   |         |    76905 |
-- | INPI   | MET2    |    14460 |
-- | INPI   | MET3    |       78 |
-- | REGPAT | MET1    |  9123851 |
-- | REGPAT | MET2    |    18317 |
-- | REGPAT | MET3    |   391680 |
-- +--------+---------+----------+

SELECT          source,count(*)
FROM            invt_adr_ifris_epwofr
WHERE           source!=''
AND             person_address=''
GROUP BY        source;
-- +--------+----------+
-- | source | count(*) |
-- +--------+----------+
-- | INPI   |    21764 |
-- | REGPAT |  9533848 |
-- +--------+----------+

SELECT          source,count(*)
FROM            invt_adr_ifris_epwofr
WHERE           source!=''
AND             person_ctry_code !=''
AND             person_ctry_code!=ctry_comp
GROUP BY        source;
-- +--------+----------+
-- | source | count(*) |
-- +--------+----------+
-- | INPI   |        8 |
-- | REGPAT |    84093 |
-- +--------+----------+
