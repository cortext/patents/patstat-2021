-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Get FR addresses from INPI if not in PATSTAT
USE patstat2021_staging;

-- ---------------------------------------------------------------------------------
-- FR inventors
DROP TABLE IF EXISTS invt_adr_ifris_epwofr;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE invt_adr_ifris_epwofr (
    appln_id int(10) NOT NULL DEFAULT '0',
    person_id int(10) NOT NULL DEFAULT '0',
    doc_std_name_id int(10) NOT NULL DEFAULT '0',
    person_name varchar(300) NOT NULL,
    person_address varchar(500) NOT NULL,
    person_ctry_code varchar(3) NOT NULL DEFAULT '',
    source varchar(7) NULL,
    qualif_comp varchar(4) NULL,
    name_comp varchar(300) NULL,
    adr_comp varchar(500) NULL,
    street_comp varchar(400) NULL,
    zip_code_comp varchar(10) NULL,
    city_comp varchar(50) NULL,
    ctry_comp varchar(2) NULL,
    methode varchar(4) NULL,
    KEY idx_appln_id (appln_id),
    KEY idx_source (source)
);
-- Query OK, 0 rows affected (0.01 sec)

-- ---------------------------------------------------------------------------------
-- Insert REGPAT names and addresses previously obtained
INSERT INTO
    invt_adr_ifris_epwofr
SELECT
    appln_id,
    person_id,
    doc_std_name_id,
    person_name,
    person_address,
    person_ctry_code,
    'REGPAT',
    '',
    name_regpat,
    adr_regpat,
    '',
    '',
    '',
    ctry_regpat,
    methode
FROM
    invt_adr_ifris_epwo
WHERE
    name_regpat != '';
-- Query OK, 9533848 rows affected (2 min 39.43 sec)
-- Records: 9533848  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Insert the rest of the lines
INSERT INTO
    invt_adr_ifris_epwofr
SELECT
    appln_id,
    person_id,
    doc_std_name_id,
    person_name,
    person_address,
    person_ctry_code,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
FROM
    invt_adr_ifris_epwo
WHERE
    name_regpat = '';
-- Query OK, 190942953 rows affected (1 hour 29 min 56.34 sec)
-- Records: 190942953  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Addition of missing FR rows as they were built after REGPAT addresses processing 
INSERT INTO
    invt_adr_ifris_epwofr
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    0,
    a.name_inpi,
    concat(
        a.street_inpi,
        " ; ",
        a.zip_code_inpi,
        " ; ",
        a.city_inpi
    ),
    a.ctry_inpi,
    'INPI',
    a.qualification_inpi,
    a.name_inpi,
    concat(
        a.street_inpi,
        " ; ",
        a.zip_code_inpi,
        " ; ",
        a.city_inpi
    ),
    a.street_inpi,
    a.zip_code_inpi,
    a.city_inpi,
    a.ctry_inpi,
    ''
FROM
    invt_fr_inpi a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 91439 rows affected, 36081 warnings (9.08 sec)
-- Records: 91439  Duplicates: 0  Warnings: 36081

ALTER TABLE
    invt_adr_ifris_epwofr
ADD
    PRIMARY KEY PK_invt_fr (appln_id, person_id);
-- Query OK, 200568240 rows affected (38 min 58.85 sec)
-- Records: 200568240  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 1
-- MET1: where person_name and orgname match strictly
UPDATE
    invt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.inventors b ON (
        a.appln_id = b.appln_nr
        AND a.person_name = concat(b.last_name, ' ', b.first_name)
    )
SET
    a.source = 'INPI',
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = CONCAT(b.last_name, ", ", b.first_name),
    a.adr_comp = CONCAT(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET1'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 0 rows affected (14 min 29.59 sec)
-- Rows matched: 0  Changed: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 2
-- MET2: PATSTAT name like INPI name
UPDATE
    invt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.inventors b ON (
        a.methode = ''
        AND a.appln_id = b.appln_nr
        AND a.person_name LIKE concat('%', b.last_name, '%', b.first_name, '%')
    )
SET
    a.source = 'INPI',
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = concat(b.last_name, ", ", b.first_name),
    a.adr_comp = concat(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET2'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 14460 rows affected (15.08 sec)
-- Rows matched: 14460  Changed: 14460 Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 3
-- MET3: INPI name like PATSTAT name 
UPDATE
    invt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.inventors b ON (
        a.methode = ''
        AND a.appln_id = b.appln_nr
        AND concat(b.last_name, ' ', b.first_name) LIKE concat('%', a.person_name, '%')
    )
SET
    a.source = 'INPI',
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = concat(b.last_name, ", ", b.first_name),
    a.adr_comp = concat(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET3'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 78 rows affected (8 min 17.82 sec)
-- Rows matched: 78  Changed: 78  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 4
-- MET4: PATSTAT name like INPI name 
UPDATE
    invt_adr_ifris_epwofr a
    INNER JOIN inpi2017_2020.inventors b ON (
        a.methode = ''
        AND a.appln_id = b.appln_nr
        AND a.person_name LIKE concat('%', b.last_name, '%,%', b.first_name, '%')
    )
SET
    a.source = 'INPI',
    a.qualif_comp = '', -- b.qualification,
    a.name_comp = concat(b.last_name, ", ", b.first_name),
    a.adr_comp = concat(b.address, ' ; ', b.postcode, ' ; ', b.city),
    a.street_comp = b.address,
    a.zip_code_comp = b.postcode,
    a.city_comp = b.city,
    a.ctry_comp = b.country,
    a.methode = 'MET4'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 0 rows affected (13.72 sec)
-- Rows matched: 0  Changed: 0  Warnings: 0
