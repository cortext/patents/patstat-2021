-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Get missing items due to INPI matching
USE patstat2021_staging;

-- ---------------------------------------------------------------------------------
-- Recovery of FR applicants
CREATE INDEX idx_appln_id ON applt_comp_ifris_epwo (appln_id);
-- Query OK, 107459075 rows affected (9 min 20.92 sec)
-- Records: 107459075  Duplicates: 0  Warnings: 0

SELECT
    @compteur := 80000000;
-- 1 row in set (0.00 sec)

DROP TABLE IF EXISTS applt_fr_inpi;
-- Query OK, 0 rows affected, 1 warning (0.01 sec)

CREATE TABLE applt_fr_inpi AS
SELECT
    DISTINCT a.appln_nr AS appln_id,
    @compteur := @compteur + 1 AS person_id,
    IFNULL(a.orgname, CONCAT(a.last_name, ", ", a.first_name)) AS name_inpi,
    '' AS pm_pp_inpi,
    '' AS qualification_inpi,
    a.address AS street_inpi,
    a.city AS city_inpi,
    a.postcode AS zip_code_inpi,
    a.country AS ctry_inpi
FROM
    inpi2017_2020.applicants a
WHERE
    NOT EXISTS (
        SELECT
            NULL
        FROM
            applt_comp_ifris_epwo b
        WHERE
            a.appln_nr = b.appln_id
    )
    AND a.appln_nr IS NOT NULL;
-- Query OK, 50353 rows affected (10.99 sec)
-- Records: 50353  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id ON applt_fr_inpi (appln_id);
-- Query OK, 50353 rows affected (0.12 sec)
-- Records: 50353  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS applt_comp_ifris_epwofr;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE applt_comp_ifris_epwofr AS
SELECT
    a.*,
    '' AS pm_pp_inpi,
    '' AS qualification_inpi
FROM
    applt_comp_ifris_epwo a;
-- Query OK, 107459075 rows affected (2 min 19.20 sec)
-- Records: 107459075  Duplicates: 0  Warnings: 0

ALTER TABLE applt_comp_ifris_epwofr
ADD street_inpi VARCHAR(400),
ADD city_inpi VARCHAR(100),
ADD zip_code_inpi VARCHAR(10);
-- Query OK, 107459075 rows affected (1 min 21.67 sec)
-- Records: 107459075  Duplicates: 0  Warnings: 0

INSERT INTO
    applt_comp_ifris_epwofr
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    a.name_inpi,
    concat(
        a.street_inpi,
        " ; ",
        a.zip_code_inpi,
        " ; ",
        a.city_inpi
    ),
    a.ctry_inpi,
    0,
    '',
    0,
    0,
    a.pm_pp_inpi,
    a.qualification_inpi,
    a.street_inpi,
    a.city_inpi,
    a.zip_code_inpi
FROM
    applt_fr_inpi a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 9536 rows affected (0.33 sec)
-- Records: 9536  Duplicates: 0  Warnings: 0

ALTER TABLE
    applt_comp_ifris_epwofr
ADD
    PRIMARY KEY PK_frapp (appln_id, person_id);
-- Query OK, 107468611 rows affected (7 min 11.54 sec)
-- Records: 107468611  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Recovery of FR inventors
CREATE INDEX idx_appln_id ON invt_comp_ifris_epwo (appln_id);
-- Query OK, 200476801 rows affected (25 min 47.70 sec)
-- Records: 200476801  Duplicates: 0  Warnings: 0

SELECT
    @compteur := 90000000;
-- 1 row in set (0.00 sec)

DROP TABLE IF EXISTS invt_fr_inpi;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE invt_fr_inpi AS
SELECT
    DISTINCT a.appln_nr AS appln_id,
    @compteur := @compteur + 1 AS person_id,
    CONCAT(a.last_name, ", ", a.first_name) AS name_inpi,
    '' AS qualification_inpi,
    a.address AS street_inpi,
    a.city AS city_inpi,
    a.postcode AS zip_code_inpi,
    a.country AS ctry_inpi
FROM
    inpi2017_2020.inventors a
WHERE
    NOT EXISTS (
        SELECT
            NULL
        FROM
            invt_comp_ifris_epwo b
        WHERE
            a.appln_nr = b.appln_id
    )
    AND a.appln_nr IS NOT NULL;
-- Query OK, 170021 rows affected (6.00 sec)
-- Records: 170021  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id ON invt_fr_inpi (appln_id);
-- Query OK, 170021 rows affected (0.31 sec)
-- Records: 170021  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS invt_comp_ifris_epwofr;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE invt_comp_ifris_epwofr AS
SELECT
    a.*,
    '' AS qualification_inpi
FROM
    invt_comp_ifris_epwo a;
-- Query OK, 200476801 rows affected (3 min 40.88 sec)
-- Records: 200476801  Duplicates: 0  Warnings: 0

ALTER TABLE invt_comp_ifris_epwofr
ADD street_inpi VARCHAR(400),
ADD city_inpi VARCHAR(100),
ADD zip_code_inpi VARCHAR(10);
-- Query OK, 200476801 rows affected (2 min 8.65 sec)
-- Records: 200476801  Duplicates: 0  Warnings: 0

INSERT INTO
    invt_comp_ifris_epwofr
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    a.name_inpi,
    concat(
        a.street_inpi,
        " ; ",
        a.zip_code_inpi,
        " ; ",
        a.city_inpi
    ),
    a.ctry_inpi,
    0,
    '',
    0,
    0,
    a.qualification_inpi,
    a.street_inpi,
    a.city_inpi,
    a.zip_code_inpi
FROM
    invt_fr_inpi a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 91439 rows affected (3.29 sec)
-- Records: 91439  Duplicates: 0  Warnings: 0

ALTER TABLE
    invt_comp_ifris_epwofr
ADD
    PRIMARY KEY PK_invFR (appln_id, person_id);
-- Query OK, 200568240 rows affected (27 min 50.43 sec)
-- Records: 200568240  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Recovery of FR ipc codes
CREATE INDEX idx_appln_id ON tls209_appln_ipc_ifris_epwo (appln_id);
-- Query OK, 281900810 rows affected (5 min 54.09 sec)
-- Records: 281900810  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS ipc_fr_inpi;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE ipc_fr_inpi AS
SELECT
    DISTINCT a.appln_nr AS appln_id,
    SUBSTRING_INDEX(a.`text`, "      ", 1) AS classement
FROM
    inpi2017_2020.ipcr_classes a
WHERE
    NOT EXISTS (
        SELECT
            NULL
        FROM
            tls209_appln_ipc_ifris_epwo b
        WHERE
            a.appln_nr = b.appln_id
    )
    AND a.appln_nr IS NOT NULL
    AND SUBSTRING_INDEX(a.`text`, "      ", 1) != '';
-- Query OK, 186152 rows affected (3.30 sec)
-- Records: 186152  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS tls209_appln_ipc_ifris_epwofr;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE tls209_appln_ipc_ifris_epwofr AS
SELECT
    *
FROM
    tls209_appln_ipc_ifris_epwo;
-- Query OK, 281900810 rows affected (3 min 45.72 sec)
-- Records: 281900810  Duplicates: 0  Warnings: 0

INSERT INTO
    tls209_appln_ipc_ifris_epwofr
SELECT
    DISTINCT appln_id,
    IF(
        substring(classement, 5, 1) = " ",
        CONCAT(
            Substring(classement, 1, 5),
            IF(
                length(substring(classement, 6, 3)) = 1,
                concat(
                    '  ',
                    substring(classement, 6, 3)
                ),
                IF(
                    length(substring(classement, 6, 3)) = 2,
                    concat(
                        ' ',
                        substring(classement, 6, 3)
                    ),
                    substring(classement, 6, 3)
                )
            ),
            substring(classement, 9, 5)
        ),
        CONCAT(
            Substring(classement, 1, 4),
            " ",
            IF(
                length(substring(classement, 5, 3)) = 1,
                concat(
                    '  ',
                    substring(classement, 5, 3)
                ),
                IF(
                    length(substring(classement, 5, 3)) = 2,
                    concat(
                        ' ',
                        substring(classement, 5, 3)
                    ),
                    substring(classement, 5, 3)
                )
            ),
            substring(classement, 8, 5)
        )
    ),
    'A',
    NULL,
    '',
    '',
    ''
FROM
    ipc_fr_inpi;
-- Query OK, 186151 rows affected (0.54 sec)
-- Records: 186151  Duplicates: 0  Warnings: 0

ALTER TABLE
    tls209_appln_ipc_ifris_epwofr
ADD
    CONSTRAINT PK_ipc_ifris_fr PRIMARY KEY (appln_id, ipc_class_symbol, ipc_class_level);
-- Query OK, 282086961 rows affected (1 hour 18 min 19.56 sec)
-- Records: 282086961  Duplicates: 0  Warnings: 0
