# Treating japanese data

## Translate and transliterate

After [loading the japanese raw data](../../load_clean_reorganize_data/load_japanese_data/), the names (companies and individuals) and addresses will be translated to make it more accurate to geocode and to get further valuable information.

### Translation to english
A script was built using Google Translator via [deep-translator](https://github.com/nidhaloff/deep-translator).

### Transliteration
Having the [romanization of the japanese characters](https://en.wikipedia.org/wiki/Romanization_of_Japanese) **could** help to allocate the addresses more than if we translate them directly to english.

Some python libraries were tried to do so:
- [posuto](https://github.com/polm/posuto) is focused on postal codes
- [jntajis](https://pypi.org/project/jntajis-python/) transliteration
- *[pykakasi](https://pypi.org/project/pykakasi/) transliteration
- *[cutlet](https://github.com/polm/cutlet)
- [kuroshiro](https://github.com/hexenq/kuroshiro)
- [kana](https://github.com/gojp/kana)

**\*** The ones actually used to test, the others don’t work as expected for this specific case.
Finally, **cutlet** is used as it showed better results when testing some samples.

## [Geocoding](addresses/README.md#geocoding)

1. Based on the address value, for inventors and applicants.
2. Based on the name value, only for applicants (legal person).