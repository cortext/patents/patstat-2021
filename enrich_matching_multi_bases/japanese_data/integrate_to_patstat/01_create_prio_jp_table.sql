-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 

USE patstat2021_staging;

DROP TABLE IF EXISTS jp_priority;
-- Query OK, 0 rows affected, 1 warning (0.02 sec)

CREATE TABLE jp_priority AS
SELECT
    concat(a.appln_auth, a.appln_nr, a.appln_kind) AS key_appln,
    a.*
FROM
    tls201_appln a
WHERE
    a.appln_auth = 'JP'
    AND a.appln_first_priority_year = 0
    AND a.appln_filing_year >= '2000';
-- Query OK, 6892361 rows affected (11 min 53.20 sec)
-- Records: 6892361  Duplicates: 0  Warnings: 0

ALTER TABLE
    jp_priority
ADD
    PRIMARY KEY (key_appln);
-- Query OK, 6892361 rows affected (3 min 52.90 sec)
-- Records: 6892361  Duplicates: 0  Warnings: 0

CREATE UNIQUE INDEX idx_appln_id ON jp_priority (appln_id);
-- Query OK, 6892361 rows affected (4 min 11.18 sec)
-- Records: 6892361  Duplicates: 0  Warnings: 0
