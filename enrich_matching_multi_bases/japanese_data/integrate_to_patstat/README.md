# Integrate JP data into PATSTAT

[01_create_prio_jp_table.sql](01_create_prio_jp_table.sql)

A table is created to store the priority japanese patent applications.

[02_japanese_applicants.sql](02_japanese_applicants.sql) and [03_japanese_inventors.sql](03_japanese_inventors.sql)

Some of the fields from the japanese [names](../names/README.md) and [addresses](../addresses/README.md) data previously treated are stored in a new table.

[04_consolidate_jp_data.sql](04_consolidate_jp_data.sql)

The japanese data is consolidated and stored along with the data already obtained from EP, WO and FR offices.