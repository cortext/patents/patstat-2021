-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 

USE patstat2021_staging;

DROP TABLE IF EXISTS jp_inventors;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE jp_inventors AS
SELECT
    DISTINCT appln_id,
    seq,
    name_en,
    address_en,
    label AS address_label,
    city
FROM
    patstat2021_lab.JPnew_applicants_enriched;
-- Query OK, 985169 rows affected (33.52 sec)
-- Records: 985169  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id ON jp_inventors (appln_id);
-- Query OK, 985169 rows affected (1.87 sec)
-- Records: 985169  Duplicates: 0  Warnings: 0

ALTER TABLE
    jp_inventors
ADD
    PRIMARY KEY (appln_id, seq);
-- Query OK, 985169 rows affected (6.54 sec)
-- Records: 985169  Duplicates: 0  Warnings: 0
