-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
USE patstat2021_staging;

DROP TABLE IF EXISTS applt_adr_ifris_epwofrjp;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE `applt_adr_ifris_epwofrjp` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `pm_pp_comp` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `idx_appln_id` (`appln_id`),
    KEY `idx_source` (`source`),
    KEY `idx_methode` (`methode`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Query OK, 0 rows affected (0.01 sec)

DROP TABLE IF EXISTS invt_adr_ifris_epwofrjp;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE `invt_adr_ifris_epwofrjp` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `person_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `person_ctry_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `source` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
    `qualif_comp` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_comp` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_comp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `street_comp` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
    `zip_code_comp` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
    `city_comp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ctry_comp` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `methode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `idx_appln_id` (`appln_id`),
    KEY `idx_source` (`source`),
    KEY `idx_methode` (`methode`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Query OK, 0 rows affected (0.01 sec)

INSERT INTO
    applt_adr_ifris_epwofrjp
SELECT
    *
FROM
    applt_adr_ifris_epwofr;
-- Query OK, 107468611 rows affected, 1290 warnings (17 min 2.86 sec)
-- Records: 107468611  Duplicates: 0  Warnings: 1290

INSERT INTO
    invt_adr_ifris_epwofrjp
SELECT
    *
FROM
    invt_adr_ifris_epwofr;
-- Query OK, 200568240 rows affected, 5864 warnings (42 min 11.48 sec)
-- Records: 200568240  Duplicates: 0  Warnings: 5864

SELECT
    @compteur := (
        SELECT
            max(person_id)
        FROM
            invt_adr_ifris_epwofrjp
    );
-- 1 row in set (1 min 1.82 sec)

INSERT INTO
    invt_adr_ifris_epwofrjp
SELECT
    appln_id,
    @compteur := @compteur + 1 AS person_id,
    0,
    '',
    '',
    '',
    'JP',
    seq,
    name_en,
    address_en,
    '',
    '',
    '',
    'JP',
    ''
FROM
    jp_inventors
WHERE
    appln_id NOT IN (
        SELECT
            appln_id
        FROM
            invt_adr_ifris_epwofrjp
    );
-- Query OK, 186893 rows affected (19.08 sec)
-- Records: 186893  Duplicates: 0  Warnings: 0

SELECT
    @compteur := (
        SELECT
            max(person_id)
        FROM
            applt_adr_ifris_epwofrjp
    );
-- 1 row in set (33.56 sec)

INSERT INTO
    applt_adr_ifris_epwofrjp
SELECT
    appln_id,
    @compteur := @compteur + 1 AS person_id,
    0,
    '',
    '',
    '',
    'JP',
    '',
    seq,
    name_en,
    address_en,
    '',
    '',
    '',
    'JP',
    ''
FROM
    jp_applicants
WHERE
    appln_id NOT IN (
        SELECT
            appln_id
        FROM
            applt_adr_ifris_epwofrjp
    );
-- Query OK, 186893 rows affected (15.20 sec)
-- Records: 186893  Duplicates: 0  Warnings: 0

UPDATE
    applt_adr_ifris_epwofrjp a,
    jp_applicants b,
    tls207_pers_appln c
SET
    a.name_comp = a.person_name,
    a.adr_comp = b.address_en,
    a.ctry_comp = 'JP',
    a.source = 'JP'
WHERE
    a.appln_id = c.appln_id
    AND a.person_id = c.person_id
    AND a.appln_id = b.appln_id
    AND b.seq = c.applt_seq_nr;
-- Query OK, 788979 rows affected (11 min 41.16 sec)
-- Rows matched: 788979  Changed: 788979  Warnings: 0

UPDATE
    invt_adr_ifris_epwofrjp a,
    jp_inventors b,
    tls207_pers_appln c
SET
    a.name_comp = a.person_name,
    a.adr_comp = b.address_en,
    a.ctry_comp = 'JP',
    a.source = 'JP'
WHERE
    a.appln_id = c.appln_id
    AND a.person_id = c.person_id
    AND a.appln_id = b.appln_id
    AND b.seq = c.invt_seq_nr;
-- Query OK, 786458 rows affected (4 min 49.34 sec)
-- Rows matched: 786458  Changed: 786458  Warnings: 0
