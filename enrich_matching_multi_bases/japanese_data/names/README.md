# Steps to treat the applicant and inventor name values

1. [01_create_table_transl.sql](01_create_table_transl.sql): Create a table that will store the distinct names from the tables `JPnew_applicant_ad8_tc5` and `JPnew_inventor_ad8_tc5`.
    ```sh
    mysql --defaults-file=~/mycnf.cnf -vvv < 01_create_table_transl.sql > 01_create_table_transl_report.txt 2>&1
    ```
    Distinct japanese names: 791.013

2. To translate and transliterate the name values. The [same steps](../addresses/README.md#steps-to-treat-the-address-values) used to treat the addresses were performed (changing 'address' by 'name' when referencing the database fields).

    ## Test quality of translated and transliterated names
    After analyzing the results, we see that the ones translated to english are better, these are some examples:

    | english translation                                     | romaji transliteration                                    |
    |---------------------------------------------------------|-----------------------------------------------------------|
    | World View Satellites Limited                           | World byuu/sateraitsu/rimiteddo                           |
    | PepsiCo Inc.                                            | Pepsico/ink                                               |
    | Edgewell Personal Care Brands Limited Liability Company | Edge well personal care buranzu limited liability company |

3. Add two columns to store the number of applicants and inventors respectively for each address.
   ```sh
    mysql --defaults-file=~/mycnf.cnf -vvv < 04_count_nb_applt_invt.sql > 04_count_nb_applt_invt_report.txt 2>&1
    ```

After the previous steps, the names in english are ready to be used.

## Note
After [geocoding addresses](../addresses/README.md#geocoding) for the japanese applicants and inventors, some of them did not throw any result. So an [additional geocoding process](../addresses/README.md#geocoding) was done based on the name, only for the applicants names (legal person).