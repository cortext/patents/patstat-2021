USE patstat2021_lab;

-- Create table to store all the distinct names
CREATE TABLE `JPnew_distinct_names` (
  `name` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(400) CHARACTER SET utf8 NOT NULL,
  `name_ro` varchar(400) CHARACTER SET utf8 NOT NULL,
  UNIQUE KEY `idx_name` (`name`(200)) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE JPnew_distinct_names
SELECT name, '' , '' 
FROM JPnew_applicant_ad8_tc5;
-- Query OK, 37159 rows affected, 65535 warnings (13,80 sec)
-- Records: 985169  Duplicates: 948010  Warnings: 338939

INSERT IGNORE JPnew_distinct_names
SELECT name, '' , '' 
FROM JPnew_inventor_ad8_tc5;
-- Query OK, 710529 rows affected (48,57 sec)
-- Records: 2352536  Duplicates: 1642007  Warnings: 0