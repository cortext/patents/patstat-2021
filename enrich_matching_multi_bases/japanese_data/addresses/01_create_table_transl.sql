USE patstat2021_lab;

-- Create table to store all the distinct addresses
CREATE TABLE `JPnew_distinct_address` (
  `address` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_en` varchar(400) CHARACTER SET utf8 NOT NULL,
  `address_ro` varchar(400) CHARACTER SET utf8 NOT NULL,
  UNIQUE KEY `idx_address` (`address`(200)) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT IGNORE JPnew_distinct_address
SELECT address, '' , '' 
FROM JPnew_applicant_ad8_tc5;
-- Query OK, 7719 rows affected, 65535 warnings (22.54 sec)
-- Records: 985169  Duplicates: 977450  Warnings: 338939

INSERT IGNORE JPnew_distinct_address
SELECT address, '' , '' 
FROM JPnew_inventor_ad8_tc5;
-- Query OK, 19290 rows affected, 65535 warnings (25.56 sec)
-- Records: 2352536  Duplicates: 2333246  Warnings: 1628965
