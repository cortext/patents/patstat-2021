#!/usr/bin/env python3

import time
from datetime import datetime
import pymysql
import pandas as pd
import cutlet
from sshtunnel import SSHTunnelForwarder
import getpass
import myconfig

ssh_host = myconfig.ssh_host
ssh_username = myconfig.ssh_username
database_name = myconfig.database_name
database_port = myconfig.database_port
localhost = myconfig.localhost
database_username = myconfig.database_username
table = myconfig.table
size_df = myconfig.size_df


def jap_to_romaji(text):
    try:
        katsu = cutlet.Cutlet()
        return katsu.romaji(text).replace('"', "")
    except Exception:
        print(f"Exception, row not updated. Value: {text}")
        return ''

def open_ssh_tunnel(verbose=False):
    """Open an SSH tunnel and connect using a username and password.
    
    :param verbose: Set to True to show logging
    :return tunnel: Global SSH tunnel connection
    """

    global tunnel
    tunnel = SSHTunnelForwarder(
        (ssh_host, 22),
        ssh_username = ssh_username,
        ssh_password = ssh_password,
        remote_bind_address = (ssh_host, database_port)
    )
    
    tunnel.start()

def mysql_connect():
    """Connect to a MySQL server using the SSH tunnel connection
    
    :return connection: Global MySQL database connection
    """
    
    global connection
    
    connection = pymysql.connect(
        host=localhost,
        user=database_username,
        passwd=database_password,
        db=database_name,
        port=tunnel.local_bind_port
    )

def run_select_query(sql):
    """Runs a given SQL query via the global database connection.
    
    :param sql: MySQL query
    :return: Pandas dataframe containing results
    """
    
    return pd.read_sql_query(sql, connection)

def run_tr_query(sql):
  cursor = connection.cursor()
  cursor.execute(sql)
  connection.commit()

try:
    ssh_password = getpass.getpass(prompt=f'password ssh user {ssh_username}: ')
    database_password = getpass.getpass(prompt=f'password db user {database_username}: ')
except Exception as error:
    print('ERROR', error)

print('=========================================')
open_ssh_tunnel()
mysql_connect()
print ("Database connected.")
start_time = time.time()
print('Start time: ', datetime.fromtimestamp(start_time).strftime("%A, %B %d, %Y %I:%M:%S"))
schema = database_name

df = run_select_query(f'''
    SELECT COUNT(*) AS c 
    FROM {schema}.{table} 
    WHERE 
        ( 
            address_ro IS NULL OR address_ro = ''
        )
        AND ( 
            address IS NOT NULL AND address != ''
        );
''')

count = df['c'][0]
print(f'count: {count}')
if count == 0: exit()

for i in range(0, count, size_df):

    query1 = f'''
        SELECT address 
        FROM {schema}.{table}
        WHERE 
            ( 
                address_ro IS NULL OR address_ro = ''
            )
            AND ( 
                address IS NOT NULL AND address != ''
            )
        LIMIT {size_df};
    '''

    df = run_select_query(query1)
    df_count = len(df.index)
    df['address_ro'] = df.apply(lambda row: jap_to_romaji(row.address) if row.address else '', axis=1)

    for row in df.itertuples():
        query2 = f'''
            UPDATE {schema}.{table}
            SET
                address_ro = "{row.address_ro}"
            WHERE address = "{row.address}";
        '''
        tr = run_tr_query(query2)
    
    print(f'rows left: {count - i - size_df}')
    if df_count < size_df:
        print("Finished!")
        break
    print("--- %s seconds ---" % (time.time() - start_time))
