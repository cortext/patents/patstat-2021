USE patstat2021_lab;

-- Create a table to store the original and translated address value, 
-- with the geocoding results and the count of applicants and inventors per address
-- Then insert first the distinct addresses as follows:
INSERT INTO JPnew_distinct_addresses_geocoded
(address, address_en, label, longitude, latitude, confidence, accuracy, layer, city, region, country, iso2, iso3, nb_applt, nb_invt)
SELECT A.address, A.address_en, '', '', '', '', '', '', '', '', '', '', '',  A.nb_applt, A.nb_invt 
FROM JPnew_distinct_address A;
-- Query OK, 27009 rows affected (0.06 sec)
-- Records: 27009  Duplicates: 0  Warnings: 0

CREATE INDEX idx_address_en USING BTREE ON JPnew_distinct_addresses_geocoded (address_en (200));
CREATE INDEX idx_address USING BTREE ON JPnew_distinct_addresses_geocoded (address (200));

-- Update based on the geocoding results
UPDATE JPnew_distinct_addresses_geocoded A
INNER JOIN JP_address_en_geocoded B 
ON A.address_en = B.address
SET
A.label = B.label, 
A.longitude = B.longitude, 
A.latitude = B.latitude, 
A.confidence = B.confidence, 
A.accuracy = B.accuracy, 
A.layer = B.layer, 
A.city = B.city, 
A.region = B.region, 
A.country = B.country, 
A.iso2 = B.iso2, 
A.iso3 = B.iso3;

---------------------------------------------------------
-- Create a table to store the japanese data that will be actually used, related to the applicants
CREATE TABLE `JPnew_applicants_enriched` (
  `appln_id` int(11) DEFAULT NULL,
  `appln_nr` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_filing_date` date DEFAULT NULL,
  `appln_filing_year` smallint(6) DEFAULT NULL,
  `seq` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_en` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` text COLLATE utf8_unicode_ci,
  `longitude` text COLLATE utf8_unicode_ci,
  `latitude` text COLLATE utf8_unicode_ci,
  `confidence` text COLLATE utf8_unicode_ci,
  `accuracy` text COLLATE utf8_unicode_ci,
  `layer` text COLLATE utf8_unicode_ci,
  `city` text COLLATE utf8_unicode_ci,
  `region` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `iso2` text COLLATE utf8_unicode_ci,
  `iso3` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `idx_address_en` (`address_en`(200)) USING BTREE,
  KEY `idx_address` (`address`(200)) USING BTREE,
  KEY `idx_name_en` (`name_en`(200)) USING BTREE,
  KEY `idx_name` (`name`(200)) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 1. Insert the distinct addresses data from the original source
INSERT INTO JPnew_applicants_enriched
(appln_id, appln_nr, appln_filing_date, appln_filing_year, seq, name, name_en, address, address_en, label, longitude, latitude, confidence, accuracy, layer, city, region, country, iso2, iso3)
SELECT appln_id, appln_nr, appln_filing_date, appln_filing_year, seq, name, '', address, '', '', '', '', '', '', '', '', '', '', '', ''
FROM JPnew_applicant_ad8_tc5;
-- Query OK, 985169 rows affected (18.25 sec)
-- Records: 985169  Duplicates: 0  Warnings: 0

-- 2. Add the english name value
UPDATE JPnew_applicants_enriched A
    INNER JOIN
        JPnew_distinct_names B
    ON A.name = B.name
SET
A.name_en = B.name_en;
-- Query OK, 985169 rows affected (4 min 25.89 sec)
-- Rows matched: 985169  Changed: 985169  Warnings: 0

-- 3. Add the english address value and the geocoding result values.
UPDATE JPnew_applicants_enriched A
    INNER JOIN
        JPnew_distinct_addresses_geocoded B
    ON A.address = B.address
SET
A.address_en = B.address_en, 
A.label = B.label, 
A.longitude = B.longitude, 
A.latitude = B.latitude, 
A.confidence = B.confidence, 
A.accuracy = B.accuracy, 
A.layer = B.layer, 
A.city = B.city, 
A.region = B.region, 
A.country = B.country, 
A.iso2 = B.iso2, 
A.iso3 = B.iso3;
-- Query OK, 985169 rows affected (6 min 36.98 sec)
-- Rows matched: 985169  Changed: 985169  Warnings: 0

---------------------------------------------------------
-- Create a table to store the japanese data that will be actually used, related to the inventors
CREATE TABLE `JPnew_inventors_enriched` (
  `appln_id` int(11) DEFAULT NULL,
  `appln_nr` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_filing_date` date DEFAULT NULL,
  `appln_filing_year` smallint(6) DEFAULT NULL,
  `seq` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_en` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` text COLLATE utf8_unicode_ci,
  `longitude` text COLLATE utf8_unicode_ci,
  `latitude` text COLLATE utf8_unicode_ci,
  `confidence` text COLLATE utf8_unicode_ci,
  `accuracy` text COLLATE utf8_unicode_ci,
  `layer` text COLLATE utf8_unicode_ci,
  `city` text COLLATE utf8_unicode_ci,
  `region` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `iso2` text COLLATE utf8_unicode_ci,
  `iso3` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `idx_address_en` (`address_en`(200)) USING BTREE,
  KEY `idx_address` (`address`(200)) USING BTREE,
  KEY `idx_name_en` (`name_en`(200)) USING BTREE,
  KEY `idx_name` (`name`(200)) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 1. Insert the distinct addresses data from the original source
INSERT INTO JPnew_inventors_enriched
(appln_id, appln_nr, appln_filing_date, appln_filing_year, seq, name, name_en, address, address_en, label, longitude, latitude, confidence, accuracy, layer, city, region, country, iso2, iso3)
SELECT appln_id, appln_nr, appln_filing_date, appln_filing_year, seq, name, '', address, '', '', '', '', '', '', '', '', '', '', '', ''
FROM JPnew_inventor_ad8_tc5;
-- Query OK, 2352536 rows affected (41.06 sec)
-- Records: 2352536  Duplicates: 0  Warnings: 0

-- 2. Add the english name value
UPDATE JPnew_inventors_enriched A
    INNER JOIN
        JPnew_distinct_names B
    ON A.name = B.name
SET
A.name_en = B.name_en;
-- Query OK, 2352536 rows affected (4 min 58.09 sec)
-- Rows matched: 2352536  Changed: 2352536  Warnings: 0

-- 3. Add the english address value and the geocoding result values.
UPDATE JPnew_inventors_enriched A
    INNER JOIN
        JPnew_distinct_addresses_geocoded B
    ON A.address = B.address
SET
A.address_en = B.address_en, 
A.label = B.label, 
A.longitude = B.longitude, 
A.latitude = B.latitude, 
A.confidence = B.confidence, 
A.accuracy = B.accuracy, 
A.layer = B.layer, 
A.city = B.city, 
A.region = B.region, 
A.country = B.country, 
A.iso2 = B.iso2, 
A.iso3 = B.iso3;
-- Query OK, 2352536 rows affected (17 min 50.41 sec)
-- Rows matched: 2352536  Changed: 2352536  Warnings: 0
