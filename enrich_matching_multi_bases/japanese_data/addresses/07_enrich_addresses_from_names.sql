USE patstat2021_lab;

-- JP_applicant_names_geocoded: Table storing:
-- - the names in english from the applicants whose address could not be geolocated
-- (with the parameters chosen). 
-- - and the geolocation values taken from geocoding by the name value.

UPDATE JP_applicant_names_geocoded a
SET a.nb_applt = (
			SELECT count(*)
			FROM JPnew_applicants_enriched b
			WHERE a.name_en = b.name_en
			GROUP BY name_en
			)
;

---------------------------------------------------------
-- Update the applicants table with the new geolocation values obtained from the name
UPDATE JPnew_applicants_enriched A
INNER JOIN 
    JP_applicant_names_geocoded B
ON A.name_en = B.name_en
SET
    A.label = B.label,
    A.longitude = B.longitude,
    A.latitude = B.latitude,
    A.confidence = B.confidence,
    A.accuracy = B.accuracy,
    A.layer = B.layer,
    A.city = B.city,
    A.region = B.region,
    A.country = B.country,
    A.iso2 = B.iso2,
    A.iso3 = B.iso3
WHERE A.label = '';
-- Query OK, 159994 rows affected (45.28 sec)
-- Rows matched: 164039  Changed: 159994  Warnings: 0
