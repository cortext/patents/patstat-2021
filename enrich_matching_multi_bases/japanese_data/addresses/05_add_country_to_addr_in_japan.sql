
USE patstat2021_lab;

-- Add field to make difference between addresses in japan and not in japan
ALTER TABLE JPnew_distinct_address ADD in_japan int(1) DEFAULT 1 NOT NULL;

-- List manually checking the addresses with higher number of applicants and inventors
UPDATE JPnew_distinct_address 
SET in_japan = 0
WHERE address_en REGEXP "France|South Korea|United States of America|The Federal Republic of Germany|People's Republic of China|United States of America|South Korea|The Federal Republic of Germany|People's Republic of China|Finland|UK|Spain|Australia|Switzerland|British Cayman Islands|India|Denmark|new Zealand|Singapore|Sweden|Netherlands|British territory, Cayman Islands|Ireland|Luxembourg|Israel|Austria|Republic of Finland|Finland|Hong Kong|Republic of Korea|Italy|Ireland|Luxembourg|Austria|Taiwan|United Kingdom|England|Canada|Sweden|Belgium|Denmark|UK|Russia|Hungary";
-- 4.463

-- List of countries based on the tls801_country table
UPDATE JPnew_distinct_address 
SET in_japan = 0
WHERE address_en REGEXP 
"Andorra|United Arab Emirates|Afghanistan|Antigua and Barbados|Anguilla|Albania|Armenia|Netherlands Antilles|Angola|African Regional Intellectual Property Organization (ARIPO)|Argentinia|Austria|Australia|Aruba|Azerbaijan|Bosnia and Herzegovina|Barbados|Bangladesh|Belgium|Burkina Faso|Bulgaria|Bahrain|Burundi|Benin|Bermuda|Brunei Daussalam|Bolivia (Plurinational State of)|Bonaire, Saint Eustatius and Saba|Brazil|Bahamas|Bhutan|Burma|Bouvet Island|Botswana|Benelux Office for Intellectual Property (BOIP)|Belarus|Belize|Canada|Democratic Republic of the Congo|Central African Republic|Congo|Switzerland|Côte d'Ivoire|Cook Islands|Chile|Cameroon|China|Colombia|Costa Rica|Czechoslovakia|Cuba|Cabo Verde|Curaçao|Cyprus|Czechia|German Democratic Republic|Germany|Djibouti|Denmark|German Democratic Republic|Dominica|Dominican Republic|Algeria|Eurasian Patent Organization (EAPO)|Ecuador|Estonia|Egypt|Western Sahara|European Union Intellectual Property Office (EUIPO)|European Patent Office (EPO)|Eritrea|Spain|Ethiopia|European Union|Finland|Fiji|Falkland Islands (Malvinas)|Faroe Islands|France|Gabon|United Kingdom|Patent Office of the Cooperation Council for the Arab States of the Gulf (GCC Patent Office)|Grenada|Georgia|Guernsey|Ghana|Gibraltar|Greenland|Gambia|Guinea|Equatorial Guinea|Greece|South Georgia and the South Sandwich Islands|Guatemala|Guinea-Bissau|Guyana|Hong Kong SAR (China)|Honduras|Croatia|Haiti|Hungary|International Bureau of the World Intellectual Property Organization (WIPO)|Indonesia|Ireland|Israel|Isle of Man|India|Iraq|Iran (Islamic Republic of)|Iceland|Italy|Jersey|Jamaica|Jordan|Kenya|Kyrgyzstan|Cambodia|Kiribati|Comoros|Saint Kitts and Nevis|Democratic People's Republic of Korea|Republic of Korea|Kuwait|Cayman Islands|Kazakhstan|Lao People's Democratic Republic|Lebanon|Saint Lucia|Liechtenstein|Sri Lanka|Liberia|Lesotho|Lithuania|Luxembourg|Latvia|Libya|Morocco|Monaco|Republic of Moldova|Montenegro|Madagascar|North Macedonia|Mali|Myanmar|Mongolia|Macao SAR (China)|Northern Mariana Islands|Mauritania|Montserrat|Malta|Mauritius|Maledives|Malawi|Mexico|Malaysia|Mozambique|Niger|Nigeria|Nicaragua|Netherlands|Norway|Nepal|Nauru|New Zealand|African Intellectual Property Organisation (OAPI)|Oman|Panama|Peru|Papua New Guinea|Philippines|Pakistan|Poland|Portugal|Palau|Paraguay|Qatar|Community Plant Variety Office (EU) (CPVO)|Romania|Serbia|Russian Federation|Rwanda|Saudia Arabia|Solomon Islands|Seychelles|Sudan|Sweden|Singapore|Saint Helena, Ascension and Tristan da Cunha|Slovenia|Slovakia|Sierra Leone|San Marino|Senegal|Somalia|Suriname|South Sudan|Sao Tome and Principe|Soviet Union|El Salvador|Sint Maarten (Dutch part)|Syrian Arab Republic|Eswatini|Turks and Caicos Islands|Chad|Togo|Thailand|Tajikistan|Timor-Leste|Turkmenistan|Tunisia|Tonga|Turkey|Trinidad and Tobago|Tuvalu|Chinese Taipei|United Republic of Tanzania|Ukraine|Uganda|United States of America|Uruguay|Uzbekistan|Holy See|Saint Vincent and the Grenadines|Venezuela (Bolivarian Republic of)|British Virgin Islands|Viet Nam|Vanuatu|World Intellectual Property Organisation (WIPO)|Samoa|Nordic Patent Institute (NPI)|International Union for the Protection of New Varieties of Plants (UPOV)|Visegrad Patent Institute (VPI)|unknown|Democratic Yemen|Yemen|Yugoslavia/Serbia and Montenegro|South Africa|Zambia|Zimbabwe";
-- 1.160

-- List manually checking after the previous update
UPDATE JPnew_distinct_address 
SET in_japan = 0
WHERE address_en REGEXP 
"America|Argentina|Iran|Antigua and Barbuda|United States|USA|Columbia|Great britain|Curacao|Saudi Arabia|Czech|Puerto Rico|Vietnam|Bosnia|Herzegovina|British|English|Korea";
-- 1.189

-- List manually checking after the previous update
UPDATE JPnew_distinct_address 
SET in_japan = 0
WHERE address_en REGEXP 
"Macau|Brunei Darussalam|Cooria";
-- 7

UPDATE JPnew_distinct_address
SET in_japan = 0
WHERE address_en = '';

SELECT count(*) FROM JPnew_distinct_address WHERE in_japan = 0;
-- 5.875

SELECT count(*) FROM JPnew_distinct_address WHERE in_japan = 1;
-- 21.134

-- For the addresses without country, we assume after checking most of them that 
-- they are located in Japan so the country is added.
UPDATE JPnew_distinct_address
SET address_en = CONCAT(TRIM('Japan' FROM address_en), ', Japan')
WHERE in_japan = 1;
-- Query OK, 21134 rows affected (1.23 sec)
-- Rows matched: 21134  Changed: 21134  Warnings: 0
