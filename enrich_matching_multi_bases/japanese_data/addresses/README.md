# Steps to treat the address values

1. [01_create_table_transl.sql](01_create_table_transl.sql): Create a table that will store the distinct addresses from the tables `JPnew_applicant_ad8_tc5` and `JPnew_inventor_ad8_tc5`.
    ```sh
    mysql --defaults-file=~/mycnf.cnf -vvv < 01_create_table_transl.sql > 01_create_table_transl_report.txt 2>&1
    ```
    Distinct japanese addresses: 27.009

2. Install requirements:
    - for the script 02, translation to english.
    ```sh
    pip install -r 02_requirements.txt
    ```
    - for the script 03, transliteration to romaji.
    ```sh
    pip install -r 03_requirements.txt
    ```

3. Translate and transliterate the address values. They can be run simultaneously.
   Make sure first you set the right configuration in the file [myconfig.py](myconfig.py).
   - Translate to english.
    ```sh
    python3 02_translate_jap_to_eng.py
    ```
   - Transliterate the address values to romaji.
    ```sh
    python3 03_transliterate_jap_to_rom.py
    ```

    ## Test quality geocoding the addresses in Cortext Manager
    As part of this step and to validate the addresses translation, we took the: japanese kanji (original), japanese romaji, and the english translation, and tried to geocode them in Cortext manager, 200 random rows were taken for each file and these are some results:
        
    |       | # rows | % of japanese kanji addresses geocoded | % of japanese romaji addresses geocoded | % of translated to english addresses geocoded |
    | ----- | ------ | -------------------------------------- | --------------------------------------- | --------------------------------------------- |
    | file1 | 200    | 79.5                                   | 95                                      | 92.5                                          |
    | file2 | 200    | 79.5                                   | 97                                      | 97.5                                          |
    | file3 | 200    | 81                                     | 91                                      | 90.5                                          |

    The addresses in romaji and english seem to have a similar result, we decide to take the english form as it easier to treat later by us.

    ### Addresses not properly geocoded
    During geocoding, we have noticed that some addresses are not located in Japan, even if they are clearly located there. Reviewing the data, we saw that the rest of the addresses have the country included in the value, contrary to those of Japan, so **step 5** was carried out.


4. Add two columns to store the number of applicants and inventors respectively for each address.
   ```sh
    mysql --defaults-file=~/mycnf.cnf -vvv < 04_count_nb_applt_invt.sql > 04_count_nb_applt_invt_report.txt 2>&1
    ```

5. Add "Japan" as country to the japanese addresses translated to english.
   All the address values have the country included, except for those which are located in Japan. Like this one located in United States:
   - United States, Wisconsin, 53188, Waukesha, North, Grandview, Boulevard, W, 710, 3000
   
   While this other in Japan:
   - 1033-1 Oshitate, Inagi-shi, Tokyo

   So we have added "Japan" to the mentioned addresses, running this sql file:
   ```sh
    mysql --defaults-file=~/mycnf.cnf -vvv < 05_add_country_to_addr_in_japan.sql > 05_add_country_to_addr_in_japan_report.txt 2>&1
    ```

After the previous steps, the addresses in english are ready to be used.

## Geocoding

1. Based on the address value
    [06_create_final_enriched_tables.sql](06_create_final_enriched_tables.sql): file with the queries applied.
    - Geocode in Cortext Manager the distinct english addresses values, setting these parameters to the script: 'county' scale and confidence > `0.4`.
        - Total addresses: 27.009
        - Geocoded: 96.1383242622829%

        Results store in the table: `JPnew_distinct_address_geocoded`.
    - Assign the geolocation values obtained, to each inventor/applicant, based on the address.

    At the end we have the tables `JPnew_applicants_enriched` and `JPnew_inventors_enriched` with the original names/addresses, names/addresses translated to english, and the geocoding values associated to each address.

2. Based on the name value
[07_enrich_addresses_from_names.sql](07_enrich_addresses_from_names.sql): file with the queries applied 

---

| Total number of applicants                          | 985.169 |         |
|-----------------------------------------------------|--------:|--------:|
| Number of applicants with address geocoded          | 821.130 | 83,35 % |
| Number of applicants geocoded by name               | 159.994 | 16,24 % |
| Number of applicants without the geolocation values |   4.045 |  0,41 % |

| Total number of inventors                          | 2.352.536 |         |
|----------------------------------------------------|----------:|--------:|
| Number of inventors with address geocoded          | 1.807.590 | 76,84 % |
| Number of inventors without the geolocation values |   544.946 | 23,16 % |