USE patstat2021_lab;

ALTER TABLE JPnew_distinct_address ADD nb_applt INT NULL; -- number of applicants
ALTER TABLE JPnew_distinct_address ADD nb_invt INT NULL; -- number of inventors

UPDATE JPnew_distinct_address a
SET a.nb_applt = (
			SELECT count(*)
			FROM JPnew_applicant_ad8_tc5 b
			WHERE a.address = b.address 
			GROUP BY address
			)
;

UPDATE JPnew_distinct_address a
SET a.nb_invt = (
			SELECT count(*)
			FROM JPnew_inventor_ad8_tc5 b
			WHERE a.address = b.address 
			GROUP BY address
			)
;