USE patstat2021_staging;
            
CREATE TABLE tls206_person_new_addresses (
    `person_id` int(11) NOT NULL DEFAULT '0',
    `person_name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
    `new_address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
    `country_final` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOAD DATA LOCAL INFILE '/home/user/tls206_person_new_addresses_libpostal.csv'  
INTO TABLE tls206_person_new_addresses  
CHARACTER SET utf8mb4   
FIELDS TERMINATED BY ','  
ENCLOSED BY '"'   
LINES TERMINATED BY '\n'  
(person_id,person_name,new_address,country_final);
--  1.295.277

-- Indexes
CREATE INDEX idx_person USING BTREE ON patstat2021_staging.tls206_person_new_addresses_pre (person_id);
CREATE INDEX idx_person USING BTREE ON patstat2021_staging.tls206_person_new_addresses (person_id);
CREATE INDEX idx_lib_ctry_harm USING BTREE ON patstat2021_staging.nomen_geo_ifris (lib_ctry_harm);
CREATE INDEX idx_country_final USING BTREE ON patstat2021_staging.tls206_person_new_addresses (country_final);

-- Update country_final field for the values containing the full country name and 
-- standardize all of them to the country code
UPDATE patstat2021_staging.tls206_person_new_addresses a
	INNER JOIN patstat2021_staging.nomen_geo_ifris b 
	ON a.country_final = b.lib_ctry_harm 
SET a.country_final = b.ctry_harm ;

-- Total
SELECT COUNT(*) FROM patstat2021_staging.tls206_person; -- 79.291.634
SELECT COUNT(*) FROM patstat2021_staging.tls206_person WHERE person_address = ''; -- 57.548.914
SELECT COUNT(*) FROM patstat2021_staging.tls206_person_new_addresses; -- 1.295.277

-- Top 5 countries where person_name field included address info 
SELECT country_final , count(*) 
FROM patstat2021_staging.tls206_person_new_addresses
GROUP BY country_final 
ORDER BY count(*) DESC
LIMIT 5; 

-- Numbers for the first 5 countries
SELECT person_ctry_code, COUNT(*) 
FROM patstat2021_staging.tls206_person 
WHERE person_ctry_code IN ('DE', 'JP', 'US', 'FR', 'GB') 
GROUP BY person_ctry_code; -- 
