USE patstat2021_staging;

ALTER TABLE applt_adr_ifris_epwofrjp ADD INDEX idx_person_id(person_id);
-- Query OK, 107655504 rows affected (35 min 45.13 sec)
-- Records: 107655504  Duplicates: 0  Warnings: 0
ALTER TABLE invt_adr_ifris_epwofrjp ADD INDEX idx_person_id(person_id);
-- Query OK, 200755133 rows affected (56 min 25.27 sec)
-- Records: 200755133  Duplicates: 0  Warnings: 0

UPDATE
    applt_adr_ifris_epwofrjp a,
    tls206_person_new_addresses b
SET
    a.adr_comp = b.new_address,
    a.ctry_comp = b.country_final,
    a.source = 'IFRIS'
WHERE
    a.person_id = b.person_id;
-- Query OK, 476920 rows affected (1 min 14.36 sec)
-- Rows matched: 476920  Changed: 476920  Warnings: 0

UPDATE
    invt_adr_ifris_epwofrjp a,
    tls206_person_new_addresses b
SET
    a.adr_comp = b.new_address,
    a.ctry_comp = b.country_final,
    a.source = 'IFRIS'
WHERE
    a.person_id = b.person_id;
-- Query OK, 1612292 rows affected (3 min 47.40 sec)
-- Rows matched: 1612292  Changed: 1612292  Warnings: 0
