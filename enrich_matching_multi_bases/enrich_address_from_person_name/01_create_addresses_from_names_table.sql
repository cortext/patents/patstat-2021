USE patstat2021_staging;

DROP TABLE IF EXISTS tls206_person_new_addresses_pre;

CREATE TABLE tls206_person_new_addresses_pre
SELECT
    *,
    person_name AS address_final,
    UPPER(
        TRIM(
            ' '
            FROM
                SUBSTRING_INDEX(person_name, ',', -1)
        )
    ) AS country_final
FROM
    tls206_person
WHERE
    person_name <> ''
    AND person_address = ''
    AND (
        LENGTH(person_name) - LENGTH(REPLACE(person_name, ',', ''))
    ) > 2
    AND EXISTS (
        SELECT
            1
        FROM
            nomen_geo_ifris
        WHERE
            UPPER(
                TRIM(
                    ' '
                    FROM
                        SUBSTRING_INDEX(person_name, ',', -1)
                )
            ) IN(UPPER(ctry_harm), UPPER(lib_ctry_harm))
    )
;
-- Query OK, 1354411 rows affected (58 min 44,64 sec)
-- Records: 1354411  Duplicates: 0  Warnings: 0
