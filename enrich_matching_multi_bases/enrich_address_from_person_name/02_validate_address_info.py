import sys
from csv import DictReader, DictWriter
import requests

import argparse
parser = argparse.ArgumentParser()


def parse_args(argv):
    global IN_FILE, OUT_FILE, LOCAL_IP, PORT
    # -i input file path -o output file path
    parser.add_argument("-i", "--input-file",
                        dest="input_file", help="Input file path")
    parser.add_argument("-o", "--output-file",
                        dest="output_file", help="Output file path")
    parser.add_argument("-ip", "--local-ip",
                        dest="local_ip", help="Local IP")
    parser.add_argument("-port", "--port",
                        dest="port", help="Local port")

    args = parser.parse_args()

    print("Input file: {} \nOutput file: {} ".format(
        args.input_file,
        args.output_file,
    ))
    IN_FILE = args.input_file
    OUT_FILE = args.output_file
    LOCAL_IP = args.local_ip
    PORT = args.port


def libpostal_api(address_text):
    resp = requests.post(BASE_URL,
                         json={'query': address_text})
    data = resp.json()
    return data


def parse_patstat_address(ifile):
    w = open(OUT_FILE, 'w')
    wcsv = DictWriter(w, fieldnames=['person_id', 'person_name', 'new_address', 'country_final'])
    wcsv.writeheader()

    for row in ifile:
        person_name = row['person_name']
        list_labels = libpostal_api(person_name)
        new_address = ''
        country_label = ''
        for tag in list_labels:
            if tag['label'] == 'country':
                country_label = tag['value']
                break

        if country_label:
            new_address = ', '.join([label['value'] for label in list_labels])

            try:
                data = {
                    'person_id': row['person_id'],
                    'person_name': person_name,
                    'new_address': new_address,
                    'country_final': row['country_final']
                }
                wcsv.writerow(data)
            except Exception as err:
                print("\t", err)
    w.close


if __name__ == "__main__":
    parse_args(sys.argv)

    BASE_URL = f'http://{LOCAL_IP}:{PORT}/parser'
    r = open(IN_FILE)
    rcsv = DictReader(r, delimiter='\t')
    parse_patstat_address(rcsv)
