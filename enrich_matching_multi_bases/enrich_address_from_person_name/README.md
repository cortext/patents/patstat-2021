# Enrich address field from person name

The latest treated tables in `patstat2021_staging` are taken to count the distinct number of applicants and inventors where the address data is missing.

Number of distinct applicants: 22.486.883 (100%)
Number of distinct applicants without address: 16.520.030 (**73.46%**)
```sql
SELECT COUNT(DISTINCT person_id) FROM applt_adr_ifris_epwofrjp WHERE person_address = '' and adr_comp = '';
```

Number of distinct inventors: 52.513.282 (100%)
Number of distinct applicants without address: 33.683.028 (**64.14%**)
```sql
SELECT COUNT(DISTINCT person_id) FROM invt_adr_ifris_epwofrjp WHERE person_address = '' and adr_comp = '';
```

As the percentage for the missing address information is high, an [analysis has been made about three years ago](/Geographical%20Information/parsed%20addresses/README.md), for those cases wherein the address field was empty but the address itself is contained into the `person_name` field.


## Extract and export data

1. [01_create_addresses_from_names_table.sql](01_create_addresses_from_names_table.sql)
   
    Create a table with the records from the `tls206_person` table that possibly contain an address in their `person_name` field. The conditions to select them are:
    - `person_name` field not empty,
    - `person_address` field empty,
    - there is at least one comma `,` in the `person_name` field, that will allow the next condition to check a possible country value,
    - the last part of the `person_name` field, splitted by comma, corresponds to a country name or a country code (`ctry_harm` and `lib_ctry_harm` fields in the `nomen_geo_ifris` table).
  
2. Set up Libpostal

    A local libpostal-rest instance is mounted in a docker container to make easier the interaction with the library, as explained [here](/Geographical%20Information/parsed%20addresses/README.md#setting-up-libpostal). 

3. [02_validate_address_info.py](02_validate_address_info.py)
    - Generate a csv file out of the table created in the file [01](01_create_addresses_from_names_table.sql), with tab `\t` as delimiter.
    - Run the following to generate a table with the new addresses by person_id:
        ```sh
        python3 02_validate_address_info.py -i /home/tls206new_addresses.csv -o /home/tls206new_addresses_libpostal.csv -ip localhost -port 7008
        ```
        - `-i`: input file
        - `-o`: output file
        - `-ip`: IP where the Libpostal service is running
        - `-port`: local port where the Libpostal service is running
        
        ### Steps
        - The script calls the Libpostal API for each row's person name, which is possibly containing an address value, to tag it.
        - The API returns an answer tagging the possible address, with words like country, postcode, city, road, house_number and some others depending on the info provided.
        - A new csv file is created to store the new address obtained for each row, along with the country value.
          - To make sure it corresponds to an address, we discard the rows whose response from Libpostal does not contain a country value (there is not a tag for country). This is an example: 
  
            | Query                             | Libpostal response                                                                                                                                                                                               | Status    |
            | --------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- |
            | {"query":" Terrn, LI"}            | [     {         "label": "house",         "value": "terrn li"     } ]                                                                                                                                            | Discarded |
            | {"query":" 42 rue Polinaire, FR"} | [     {         "label": "house_number",         "value": "42"     },     {         "label": "road",         "value": "rue polinaire"     },     {         **"label": "country"**,         "value": "fr"     } ] | Included  |

        ### Results
        
        [03_load_csv_into_table_and_stats.sql](03_load_csv_into_table_and_stats.sql)
        
        Load csv file into a table called `tls206_person_new_addresses` and make some stats.

        | Country | tls206_person | tls206_person_new_addresses |
        |---------|---------------|-----------------------------|
        | DE      | 4.449.093     | 574.476                     |
        | FR      | 1.937.430     | 242.382                     |
        | GB      | 1.748.328     | 219.501                     |
        | JP      | 5.991.493     | 56.243                      |
        | US      | 12.407.616    | 45.980                      |
        | ...     |               |                             |
        | Total   | 79.291.634    | 1.295.277                   |

        Total tls206_person: 79.291.634 (100%)

        Total missing addresses: 57.548.914 (72,58%)

        Total parsed addresses through this method: 1.295.277 (1,63%)

## Integrate new addresses data into the IFRIS tables

[04_integrate_addresses_ifris_tables.sql](04_integrate_addresses_ifris_tables.sql)

Number of rows updated from the `tls206_person_new_addresses` table for:
- applicants: 476.920
- inventors: 1.612.292