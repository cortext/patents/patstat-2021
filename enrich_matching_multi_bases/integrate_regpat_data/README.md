# Integrate REGPAT data

*The scripts used were adapted from the MySQL scripts applied in previous versions*
For the EP and WO office, REGPAT is used, so if a patent has no applicant/inventor/ipc associated in PATSTAT, those of REGPAT are taken.
- They are linked by the appln_id provided by the OECD.
- Person IDs are created as they are not provided:
  - from 60,000,000 for WO depositors
  - from 70,000,000 for WO inventors

Tables with data obtained from REGPAT that was not in Patstat:
- applt_ep_regpat
- applt_wo_regpat
- invt_ep_regpat
- invt_wo_regpat
- ipc_ep_regpat
- ipc_wo_regpat

Tables with final data consolidated from EP and WO:
- applt_comp_ifris_epwo 
- invt_comp_ifris_epwo 
- tls209_appln_ipc_ifris_epwo 

## Steps
[01_missing_EP.sql](01_missing_EP.sql)

- Getting the EP applicants from REGPAT missing in Patstat: 0
- Getting the EP inventors from REGPAT missing in Patstat: 0
- Getting the EP IPC codes from REGPAT missing in Patstat: 0

[02_missing_WO.sql](02_missing_WO.sql)
The obtained IPCs are assigned to level A.

- Getting the WO applicants from REGPAT missing in Patstat: 25
- Getting the WO inventors from REGPAT missing in Patstat: 5.714
- Getting the WO IPC codes from REGPAT missing in Patstat: 43.134

[03_addresses_appt_EP.sql](03_addresses_appt_EP.sql)
Bringing to the table `applt_adr_ifris_ep` the address information of the *applicants* where the Patstat name matches (strictly and not strictly) the REGPAT name: 69 rows updated.

[04_addresses_invt_EP.sql](04_addresses_invt_EP.sql)
Bringing to the table `invt_adr_ifris_ep` the address information of the *inventors* where the Patstat name matches (strictly and not strictly) the REGPAT name: 6.664 rows updated.

[05_addresses_appt_WO.sql](05_addresses_appt_WO.sql)
Bringing to the table `applt_adr_ifris_epwo` the address information of the *applicants* where the Patstat name matches (strictly and not strictly) the REGPAT name: 3.261.943 rows updated.

[06_addresses_invt_WO.sql](06_addresses_invt_WO.sql)
Bringing to the table `applt_adr_ifris_epwo` the address information of the *applicants* where the Patstat name matches (strictly and not strictly) the REGPAT name: 9.527.187 rows updated.