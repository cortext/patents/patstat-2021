-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Recovery of missing items due to REGPAT matching
USE patstat2021_staging;

CREATE INDEX idx_appln_id ON applt_comp_ifris_ep (appln_id);

-- ---------------------------------------------------------------------------------
-- Recovery of WO applicants
SELECT @compteur := 60000000;

DROP TABLE IF EXISTS applt_wo_regpat;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE applt_wo_regpat AS
SELECT
    a.appln_id,
    @compteur := @compteur + 1 AS person_id,
    a.ctry_code AS ctry_code,
    a.app_name AS app_name,
    a.address AS address
FROM
    regpatJul2021.pct_app_reg a
WHERE
    NOT EXISTS (
        SELECT
            NULL
        FROM
            applt_comp_ifris_ep b
        WHERE
            a.appln_id = b.appln_id
    )
    AND a.appln_id != 0
GROUP BY
    a.appln_id,
    a.ctry_code,
    a.app_name;
-- Query OK, 25 rows affected (31.26 sec)
-- Records: 25  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id ON applt_wo_regpat (appln_id);
-- Query OK, 25 rows affected (0.02 sec)
-- Records: 25  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS applt_comp_ifris_epwo;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE applt_comp_ifris_epwo AS
SELECT
    *
FROM
    applt_comp_ifris_ep;
-- Query OK, 107459050 rows affected (1 min 58.35 sec)
-- Records: 107459050  Duplicates: 0  Warnings: 0

INSERT INTO
    applt_comp_ifris_epwo
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    a.app_name,
    a.address,
    a.ctry_code,
    0,
    '',
    0,
    0
FROM
    applt_wo_regpat a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 25 rows affected (0.00 sec)
-- Records: 25  Duplicates: 0  Warnings: 0

ALTER TABLE
    applt_comp_ifris_epwo
ADD
    PRIMARY KEY PK_epwoapp (appln_id, person_id);
-- Query OK, 107459075 rows affected (6 min 36.02 sec)
-- Records: 107459075  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Recovery of WO inventors
CREATE INDEX idx_appln_id ON invt_comp_ifris_ep (appln_id);
-- Query OK, 200471087 rows affected (28 min 49.00 sec)
-- Records: 200471087  Duplicates: 0  Warnings: 0

SELECT @compteur := 70000000;

DROP TABLE IF EXISTS invt_wo_regpat;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE invt_wo_regpat AS
SELECT
    a.appln_id,
    @compteur := @compteur + 1 AS person_id,
    a.ctry_code,
    a.inv_name,
    max(a.address) AS address
FROM
    regpatJul2021.pct_inv_reg a
WHERE
    NOT EXISTS (
        SELECT
            NULL
        FROM
            invt_comp_ifris_ep b
        WHERE
            a.appln_id = b.appln_id
    )
    AND a.appln_id != 0
GROUP BY
    a.appln_id,
    a.ctry_code,
    a.inv_name;
-- Query OK, 5714 rows affected (37.58 sec)
-- Records: 5714  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id ON invt_wo_regpat (appln_id);
-- Query OK, 5714 rows affected (0.03 sec)
-- Records: 5714  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS invt_comp_ifris_epwo;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE invt_comp_ifris_epwo AS
SELECT
    *
FROM
    invt_comp_ifris_ep;
-- Query OK, 200471087 rows affected (3 min 29.56 sec)
-- Records: 200471087  Duplicates: 0  Warnings: 0

INSERT INTO
    invt_comp_ifris_epwo
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    a.inv_name,
    a.address,
    a.ctry_code,
    0,
    '',
    0,
    0
FROM
    invt_wo_regpat a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 5714 rows affected (18.11 sec)
-- Records: 5714  Duplicates: 0  Warnings: 0

ALTER TABLE
    invt_comp_ifris_epwo
ADD
    PRIMARY KEY PK_appln_person_id (appln_id, person_id);
-- Query OK, 200476801 rows affected (28 min 28.05 sec)
-- Records: 200476801  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Recovery of WO IPC codes

CREATE INDEX idx_pct_nbr USING BTREE ON regpatJul2021.pct_ipc (pct_nbr(14));
CREATE INDEX idx_ipc USING BTREE ON regpatJul2021.pct_ipc (IPC(12));
CREATE INDEX idx_pct_nbr USING BTREE ON regpatJul2021.pct_app_reg (pct_nbr(14));
CREATE INDEX idx_appln_id USING BTREE ON regpatJul2021.pct_app_reg (appln_id);
CREATE INDEX idx_pct_nbr USING BTREE ON regpatJul2021.pct_inv_reg (pct_nbr(14));
CREATE INDEX idx_appln_id USING BTREE ON regpatJul2021.pct_inv_reg (appln_id);
CREATE INDEX idx_appln_id USING BTREE ON tls209_appln_ipc_ifris_ep (appln_id);

DROP TABLE IF EXISTS ipc_wo_regpat;
-- Query OK, 0 rows affected, 1 warning (0.01 sec)

CREATE TABLE ipc_wo_regpat AS
SELECT
    e.appln_id AS appln_id,
    a.IPC
FROM
    regpatJul2021.pct_ipc a,
    regpatJul2021.pct_app_reg e
WHERE
    a.pct_nbr = e.pct_nbr
    AND NOT EXISTS (
        SELECT
            NULL
        FROM
            tls209_appln_ipc_ifris_ep b
        WHERE
            e.appln_id = b.appln_id
    )
    AND e.appln_id != 0
    AND a.IPC != '';
-- Query OK, 43128 rows affected (7 min 23.13 sec)
-- Records: 43128  Duplicates: 0  Warnings: 0

INSERT INTO
    ipc_wo_regpat
SELECT
    e.appln_id AS appln_id,
    a.IPC
FROM
    regpatJul2021.pct_ipc a,
    regpatJul2021.pct_inv_reg e
WHERE
    a.pct_nbr = e.pct_nbr
    AND NOT EXISTS (
        SELECT
            NULL
        FROM
            tls209_appln_ipc_ifris_ep b
        WHERE
            e.appln_id = b.appln_id
    )
    AND e.appln_id != 0
    AND a.IPC != ''
    AND (e.appln_id, a.IPC) NOT IN (
        SELECT
            appln_id,
            IPC
        FROM
            ipc_wo_regpat
    );
-- Query OK, 6 rows affected (4 hours 16 min 56.31 sec)
-- Records: 6  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS tls209_appln_ipc_ifris_epwo;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE tls209_appln_ipc_ifris_epwo AS
SELECT
    *
FROM
    tls209_appln_ipc_ifris_ep;
-- Query OK, 281862225 rows affected (4 min 4.27 sec)
-- Records: 281862225  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Add the new IPC codes formatted as the ones already there

ALTER TABLE tls209_appln_ipc_ifris_epwo MODIFY COLUMN ipc_version date DEFAULT '9999-12-31';
-- Query OK, 281862225 rows affected (2 min 48.36 sec)
-- Records: 281862225  Duplicates: 0  Warnings: 0

INSERT INTO
    tls209_appln_ipc_ifris_epwo
SELECT
    DISTINCT appln_id,
    CONCAT(
        Substring(IPC, 1, 4),
        " ",
        IF(
            length(substring(IPC, 5, 3)) = 1,
            concat('  ', substring(IPC, 5, 3)),
            IF(
                length(substring(IPC, 5, 3)) = 2,
                concat(' ', substring(IPC, 5, 3)),
                substring(IPC, 5, 3)
            )
        ),
        substring(IPC, 8, 5)
    ),
    'A',
    NULL,
    '',
    '',
    ''
FROM
    ipc_wo_regpat;
-- Query OK, 38585 rows affected (0.13 sec)
-- Records: 38585  Duplicates: 0  Warnings: 0
