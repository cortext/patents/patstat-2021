-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Get EP addresses if not in PATSTAT
USE patstat2021_staging;

-- ---------------------------------------------------------------------------------
-- EP inventors
DROP TABLE IF EXISTS invt_adr_ifris_ep;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE invt_adr_ifris_ep (
    appln_id int(10) NOT NULL DEFAULT '0',
    person_id int(11) NOT NULL DEFAULT '0',
    person_name varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    person_address varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
    person_ctry_code varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    doc_std_name_id int(11) NOT NULL DEFAULT '0',
    doc_std_name varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    psn_id int(11) DEFAULT NULL,
    han_id int(11) DEFAULT NULL,
    name_regpat varchar(300) NOT NULL,
    adr_regpat varchar(500) NOT NULL,
    ctry_regpat varchar(3) NOT NULL DEFAULT '',
    methode varchar(4),
    KEY idx_appln_id (appln_id)
);
-- Query OK, 0 rows affected (0.03 sec)

INSERT INTO
    invt_adr_ifris_ep
SELECT
    a.*,
    '' AS name_regpat,
    '' AS adr_regpat,
    '' AS ctry_regpat,
    '' AS methode
FROM
    invt_comp_ifris_epwo a;
-- Query OK, 200476801 rows affected (10 min 54.75 sec)
-- Records: 200476801  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 1
-- MET1: where application id and inventor name in Patstat match the respective
-- values in Regpat 
UPDATE
    invt_adr_ifris_ep a
    INNER JOIN regpatJul2021.epo_inv_reg b ON (
        a.appln_id = b.appln_id
        AND a.person_name = b.inv_name COLLATE utf8_unicode_ci
    )
SET
    a.name_regpat = b.inv_name,
    a.adr_regpat = b.address,
    a.ctry_regpat = b.ctry_code,
    a.methode = 'MET1'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 599 rows affected (7 min 11.93 sec)
-- Rows matched: 599  Changed: 599  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 2
-- MET2: where method field has no value, application id match and Patstat name
-- like REGPAT name
UPDATE
    invt_adr_ifris_ep a
    INNER JOIN regpatJul2021.epo_inv_reg b ON (
        a.methode = ''
        AND a.appln_id = b.appln_id
        AND a.person_name LIKE concat('%', b.inv_name, '%') COLLATE utf8_unicode_ci
    )
SET
    a.name_regpat = b.inv_name,
    a.adr_regpat = b.address,
    a.ctry_regpat = b.ctry_code,
    a.methode = 'MET2'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 0 rows affected (6 min 57.77 sec)
-- Rows matched: 0  Changed: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 3
-- MET3: where method field has no value, application id match and REGPAT name like
-- Patstat name 
UPDATE
    invt_adr_ifris_ep a
    INNER JOIN regpatJul2021.epo_inv_reg b ON (
        a.methode = ''
        AND a.appln_id = b.appln_id
        AND b.inv_name LIKE concat('%', a.person_name, '%') COLLATE utf8_unicode_ci
    )
SET
    a.name_regpat = b.inv_name,
    a.adr_regpat = b.address,
    a.ctry_regpat = b.ctry_code,
    a.methode = 'MET3'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 6065 rows affected (7 min 2.35 sec)
-- Rows matched: 6065  Changed: 6065  Warnings: 0

CREATE INDEX idx_methode ON invt_adr_ifris_ep (methode);
-- Query OK, 200476801 rows affected (12 min 36.08 sec)
-- Records: 200476801  Duplicates: 0  Warnings: 0
