#!/bin/sh

mysql --defaults-file=~/mycnf.cnf -vvv < 01_missing_EP.sql > 01_missing_EP_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 02_missing_WO.sql > 02_missing_WO_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 03_addresses_appt_EP.sql > 03_addresses_appt_EP_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 04_addresses_invt_EP.sql > 04_addresses_invt_EP_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 05_addresses_appt_WO.sql > 05_addresses_appt_WO_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < 06_adresses_invt_WO.sql > 06_adresses_invt_WO_report.txt 2>&1 &