-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Get EP addresses if not in PATSTAT
USE patstat2021_staging;

-- ---------------------------------------------------------------------------------
-- EP applicants
DROP TABLE IF EXISTS applt_adr_ifris_ep;
-- Query OK, 0 rows affected, 1 warning (0.45 sec)

CREATE TABLE applt_adr_ifris_ep (
    appln_id int(10) NOT NULL DEFAULT '0',
    person_id int(11) NOT NULL DEFAULT '0',
    person_name varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    person_address varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
    person_ctry_code varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    doc_std_name_id int(11) NOT NULL DEFAULT '0',
    doc_std_name varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    psn_id int(11) DEFAULT NULL,
    han_id int(11) DEFAULT NULL,
    name_regpat varchar(300) NOT NULL,
    adr_regpat varchar(500) NOT NULL,
    ctry_regpat varchar(3) NOT NULL DEFAULT '',
    methode varchar(4),
    KEY idx_appln_id (appln_id)
);
-- Query OK, 0 rows affected (0.02 sec)

INSERT INTO
    applt_adr_ifris_ep
SELECT
    a.*,
    '' AS name_regpat,
    '' AS adr_regpat,
    '' AS ctry_regpat,
    '' AS methode
FROM
    applt_comp_ifris_epwo a;
-- Query OK, 107459075 rows affected (5 min 15.73 sec)
-- Records: 107459075  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 1
-- MET1: where application id and applicant name in Patstat match the respective
-- values in Regpat
UPDATE
    applt_adr_ifris_ep a
    INNER JOIN regpatJul2021.epo_app_reg b ON (
        a.appln_id = b.appln_id
        AND a.person_name = b.app_name COLLATE utf8_unicode_ci
    )
SET
    a.name_regpat = b.app_name,
    a.adr_regpat = b.address,
    a.ctry_regpat = b.ctry_code,
    a.methode = 'MET1'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 69 rows affected (1 min 4.91 sec)
-- Rows matched: 69  Changed: 69  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 2
-- MET2: where method field has no value, application id match and Patstat name
-- like REGPAT name
UPDATE
    applt_adr_ifris_ep a
    INNER JOIN regpatJul2021.epo_app_reg b ON (
        a.methode = ''
        AND a.appln_id = b.appln_id
        AND a.person_name LIKE concat('%', b.app_name, '%') COLLATE utf8_unicode_ci
    )
SET
    a.name_regpat = b.app_name,
    a.adr_regpat = b.address,
    a.ctry_regpat = b.ctry_code,
    a.methode = 'MET2'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 0 rows affected (1 min 2.97 sec)
-- Rows matched: 0  Changed: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Method 3
-- MET3: where method field has no value, application id match and REGPAT name like
-- Patstat name
UPDATE
    applt_adr_ifris_ep a
    INNER JOIN regpatJul2021.epo_app_reg b ON (
        a.methode = ''
        AND a.appln_id = b.appln_id
        AND b.app_name LIKE concat('%', a.person_name, '%') COLLATE utf8_unicode_ci
    )
SET
    a.name_regpat = b.app_name,
    a.adr_regpat = b.address,
    a.ctry_regpat = b.ctry_code,
    a.methode = 'MET3'
WHERE
    (
        a.person_address IS NULL
        OR a.person_address = ''
    );
-- Query OK, 0 rows affected (1 min 3.24 sec)
-- Rows matched: 0  Changed: 0  Warnings: 0

CREATE INDEX idx_methode ON applt_adr_ifris_ep (methode);
-- Query OK, 107459075 rows affected (6 min 52.00 sec)
-- Records: 107459075  Duplicates: 0  Warnings: 0