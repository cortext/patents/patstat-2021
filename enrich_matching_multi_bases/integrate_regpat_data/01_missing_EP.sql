-- ---------------------------------------------------------------------------------
-- Adapted from the MySQL scripts applied in previous versions 
-- ---------------------------------------------------------------------------------
-- Recovery of missing items due to REGPAT matching
-- Note: change regpatJul2021 for APPT and INVT tables
USE patstat2021_staging;

-- ---------------------------------------------------------------------------------
-- Recovery of EP applicants
CREATE INDEX idx_appln_id ON applt_ifris (appln_id);
CREATE INDEX idx_appln_id USING BTREE ON regpatJul2021.epo_app_reg (appln_id);

SELECT
    count(*)
FROM
    regpatJul2021.epo_app_reg a
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            applt_ifris b
        WHERE
            a.Appln_id = b.appln_id 
    );
-- 0 rows

-- The previous count is zero, so in this case the following related tables do not need 
-- to be created. But they will, to avoid changing future references.
DROP TABLE IF EXISTS applt_ep_regpat;

CREATE TABLE applt_ep_regpat AS
SELECT
    a.appln_id AS appln_id,
    a.person_id AS person_id,
    a.ctry_code AS ctry_code,
    a.app_name AS applt_name,
    a.address AS address
FROM
    regpatJul2021.epo_app_reg a
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            applt_ifris b
        WHERE
            a.appln_id = b.appln_id 
    )
GROUP BY
    a.appln_id,
    a.person_id;
-- Query OK, 0 rows affected (12.35 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id ON applt_ep_regpat (appln_id);
-- Query OK, 0 rows affected (0.02 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS applt_comp_ifris_ep;

CREATE TABLE applt_comp_ifris_ep AS
SELECT
    *
FROM
    applt_ifris;
-- Query OK, 107459050 rows affected (2 min 6.33 sec)
-- Records: 107459050  Duplicates: 0  Warnings: 0

INSERT INTO
    applt_comp_ifris_ep
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    a.applt_name,
    a.address,
    a.ctry_code,
    '',
    '',
    '',
    ''
FROM
    applt_ep_regpat a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 0 rows affected (0.07 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

ALTER TABLE
    applt_comp_ifris_ep
ADD
    CONSTRAINT PK_appln_person_id PRIMARY KEY (appln_id, person_id);
-- Query OK, 107459050 rows affected (17 min 16.21 sec)
-- Records: 107459050  Duplicates: 0  Warnings: 0

-- ---------------------------------------------------------------------------------
-- Recovery of EP inventors
CREATE INDEX idx_appln_id ON invt_ifris (appln_id);
CREATE INDEX idx_appln_id USING BTREE ON regpatJul2021.epo_inv_reg (appln_id);

SELECT
    count(*)
FROM
    regpatJul2021.epo_inv_reg a
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            invt_ifris b
        WHERE
            a.appln_id = b.appln_id
    );
-- 0 rows

-- The previous count is zero, so in this case the following related tables do not need 
-- to be created. But they will, to avoid changing future references.

DROP TABLE IF EXISTS invt_ep_regpat;

CREATE TABLE invt_ep_regpat AS
SELECT
    a.appln_id AS appln_id,
    a.person_id AS person_id,
    a.ctry_code AS ctry_code,
    a.inv_name AS invt_name,
    max(a.address) AS address
FROM
    regpatJul2021.epo_inv_reg a
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            invt_ifris b
        WHERE
            a.appln_id = b.appln_id 
    )
GROUP BY
    a.appln_id,
    a.person_id
;
-- Query OK, 0 rows affected (1 min 17.90 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

CREATE INDEX idx_appln_id ON invt_ep_regpat (appln_id);
-- Query OK, 0 rows affected (0.02 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS invt_comp_ifris_ep;

CREATE TABLE invt_comp_ifris_ep AS
SELECT
    *
FROM
    invt_ifris;
-- Query OK, 200471087 rows affected (3 min 45.92 sec)
-- Records: 200471087  Duplicates: 0  Warnings: 0

INSERT INTO
    invt_comp_ifris_ep
SELECT
    DISTINCT a.appln_id,
    a.person_id,
    a.invt_name,
    a.address,
    a.ctry_code,
    0,
    '',
    0,
    0
FROM
    invt_ep_regpat a,
    tls201_appln b
WHERE
    a.appln_id = b.appln_id;
-- Query OK, 0 rows affected (0.00 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

ALTER TABLE
    invt_comp_ifris_ep
ADD
    CONSTRAINT PK_appln_person_id PRIMARY KEY (appln_id, person_id);

-- ---------------------------------------------------------------------------------
-- Recovery of EP IPC codes
ALTER TABLE regpatJul2021.epo_ipc ADD CONSTRAINT pk_appln_id_ipc PRIMARY KEY (appln_id,IPC(200));

SELECT
    count(*)
FROM
    regpatJul2021.epo_ipc a
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            tls209_appln_ipc b
        WHERE
            a.appln_id = b.appln_id 
    )
    AND a.IPC != '';
-- 0 rows

-- The previous count is zero, so in this case the following related tables do not need 
-- to be created. But they will, to avoid changing future references.

DROP TABLE IF EXISTS ipc_ep_regpat;

CREATE TABLE ipc_ep_regpat AS
SELECT
    a.*
FROM
    regpatJul2021.epo_ipc a
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            tls209_appln_ipc b
        WHERE
            a.appln_id = b.appln_id
    )
    AND a.IPC != '';
-- Query OK, 0 rows affected (2 min 53.87 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS tls209_appln_ipc_ifris_ep;

CREATE TABLE tls209_appln_ipc_ifris_ep AS
SELECT
    *
FROM
    tls209_appln_ipc;
-- Query OK, 281862225 rows affected (3 min 55.99 sec)
-- Records: 281862225  Duplicates: 0  Warnings: 0

-- IPC format update
INSERT INTO
    tls209_appln_ipc_ifris_ep
SELECT
    DISTINCT appln_id,
    CONCAT(
        Substring(IPC, 1, 4),
        " ",
        IF(
            length(CONVERT(substring(IPC, 5, 3), UNSIGNED)) = 1,
            concat('  ', CONVERT(substring(IPC, 5, 3), UNSIGNED)),
            IF(
                length(CONVERT(substring(IPC, 5, 3), UNSIGNED)) = 2,
                concat(' ', CONVERT(substring(IPC, 5, 3), UNSIGNED)),
                substring(IPC, 5, 3)
            )
        ),
        substring(IPC, 8, 5)
    ),
    'A',
    '',
    '',
    '',
    ''
FROM
    ipc_ep_regpat;
-- Query OK, 0 rows affected (0.00 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

ALTER TABLE
    tls209_appln_ipc_ifris_ep
ADD
    CONSTRAINT PK_ipc_ifris_ep PRIMARY KEY (appln_id, ipc_class_symbol, ipc_class_level);
