#!/usr/local/bin/perl

######################################################
#              Connexion a la base de donnees       #
######################################################

use warnings;

use DBI;    # Charger le module DBI

# Parametres de connexion a la base de donnees

my $BaseDeDonnees = "db_name";
my $NomHote       = "ssh_host"; 
my $login         = "db_user";     
my $MotDePass     = "password";
my $port	  = "3307";

# Connection  a la base de donnees mysql

 $dbh = DBI->connect( "dbi:mysql:dbname=$BaseDeDonnees;host=$NomHote;port=$port;", $login, $MotDePass ) or die "Connection impossible � la base de donnees $BaseDeDonnees !";
 print"==> CONNECTION A LA BASE DE DONNEES $BaseDeDonnees\n";



