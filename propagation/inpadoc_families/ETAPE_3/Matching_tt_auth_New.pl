#!/usr/local/bin/perl

require "ETAPE_3/Matching.pl";
  @res=();
 $max=" ";
 $cle_max_long=" ";
 %res=();
 $nb=0;
 $add1=0;
 $add2=0;
 
  # NEW
 %resultatGlobal1=();
 %resultatGlobal2=();
 
 %resultatGlobal=();
 $cleGlobal=0;
 
 print "ETAPE 3 - Matching tout confondu - debut :", `date`;
#########################################################################
#           MEME FAMILLE, tt OFFICE, tt TYPE D'ACTEURS , date=20 ans    #
#########################################################################
##########I.1:   MEME FAMILLE, TT OFFICE, TT TYPE D'ACTEURS
print"==> MEME FAMILLE, TOUS OFFICES, TOUS TYPES D'ACTEURS, DATE=20 ANS\n";

################ refaire la boucle au tant de $nbre_enregistrement

my $req="SELECT count(person_id)FROM dep_sans_address
     where person_name <> ''
     AND address_recup is not null
     AND address_recup=''
    ";
###prepare la requ�te sql
my  $version0 = $dbh->prepare($req);

###execution de la requete sql
$version0-> execute() || die "pb de selection : $DBI::errstr";
@nbre_famille = $version0->fetchrow_array;
$nbre_enregistrement=$nbre_famille[0];


### SELECTIONER LES NOMS DES ENREGISTREMENT QUI N'ONT PAS D'ADREESSES DANS LA MEME FAMILLE
my $requete="SELECT   inpadoc_family_id, appln_auth, appln_filing_date,person_id, name_parsing
		FROM dep_sans_address
		where person_name <> ''
        and address_recup=''
      order by inpadoc_family_id;";
###prepare la requete sql
my  $version = $dbh->prepare($requete);

###execution de la requete sql
$version-> execute() || die "pb de selection : $DBI::errstr";

 # il faut remplacer ce nombre par le nombre d'applications sans @ dans une famille: $nbre_enregistrement
while ((my  ( $inpadoc_family_id, $appln_auth,  $appln_filing_date, $person_id, $name_parsing) = $version -> fetchrow_array)&&($nbre_enregistrement!=0))
{
	%short=();
   %short = ( $person_id     => "$name_parsing" );    #remplir short par le nom qui n'a pas d'adresse

    $nbre_enregistrement--;

### SELECTIONER LES NOMS DES ENREGISTREMENT QUI ONT DES ADRESSES DANS LA MEME FAMILLE,tous OFFICE, DATE 20ANS
my $requete1="SELECT person_id, name_parsing
		FROM dep_avec_address
	where person_name <> ''
       and abs(DATEDIFF(appln_filing_date,'$appln_filing_date'))<7300
        and inpadoc_family_id=$inpadoc_family_id
      ;";
###prepare la requete sql
my  $version1 = $dbh->prepare($requete1);

###execution de la requ�te sql
$version1-> execute() || die "pb de selection : $DBI::errstr";

 %long=();# initiliser long � vide pour ne contenir que les elements d'une meme famille
while (my  ( $person_id, $name_parsing) = $version1 -> fetchrow_array)
{
     %long = ( $person_id     => "$name_parsing" );

}  #fin du 2eme while

my $requete2="SELECT person_id, name_parsing
		FROM invt_avec_address 
	where person_name <> ''
    and abs(DATEDIFF(appln_filing_date,'$appln_filing_date'))<7300
     and inpadoc_family_id=$inpadoc_family_id
      ;";
###prepare la requete sql
my  $version2 = $dbh->prepare($requete2);

###execution de la requ�te sql
$version2-> execute() || die "pb de selection : $DBI::errstr";

while (my  ( $person_id, $name_parsing) = $version2 -> fetchrow_array)
{
     %long = ( $person_id     => "$name_parsing" );

}  #fin du 2eme while

###############################################################
#            DEBUT PROGRAMME DE COMPARAISON: RAFOO            #
###############################################################


if (keys(%short)&& keys(%long)) {%resultat=(); $nb++; #si  short et long non vide
                                %resultat=&Matching(\%long,\%short);#Appel au Matching.

                                  while(($cleF,$valF)=each %resultat){
                                 @res=split(/,/, $valF);
                                 $max=$res[1];
                                  if($max>$res[1]){$max=$max;$cle_max_long=$cleF;}else{$max=$res[1];$cle_max_long=$cleF;}
                                 }
                               $cle_short=$res[0];
                                $cleGlobal++;}#FIN if DE SI LONG ET SHORT NON VIDE.
        # OLD
		# $resultatGlobal{"$cleGlobal" }= "$cle_max_long,$cle_short,$max";#sauvegarder resultat en table de hachage. elle va etre utilisee pour la recuperation des donn�es de la base

		# NEW
		$resultatGlobal1{"$cle_short"}="$cle_max_long";
		$resultatGlobal2{"$cle_short"}="$max";

} #fin le 1er while

# NEW
while(($cle1,$val1)=each %resultatGlobal1)#recuperation des resultats de la table de hachage et l'insertion dans la base de donnees.
{
	$val2=$resultatGlobal2{"$cle1"};
	     
	###############REQUETE D'INSERTION DANS SANS_ADDRESS

my $Requete_Address_Recup= <<"SQL";
UPDATE 
	dep_sans_address a, 
	(
		SELECT person_address, person_ctry_code, person_id, appln_id
		FROM (
			SELECT COUNT(*) as countaddr, person_address, person_ctry_code, person_id, appln_id
			FROM dep_avec_address 
			WHERE person_id = "$val1"
			GROUP BY person_address, person_ctry_code
			ORDER BY countaddr DESC
			LIMIT 1
		) c 
	) b
SET 
	a.address_recup = b.person_address, 
	a.ctry_code_recup = b.person_ctry_code, 
	a.sim = "$val2", 
	a.person_id_recup = b.person_id,
	a.appln_id_recup = b.appln_id,
	a.prov = "TA"
WHERE a.person_id = "$cle1";
SQL

$dbh->do($Requete_Address_Recup) or die "Impossible d'executer la requete\n";

my $Requete_Address_Recup= <<"SQL";
UPDATE 
	dep_sans_address a, 
	(
		SELECT person_address, person_ctry_code, person_id, appln_id
		FROM (
			SELECT COUNT(*) as countaddr, person_address, person_ctry_code, person_id, appln_id
			FROM invt_avec_address 
			WHERE person_id = "$val1"
			GROUP BY person_address, person_ctry_code
			ORDER BY countaddr DESC
			LIMIT 1
		) c 
	) b
SET 
	a.address_recup = b.person_address, 
	a.ctry_code_recup = b.person_ctry_code, 
	a.sim = "$val2", 
	a.person_id_recup = b.person_id,
	a.appln_id_recup = b.appln_id,
	a.prov = "TI"
WHERE a.person_id = "$cle1";
SQL

$dbh->do($Requete_Address_Recup) or die "Impossible d'executer la requete\n";

}



                        #######################meme chose pour les invts#######################""

  @res=();
 $max=" ";
 $cle_max_long=" ";
 %res=();
 $nb=0;
 $add1=0;
 $add2=0;
 
 %resultatGlobal=();
 %resultatGlobal1=();
 %resultatGlobal2=();
 $cleGlobal=0;

#########################################################################
#           MEME FAMILLE, tt OFFICE, tt TYPE D'ACTEURS , date=20 ans    #
#########################################################################
##########I.1:   MEME FAMILLE, TT OFFICE, TT TYPE D'ACTEURS
print"==> MEME FAMILLE, TOUS OFFICES, TOUS TYPES D'ACTEURS, DATE=20 ANS\n";


################ refaire la boucle au tant de $nbre_enregistrement

my $req="SELECT count(person_id)FROM invt_sans_address
     where person_name <> ''
     AND address_recup is not null
     AND address_recup='';";
###pr�pare la requ�te sql
my  $version0 = $dbh->prepare($req);

###ex�cution de la requ�te sql
$version0-> execute() || die "pb de selection : $DBI::errstr";
@nbre_famille = $version0->fetchrow_array;
$nbre_enregistrement=$nbre_famille[0];


###SELECTIONER LES NOMS DES ENREGISTREMENT QUI N'ONT PAS D'ADREESSES DANS LA MEME FAMILLE
my $requete="SELECT   inpadoc_family_id, appln_auth, appln_filing_date, person_id, name_parsing
		FROM invt_sans_address
		where person_name <> ''
             and address_recup=''
      order by inpadoc_family_id;";
###pr�pare la requ�te sql
my  $version = $dbh->prepare($requete);

###ex�cution de la requ�te sql
$version-> execute() || die "pb de selection : $DBI::errstr";

# il faut remplacer ce nombre par le nombre d'applications sans @ dans une famille: $nbre_enregistrement
while ((my  ( $inpadoc_family_id, $appln_auth,  $appln_filing_date, $person_id,$name_parsing) = $version -> fetchrow_array)&&($nbre_enregistrement!=0))
{
	%short=();
   %short = ( $person_id     => "$name_parsing" );    #remplir short par le nom qui n'a pas d'adresse
 
    $nbre_enregistrement--;

###############################SELECTIOONER LES NOMS DES ENREGISTREMENT QUI ONT DES ADREESSES DANS LA MEME FAMILLE,tous OFFICE, DATE 20ANS#####################################################
my $requete1="SELECT person_id, name_parsing
		FROM dep_avec_address 
	where person_name <> ''
     and abs(DATEDIFF(appln_filing_date,'$appln_filing_date'))<7300
     and inpadoc_family_id=$inpadoc_family_id
      ;";
###pr�pare la requ�te sql
my  $version1 = $dbh->prepare($requete1);

###ex�cution de la requ�te sql
$version1-> execute() || die "pb de selection : $DBI::errstr";
 %long=();# initiliser long � vide pour ne contenir que les elements d'une meme famille
while (my  ( $person_id, $name_parsing) = $version1 -> fetchrow_array)
{
  
   %long = ( $person_id     => "$name_parsing" );
 
}  #fin du 2eme while

my $requete2="SELECT person_id, name_parsing
		FROM invt_avec_address 
	where person_name <> ''
    and    abs(DATEDIFF(appln_filing_date,'$appln_filing_date'))<7300
     and inpadoc_family_id=$inpadoc_family_id
      ;";
###pr�pare la requ�te sql
my  $version2 = $dbh->prepare($requete2);

###ex�cution de la requ�te sql
$version2-> execute() || die "pb de selection : $DBI::errstr";

while (my  ( $person_id, $name_parsing) = $version2 -> fetchrow_array)
{
  
   %long = ( $person_id     => "$name_parsing" );
 
}  #fin du 2eme while

##################################################################

###############################################################
#            DEBUT PROGRAMME DE COMPARAISON: RAFOO            #
###############################################################


if (keys(%short)&& keys(%long)) {%resultat=(); $nb++; #si  short et long non vide
                                %resultat=&Matching(\%long,\%short);#Appel au Matching.

                                  while(($cleF,$valF)=each %resultat){
                                  #r�cup�ration des r�sulatat de matchinig.
                                 @res=split(/,/, $valF);
                                 $max=$res[1];
                                  if($max>$res[1]){$max=$max;$cle_max_long=$cleF;}else{$max=$res[1];$cle_max_long=$cleF;}
                                 }
                               $cle_short=$res[0];
                             
                                $cleGlobal++;}#FIN if DE SI LONG ET SHORT NON VIDE.
        
		# OLD
		# $resultatGlobal{"$cleGlobal" }= "$cle_max_long,$cle_short,$max";#sauvegarder r�sultat en table de hachage. elle va etre utilis� pour la r�cuperation des donn�es de la base

		# NEW
		$resultatGlobal1{"$cle_short"}="$cle_max_long";
		$resultatGlobal2{"$cle_short"}="$max";

} #fin le 1er while

# NEW
while(($cle1,$val1)=each %resultatGlobal1)#recuperation des resultats de la table de hachage et l'insertion dans la base de donnees.
{
	$val2=$resultatGlobal2{"$cle1"};
	     
	###############REQUETE D'INSERTION DANS SANS_ADDRESS

my $Requete_Address_Recup= <<"SQL";
UPDATE 
	invt_sans_address a, 
	(
		SELECT person_address, person_ctry_code, person_id, appln_id
		FROM (
			SELECT COUNT(*) as countaddr, person_address, person_ctry_code, person_id, appln_id
			FROM dep_avec_address 
			WHERE person_id = "$val1"
			GROUP BY person_address, person_ctry_code
			ORDER BY countaddr DESC
			LIMIT 1
		) c 
	) b
SET 
	a.address_recup = b.person_address, 
	a.ctry_code_recup = b.person_ctry_code, 
	a.sim = "$val2", 
	a.person_id_recup = b.person_id,
	a.appln_id_recup = b.appln_id,
	a.prov = "TA"
WHERE a.person_id = "$cle1";
SQL

$dbh->do($Requete_Address_Recup) or die "Impossible d'executer la requete\n";

my $Requete_Address_Recup= <<"SQL";
UPDATE 
	invt_sans_address a, 
	(
		SELECT person_address, person_ctry_code, person_id, appln_id
		FROM (
			SELECT COUNT(*) as countaddr, person_address, person_ctry_code, person_id, appln_id
			FROM invt_avec_address 
			WHERE person_id = "$val1"
			GROUP BY person_address, person_ctry_code
			ORDER BY countaddr DESC
			LIMIT 1
		) c 
	) b
SET 
	a.address_recup = b.person_address, 
	a.ctry_code_recup = b.person_ctry_code, 
	a.sim = "$val2", 
	a.person_id_recup = b.person_id,
	a.appln_id_recup = b.appln_id,
	a.prov = "TI"
WHERE a.person_id = "$cle1";
SQL

$dbh->do($Requete_Address_Recup) or die "Impossible d'executer la requete\n";

}

   
   
########POUR CONNAITRE LE NOMBRE D'ADRESSE RECUPEREES
my $req_add="SELECT count(appln_id)FROM dep_sans_address a
where a.address_recup is not null
and a.address_recup<>''
;";
###pr�pare la requ�te sql
my  $version = $dbh->prepare($req_add);

###ex�cution de la requ�te sql
$version-> execute() || die "pb de selection : $DBI::errstr";

########################################################################

my $req_add2="SELECT count(appln_id)FROM invt_sans_address b
where b.address_recup is not null
and b.address_recup<>'';";
###pr�pare la requ�te sql
my  $version2 = $dbh->prepare($req_add2);
$version2-> execute() || die "pb de selection : $DBI::errstr";


@add1 = $version->fetchrow_array;
$add1=$add1[0];
@add2 = $version2->fetchrow_array;
$add2=$add2[0];
###############

 print "==> SUR $nb COMPARAISONS,  TOTAL ADRESSES RECUPEREES DEP :$add1  INVT :$add2\n\n\n";

  print "ETAPE 3 - Matching tout confondu - fin :", `date`;
