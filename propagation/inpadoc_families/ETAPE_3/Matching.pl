#!/usr/local/bin/perl
require "ETAPE_3/Raffo/2-gram.pl";
require "ETAPE_3/Raffo/jaccard.pl";
require "ETAPE_3/Raffo/create_index_long.pl";
require "ETAPE_3/Raffo/create_index_weight.pl";
require "ETAPE_3/Raffo/create_long_weights.pl";
require "ETAPE_3/Raffo/create_short_index.pl";
require "ETAPE_3/Raffo/apply_index.pl";
require "ETAPE_3/Raffo/calculate_matches.pl";
sub Matching
{
$long=shift;
 $short=shift;
%resultat=();
$similarity=0;


#######################******TRAITEMENT DE LONG**********############
@t= &bigram(\%long);
##suppression des doublons de mon tableau @t
my (%saw,@t_sans_doublons)=();
undef %saw;
@t_sans_doublons = sort(grep(!$saw{$_}++, @t));

####appel indexing
%Long_idx=&create_index_long(\%long,@t_sans_doublons);
%index_wgt=&create_index_weight(\%Long_idx);

 %long_wgt= &create_long_weights(\%long,\%index_wgt);


#######################******TRAITEMENT DE SHORT**********############

%short_wgt=&create_short_index(\%short,\%index_wgt);



#######################******INTERSECTION ENTRE LONG ET SHORT**********############
for $k (keys %short_wgt){if($k ne "poid"){ #Recuperer le tableau qui contient la chaine where cle!=poid
        $Key=$k;
        @short_Key=@{$short_wgt{$Key}};}}  #le tableau est @short_Key.
        
%Idx_Merges=&apply_index(\%Long_idx,@short_Key);# l'intersection entre LONG et SHORT

for $k (keys %short_wgt){if($k ne "poid"){@bigram_short=@{$short_wgt{$k}};}}
%Matche=&calculate_matches(\%Idx_Merges,\%index_wgt,@bigram_short);


 while(($cle,$val)=each %Matche){
$similarity=&jaccard($val,@{$short_wgt{'poid'}},$long_wgt{$cle});#  la distance de jaccard.
$similarity=int((10**4)*$similarity + 0.5) / (10**4);#limiter les chiffre apres la virgule a 4.

   if($similarity>=0.35){

     if($similarity==1)#si $similarity=1 arret ne pas terminer.
      {$resultat{$cle}= $Key.','.$similarity;  #le resutlat est une table de hachage ou sa cl� =cl� de chaque chaine dans long
                                        #et la valeur =cl� de la chaine dans short, la similarity
       %long=();
       %short=();
      $similarity=0;
       return %resultat; }

     $resultat{$cle}= $Key.','.$similarity;  #le resutlat est une table de hachage ou sa cl� =cl� de chaque chaine dans long
                                        #et la valeur =cl� de la chaine dans short, la similarity
                        }
 }
 %long=();
 %short=();
$similarity=0;
 return %resultat;
 }
 1;

