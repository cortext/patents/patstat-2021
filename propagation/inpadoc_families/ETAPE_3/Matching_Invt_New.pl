#!/usr/local/bin/perl

require "ETAPE_3/Matching.pl";

 @res=();
 $max=" ";
 $cle_max_long=" ";
 %res=();
 $nb=0;
 $add=0;
 %resultatGlobal=();
 
 # NEW
 %resultatGlobal1=();
 %resultatGlobal2=();
	
 $cleGlobal=0;

print "ETAPE 3 - Matching invt - debut :", `date`;
#########################################################################
#             MEME FAMILLE, MEME OFFICE, MEME TYPE D'ACTEURS             #
#########################################################################
##########I.1:   MEME FAMILLE, MEME OFFICE, MEME TYPE D'ACTEURS=INVT
print"==> MEME FAMILLE, MEME OFFICE, MEME TYPE D'ACTEURS=INVENTEURS, DATE=4 ANS\n";


################ refaire la boucle autant que de nbre_enregistrement

my $req="SELECT count(person_id)FROM invt_sans_address
     where person_name <> ''
     and address_recup IS NOT NULL
     AND address_recup='';";
### prepare la requete sql
my  $version0 = $dbh->prepare($req);

### execution de la requete sql
$version0-> execute() || die "pb de selection : $DBI::errstr";
@nbre_famille = $version0->fetchrow_array;
$nbre_enregistrement=$nbre_famille[0];


### SELECTIONER LES NOMS DES ENREGISTREMENT QUI N'ONT PAS D'ADRESSES DANS LA MEME FAMILLE
my $requete="SELECT   inpadoc_family_id, appln_auth, appln_filing_date, person_id, name_parsing
		FROM invt_sans_address
		WHERE person_name <> ''
		order by inpadoc_family_id;";
###prepare la requete sql
my  $version = $dbh->prepare($requete);

###execution de la requete sql
$version-> execute() || die "pb de selection : $DBI::errstr";

# il faut remplacer ce nombre par le nombre d'applications sans @ dans une famille: $nbre_enregistrement
while ((my  ( $inpadoc_family_id, $appln_auth, $appln_filing_date,$person_id,  $name_parsing) = $version -> fetchrow_array)&&($nbre_enregistrement!=0))
{
   %short=();
   %short = ( $person_id     => "$name_parsing" );    #remplir short par le nom qui n'a pas d'adresse

    $nbre_enregistrement--;

### SELECTIONER LES NOMS DES ENREGISTREMENT QUI ONT DES ADRESSES DANS LA MEME FAMILLE,MEME OFFICE, DATE 3ANS
  my $requete1="SELECT person_id, name_parsing
  FROM invt_avec_address
  where person_name <> ''
  and   appln_auth='$appln_auth'
  and abs(DATEDIFF(appln_filing_date,'$appln_filing_date'))<1460
  and inpadoc_family_id=$inpadoc_family_id
  ;";
###prepare la requete sql
  my  $version1 = $dbh->prepare($requete1);

###execution de la requete sql
  $version1-> execute() || die "pb de selection : $DBI::errstr";


  %long=();# initialiser long � vide pour ne contenir que les elements d'une meme famille
  while (my  ($person_id,  $name_parsing) = $version1 -> fetchrow_array)
  {
	 %long = ( $person_id     => "$name_parsing" )
  }  #fin du 2eme while


###############################################################
#            DEBUT PROGRAMME DE COMPARAISON: RAFO            #
###############################################################


   if (keys(%short)&& keys(%long)) 
   {
	%resultat=(); $nb++; #si  short et long non vide
        %resultat=&Matching(\%long,\%short);#Appel au Matching.

        while(($cleF,$valF)=each %resultat)
	{
	    @res=split(/,/, $valF);
            $max=$res[1];
            if($max>$res[1])
		{
		$max=$max;$cle_max_long=$cleF;
		}
	else
		{
			$max=$res[1];
			$cle_max_long=$cleF;
		}
        }
        $cle_short=$res[0];
                           
        $cleGlobal++;
	}#FIN if DE SI LONG ET SHORT NON VIDE.

	# OLD
	#$resultatGlobal{"$cleGlobal" }= "$cle_max_long,$cle_short,$max";#sauvegarder resultat en table de hachage. elle va etre utilise pour la recuperation des donnees de la base
	
	# NEW
	$resultatGlobal1{"$cle_short"}="$cle_max_long";
	$resultatGlobal2{"$cle_short"}="$max";
	
} #fin le 1er while

# NEW
while(($cle1,$val1)=each %resultatGlobal1)#recuperation des resultats de la table de hachage et l'insertion dans la base de donnees.
{
	$val2=$resultatGlobal2{"$cle1"};
	     
	###############REQUETE D'INSERTION DANS SANS_ADDRESS

my $Requete_Address_Recup= <<"SQL";
UPDATE 
	invt_sans_address a, 
	(
		SELECT person_address, person_ctry_code, person_id, appln_id
		FROM (
			SELECT COUNT(*) as countaddr, person_address, person_ctry_code, person_id, appln_id
			FROM invt_avec_address 
			WHERE person_id = "$val1"
			GROUP BY person_address, person_ctry_code
			ORDER BY countaddr DESC
			LIMIT 1
		) c 
	) b
SET 
	a.address_recup = b.person_address, 
	a.ctry_code_recup = b.person_ctry_code, 
	a.sim = "$val2", 
	a.person_id_recup = b.person_id,
	a.appln_id_recup = b.appln_id,
	a.prov = "I"
WHERE a.person_id = "$cle1";
SQL

$dbh->do($Requete_Address_Recup) or die "Impossible d'executer la requete\n";

}
  


########POUR CONNAITRE LE NOMBRE D'ADRESSE RECUPEREES
my $req_add="SELECT count(appln_id)FROM invt_sans_address
where address_recup is not null
and address_recup<>'';";
###prepare la requete sql
my  $version = $dbh->prepare($req_add);

###execution de la requete sql
$version-> execute() || die "pb de selection : $DBI::errstr";
@add = $version->fetchrow_array;
$add=$add[0];
###############

 print "==> POUR LES INVENTEURS: SUR $nb COMPARAISONS,  TOTAL ADRESSES RECUPEREES $add\n\n";

print "ETAPE 3 - Matching invt - fin :", `date`;
