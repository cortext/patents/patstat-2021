-- ---------------------------------------------------------------------------------
-- Slightly adapted from the MySQL scripts applied in version 2017
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;


DROP TABLE IF EXISTS save_applt_adr_ifris_epwofrjp;

CREATE TABLE save_applt_adr_ifris_epwofrjp AS
SELECT * FROM applt_adr_ifris_epwofrjp;

DROP TABLE IF EXISTS save_invt_adr_ifris_epwofrjp;

CREATE TABLE save_invt_adr_ifris_epwofrjp AS
SELECT * FROM invt_adr_ifris_epwofrjp;

CREATE INDEX idx_address_recup ON dep_sans_address (address_recup(100));

CREATE INDEX idx_person_id_recup ON dep_sans_address (person_id_recup);

DROP INDEX `idx_source` ON applt_adr_ifris_epwofrjp;

DROP TABLE IF EXISTS tmp_dep;

CREATE TABLE tmp_dep AS
SELECT
    DISTINCT address_recup,
    ctry_code_recup,
    person_id_recup
FROM
    dep_sans_address
WHERE
    address_recup != '';

CREATE INDEX idx_1 ON tmp_dep (person_id_recup);

UPDATE
    applt_adr_ifris_epwofrjp a,
    tmp_dep b
SET
    a.source = 'PROPAG',
    a.person_address = b.address_recup,
    a.person_ctry_code = b.ctry_code_recup
WHERE
    a.person_id = b.person_id_recup;

DROP INDEX `idx_source` ON invt_adr_ifris_epwofrjp;

DROP TABLE IF EXISTS tmp_inv;

CREATE TABLE tmp_inv AS
SELECT
    DISTINCT address_recup,
    ctry_code_recup,
    person_id_recup
FROM
    invt_sans_address
WHERE
    address_recup != '';

CREATE INDEX idx_2 ON tmp_inv (person_id_recup);

UPDATE
    invt_adr_ifris_epwofrjp a,
    tmp_inv b
SET
    a.source = 'PROPAG',
    a.person_address = b.address_recup,
    a.person_ctry_code = b.ctry_code_recup
WHERE
    a.person_id = b.person_id_recup;

DROP TABLE IF EXISTS tmp_dep;

CREATE TABLE tmp_dep AS
SELECT
    DISTINCT address_recup,
    ctry_code_recup,
    person_id
FROM
    dep_sans_address
WHERE
    address_recup != '';

ALTER TABLE
    tmp_dep
ADD
    PRIMARY KEY PK_id (person_id);

UPDATE
    applt_adr_ifris_epwofrjp a
    INNER JOIN tmp_dep b ON (a.person_id = b.person_id)
SET
    a.source = 'PROPAG',
    a.person_address = b.address_recup,
    a.person_ctry_code = b.ctry_code_recup;

DROP TABLE IF EXISTS tmp_inv;

CREATE TABLE tmp_inv AS
SELECT
    DISTINCT address_recup,
    ctry_code_recup,
    person_id
FROM
    invt_sans_address
WHERE
    address_recup != '';

ALTER TABLE
    tmp_inv
ADD
    PRIMARY KEY PK_id_inv (person_id);

UPDATE
    invt_adr_ifris_epwofrjp a
    INNER JOIN tmp_inv b ON (a.person_id = b.person_id)
SET
    a.source = 'PROPAG',
    a.person_address = b.address_recup,
    a.person_ctry_code = b.ctry_code_recup;

CREATE INDEX idx_person_address ON invt_adr_ifris_epwofrjp (person_address(100));

CREATE INDEX idx_source ON invt_adr_ifris_epwofrjp (source);

CREATE INDEX idx_person_ctry_code ON invt_adr_ifris_epwofrjp (person_ctry_code);

CREATE INDEX idx_person_address ON applt_adr_ifris_epwofrjp (person_address(100));

CREATE INDEX idx_source ON applt_adr_ifris_epwofrjp (source);

CREATE INDEX idx_person_ctry_code ON applt_adr_ifris_epwofrjp (person_ctry_code);

UPDATE
    applt_adr_ifris_epwofrjp
SET
    source = 'PATSTAT'
WHERE
    source = ''
    AND person_address != '';

UPDATE
    applt_adr_ifris_epwofrjp
SET
    source = 'MISSING'
WHERE
    source = ''
    AND person_address = '';

UPDATE
    invt_adr_ifris_epwofrjp
SET
    source = 'PATSTAT'
WHERE
    source = ''
    AND person_address != '';

UPDATE
    invt_adr_ifris_epwofrjp
SET
    source = 'MISSING'
WHERE
    source = ''
    AND person_address = '';

ALTER TABLE
    `applt_adr_ifris_epwofrjp` RENAME TO `applt_addr_ifris`;

ALTER TABLE
    `invt_adr_ifris_epwofrjp` RENAME TO `invt_addr_ifris`;