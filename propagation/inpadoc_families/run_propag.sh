#!/bin/sh

# ETAPE 1, 2 and 3
perl Script.pl  >> log.txt 2>&1 &&

# Update
mysql --defaults-file=~/mycnf.cnf -vvv < UPDATE/update_data.sql  > UPDATE/update_data_report.txt 2>&1 &&

# Verify
mysql --defaults-file=~/mycnf.cnf -vvv < VERIF/propag.sql  > VERIF/propag_report.txt 2>&1 &&

# Stats

perl VERIF/stat_dep.pl  > VERIF/stat_dep_report.txt 2>&1 &&

perl VERIF/stat_invt.pl  > VERIF/stat_invt_report.txt 2>&1 &&

# Update type after propagation

mysql --defaults-file=~/mycnf.cnf -vvv < ajout_donnees_base/MySql_Update_TypeAppln.sql  > ajout_donnees_base/MySql_Update_TypeAppln_report.txt 2>&1 &&

mysql --defaults-file=~/mycnf.cnf -vvv < ajout_donnees_base/MySql_Update_Nbperson.sql  > ajout_donnees_base/MySql_Update_Nbperson_report.txt 2>&1 &&

# Add new column after propagation

mysql --defaults-file=~/mycnf.cnf -vvv < ajout_donnees_base/MySql_invt_seq_nr.sql  > ajout_donnees_base/MySql_invt_seq_nr_report.txt 2>&1 &&

# Tests, verify

mysql --defaults-file=~/mycnf.cnf -vvv < tests_verif/verif_ajout.sql > tests_verif/verif_ajout_report.txt 2>&1 &