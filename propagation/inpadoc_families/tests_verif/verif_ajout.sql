-- ---------------------------------------------------------------------------------
-- Slightly adapted from the MySQL scripts applied in version 2017
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

SELECT count(*) FROM patstat2021_raw_data.tls201_appln;
SELECT count(*) FROM tls201_appln;

SELECT count(*) FROM patstat2021_raw_data.tls211_pat_publn;
SELECT count(*) FROM tls211_pat_publn;

SELECT count(*) FROM patstat2021_raw_data.tls204_appln_prior;
SELECT count(*) FROM tls204_appln_prior;

SELECT DISTINCT appln_filing_year FROM tls201_appln;

SELECT
	no_appt_invt,count(*)
FROM	tls201_appln
GROUP BY no_appt_invt
;

SELECT
        no_ipc,count(*)
FROM    tls201_appln
GROUP BY no_ipc
;

SELECT
        artificial,count(*)
FROM    tls201_appln
GROUP BY artificial
;


SELECT
        singleton,count(*)
FROM    tls201_appln
GROUP BY singleton
;

SELECT
        transnat,count(*)
FROM    tls201_appln
GROUP BY transnat
;
