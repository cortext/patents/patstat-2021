#!/usr/bin/perl

require "Fonctions/fonctions.pl";

print"         ######################################################\n";
print"         #              SCRIPT PROPAGATION DES ADRESSES       # \n";
print"         ###################################################### \n\n\n";

require "Includes/connexion.pl";

print"      ####################################################################\n";
print"      #      ETAPE I: CREATION DES TABLES, SELECTION DES DONNEES         #\n";
print"      ####################################################################\n\n";
print "==> CREATION  ET REMPLISSAGES DES TABLES EN COURS...\n";

require "ETAPE_1/table.pl";

require "ETAPE_1/selection_donnees.pl";

print"     ######################################################\n";
print"     #                    ETAPE II: PARSING               #\n";
print"     ######################################################\n\n";
print "==> PARSING EN COURS...\n";

require "ETAPE_2/parsing.pl";

print"     ######################################################\n";
print"     # ETAPE III: MATCHING UTILISATION DE L'ALGO DE RAFFO #\n";
print"     ######################################################\n\n";

###### etapeIII.1: Selection des applications de la meme famille, et mm office, mm type d'acteurs et moins de 4 ans(remplir la table de hachage short et long)

require "ETAPE_3/Matching_Invt_New.pl"; # type d'acteurs=INVT

require "ETAPE_3/Matching_Dep_New.pl";  # type d'acteurs=APPLT

###### etapeIII.2: Selection des applications de la meme famille, tt office, tt type d'acteurs et moins de 20 ans(remplir la table de hachage short et long)

require "ETAPE_3/Matching_tt_auth_New.pl";

print"          ##########################################\n";
print"          #               FIN PROGRAMME            #\n";
print"          ##########################################\n\n";

