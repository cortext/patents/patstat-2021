# Propagation of the inpadoc families

## Scripts version 2017 

The scripts used for the propagation in Patstat 2017 are described in the [documentation](../../2018-12-20-ST-Mise_en_base-PATSTAT.pdf), and in the [scripts log file](../../scripts_log_patstat_2017.xlsx). They were created by old members.

### `Script.pl` (ETAPE 1, 2 and 3) steps description
1. Import the functions that will be used:
   - trim, which will remove the leading and trailing spaces in the string.
   - NomPatstatStd, standardize some part of entity names. Example:  univ, tech, pharma, lab, inst...
   - NomStd, clean names removing double spaces, quotation marks, and some others that need to be checked.

2. Make the connection to the database and store the instance in the variable `$dbh`

3. ETAPE_1/table.pl 
   - Creation of the "appl_family" table which contains applications related to families
   - Creation of the "dep_sans_address" table which contains records associated with the appln of the "appl_family" table which have no address
   - Creation of the "invt_sans_address" table which contains records (Inv/address) associated with the appln of the "appl_family" table which have no address
   - Creation of the "dep_avec_address" table which contains records associated with the appln of the "appl_family" table which have addresses
   - Creation of the "invt_avec_address" table which contains records associated with the appln of the "appl_family" table which have addresses

4. ETAPE_1/selection_donnees.pl
   *Check the tablenames and the fields used as they might have changed from the previous to the version that will be treated this time.*
   - Insertion in the appl_family TABLE: applications of the same families
   - Insertion into the dep_sans_address TABLE: records associated with appln in the appl_family table that have no address
   - Insertion in the TABLE invt_sans_address: records associated with the appln of the "appl_family" table which have no address
   - Insertion into the dep_avec_address TABLE: records associated with the appln of the "appl_family" table which have addresses
   - Insertion in the TABLE invt_avec_address: records associated with appln of the "appl_family" table which have addresses
   
   Tables affected:
   - appl_family
   - dep_sans_address
   - invt_sans_address
   - dep_avec_address
   - invt_avec_address

5. ETAPE_2/parsing.pl.
   - Parsing names from invt_sans_address -> *this section is commented so it needs to be discussed if it will be included or not in this version*.
   - Parsing names from invt_avec_address -> *same as previous point*
   - Parsing names from dep_avec_addres
   - Parsing names from dep_sans_addresss

These last steps below are related to the patent matching algorithm by Raffo, that's why they all use `ETAPE_3/Matching.pl`.

6. ETAPE_3/Matching_Invt_New.pl
   - I.1: same family, same office, same type of actors=invt
   - Select names from records that do not have addresses in the same family
   - Select the names of the records that have addresses in the same family, same office, date 3 years
   - Comparison matching, calling Matching.pl (Raffo algorithm)
   - Update table invt_sans_address
     - `prov` field value set to `I`, which means the address data recovered (meeting some conditions) comes from the `I`nventors with address (invt_avec_address)*.
   - Count the number of recovered addresses

7. ETAPE_3/Matching_Dep_New.pl
   - I.1: same family, same office, same type of actors=dep
   - Select names from records that do not have addresses in the same family
   - Select the names of the records that have addresses in the same family, same office, date 3 years
   - Comparison matching, calling Matching.pl (Raffo algorithm)
   - Update table dep_sans_address
     - `prov` field value set to `A`, which means the address data recovered (meeting some conditions) comes from the `A`ctors with address (dep_avec_address)*.
   - Count the number of recovered addresses

8. ETAPE_3/Matching_tt_auth_New.pl
   - I.1: same family, tt office, tt type of actors, date=20 years
   - Select names from records that do not have addresses in the same family
   - Select the names of the records that have addresses in the same family, all office, date 20 years
   - Comparison matching, calling Matching.pl (Raffo algorithm)
   - Same previous steps for inventors
   - Update tables 
     - `prov` field value set to `TA`, which means the address data recovered (meeting some conditions) was obtained after the step 6 and 7 and comes from the `A`ctors with address (dep_avec_address)*.
     - `prov` field value set to `TI`, which means the address data recovered (meeting some conditions) was obtained after the step 6 and 7 and comes from the `I`nventors with address (invt_avec_address)*.
   - Count the number of recovered addresses

\* Field `prov` possibly means provenance. This info was a conclusion after an analysis as there is no documentation about this for the old version scripts.

## Scripts version 2021 

As the same propagation needs to be run for the current version, the scripts are *slightly modified* just in order to make them work, regarding changes in database entity names, types compatibility, and some others. Most of the script names and the folders distribution remain the same as the original ones to avoid problems.

### **Run propagation**

Install perl modules:
```sh
sudo cpan DBD::mysql
```

Export the path to the containing folder, so the perl modules inside can be imported properly:
```sh
export PERL5LIB=/home/patstat-2021/propagation/inpadoc_families
```

- Run
```sh
chmod +x run_propag.sh
./run_propag.sh
```

#### ***Steps description***

- ETAPE 1, 2 and 3: already described here in the [`Script.pl` (ETAPE 1, 2 and 3) steps description section](#scriptpl-etape-1-2-and-3-steps-description)
  - In the `ETAPE 3` it was found that an update query was taking too long, so an [issue](https://gitlab.com/cortext/patents/patstat/-/issues/45) was created to analyze and try to solve this problem. A new query was introduced in this step, so the recovered address were taken based on a defined criteria (the most repeated one), instead of just whichever. However, to avoid executing the propagation from the start, after the query was changed it was re-run again only from this step. Which means the addresses already recovered in the old way in the applicants table (`dep_sans_address`) remain like that.

- Update: makes a backup of the currently used tables for applicants and inventors, then updates them setting the `source` field accordingly to some conditions, and rename them to `applt_addr_ifris` and `invt_addr_ifris` respectively.

- Verify: [Propagation report file](VERIF/propag_report.txt)
    |                                       | **From actors without address** | **From inventors without address** |
    |---------------------------------------|---------------------------------|------------------------------------|
    | **Total**                             | 51.814.325 (100 %)              | 109.434.222 (100 %)                |
    | Address recovered and not singleton   | 4.790.172 (9.24 %)              | 7.937.015 (7.25 %)                 |
    | Address recovered and singleton       | 9.899.633 (19.10 %)             | 28.530.348 (26.07 %)               |
    | Address still empty and singleton     | 32.541.003 (62.80 %)            | 64.441.277 (58.88 %)               |
    | Address still empty and not singleton | 4.583.517 (8.84 %)              | 8.525.582 (7.79 %)                 |

- Stats: 
  - [Stats for applicants](VERIF/stat-dep.txt)
  - [Stats for inventors](VERIF/stat-invt.txt)

- Update type after propagation
  - Sets `no_appt_invt` to `0` by default and to `1` for the applications without applicants nor inventors associated.
  - Sets `no_ipc` to `0` by default and to `1` for the applications without technology associated.
  - Update `nb_applt` and `nb_invt` with the count of actor applicants and inventors respectively in the table `tls201_appln`.
- Add new column after propagation
  - Add `invt_seq_nr` for both applicants and inventors tables, bringing the data from `tls207_pers_appln` table.
- Tests, verify
  - Counting what was added and updated.
