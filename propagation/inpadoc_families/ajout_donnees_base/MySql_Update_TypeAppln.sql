-- ---------------------------------------------------------------------------------
-- Slightly adapted from the MySQL scripts applied in version 2017
-- ---------------------------------------------------------------------------------
-- Generation Time: Mar 24, 2010
--
-- Database: `patstat`
-- Apr�s les ajouts regpat et inpi il faut updater les colonnes sans inv ni dep et sans ipc
-- --------------------------------------------------------
USE patstat2021_staging;

CREATE TABLE tls201_appln_before_updatepropag AS
SELECT
  *
FROM
  tls201_appln;

-- Update de la colonne permettant d'identifier les applications sans d�posant et sans inventeur
UPDATE
  tls201_appln a
SET
  no_appt_invt = 0;

UPDATE
  tls201_appln a
SET
  no_appt_invt = 1
WHERE
  NOT EXISTS (
    SELECT
      NULL
    FROM
      applt_addr_ifris b
    WHERE
      a.appln_id = b.appln_id
  )
  AND NOT EXISTS (
    SELECT
      NULL
    FROM
      invt_addr_ifris c
    WHERE
      a.appln_id = c.appln_id
  );

-- Update de la colonne pour les applications sans technologie
UPDATE
  tls201_appln a
SET
  no_ipc = 0;

UPDATE
  tls201_appln a
SET
  no_ipc = 1
WHERE
  NOT EXISTS (
    SELECT
      NULL
    FROM
      tls209_appln_ipc_ifris_epwofr b
    WHERE
      a.appln_id = b.appln_id
  );