-- ---------------------------------------------------------------------------------
-- Slightly adapted from the MySQL scripts applied in version 2017
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

ALTER TABlE applt_addr_ifris ADD invt_seq_nr smallint(4) DEFAULT NULL;

UPDATE	applt_addr_ifris a, tls207_pers_appln b
SET	a.invt_seq_nr = b.invt_seq_nr
WHERE	a.appln_id = b.appln_id
AND	a.person_id = b.person_id
;

ALTER TABlE invt_addr_ifris ADD invt_seq_nr smallint(4) DEFAULT NULL;

UPDATE  invt_addr_ifris a, tls207_pers_appln b
SET     a.invt_seq_nr = b.invt_seq_nr
WHERE   a.appln_id = b.appln_id
AND     a.person_id = b.person_id
;

