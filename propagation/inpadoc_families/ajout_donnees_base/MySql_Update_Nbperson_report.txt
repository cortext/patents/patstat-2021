--------------
UPDATE  tls201_appln
SET 	nb_applt = 0,
	nb_invt=0
--------------

Query OK, 89188936 rows affected (56 min 9.41 sec)
Rows matched: 111116943  Changed: 89188936  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_applt
--------------

Query OK, 0 rows affected (0.59 sec)

--------------
CREATE TABLE tmp_applt AS
SELECT
	a.appln_id,
	count(distinct b.person_id)	AS	nb_applt
FROM	tls201_appln a,  
	applt_addr_ifris b
WHERE   a.appln_id = b.appln_id
GROUP BY	a.appln_id
--------------

Query OK, 88465138 rows affected (9 min 7.91 sec)
Records: 88465138  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE tmp_applt ADD PRIMARY KEY PB_nb_applt (appln_id)
--------------

Query OK, 88465138 rows affected (4 min 0.09 sec)
Records: 88465138  Duplicates: 0  Warnings: 0

--------------
UPDATE	tls201_appln a,  tmp_applt b
SET	a.nb_applt = b.nb_applt
WHERE	a.appln_id = b.appln_id
--------------

Query OK, 88465138 rows affected (1 hour 3 min 52.17 sec)
Rows matched: 88465138  Changed: 88465138  Warnings: 0

--------------
DROP TABLE IF EXISTS tmp_invt
--------------

Query OK, 0 rows affected (2.03 sec)

--------------
CREATE TABLE tmp_invt AS
SELECT
        a.appln_id,
        count(distinct b.person_id)     AS      nb_invt
FROM    tls201_appln a,
        invt_addr_ifris b
WHERE   a.appln_id = b.appln_id
GROUP BY        a.appln_id
--------------

Query OK, 82308279 rows affected (19 min 51.42 sec)
Records: 82308279  Duplicates: 0  Warnings: 0

--------------
ALTER TABLE tmp_invt ADD PRIMARY KEY PB_nb_invt (appln_id)
--------------

Query OK, 82308279 rows affected (3 min 28.29 sec)
Records: 82308279  Duplicates: 0  Warnings: 0

--------------
UPDATE  tls201_appln a,  tmp_invt b
SET     a.nb_invt = b.nb_invt
WHERE   a.appln_id = b.appln_id
--------------

Query OK, 82308279 rows affected (1 hour 1.96 sec)
Rows matched: 82308279  Changed: 82308279  Warnings: 0

Bye
