-- ---------------------------------------------------------------------------------
-- Slightly adapted from the MySQL scripts applied in version 2017
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

UPDATE
  tls201_appln
SET
  nb_applt = 0,
  nb_invt = 0;

DROP TABLE IF EXISTS tmp_applt;

CREATE TABLE tmp_applt AS
SELECT
  a.appln_id,
  count(DISTINCT b.person_id) AS nb_applt
FROM
  tls201_appln a,
  applt_addr_ifris b
WHERE
  a.appln_id = b.appln_id
GROUP BY
  a.appln_id;

ALTER TABLE
  tmp_applt
ADD
  PRIMARY KEY PB_nb_applt (appln_id);

UPDATE
  tls201_appln a,
  tmp_applt b
SET
  a.nb_applt = b.nb_applt
WHERE
  a.appln_id = b.appln_id;

DROP TABLE IF EXISTS tmp_invt;

CREATE TABLE tmp_invt AS
SELECT
  a.appln_id,
  count(DISTINCT b.person_id) AS nb_invt
FROM
  tls201_appln a,
  invt_addr_ifris b
WHERE
  a.appln_id = b.appln_id
GROUP BY
  a.appln_id;

ALTER TABLE
  tmp_invt
ADD
  PRIMARY KEY PB_nb_invt (appln_id);

UPDATE
  tls201_appln a,
  tmp_invt b
SET
  a.nb_invt = b.nb_invt
WHERE
  a.appln_id = b.appln_id;