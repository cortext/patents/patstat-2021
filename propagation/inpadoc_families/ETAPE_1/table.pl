#!/usr/local/bin/perl

print "\nETAPE 1 debut :", `date`,"\n";

######################################################
#              Creation des Tables et Index          #
######################################################


### I. Creation de la table "appl_family" qui contient des applications liees aux familles

$dbh->do("DROP TABLE IF EXISTS appl_family;")
    or die "Impossible de supprimer la table: appl_family\n";

my $SQLCreationTables_appl_family = <<"SQL";
CREATE TABLE appl_family
(
inpadoc_family_id int(10) NOT NULL default '0',
appln_id int(10) NOT NULL default '0',
appln_filing_date date default NULL,
appln_filing_year int(4) default NULL,
appln_first_priority_year int(4) default NULL,
appln_auth varchar(2) NOT NULL default '',
PRIMARY KEY (appln_id),
INDEX (appln_id,appln_filing_year),
INDEX (appln_id,appln_filing_date),
INDEX (inpadoc_family_id),
INDEX (appln_first_priority_year)
);
SQL
$dbh->do($SQLCreationTables_appl_family) or die "Impossible de creer la table: appl_family\n";
print "==> TABLE appl_family CREE\n";


### II. Creation de la table "dep_sans_address" qui contient des enregistrements associees aux appln de la table "appl_family" qui n'ont pas d'adresse

$dbh->do("DROP TABLE IF EXISTS dep_sans_address;")
    or die "Impossible de supprimer la table :dep_sans_address\n";

my $SQLCreationTables_appl_dep_sans_address = <<"SQL";
CREATE TABLE dep_sans_address
(
inpadoc_family_id int(10) NOT NULL default '0',
appln_id int(10) NOT NULL default '0',
appln_auth varchar(2) NOT NULL default '',
appln_filing_date date default NULL,
appln_filing_year int(4) default NULL,
appln_first_priority_year int(4) default NULL,
person_id int(10) NOT NULL default '0',
person_name varchar(300) NOT NULL,
person_ctry_code varchar(3) NOT NULL default '',
person_address varchar(500) default null,

sim float NOT NULL default '0',
address_recup varchar(500) default null,
ctry_code_recup varchar(3) NOT NULL default '',
name_parsing varchar(300) not null,
person_id_recup int(10) NOT NULL default '0',
appln_id_recup int(10) NOT NULL default '0',
prov varchar(2) NULL,

PRIMARY KEY (appln_id,person_id),
INDEX (appln_id,appln_filing_year),
INDEX (appln_filing_year),
INDEX (appln_id,appln_filing_date),
INDEX (inpadoc_family_id),
INDEX (appln_first_priority_year),
INDEX (appln_id),
INDEX (person_id),
INDEX (person_address(10)),
INDEX (prov),
INDEX (name_parsing(20)),
INDEX (appln_auth)
);
SQL
$dbh->do($SQLCreationTables_appl_dep_sans_address) or die "Impossible de creer la table : dep_sans_address\n";
print "==> TABLE dep_sans_address CREE\n";


### III. Creation de la table "invt_sans_address" qui contient des enregistrements (Inv/address) associees aux appln de la table "appl_family" qui n'ont pas d'adresse.###

$dbh->do("DROP TABLE IF EXISTS invt_sans_address;")
    or die "Impossible de supprimer la table :invt_sans_address\n";

my $SQLCreationTables_appl_dep_sans_address = <<"SQL";
CREATE TABLE invt_sans_address
(
inpadoc_family_id int(10) NOT NULL default '0',
appln_id int(10) NOT NULL default '0',
appln_auth varchar(2) NOT NULL default '',
appln_filing_date date default NULL,
appln_filing_year int(4) default NULL,
appln_first_priority_year int(4) default NULL,
person_id int(10) NOT NULL default '0',
person_name varchar(300) NOT NULL,
person_ctry_code varchar(3) NOT NULL default '',
person_address varchar(500) default null,

sim float NOT NULL default '0',
address_recup varchar(500) default null,
ctry_code_recup varchar(3) NOT NULL default '',
name_parsing varchar(300) not null,
person_id_recup int(10) NOT NULL default '0',
appln_id_recup int(10) NOT NULL default '0',
prov varchar(2) NULL,

PRIMARY KEY (appln_id,person_id),
INDEX (appln_id,appln_filing_year),
INDEX (appln_filing_year),
INDEX (appln_id,appln_filing_date),
INDEX (inpadoc_family_id),
INDEX (appln_first_priority_year),
INDEX (appln_id),
INDEX (person_id),
INDEX (person_address(10)),
INDEX (prov),
INDEX (name_parsing(20)),
INDEX (appln_auth)
);
SQL
$dbh->do($SQLCreationTables_appl_dep_sans_address) or die "Impossible de creer la table : invt_sans_address\n";
print "==> TABLE invt_sans_address CREE\n";


### III. Creation de la table "dep_avec_address" qui contient des enregistrements associees aux appln de la table "appl_family" qui ont des adresses

$dbh->do("DROP TABLE IF EXISTS dep_avec_address;")
    or die "Impossible de supprimer la table :dep_avec_addresss\n";


my $SQLCreationTables_appl_dep_sans_address = <<"SQL";
CREATE TABLE dep_avec_address
(

inpadoc_family_id int(10) NOT NULL default '0',
appln_id int(10) NOT NULL default '0',
appln_auth varchar(2) NOT NULL default '',
appln_filing_date date default NULL,
appln_filing_year int(4) default NULL,
appln_first_priority_year int(4) default NULL,
person_id int(10) NOT NULL default '0',
person_name varchar(300) NOT NULL,
person_ctry_code varchar(3) NOT NULL default '',
person_address varchar(500) default null,
name_parsing varchar(300) not null,

PRIMARY KEY (appln_id, person_id),
INDEX (appln_id,appln_filing_year),
INDEX (appln_filing_year),
INDEX (appln_id,appln_filing_date),
INDEX (inpadoc_family_id),
INDEX (appln_first_priority_year),
INDEX (appln_id),
INDEX (person_id),
INDEX (person_address(10)),
INDEX (name_parsing(20)),
INDEX (appln_auth)
);
SQL
$dbh->do($SQLCreationTables_appl_dep_sans_address) or die "Impossible de creer la table : dep_avec_address\n";
print "==> TABLE dep_avec_address CREE\n";


### IV. Creation de la table "invt_avec_address" qui contient des enregistrements associees aux appln de la table "appl_family" qui ont des adresses

$dbh->do("DROP TABLE IF EXISTS invt_avec_address;")
    or die "Impossible de supprimer la table :invt_avec_address\n";

my $SQLCreationTables_appl_invt_sans_address = <<"SQL";
CREATE TABLE invt_avec_address
(

inpadoc_family_id int(10) NOT NULL default '0',
appln_id int(10) NOT NULL default '0',
appln_auth varchar(2) NOT NULL default '',
appln_filing_date date default NULL,
appln_filing_year int(4) default NULL,
appln_first_priority_year int(4) default NULL,
person_id int(10) NOT NULL default '0',
person_name varchar(300) NOT NULL,
person_ctry_code varchar(3) NOT NULL default '',
person_address varchar(500) default null,
name_parsing varchar(300) not null,

PRIMARY KEY (appln_id, person_id),
INDEX (appln_id,appln_filing_year),
INDEX (appln_filing_year),
INDEX (appln_id,appln_filing_date),
INDEX (inpadoc_family_id),
INDEX (appln_first_priority_year),
INDEX (appln_id),
INDEX (person_id),
INDEX (person_address(10)),
INDEX (name_parsing(20)),
INDEX (appln_auth)
);
SQL
$dbh->do($SQLCreationTables_appl_invt_sans_address) or die "Impossible de creer la table : invt_avec_address\n";
print "==> TABLE invt_avec_address CREE\n\n\n";

