# #!/usr/local/bin/perl

### INSERTION DANS LA TABLE  appl_family : applications des memes familles ###

# Desactiver les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE appl_family DISABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQLd : $DBI::errstr";

# Insertion

my $RequeteSQL = <<"SQL";
INSERT INTO appl_family
SELECT B.inpadoc_family_id, B.appln_id, B.appln_filing_date,B.appln_filing_year,B.appln_first_priority_year, B.appln_auth 
FROM tls201_appln B
ORDER BY B.inpadoc_family_id
;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";
print "==> TABLE appl_family REMPLIE\n";

# Activer les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE appl_family ENABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";



### INSERTION DANS LA TABLE  dep_sans_address : enregistrements associees aux appln de la table appl_family qui n'ont pas d'adresse. ###

# Desactiver les indexs 

my $RequeteSQL = <<"SQL";
ALTER TABLE dep_sans_address DISABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";


# Insertion

my $RequeteSQL = <<"SQL";
INSERT INTO dep_sans_address
SELECT  a.inpadoc_family_id,a.appln_id, a.appln_auth, a.appln_filing_date, a.appln_filing_year, a.appln_first_priority_year, b.person_id,
 IF(b.person_name = '', b.name_comp, b.person_name), b.ctry_comp, b.adr_comp, 0, "", "", "", 0, 0, ""  
FROM appl_family a, applt_adr_ifris_epwofrjp b
WHERE a.appln_id=b.appln_id
AND a.appln_first_priority_year=0
AND b.adr_comp=""
AND appln_filing_year >1980
AND appln_filing_year<>9999
ORDER BY a.inpadoc_family_id ;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";
print "==> TABLE dep_sans_address REMPLIE\n";

# Activer les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE dep_sans_address ENABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";


### INSERTION DANS LA TABLE  invt_sans_address: enregistrements associees aux appln de la table "appl_family qui n'ont pas d'adresse

# Desactiver les indexs #

my $RequeteSQL = <<"SQL";
ALTER TABLE invt_sans_address DISABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";


# Insertion

my $RequeteSQL = <<"SQL";
INSERT INTO invt_sans_address
SELECT a.inpadoc_family_id, a.appln_id, a.appln_auth, a.appln_filing_date, a.appln_filing_year, a.appln_first_priority_year, b.person_id,
 IF(b.person_name = '', b.name_comp, b.person_name), b.ctry_comp, b.adr_comp, 0, "", "", "", 0, 0, ""
FROM appl_family a, invt_adr_ifris_epwofrjp b
WHERE a.appln_id=b.appln_id
AND a.appln_first_priority_year=0
AND b.adr_comp=""
AND appln_filing_year >1980
AND appln_filing_year<>9999
ORDER BY a.inpadoc_family_id ;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";
print "==> TABLE invt_sans_address REMPLIE\n";

# Activer les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE invt_sans_address ENABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";


### INSERTION DANS LA TABLE  dep_avec_address: enregistrements associees aux appln de la table "appl_family qui ont des adresses

# Desactiver les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE dep_avec_address DISABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";


# Insertion

my $RequeteSQL = <<"SQL";
INSERT INTO dep_avec_address
SELECT a.inpadoc_family_id, a.appln_id, a.appln_auth, a.appln_filing_date, a.appln_filing_year, a.appln_first_priority_year, b.person_id,
  IF(b.person_name = '', b.name_comp, b.person_name), b.ctry_comp, b.adr_comp, ""  
FROM appl_family a, applt_adr_ifris_epwofrjp b
WHERE b.adr_comp<>""
AND a.appln_id=b.appln_id
ORDER BY a.inpadoc_family_id ;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";
print "==> TABLE dep_avec_address REMPLIE\n\n";


# Activer les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE dep_avec_address ENABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";


### INSERTION DANS LA TABLE  invt_avec_address: enregistrements associees aux appln de la table "appl_family qui ont des adresses

# Desactiver les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE invt_avec_address DISABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";


# Insertion

my $RequeteSQL = <<"SQL";
INSERT INTO invt_avec_address
SELECT a.inpadoc_family_id, a.appln_id, a.appln_auth, a.appln_filing_date, a.appln_filing_year, a.appln_first_priority_year, b.person_id,
  IF(b.person_name = '', b.name_comp, b.person_name), b.ctry_comp, b.adr_comp, ""  
FROM appl_family a, invt_adr_ifris_epwofrjp b
WHERE b.adr_comp<>""
AND a.appln_id=b.appln_id
ORDER BY a.inpadoc_family_id ;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";
print "==> TABLE invt_avec_address REMPLIE\n\n";


# Activer les indexs

my $RequeteSQL = <<"SQL";
ALTER TABLE invt_avec_address ENABLE KEYS;
SQL
$dbh->do($RequeteSQL) or die "Echec Requete $RequeteSQL : $DBI::errstr";

print "ETAPE 1 fin :", `date`,"\n";





