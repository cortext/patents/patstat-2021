#!/usr/local/bin/perl
require "Includes/connexion.pl";

#cr�er le fichier stat
$file="stat-dep.txt";
if (! -e $file){
		open(DESCR,">$file");
		close(DESCR);
	}
#ecrire dans le fichier stat PAR OFFICE
if(  open(FIC,"+>stat-dep.txt") )
{     print( FIC "=> Adresse par Office:\n\n" );
     print( FIC " OFFICE| ADRESSE TOTALE | ADRESSE RECUPERE\n" );
    print( FIC "--------------------------------------------\n" );

###commencer la recuperation des info
my $requete1="SELECT appln_auth, count(person_address) FROM dep_sans_address
GROUP BY appln_auth;";
###pr�pare la requ�te sql
my  $version1 = $dbh->prepare($requete1);

###ex�cution de la requ�te sql
$version1-> execute() || die "pb de selection : $DBI::errstr";



while (my  ($auth, $nbre) = $version1 -> fetchrow_array)
{
#######################
my $req="SELECT count(address_recup)FROM dep_sans_address
     where appln_auth='$auth'
     and address_recup is not null
     and address_recup<>'';";
###pr�pare la requ�te sql
my  $version0 = $dbh->prepare($req);

###ex�cution de la requ�te sql
$version0-> execute() || die "pb de selection : $DBI::errstr";
@nbre_recup = $version0->fetchrow_array;
$nbre_recup=$nbre_recup[0];

####################""

   $var="  $auth   |$nbre | $nbre_recup\n";
   print( FIC $var );
}
print( FIC "--------------------------------------------\n\n\n" );

 }
 close(FIC);



##############################3
#ecrire dans le fichier stat PAR ANNEE DE DEPOT
if(  open(FIC,">>stat-dep.txt") )
{     print( FIC "=> Adresse par ann�e de d�pot:\n\n" );
     print( FIC " ANNEE| ADRESSE TOTALE | ADRESSE RECUPERE\n" );
    print( FIC "--------------------------------------------\n" );

###commencer la recuperation des info
my $requete1="SELECT appln_filing_year, count(person_address) FROM dep_sans_address
GROUP BY appln_filing_year;";
###pr�pare la requ�te sql
my  $version1 = $dbh->prepare($requete1);

###ex�cution de la requ�te sql
$version1-> execute() || die "pb de selection : $DBI::errstr";



while (my  ($annee, $nbre) = $version1 -> fetchrow_array)
{
#######################
my $req="SELECT count(address_recup)FROM dep_sans_address
     where appln_filing_year='$annee'
     and address_recup is not null
     and address_recup<>'';";
###pr�pare la requ�te sql
my  $version0 = $dbh->prepare($req);

###ex�cution de la requ�te sql
$version0-> execute() || die "pb de selection : $DBI::errstr";
@nbre_recup = $version0->fetchrow_array;
$nbre_recup=$nbre_recup[0];

####################""

   $var="  $annee   |$nbre | $nbre_recup\n";
   print( FIC $var );
}
print( FIC "--------------------------------------------\n\n\n" );

 }
  close(FIC);

##############################3
#ecrire dans le fichier stat PAR pays d'acteurs
if(  open(FIC,">>stat-dep.txt") )
{     print( FIC "=> Adresse par pays d'acteurs:\n\n" );
     print( FIC " CTRY_CODE| ADRESSE TOTALE | ADRESSE RECUPERE\n" );
    print( FIC "--------------------------------------------\n" );

###commencer la recuperation des info
my $requete1="SELECT person_ctry_code, count(person_address) FROM dep_sans_address
GROUP BY person_ctry_code;";
###pr�pare la requ�te sql
my  $version1 = $dbh->prepare($requete1);

###ex�cution de la requ�te sql
$version1-> execute() || die "pb de selection : $DBI::errstr";



while (my  ($pays, $nbre) = $version1 -> fetchrow_array)
{
#######################
my $req="SELECT count(address_recup)FROM dep_sans_address
     where person_ctry_code='$pays'
     and address_recup is not null
     and address_recup<>'';";
###pr�pare la requ�te sql
my  $version0 = $dbh->prepare($req);

###ex�cution de la requ�te sql
$version0-> execute() || die "pb de selection : $DBI::errstr";
@nbre_recup = $version0->fetchrow_array;
$nbre_recup=$nbre_recup[0];

####################""

   $var="  $pays   |$nbre | $nbre_recup\n";
   print( FIC $var );
}
print( FIC "--------------------------------------------\n\n\n" );

 }
  close(FIC);


