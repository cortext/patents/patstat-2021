-- ---------------------------------------------------------------------------------
-- Slightly adapted from the MySQL scripts applied in version 2017
-- ---------------------------------------------------------------------------------
USE patstat2021_staging;

select count(*) 
from tls201_appln
where singleton = 1;

-- deposants

SELECT count(*) FROM dep_avec_address d;

SELECT count(*) FROM dep_sans_address d;

SELECT count(*) FROM dep_sans_address d
where address_recup!='';

SELECT count(*) FROM dep_sans_address d
where address_recup='';

select count(*) FROM dep_sans_address d
where address_recup!=''
and not exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id);

select count(*) FROM dep_sans_address d
where address_recup!=''
and exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id);

select count(*) FROM dep_sans_address d
where address_recup!=''
and exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
and appln_id!=appln_id_recup;

select prov,count(*) FROM dep_sans_address d
where address_recup!=''
and exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
group by prov;

select count(*) FROM dep_sans_address d
where address_recup=''
and  exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
;

select count(*) FROM dep_sans_address d
where address_recup=''
and  not exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
;

select prov,count(*) FROM dep_sans_address d
where address_recup!=''
and  not exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
group by prov
;


-- inventeurs

SELECT count(*) FROM invt_avec_address d;

SELECT count(*) FROM invt_sans_address d;

SELECT count(*) FROM invt_sans_address d
where address_recup!='';

SELECT count(*) FROM invt_sans_address d
where address_recup='';

select count(*) FROM invt_sans_address d
where address_recup!=''
and not exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id);

select count(*) FROM invt_sans_address d
where address_recup!=''
and exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id);

select count(*) FROM invt_sans_address d
where address_recup!=''
and exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
and appln_id!=appln_id_recup;

select prov,count(*) FROM invt_sans_address d
where address_recup!=''
and exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
group by prov;

select count(*) FROM invt_sans_address d
where address_recup=''
and  exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
;

select count(*) FROM invt_sans_address d
where address_recup=''
and  not exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
;

select prov,count(*) FROM invt_sans_address d
where address_recup!=''
and  not exists (select 1 from tls201_appln b where b.singleton = 1 and d.appln_id=b.appln_id)
group by prov
;

