USE patstat2021_staging;

UPDATE applt_addr_ifris a 
  INNER JOIN tls206_person b 
  ON a.person_id = b.person_id
SET 
  a.type = 'person'
WHERE 
  (a.type IS NULL AND b.psn_sector IN (
      'COMPANY',
      'INDIVIDUAL',
      'UNKNOWN'
    )
  ) OR 
  (a.type = 'legal' AND b.psn_sector = '') OR 
  (a.type = 'person' AND b.psn_sector IN (
      '',
      'INDIVIDUAL',
      'UNKNOWN'
    )
  )
;
-- Query OK, 19644 rows affected (1 hour 23 min 35.67 sec)
-- Rows matched: 17237402  Changed: 19644  Warnings: 0

UPDATE applt_addr_ifris a 
  INNER JOIN tls206_person b 
  ON a.person_id = b.person_id
SET 
  a.type = 'legal'
WHERE 
  (a.type IS NULL AND b.psn_sector IS NULL) OR 
  (a.type IS NULL AND b.psn_sector = '') OR 
  (a.type = 'legal' AND b.psn_sector IN ( 
      'COMPANY'
      'COMPANY GOV NON-PROFIT'
      'COMPANY GOV NON-PROFIT UNIVERSITY'
      'COMPANY HOSPITAL'
      'COMPANY INDIVIDUAL'
      'COMPANY UNIVERSITY'
      'GOV NON-PROFIT'
      'GOV NON-PROFIT HOSPITAL'
      'GOV NON-PROFIT UNIVERSITY'
      'HOSPITAL'
      'INDIVIDUAL'
      'UNIVERSITY'
      'UNKNOWN'
    )
  ) OR 
  (a.type = 'person' AND b.psn_sector IN (
      'COMPANY'
      'COMPANY GOV NON-PROFIT'
      'COMPANY GOV NON-PROFIT UNIVERSITY'
      'COMPANY HOSPITAL'
      'COMPANY INDIVIDUAL'
      'COMPANY UNIVERSITY'
      'GOV NON-PROFIT'
      'GOV NON-PROFIT UNIVERSITY'
      'HOSPITAL',
      'UNIVERSITY'
    )
  )
;
-- Query OK, 11851 rows affected (47 min 45.42 sec)
-- Rows matched: 11851  Changed: 11851  Warnings: 0
