# Patent applicants classification: Classifying legal entities and individuals

The steps described in this document are taken from the ones applied for Patstat version April-2017, and they are slightly modified. In a future version some of the steps can be synthesized, like steps 2, 3 and 7 that correspond to firms detection based on some common identifiers.
- [General description](https://gitlab.com/cortext/patents/patstat/-/blob/dev/Actors/applicants%20classification/README.md)
- [Complete documentation with steps explained](https://gitlab.com/cortext/patents/patstat/-/blob/dev/Actors/applicants%20classification/technical%20doc/README.md)

### 1 - First classification: creating sets for probably legal, probably person and unknown applicants.

Before running any regular expression method, we try to classify the applicants with simpler processes first: [01_first_classification.sql](01_first_classification.sql)

#### Sub-steps:
1. Inserts into the 'probably legal' table all the applicants that have the ownership of the patent in some of their applications (whose 'inventors sequence' number is 0).

    Total applicants: 11.114.275
    Total 'probably legal' inserted: 8.842.743

2. Inserts into the 'unknown' table all the applicants that do not have any ownership patent (whose 'inventors sequence' number is higher than 0).

    Total 'unknown' inserted: 2.709.300 
    Total 'unknown' removed: 437.668 

3. Inserts into the 'probably person' table all the applicants from the 'unknown' table whose source is different to 'missing' in the Patstat inventors table. 

    Total 'probably person' inserted: 2.028.488 
    Total 'unknown' removed: 2.009.217

4. Inserts into the 'probably person' table all the applicants that have only 1 patent application from the 'unknown' table.

    Total 'probably person' inserted: 195.265
    Total 'unknown' removed: 195.265

5. Inserts into the 'probably person' table all the applicants in 'probably legal' that exist in the Patstat inventors table and have less than 20 patent applications.

    Total 'probably person' inserted: 1.880.329
    Total 'probably legal' removed: 1.880.329

6. Inserts into 'probably person' from 'probably legal' table all the applicants whose person name has a structure similar to `___, ___` and whose number of patent applications is less than 200.

    Total 'probably person' inserted: 29.939
    Total 'probably legal' removed: 29.939

7. Inserts into a temporal table all the applicants that have less of 80% of the ownership of their patent applications, to then insert them into the 'probably person' table.

    Total 'probably person' inserted: 31.985
    Total 'probably legal' removed: 31.985

#### First classification results
- Probably legal entities: 6.900.490
- Probably person: 4.166.006
- Unknown: 47.778

### 2 - Firms identifiers - Regex

[02_1_firm_detection_function.sql](02_1_firm_detection_function.sql) We create a function to check over a regular expression list if a record is a firm or not. The list contains some common firms identifiers. 

[02_2_classify_with_firm_detection.sql](02_2_classify_with_firm_detection.sql) We use the firm detection function to check some 'probably legal' applicants from the 'unknown' set: 104

### 3 - New firms detection - Regex

[03_1_firm_detection_2_function](03_1_firm_detection_2_function) 

[03_2_classify_with_firm_detection_2.sql](03_2_classify_with_firm_detection_2.sql)
- Firms detected from 'unknown': 34

### 4 - Common names for individuals

[04_1_individuals_detection_function.sql](04_1_individuals_detection_function.sql): function to identify some common words for a natural person inside the name.
[04_2_classify_with_individuals_detection.sql](04_2_classify_with_individuals_detection.sql)
- Individuals detected from 'unknown': 284
- Individuals detected from 'probably legal': 87.241

### 5 - Common names and surnames
- We [create](05_2_classify_with_surname_detection.sql) a table called `names_and_lastnames` to load the [common names](../../Actors/applicants%20classification/resources/common_names.csv) and [common last names](../../Actors/applicants%20classification/resources/lastnames.csv) lists.
  
[05_1_person_name_detection_function.sql](05_1_person_name_detection_function.sql): function to detect names based on the tables loaded just before.
[05_2_classify_with_person_name_detection.sql](05_2_classify_with_person_name_detection.sql).
- Individuals detected from 'unknown': 24.324

### 6 - Names pattern

[06_classify_by_names_pattern.sql](06_classify_by_names_pattern.sql)
- Individuals detected from 'unknown': 16.122
  - Pattern: 3 words and a comma inside the `person_name` column. To get the number of words we calculated the number of spaces plus one. We counted comma `,` as a space because there are values with name and surname separated by comma.
- Firms detected from 'unknown': 499
  - Pattern: the `doc_std_name` column value contains only 1 word or more than 4.

#### 7 - Final Firms Identifiers

[07_classify_with_firm_common_words.sql](07_classify_with_firm_common_words.sql)

Classify based on some other common identifiers related with firms.
- Firms detected from 'unknown': 0

#### Final classification results
- Probably legal entities: 6.813.886
- Probably person: 4.293.977
- Unknown: 6.411

#### 8 - Field `type` to tag the applicants as legal or person

[08_add_field_type_to_applicants_table.sql](08_add_field_type_to_applicants_table.sql)
- Number of legal type: 86.308.171
- Number of natural person type: 17.770.967

#### 9 - Comparison with tagging coming from raw Patstat data

After analyzing we have decided to change the tagging based on this file [verif_type_applicant.xlsx](/uploads/314de06c41cb392b3b84aa576149b46b/verif_type_applicant.xlsx). We will mix the tagging coming from raw Patstat data (tls206_person table) and the tagging we have produced (field `type` in applt_addr_ifris table). There are some cases that need further checking but that will be done in a next version of RPD.
