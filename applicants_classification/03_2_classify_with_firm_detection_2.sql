USE patstat2021_lab;

DROP TABLE IF EXISTS entities_recognition_probably_legal_tmp;

CREATE TABLE entities_recognition_probably_legal_tmp AS
SELECT
    doc_std_name_id,
    doc_std_name,
    person_id,
    person_name,
    invt_seq_nr
FROM entities_recognition_unknown
WHERE firm_detection_2(doc_std_name) = 'legal person' OR firm_detection_2(person_name) = 'legal person';
-- Query OK, 34 rows affected (9 min 31,48 sec)
-- Records: 34  Duplicates: 0  Warnings: 0

INSERT INTO entities_recognition_probably_legal
SELECT * 
FROM entities_recognition_probably_legal_tmp;
-- 34

-- Deletes from unknown by doc_std_name_id
DELETE entities_recognition_unknown FROM entities_recognition_unknown
WHERE EXISTS (
  SELECT 1
  FROM entities_recognition_probably_legal_tmp
  WHERE entities_recognition_unknown.doc_std_name_id = entities_recognition_probably_legal_tmp.doc_std_name_id
);
-- 34
