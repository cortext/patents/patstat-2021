USE patstat2021_lab;

DROP TABLE IF EXISTS entities_recognition_probably_legal_tmp;

CREATE TABLE entities_recognition_probably_legal_tmp AS
SELECT
  doc_std_name_id,
  doc_std_name,
  person_id,
  person_name,
  invt_seq_nr
FROM
  entities_recognition_unknown
WHERE
  doc_std_name LIKE '%Management%'
  OR doc_std_name LIKE '%Financial%'
  OR doc_std_name LIKE '%Advisors%'
  OR doc_std_name LIKE '%Wealth%'
  OR doc_std_name LIKE '%Asset%'
  OR doc_std_name LIKE '%Advisory%'
  OR doc_std_name LIKE '%Planning%'
  OR doc_std_name LIKE '%Investments%'
  OR doc_std_name LIKE '% The %'
  OR doc_std_name LIKE '%Strategies%'
  OR doc_std_name LIKE '%Advisers%'
  OR doc_std_name LIKE '% And %'
  OR doc_std_name LIKE '%Retirement%'
  OR doc_std_name LIKE '%Fund%'
  OR doc_std_name LIKE '%Investors%'
  OR doc_std_name LIKE '%Private%'
  OR doc_std_name LIKE '%Securities%'
  OR doc_std_name LIKE '%Counsel%'
  OR doc_std_name LIKE '%Portfolio%'
  OR doc_std_name LIKE '%Street%'
  OR doc_std_name LIKE '%Strategic%'
  OR doc_std_name LIKE '%Advisor%'
  OR doc_std_name LIKE '%Equity%'
  OR doc_std_name LIKE '%Consultants%'
  OR doc_std_name LIKE '%First %'
  OR doc_std_name LIKE '%Money %'
  OR doc_std_name LIKE '% by %'
  OR doc_std_name LIKE '% New %'
  OR doc_std_name LIKE '% Of %'
  OR doc_std_name LIKE '% Hill %';
-- Query OK, 0 rows affected (0,25 sec)
-- Records: 0  Duplicates: 0  Warnings: 0

-- Final classification results
SELECT COUNT(*) FROM patstat2021_lab.entities_recognition_probably_legal; -- 6.813.886
SELECT COUNT(*) FROM patstat2021_lab.entities_recognition_probably_person; -- 4.293.977
SELECT COUNT(*) FROM patstat2021_lab.entities_recognition_unknown; -- 6.411