USE patstat2021_lab;

CREATE INDEX idx_doc_std_name USING BTREE
ON entities_recognition_unknown(doc_std_name(200))
-- Query OK, 47778 rows affected (0.53 sec)
-- Records: 47778  Duplicates: 0  Warnings: 0

CREATE INDEX idx_person_name USING BTREE
ON entities_recognition_unknown(person_name(200))
-- Query OK, 47778 rows affected (0.32 sec)
-- Records: 47778  Duplicates: 0  Warnings: 0

DROP TABLE IF EXISTS entities_recognition_probably_legal_tmp;

CREATE TABLE entities_recognition_probably_legal_tmp AS
SELECT
    doc_std_name_id,
    doc_std_name,
    person_id,
    person_name,
    invt_seq_nr
FROM entities_recognition_unknown
WHERE firm_detection(doc_std_name) = 'legal person' OR firm_detection(person_name) = 'legal person'
-- Query OK, 104 rows affected (40 min 51.30 sec)
-- Records: 104  Duplicates: 0  Warnings: 0

INSERT INTO entities_recognition_probably_legal
SELECT * 
FROM entities_recognition_probably_legal_tmp;

-- Deletes from unknown by doc_std_name_id
DELETE entities_recognition_unknown FROM entities_recognition_unknown
WHERE EXISTS (
  SELECT 1
  FROM entities_recognition_probably_legal_tmp
  WHERE entities_recognition_unknown.doc_std_name_id = entities_recognition_probably_legal_tmp.doc_std_name_id
);
-- 104
