USE patstat2021_lab;

CREATE TABLE names_and_lastnames (
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

LOAD DATA LOCAL INFILE '/home/patstat-2021/applicants_classification/resources/common_names.csv' 
INTO TABLE names_and_lastnames 
LINES TERMINATED BY "\n";
-- 5.494

LOAD DATA LOCAL INFILE '/home/patstat-2021/applicants_classification/resources/lastnames.csv' 
INTO TABLE names_and_lastnames 
LINES TERMINATED BY "\n";
-- 3.162

DROP TABLE IF EXISTS entities_recognition_probably_person_tmp_unknown;

CREATE TABLE entities_recognition_probably_person_tmp_unknown AS
SELECT
    doc_std_name_id,
    doc_std_name,
    person_id,
    person_name,
    invt_seq_nr
FROM entities_recognition_unknown
WHERE names_detection(doc_std_name) = 'person' OR names_detection(person_name) = 'person';
-- Query OK, 24324 rows affected (1 hour 43 min 5.06 sec)
-- Records: 24324  Duplicates: 0  Warnings: 0

INSERT INTO entities_recognition_probably_person
SELECT * 
FROM entities_recognition_probably_person_tmp_unknown;
-- 24.324

CREATE INDEX idx_doc_std_name_id USING BTREE ON entities_recognition_probably_person_tmp_unknown (doc_std_name_id);

-- Deletes from unknown by doc_std_name_id
DELETE FROM entities_recognition_unknown
WHERE doc_std_name_id IN (
  SELECT doc_std_name_id
  FROM entities_recognition_probably_person_tmp_unknown
);
-- 24.324
