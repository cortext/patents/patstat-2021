USE patstat2021_lab;

-- Total number of applicants: 11.114.275
SELECT COUNT(DISTINCT doc_std_name_id) FROM patstat2021_staging.applt_addr_ifris_frac;

-- --------------------------------------------------------------------------------------
-- STEP 1. Creates a table with probably legal entities where inventors sequence number 
-- is equal to 0 
CREATE TABLE entities_recognition_probably_legal AS
SELECT
    doc_std_name_id,
    doc_std_name,
    person_id,
    person_name,
    invt_seq_nr
FROM
    patstat2021_staging.applt_addr_ifris_frac
WHERE
    invt_seq_nr = 0
GROUP BY
    doc_std_name_id;
-- Query OK, 8842743 rows affected (18 min 26,41 sec)
-- Records: 8842743  Duplicates: 0  Warnings: 0

-- ------------------------------------------------------------------------------------
-- STEP 2. Creates a table with the unknown entities, where inventors sequence number 
-- is higher than 0
CREATE TABLE entities_recognition_unknown AS
SELECT
    doc_std_name_id,
    doc_std_name,
    person_id,
    person_name,
    invt_seq_nr
FROM
    patstat2021_staging.applt_addr_ifris_frac
WHERE
    invt_seq_nr > 0
GROUP BY
    doc_std_name_id;
-- Query OK, 2709300 rows affected (1 min 16,00 sec)
-- Records: 2709300  Duplicates: 0  Warnings: 0

CREATE INDEX idx_doc_std_name_id USING BTREE 
ON entities_recognition_unknown (doc_std_name_id);

-- Cleaning repeated data
DELETE FROM
    patstat2021_lab.entities_recognition_unknown
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            patstat2021_lab.entities_recognition_probably_legal
    );
-- Query OK, 437668 rows affected (41.32 sec)

-- ------------------------------------------------------------------------------------
-- STEP 3. Creates a table with the probably individuals where source is different 
-- than "MISSING"
CREATE INDEX idx_source USING BTREE ON patstat2021_staging.invt_addr_ifris_frac (source);

CREATE TABLE entities_recognition_probably_person AS
SELECT
    doc_std_name_id,
    doc_std_name,
    person_id,
    person_name,
    invt_seq_nr
FROM
    entities_recognition_unknown
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            patstat2021_staging.invt_addr_ifris_frac
        WHERE
            SOURCE <> "MISSING"
    );
-- Query OK, 2028488 rows affected (10 min 3.33 sec)
-- Records: 2028488  Duplicates: 0  Warnings: 0

CREATE INDEX idx_doc_std_name_id USING BTREE 
ON entities_recognition_probably_person (doc_std_name_id);

-- Cleaning repeated data
DELETE FROM
    entities_recognition_unknown
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            entities_recognition_probably_person
    );
-- Query OK, 2009217 rows affected (1 min 15.53 sec)

-- ------------------------------------------------------------------------------------
-- STEP 4. Inserts the applicants with no more than 1 patent application from the 
-- unknown into the probably person table
INSERT INTO
    entities_recognition_probably_person
SELECT
    a.doc_std_name_id,
    a.doc_std_name,
    a.person_id,
    a.person_name,
    a.invt_seq_nr
FROM
    entities_recognition_unknown AS a
    INNER JOIN patstat2021_staging.applt_addr_ifris_frac AS b ON a.doc_std_name_id = b.doc_std_name_id
GROUP BY
    doc_std_name_id
HAVING
    COUNT(a.doc_std_name_id) < 2;
-- Query OK, 195265 rows affected (16.57 sec)
-- Records: 195265  Duplicates: 0  Warnings: 0

-- Cleaning data
DELETE FROM
    entities_recognition_unknown
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            entities_recognition_probably_person
    );
-- Query OK, 195265 rows affected (16.71 sec)

-- ------------------------------------------------------------------------------------
-- STEP 5. Inserts into the 'probably person' table when the 'probably legal' applicant
-- exists in the inventors table from Patstat and it has no more than 20 applications

CREATE INDEX idx_doc_std_name_id USING BTREE 
ON entities_recognition_probably_legal (doc_std_name_id);

INSERT INTO
    entities_recognition_probably_person
SELECT
    a.doc_std_name_id,
    a.doc_std_name,
    a.person_id,
    a.person_name,
    a.invt_seq_nr
FROM
    entities_recognition_probably_legal AS a
    INNER JOIN patstat2021_staging.applt_addr_ifris_frac AS b ON a.doc_std_name_id = b.doc_std_name_id
WHERE
    a.doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            patstat2021_staging.invt_addr_ifris_frac
    )
GROUP BY
    doc_std_name_id
HAVING
    COUNT(a.doc_std_name_id) < 21
ORDER BY
    COUNT(a.doc_std_name_id) DESC;
-- Query OK, 1880329 rows affected (2 min 11.93 sec)
-- Records: 1880329  Duplicates: 0  Warnings: 0

-- Cleaning data
DELETE FROM
    entities_recognition_probably_legal
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            entities_recognition_probably_person
    );
-- Query OK, 1880329 rows affected (1 min 8.48 sec)

-- ------------------------------------------------------------------------------------
-- STEP 6. Inserts from 'probably legal' to 'probably person' table
INSERT INTO
    entities_recognition_probably_person
SELECT
    a.doc_std_name_id,
    a.doc_std_name,
    a.person_id,
    a.person_name,
    a.invt_seq_nr
FROM
    entities_recognition_probably_legal AS a
    INNER JOIN patstat2021_staging.applt_addr_ifris_frac AS b ON a.doc_std_name_id = b.doc_std_name_id
WHERE
    (
        a.person_name LIKE "%,%"
        OR a.person_name LIKE "%;%"
    )
    AND (
        a.doc_std_name_id IN (
            SELECT
                doc_std_name_id
            FROM
                patstat2021_staging.invt_addr_ifris_frac
        )
    )
GROUP BY
    doc_std_name_id
HAVING
    Count(a.doc_std_name_id) < 200;
-- Query OK, 29939 rows affected (18.08 sec)
-- Records: 29939  Duplicates: 0  Warnings: 0

-- Cleaning data
DELETE FROM
    entities_recognition_probably_legal
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            entities_recognition_probably_person
    );
-- Query OK, 29939 rows affected (16.53 sec)

-- ------------------------------------------------------------------------------------
-- STEP 7. Creates a temporal table trying to identify more 'probaly person' applicants

CREATE TABLE temporal AS
SELECT
    a.doc_std_name_id,
    a.person_id,
    a.person_name,
    a.invt_seq_nr,
    a.cnt,
    b.cnt AS cnt2
FROM
    (
        SELECT
            doc_std_name_id,
            person_id,
            person_name,
            invt_seq_nr,
            Count(*) AS cnt
        FROM
            patstat2021_staging.applt_addr_ifris_frac
        WHERE
            invt_seq_nr < 1
        GROUP BY
            doc_std_name_id
    ) AS a
    INNER JOIN (
        SELECT
            doc_std_name_id,
            Count(*) AS cnt
        FROM
            patstat2021_staging.applt_addr_ifris_frac
        WHERE
            invt_seq_nr > 0
        GROUP BY
            doc_std_name_id
    ) AS b ON a.doc_std_name_id = b.doc_std_name_id
HAVING
    (a.cnt * 100 / (b.cnt + a.cnt)) < 20;
-- Query OK, 109484 rows affected (4 min 2.62 sec)
-- Records: 109484  Duplicates: 0  Warnings: 0

INSERT INTO
    entities_recognition_probably_person
SELECT
    a.doc_std_name_id,
    a.doc_std_name,
    a.person_id,
    a.person_name,
    a.invt_seq_nr
FROM
    entities_recognition_probably_legal AS a
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            temporal
    );
-- Query OK, 31985 rows affected (0.81 sec)
-- Records: 31985  Duplicates: 0  Warnings: 0

-- Cleaning data
DELETE FROM
    entities_recognition_probably_legal
WHERE
    doc_std_name_id IN (
        SELECT
            doc_std_name_id
        FROM
            entities_recognition_probably_person
    );
-- Query OK, 31985 rows affected (17.50 sec)

-- First classification results
SELECT COUNT(*) FROM entities_recognition_probably_legal; -- 6.900.490
SELECT COUNT(*) FROM entities_recognition_probably_person; -- 4.166.006
SELECT COUNT(*) FROM entities_recognition_unknown; -- 47.778