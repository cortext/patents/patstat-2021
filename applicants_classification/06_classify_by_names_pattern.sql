USE patstat2021_lab;

DROP TABLE IF EXISTS entities_recognition_probably_person_tmp;
DROP TABLE IF EXISTS entities_recognition_probably_legal_tmp;

-- ---------------------------------------------------------------------------------------------------
-- Pattern for natural person names: person_name column with three words and one comma
CREATE TABLE entities_recognition_probably_person_tmp AS
SELECT
  doc_std_name_id,
  doc_std_name,
  person_id,
  person_name,
  invt_seq_nr
FROM
  entities_recognition_unknown
WHERE
  CHAR_LENGTH(REPLACE(person_name, ",", "")) - 
    CHAR_LENGTH(REPLACE(REPLACE(person_name, ",", " "), " ", "")) + 1 IN (2, 3)
  AND CHAR_LENGTH(person_name) - CHAR_LENGTH(REPLACE (person_name, ",", "")) = 1;
-- Query OK, 16122 rows affected (1,08 sec)
-- Records: 16122  Duplicates: 0  Warnings: 0

INSERT INTO entities_recognition_probably_person
SELECT * 
FROM entities_recognition_probably_person_tmp;
-- 16.122

CREATE INDEX idx_person_id USING BTREE ON entities_recognition_probably_person_tmp (person_id);

-- Deletes from unknown by person_id
DELETE FROM entities_recognition_unknown
WHERE person_id IN (
  SELECT person_id
  FROM entities_recognition_probably_person_tmp
);
-- 16.122

-- ---------------------------------------------------------------------------------------------------
-- Pattern for legal person names: doc_std_name column with only one word or more than four.
CREATE TABLE entities_recognition_probably_legal_tmp AS
SELECT
  doc_std_name_id,
  doc_std_name,
  person_id,
  person_name,
  invt_seq_nr
FROM
  entities_recognition_unknown
WHERE
  CHAR_LENGTH(doc_std_name) - CHAR_LENGTH(REPLACE(doc_std_name, " ", "")) + 1 NOT IN (2, 3);
-- Query OK, 499 rows affected (0,21 sec)
-- Records: 499  Duplicates: 0  Warnings: 0

INSERT INTO entities_recognition_probably_legal
SELECT * 
FROM entities_recognition_probably_legal_tmp;
-- 499

CREATE INDEX idx_doc_std_name_id USING BTREE ON entities_recognition_probably_legal_tmp (doc_std_name_id);

-- Deletes from unknown by doc_std_name_id
DELETE FROM entities_recognition_unknown
WHERE doc_std_name_id IN (
  SELECT doc_std_name_id
  FROM entities_recognition_probably_legal_tmp
);
-- 499
