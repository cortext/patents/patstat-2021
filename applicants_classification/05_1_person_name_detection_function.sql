DROP FUNCTION IF EXISTS patstat2021_lab.names_detection;

DELIMITER $$
$$

CREATE DEFINER=`user`@`%` FUNCTION `patstat2021_lab`.`names_detection`(myString VARCHAR(300)) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
BEGIN
	
    DECLARE done INT DEFAULT FALSE;    
    DECLARE Tag VARCHAR(15);
    DECLARE surn VARCHAR(50);
    DECLARE regex VARCHAR(300);
    
    
    DECLARE curs CURSOR FOR 
        SELECT `name` FROM names_and_lastnames;
     -- declare NOT FOUND handler
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
  	OPEN curs;
  	
  	get_name: LOOP
  		FETCH curs INTO surn;
  		
  		
  		SET regex = CONCAT('( |^)',surn,'([^a-zA-Z0-9_]|$)');
  		IF myString regexp(regex) THEN 
  		 	SET Tag = 'person';
  		END IF;
   	    
  		IF Tag = 'person' THEN
			LEAVE get_name;
   	    END IF;
   	    
   	   IF done THEN 
 			LEAVE get_name;
 	   END IF;

        END LOOP;
   
        CLOSE curs;

    RETURN Tag;

END;
$$
DELIMITER ;