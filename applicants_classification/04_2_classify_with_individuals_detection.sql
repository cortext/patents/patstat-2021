USE patstat2021_lab;

DROP TABLE IF EXISTS entities_recognition_probably_person_tmp_legal;
DROP TABLE IF EXISTS entities_recognition_probably_person_tmp_unknown;

-- ---------------------------------------------------------------------------------------------------
-- Identify natural person applicants from the `unknown` table
CREATE TABLE entities_recognition_probably_person_tmp_unknown AS
SELECT
  doc_std_name_id,
  doc_std_name,
  person_id,
  person_name,
  invt_seq_nr
FROM
  entities_recognition_unknown
WHERE
  individual_detection(doc_std_name) = 'natural person'
  OR individual_detection(person_name) = 'natural person';
-- Query OK, 284 rows affected (54.14 sec)
-- Records: 284  Duplicates: 0  Warnings: 0

INSERT INTO entities_recognition_probably_person
SELECT * 
FROM entities_recognition_probably_person_tmp_unknown;
-- 284

-- Deletes from unknown by doc_std_name_id
DELETE entities_recognition_unknown FROM entities_recognition_unknown
WHERE EXISTS (
  SELECT 1
  FROM entities_recognition_probably_person_tmp_unknown
  WHERE entities_recognition_unknown.doc_std_name_id = 
    entities_recognition_probably_person_tmp_unknown.doc_std_name_id
);
-- 284

-- ---------------------------------------------------------------------------------------------------
-- Identify natural person applicants from the `legal` table
CREATE TABLE entities_recognition_probably_person_tmp_legal AS
SELECT
  doc_std_name_id,
  doc_std_name,
  person_id,
  person_name,
  invt_seq_nr
FROM
  entities_recognition_probably_legal
WHERE
  individual_detection(doc_std_name) = 'natural person'
  OR individual_detection(person_name) = 'natural person';
-- Query OK, 87241 rows affected (2 hours 25 min 4.38 sec)
-- Records: 87241  Duplicates: 0  Warnings: 0

INSERT INTO entities_recognition_probably_person
SELECT * 
FROM entities_recognition_probably_person_tmp_legal;
-- 87.241

CREATE INDEX idx_doc_std_name_id USING BTREE 
  ON entities_recognition_probably_person_tmp_legal (doc_std_name_id);

-- Deletes from unknown by doc_std_name_id
DELETE FROM entities_recognition_probably_legal
WHERE doc_std_name_id IN (
  SELECT doc_std_name_id
  FROM entities_recognition_probably_person_tmp_legal
);
-- 87.241 
