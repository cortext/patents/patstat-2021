USE patstat2021_staging;

-- --------------------------------------------------------------------------------------
-- Backup tables and rename to keep `applt_addr_ifris` as the latest
RENAME TABLE applt_addr_ifris TO backup_06092022_applt_addr_ifris;

CREATE TABLE backup_05102022_applt_addr_ifris_frac LIKE applt_addr_ifris_frac;

INSERT INTO backup_05102022_applt_addr_ifris_frac 
SELECT * FROM applt_addr_ifris_frac;
-- 104.100.624

RENAME TABLE applt_addr_ifris_frac TO applt_addr_ifris;

-- --------------------------------------------------------------------------------------
-- Add the column `type`
ALTER TABLE applt_addr_ifris
ADD COLUMN `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

-- --------------------------------------------------------------------------------------
-- Update based on person_id

CREATE INDEX idx_person_id USING BTREE 
  ON patstat2021_lab.entities_recognition_probably_person (person_id);
-- 4.293.977
CREATE INDEX idx_person_id USING BTREE ON applt_addr_ifris (person_id);
-- 104.100.624

UPDATE applt_addr_ifris a
SET `type` = 'legal'
WHERE EXISTS (
  SELECT 1
  FROM patstat2021_lab.entities_recognition_probably_legal b
  WHERE a.person_id = b.person_id
);
-- Query OK, 27778115 rows affected (39 min 2.44 sec)
-- Rows matched: 27778115  Changed: 27778115  Warnings: 0

UPDATE applt_addr_ifris a
SET `type` = 'person'
WHERE EXISTS (
  SELECT 1
  FROM patstat2021_lab.entities_recognition_probably_person b
  WHERE a.person_id = b.person_id
);
-- Query OK, 9561907 rows affected (34 min 54.06 sec)
-- Rows matched: 9561907  Changed: 9561907  Warnings: 0

-- --------------------------------------------------------------------------------------
-- Update based on doc_std_name_id
UPDATE applt_addr_ifris a
SET `type` = 'legal'
WHERE a.`type` IS NULL AND 
  EXISTS (
    SELECT 1
    FROM patstat2021_lab.entities_recognition_probably_legal b
    WHERE a.doc_std_name_id = b.doc_std_name_id
  );
-- Query OK, 58530056 rows affected (50 min 18.98 sec)
-- Rows matched: 58530056  Changed: 58530056  Warnings: 0

UPDATE applt_addr_ifris a
SET `type` = 'person'
WHERE a.`type` IS NULL AND 
  EXISTS (
    SELECT 1
    FROM patstat2021_lab.entities_recognition_probably_person b
    WHERE a.doc_std_name_id = b.doc_std_name_id
  );
-- Query OK, 8209060 rows affected (33 min 8.77 sec)
-- Rows matched: 8209060  Changed: 8209060  Warnings: 0

CREATE INDEX idx_type USING BTREE ON applt_addr_ifris (`type`);
-- Query OK, 104100624 rows affected (28 min 0,48 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

SELECT COUNT(*) FROM applt_addr_ifris WHERE `type` = 'legal'; -- 86.308.171
SELECT COUNT(*) FROM applt_addr_ifris WHERE `type` = 'person'; -- 17.770.967
