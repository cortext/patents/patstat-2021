# Geomapping

1. Build a dictionary of longitude, latitude values: `addr_ifris_geo_mapping` table. 

    [01_addr_ifris_geo_mapping_table.sq](01_addr_ifris_geo_mapping_table.sql)
  - Creates a table with the columns: longitude, latitude, ura_uniqueid, rurban_area_id, rurban_area_name, rurban_area_characteristics, nuts_source, nuts_id.
  - Takes all the distinct longitude, latitude values from the dictionary of all the Patstat addresses, building a dictionary.

2. Fill table with values from previous version.
  
    [02_fill_with_available_data.sql](02_fill_with_available_data.sql) 
  - Bring data from 
  Note: The table containing all the information regarding urban and rural areas (URA) (`ura_all_info`) was included in `patstat2021_lab`. It comes from the Cortext Manager [documentation](https://docs.cortext.net/cortext-geospatial-exploration-tool/#ura-data-sources-references) where you can [download it](https://docs.cortext.net/wp-content/uploads/ura-all-info.csv).

3. Get URA info for the geographical coordinates with Cortext Manager
  - Build a file with the distinct longitude and latitude values.
    ```sh
    $ mysql -u <your_user> -p -h <ssh_host> -P <db_port> -e 'SELECT longitude, latitude, CONCAT(longitude, "|", latitude) AS longlat FROM patstat2021_lab.addr_ifris_geo_mapping WHERE ura_uniqueid IS NULL' > /home/patstat2021_longlat_for_geomapping.csv
    ```
  - Upload the file, parse it and run the Geospatial exploration script in Cortext Manager.
  - [03_load_longlat_geomapped.sql](03_load_longlat_geomapped.sql): Once it is finished, take the results and load them into a table, and based on it update `addr_ifris`.
