USE patstat2021_lab;

CREATE TABLE `addr_ifris_geo_mapping` (
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `ura_uniqueid` int(8) DEFAULT NULL,
  `rurban_area_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rurban_area_name` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rurban_area_characteristics` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nuts_source` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nuts_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`longitude`,`latitude`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0,04 sec)

INSERT IGNORE INTO addr_ifris_geo_mapping (longitude, latitude)
SELECT DISTINCT longitude, latitude
FROM addr_ifris;
-- Query OK, 588797 rows affected, 2 warnings (1 min 0,52 sec)
-- Records: 588798  Duplicates: 1  Warnings: 2
