USE patstat2021_lab;

-- Create indexes if needed

-- Bring data from table tmp_geo_mapping built in Patstat 2017
UPDATE addr_ifris_geo_mapping a 
  INNER JOIN patstatAvr2017_lab.tmp_geo_mapping b
    ON a.longitude = b.longitude AND a.latitude = b.latitude 
  INNER JOIN ura_all_info c
    ON b.ura_id = c.uniqueid
SET
  a.ura_uniqueid = b.ura_id, 
  a.rurban_area_id = b.id_ura, 
  a.rurban_area_name = c.uraname, 
  a.rurban_area_characteristics = c.uratype, 
  a.nuts_source = b.source_nuts, 
  a.nuts_id = b.sourceid_nuts;
-- Query OK, 249944 rows affected (58,00 sec)
-- Rows matched: 249944  Changed: 249944  Warnings: 0

-- Bring data from rpd_actors
UPDATE addr_ifris_geo_mapping a 
  INNER JOIN risis_patents_17_2_0.rpd_actors b
    ON a.longitude = b.longitude AND a.latitude = b.latitude 
SET
  a.ura_uniqueid = b.ura_uniqueid, 
  a.rurban_area_id = b.rurban_area_id, 
  a.rurban_area_name = b.rurban_area_name, 
  a.rurban_area_characteristics = b.rurban_area_characteristics, 
  a.nuts_source = b.nuts_source, 
  a.nuts_id = b.nuts_id
WHERE a.ura_uniqueid IS NULL AND b.ura_uniqueid IS NOT NULL;
-- Query OK, 2122 rows affected (30,84 sec)
-- Rows matched: 2122  Changed: 2122  Warnings: 0

-- Bring data from rpd_inventors
UPDATE addr_ifris_geo_mapping a 
  INNER JOIN risis_patents_17_2_0.rpd_inventors b
    ON a.longitude = b.longitude AND a.latitude = b.latitude 
SET
  a.ura_uniqueid = b.ura_uniqueid, 
  a.rurban_area_id = b.rurban_area_id, 
  a.rurban_area_name = b.rurban_area_name, 
  a.rurban_area_characteristics = b.rurban_area_characteristics, 
  a.nuts_source = b.nuts_source, 
  a.nuts_id = b.nuts_id
WHERE a.ura_uniqueid IS NULL AND b.ura_uniqueid IS NOT NULL;
-- Query OK, 1081 rows affected (6 min 16,05 sec)
-- Rows matched: 1081  Changed: 1081  Warnings: 0

-- Counting
SELECT COUNT(*) FROM addr_ifris_geo_mapping; -- 588.797
SELECT COUNT(*) FROM addr_ifris_geo_mapping WHERE ura_uniqueid IS NULL; -- 335.650
SELECT COUNT(*) FROM addr_ifris_geo_mapping WHERE ura_uniqueid IS NOT NULL; -- 253.147
