-- select the long lat values without info filled to run the geomapping script

USE patstat2021_lab;

CREATE TABLE patstat2021_lab.`tmp_longlat` (
  `file` text COLLATE utf8_unicode_ci,
  `id` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `parserank` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `data_idx` (`data`(40))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/user/Downloads/geomapped/longlat.csv'
INTO TABLE tmp_longlat
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
-- Query OK, 335650 rows affected (13,19 sec)
-- Records: 335650  Deleted: 0  Skipped: 0  Warnings: 0

CREATE TABLE patstat2021_lab.`tmp_map_id_nuts` (
  `file` text COLLATE utf8_unicode_ci,
  `id` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `parserank` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/user/Downloads/geomapped/map_id_NUTS.csv'
INTO TABLE tmp_map_id_nuts
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
-- Query OK, 218290 rows affected (5,06 sec)
-- Records: 218290  Deleted: 0  Skipped: 0  Warnings: 0

CREATE TABLE patstat2021_lab.`tmp_map_id_rural_urban_areas` (
  `file` text COLLATE utf8_unicode_ci,
  `id` int(11) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `parserank` int(11) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/user/Downloads/geomapped/map_id_rural_urban_areas.csv'
INTO TABLE tmp_map_id_rural_urban_areas
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
-- Query OK, 334545 rows affected (6,02 sec)
-- Records: 334545  Deleted: 0  Skipped: 0  Warnings: 0

CREATE TABLE patstat2021_lab.`tmp_nuts` (
  `Zone name` text COLLATE utf8_unicode_ci,
  `Country code` text COLLATE utf8_unicode_ci,
  `Population` double DEFAULT NULL,
  `Area (square kilometers)` double DEFAULT NULL,
  `Name of source` text COLLATE utf8_unicode_ci,
  `UniqueID` text COLLATE utf8_unicode_ci NOT NULL,
  `Number of addresses` int(11) DEFAULT NULL,
  `Addresses per 1000 inhabitants` text COLLATE utf8_unicode_ci,
  `Addresses per square km` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`UniqueID`(6))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/user/Downloads/geomapped/nuts.csv'
INTO TABLE tmp_nuts
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
-- Query OK, 1500 rows affected (0,29 sec)
-- Records: 1500  Deleted: 0  Skipped: 0  Warnings: 0

CREATE TABLE patstat2021_lab.`tmp_rural_urban_areas` (
  `Zone name` text COLLATE utf8_unicode_ci,
  `Country code` text COLLATE utf8_unicode_ci,
  `Population` double DEFAULT NULL,
  `Area (square kilometers)` double DEFAULT NULL,
  `Name of source` text COLLATE utf8_unicode_ci,
  `UniqueID` int(11) NOT NULL,
  `Number of addresses` int(11) DEFAULT NULL,
  `Addresses per 1000 inhabitants` text COLLATE utf8_unicode_ci,
  `Addresses per square km` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`UniqueID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOAD DATA LOCAL INFILE '/home/user/Downloads/geomapped/rural-urban-areas.csv'
INTO TABLE tmp_rural_urban_areas
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
-- Query OK, 5368 rows affected (0,57 sec)
-- Records: 5368  Deleted: 0  Skipped: 0  Warnings: 0

UPDATE addr_ifris_geo_mapping AS dict
INNER JOIN tmp_longlat AS a
  ON CONCAT(dict.longitude, "|", dict.latitude) = a.`data`
INNER JOIN tmp_map_id_nuts AS b 
  ON b.id = a.id
INNER JOIN tmp_map_id_rural_urban_areas AS c 
  ON c.id = b.id
INNER JOIN tmp_nuts AS d 
  ON d.UniqueID = b.data
INNER JOIN tmp_rural_urban_areas AS e 
  ON e.UniqueID = c.data
INNER JOIN ura_all_info AS f
  ON f.uniqueid = c.data
SET
  dict.ura_uniqueid = c.data,
  dict.nuts_source = SUBSTRING_INDEX(SUBSTRING_INDEX(d.`Name of source`, ' ', 1), ' ', -1),
  dict.nuts_id = SUBSTRING_INDEX(SUBSTRING_INDEX(d.`Name of source`, ' ', 2), ' ', -1),
  dict.rurban_area_id = e.`Name of source`,
  dict.rurban_area_name = e.`Zone name`,
  dict.rurban_area_characteristics = f.`uratype`
WHERE dict.ura_uniqueid IS NULL;
-- Query OK, 217926 rows affected (36,30 sec)
-- Rows matched: 217926  Changed: 217926  Warnings: 0

CREATE INDEX idx_ura_uniqueid USING BTREE ON addr_ifris_geo_mapping (ura_uniqueid);
-- Query OK, 588797 rows affected (7,84 sec)
-- Records: 588797  Duplicates: 0  Warnings: 0

SELECT COUNT(*) FROM addr_ifris_geo_mapping; -- 588.797
SELECT COUNT(*) FROM addr_ifris_geo_mapping WHERE ura_uniqueid IS NULL; -- 117.724
SELECT COUNT(*) FROM addr_ifris_geo_mapping WHERE ura_uniqueid IS NOT NULL; -- 471.073

DROP TABLE patstat2021_lab.`tmp_longlat`;
DROP TABLE patstat2021_lab.`tmp_map_id_nuts`;
DROP TABLE patstat2021_lab.`tmp_map_id_rural_urban_areas`;
DROP TABLE patstat2021_lab.`tmp_nuts`;
DROP TABLE patstat2021_lab.`tmp_rural_urban_areas`;
