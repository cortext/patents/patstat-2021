USE patstat2021_staging;

SELECT COUNT(DISTINCT person_id) 
FROM applt_addr_ifris 
WHERE adr_final = '';
-- 12.333.507

-- -----------------------------------------------------------------------
DROP TABLE IF EXISTS patstat2021_lab.tmp_person_id_empty_adr;
CREATE TABLE patstat2021_lab.tmp_person_id_empty_adr (
  person_id int(10) NOT NULL,
  PRIMARY KEY (person_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0,07 sec)

INSERT IGNORE INTO patstat2021_lab.tmp_person_id_empty_adr 
SELECT person_id
FROM patstat2021_staging.applt_addr_ifris
WHERE adr_final = '';
-- Query OK, 12333507 rows affected (19 min 55,33 sec)
-- Records: 57071507  Duplicates: 44738000  Warnings: 0

DROP TABLE IF EXISTS patstat2021_lab.tmp_person_id_obtain_adr_from_2017;
CREATE TABLE patstat2021_lab.tmp_person_id_obtain_adr_from_2017 (
  person_id int(10) NOT NULL,
  PRIMARY KEY (person_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0,05 sec)

INSERT IGNORE INTO patstat2021_lab.tmp_person_id_obtain_adr_from_2017 
SELECT a.person_id
FROM patstatAvr2017.applt_addr_ifris a
WHERE a.adr_final != '' AND CHAR_LENGTH(a.adr_final) > 1 AND 
  EXISTS (
    SELECT 1
    FROM patstat2021_lab.tmp_person_id_empty_adr b
    WHERE a.person_id = b.person_id);
-- Query OK, 1563415 rows affected (5 min 9,27 sec)
-- Records: 17886613  Duplicates: 16323198  Warnings: 0

-- Count nb of applicants for which we can be obtained their address 
-- from Patstat2017 
SELECT COUNT(*) FROM patstat2021_lab.tmp_person_id_obtain_adr_from_2017;
-- 1.563.415

-- -----------------------------------------------------------------------

DROP TABLE IF EXISTS patstat2021_lab.tmp_adr_from2017;
CREATE TABLE patstat2021_lab.tmp_adr_from2017 (
  person_id int(10) NOT NULL,
  source varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  methode varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  adr_final varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  iso_ctry varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  label varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  longitude double DEFAULT NULL,
  latitude double DEFAULT NULL,
  accuracy varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  layer varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  city varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  region varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  iso3 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (person_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0,12 sec)

INSERT IGNORE INTO patstat2021_lab.tmp_adr_from2017
SELECT person_id, source, methode, adr_final, iso_ctry, label, 
  longitude, latitude, accuracy, layer, city, region, iso3
FROM patstatAvr2017.applt_addr_ifris b
WHERE adr_final != ''  AND EXISTS (
  SELECT 1
  FROM patstat2021_lab.tmp_person_id_obtain_adr_from_2017 d
  WHERE b.person_id = d.person_id
);
-- Query OK, 1563415 rows affected (4 min 29,54 sec)
-- Records: 17886618  Duplicates: 16323203  Warnings: 0

CREATE INDEX idx_adr_final USING BTREE ON patstat2021_staging.applt_addr_ifris (adr_final);

-- Bring those addresses to Patstat2021
UPDATE patstat2021_staging.applt_addr_ifris a 
  INNER JOIN patstat2021_lab.tmp_adr_from2017 b
    ON a.person_id = b.person_id
  LEFT JOIN patstatAvr2017.tls801_country c 
  	ON b.iso3 = c.iso_alpha3 
SET
  a.adr_final = b.adr_final,
  a.source = b.source,
  a.methode = b.methode,
  a.iso_ctry = b.iso_ctry,
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = c.ctry_code
WHERE a.adr_final = '';

/* 
As the above query might take too long we split it in several queries:
Iterating the following query until the number of updated rows is zero

UPDATE patstat2021_staging.applt_addr_ifris a 
  INNER JOIN (
  	SELECT G.*
	  FROM patstat2021_lab.tmp_adr_from2017 G
	  INNER JOIN patstat2021_staging.applt_addr_ifris Y 
	  ON G.person_id = Y.person_id
	  WHERE Y.adr_final = ''
	  LIMIT 10000
  ) b
    ON a.person_id = b.person_id
  LEFT JOIN patstatAvr2017.tls801_country c 
  	ON b.iso3 = c.iso_alpha3 
SET
  a.adr_final = b.adr_final,
  a.source = b.source,
  a.methode = b.methode,
  a.iso_ctry = b.iso_ctry,
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = c.ctry_code
WHERE a.adr_final = '' ;
*/

SELECT COUNT(DISTINCT person_id) 
FROM patstat2021_staging.applt_addr_ifris 
WHERE adr_final = '';
-- 10.770.092
