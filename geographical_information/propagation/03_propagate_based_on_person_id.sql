USE patstat2021_staging;

CREATE TABLE tmp_person_id_missing_adr (
  person_id int(10) NOT NULL DEFAULT '0',
  source varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  adr_final varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  iso_ctry varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  label varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  longitude double DEFAULT NULL,
  latitude double DEFAULT NULL,
  accuracy varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  layer varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  city varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  region varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  iso2 varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (person_id),
  KEY idx_adr_final (adr_final(200))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- Query OK, 0 rows affected (0,05 sec)

INSERT INTO tmp_person_id_missing_adr (person_id)
SELECT DISTINCT person_id
FROM applt_addr_ifris
WHERE adr_final = '';
-- Query OK, 9355399 rows affected (8 min 10,67 sec)
-- Records: 9355399  Duplicates: 0  Warnings: 0

UPDATE tmp_person_id_missing_adr a 
  INNER JOIN (
    SELECT * FROM applt_addr_ifris
    WHERE adr_final != ''
    GROUP BY person_id
  )b 
  ON a.person_id = b.person_id
SET 
  a.adr_final = b.adr_final,
  a.iso_ctry = b.iso_ctry,
  a.source = 'STATPER',
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = b.iso2;
-- Query OK, 29417 rows affected (15 min 4,25 sec)
-- Rows matched: 29417  Changed: 29417  Warnings: 0

DELETE FROM tmp_person_id_missing_adr
WHERE adr_final IS NULL;
-- Query OK, 9325982 rows affected (5 min 1,59 sec)

UPDATE applt_addr_ifris a 
  INNER JOIN tmp_person_id_missing_adr b 
  ON a.person_id = b.person_id
SET 
  a.adr_final = b.adr_final,
  a.iso_ctry = b.iso_ctry,
  a.source = b.source,
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = b.iso2
WHERE a.adr_final = '';
-- Query OK, 427131 rows affected (7 min 21,63 sec)
-- Rows matched: 427131  Changed: 427131  Warnings: 0

SELECT COUNT(DISTINCT person_id) 
FROM applt_addr_ifris 
WHERE adr_final = '';
-- 9.325.982
