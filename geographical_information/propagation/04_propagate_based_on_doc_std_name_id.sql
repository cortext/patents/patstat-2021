USE patstat2021_staging;

-- Create a table with all the distinct doc_std_name_id values and the count of the rows 
-- related to it containing an address value (adr_final != '')
CREATE TABLE patstat2021_lab.tmp_count_addr_doc_std_name_id AS
SELECT doc_std_name_id, count(*) AS count_addr
FROM applt_addr_ifris
WHERE adr_final != ''
GROUP BY doc_std_name_id;
-- Query OK, 3964988 rows affected (2 min 26.05 sec)
-- Records: 3964988  Duplicates: 0  Warnings: 0

ALTER TABLE patstat2021_lab.tmp_count_addr_doc_std_name_id
ADD INDEX idx_doc_std_name_id (doc_std_name_id);
-- Query OK, 3964988 rows affected (2.17 sec)
-- Records: 3964988  Duplicates: 0  Warnings: 0

-- Create a table to store for each doc_std_name_id value, the address existing in more 
-- than the 60% of the rows associated with it (if any)
CREATE TABLE patstat2021_lab.tmp_final_address_by_doc_std_name_id AS 
SELECT c.doc_std_name_id, c.adr_final, count(*), c.source, c.iso_ctry, c.label, 
  c.longitude, c.latitude, c.accuracy, c.layer, c.city, c.region, c.iso2 
FROM applt_addr_ifris AS c 
WHERE c.adr_final != '' 
GROUP BY c.doc_std_name_id, c.adr_final
HAVING count(*) >= (
    SELECT tmp_count.count_addr * 0.6
    FROM patstat2021_lab.tmp_count_addr_doc_std_name_id AS tmp_count
    WHERE tmp_count.doc_std_name_id = c.doc_std_name_id
);
-- Query OK, 3547434 rows affected (10 min 3.31 sec)
-- Records: 3547434  Duplicates: 0  Warnings: 0

ALTER TABLE patstat2021_lab.tmp_final_address_by_doc_std_name_id
ADD INDEX idx_doc_std_name_id (doc_std_name_id);
-- Query OK, 3547434 rows affected (7.11 sec)
-- Records: 3547434  Duplicates: 0  Warnings: 0

-- Update based on doc_std_name_id
UPDATE applt_addr_ifris a
  INNER JOIN patstat2021_lab.tmp_final_address_by_doc_std_name_id b
  ON a.doc_std_name_id = b.doc_std_name_id
SET 
  a.adr_final = b.adr_final,
  a.iso_ctry = b.iso_ctry,
  a.source = 'STATDS',
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = b.iso2
WHERE a.adr_final = '';
-- Query OK, 1361758 rows affected (26 min 10.72 sec)
-- Rows matched: 1361758  Changed: 1361758  Warnings: 0

-- Count rows missing address after propagation
SELECT COUNT(*) FROM applt_addr_ifris WHERE adr_final = '';
-- 31.833.060

SELECT COUNT(DISTINCT person_id) 
FROM applt_addr_ifris 
WHERE adr_final = '';
-- 9.050.991
