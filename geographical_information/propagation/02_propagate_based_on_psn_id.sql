USE patstat2021_staging;

-- Count rows missing address before propagation
SELECT COUNT(*) FROM applt_addr_ifris WHERE adr_final = '';
-- 40.056.949

SELECT COUNT(DISTINCT person_id) 
FROM applt_addr_ifris 
WHERE adr_final = '';
-- 10.770.092

-- Add psn_id field 
ALTER TABLE applt_addr_ifris ADD COLUMN psn_id INT(11) NULL AFTER `type`;
-- Query OK, 104100624 rows affected (1 hour 11 min 52.46 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

-- Set a value for psn_id field
UPDATE
  applt_addr_ifris AS a
  INNER JOIN tls206_person AS b ON a.person_id = b.person_id
SET
  a.psn_id = b.psn_id;
-- Query OK, 104098782 rows affected (1 hour 32 min 21,77 sec)
-- Rows matched: 104098782  Changed: 104098782  Warnings: 0

-- Take the rest from Patstat2017
UPDATE
  applt_addr_ifris AS a
  INNER JOIN patstatAvr2017.applt_addr_ifris AS b ON a.person_id = b.person_id
SET
  a.psn_id = b.psn_id
WHERE a.psn_id IS NULL;
-- Query OK, 0 rows affected (7 min 23,82 sec)
-- Rows matched: 38  Changed: 0  Warnings: 0

ALTER TABLE applt_addr_ifris ADD INDEX idx_psn_id (psn_id);
-- Query OK, 104100624 rows affected (1 hour 31 min 11.28 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

-- Create a table with all the distinct psn_id values and the count of the rows 
-- related to it containing an address value (adr_final != '')
CREATE TABLE patstat2021_lab.tmp_count_addr_psn_id AS
SELECT psn_id, count(*) AS count_addr
FROM applt_addr_ifris
WHERE adr_final != ''
GROUP BY psn_id;
-- Query OK, 3513417 rows affected (2 min 10.15 sec)
-- Records: 3513417  Duplicates: 0  Warnings: 0

ALTER TABLE patstat2021_lab.tmp_count_addr_psn_id
ADD INDEX idx_psn_id (psn_id);
-- Query OK, 3513417 rows affected (2.17 sec)
-- Records: 3513417  Duplicates: 0  Warnings: 0

-- Create a table to store for each psn_id value, the address existing in more 
-- than the 60% of the rows associated with it (if any)
CREATE TABLE patstat2021_lab.tmp_final_address_by_psn_id AS 
SELECT c.psn_id, c.adr_final, count(*), c.source, c.iso_ctry, c.label, 
  c.longitude, c.latitude, c.accuracy, c.layer, c.city, c.region, c.iso2 
FROM applt_addr_ifris AS c 
WHERE c.adr_final != '' 
GROUP BY c.psn_id, c.adr_final
HAVING count(*) >= (
    SELECT tmp_count.count_addr * 0.6
    FROM patstat2021_lab.tmp_count_addr_psn_id AS tmp_count
    WHERE tmp_count.psn_id = c.psn_id
);
-- Query OK, 3089833 rows affected (9 min 19.73 sec)
-- Records: 3089833  Duplicates: 0  Warnings: 0

ALTER TABLE patstat2021_lab.tmp_final_address_by_psn_id
ADD INDEX idx_psn_id (psn_id);
-- Query OK, 3089833 rows affected (6.24 sec)
-- Records: 3089833  Duplicates: 0  Warnings: 0

-- Update based on psn_id
UPDATE applt_addr_ifris a
  INNER JOIN patstat2021_lab.tmp_final_address_by_psn_id b
  ON a.psn_id = b.psn_id
SET 
  a.adr_final = b.adr_final,
  a.iso_ctry = b.iso_ctry,
  a.source = 'STATPSN',
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = b.iso2
WHERE a.adr_final = '';
-- Query OK, 6435000 rows affected (1 hour 17 min 24.23 sec)
-- Rows matched: 6435000  Changed: 6435000  Warnings: 0

-- Count rows missing address after propagation
SELECT COUNT(*) FROM applt_addr_ifris WHERE adr_final = '';
-- 33.621.949

SELECT COUNT(DISTINCT person_id) 
FROM applt_addr_ifris 
WHERE adr_final = '';
-- 9.355.399
