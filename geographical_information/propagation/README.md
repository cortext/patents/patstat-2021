# Propagation

## Bring missing addresses from Patstat-2017

- Number of distinct applicants: 17.789.634
- Number of applicants without address (before): 12.333.507 (69.32%)
- **Number of applicants without address** (**after**): 10.770.092 (**60.54%**)

- Number of distinct inventors: 47.733.543
- Number of inventors without address (before): 27.764.323 (58.16%)
- **Number of inventors without address** (**after**): 25.622.116 (**53.68%**)

## Propagate applicants addresses based on psn_id, person_id and doc_std_name_id

- Based on psn_id: Taking from the applicants table all the rows with the same psn_id, we count if the 60% or more contain the same address then we use this address to fill the other rows with the same psn_id in which is missing.
  - Number of applicants without address (**before**): 10.770.092 (**60.54%**)
  - Number of applicants without address (**after**): 9.355.399 (**52.59%**) 

- Based on person_id.
  - Number of applicants without address (**before**): 9.355.399 (**52.59%**) 
  - Number of applicants without address (**after**): 9.325.982 (**52.42%**) 

- Based on doc_std_name_id: Same procedure followed with psn_id.
  - Number of applicants without address (**before**): 9.325.982 (**52.42%**) 
  - Number of applicants without address (**after**):  (**50.88%**)