USE patstat2021_staging;
-- USE patstat2021_main;

CREATE INDEX idx_adr_final USING BTREE ON patstat2021_staging.applt_addr_ifris (adr_final(200));
-- Query OK, 104100624 rows affected (1 hour 11 min 21,85 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

CREATE INDEX idx_iso_ctry USING BTREE ON patstat2021_staging.applt_addr_ifris (iso_ctry);
-- Query OK, 104100624 rows affected (57 min 39,47 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

-- ----------------------------------------------------------------------------
-- Count nb of distinct person_id values without address info but longitude and latitude info assigned
SELECT COUNT(*) FROM ( 
  SELECT DISTINCT person_id 
  FROM patstat2021_staging.applt_addr_ifris 
  WHERE adr_final = '' AND iso_ctry = '' AND (label IS NOT NULL OR label != '') 
  ) AS a;
-- 6.613.775

-- Delete that info from the dictionary of addresses in Patstat
DELETE FROM
  patstat2021_lab.addr_ifris
WHERE
  addr_final = ''
  AND iso_ctry = '';
-- Query OK, 1 row affected (0,01 sec)

-- Delete that info from Patstat
UPDATE applt_addr_ifris
SET
  label = NULL,
  longitude = NULL,
  latitude = NULL,
  accuracy = NULL,
  layer = NULL,
  city = NULL,
  region = NULL,
  iso2 = NULL
WHERE adr_final = '' AND iso_ctry = '';
-- Query OK, 36335401 rows affected (57 min 1,57 sec)
-- Rows matched: 36335401  Changed: 36335401  Warnings: 0

UPDATE invt_addr_ifris
SET
  label = NULL,
  longitude = NULL,
  latitude = NULL,
  accuracy = NULL,
  layer = NULL,
  city = NULL,
  region = NULL,
  iso2 = NULL
WHERE adr_final = '' AND iso_ctry = '';
-- 76.274.640

SELECT COUNT(DISTINCT person_id) FROM applt_addr_ifris;
-- 17.789.634
SELECT COUNT(DISTINCT person_id) FROM applt_addr_ifris WHERE adr_final = '';
-- 12.333.507

SELECT COUNT(DISTINCT person_id) FROM invt_addr_ifris;
-- 47.733.543
SELECT COUNT(DISTINCT person_id) FROM invt_addr_ifris WHERE adr_final = '';
-- 27.764.323
