USE patstat2021_staging;
-- USE patstat2021_main;

ALTER TABLE applt_addr_ifris
ADD COLUMN `label` VARCHAR(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `longitude` DOUBLE DEFAULT NULL,
ADD COLUMN `latitude` DOUBLE DEFAULT NULL,
ADD COLUMN `accuracy` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL ,
ADD COLUMN `layer` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `city` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `region` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `iso2` VARCHAR(2) COLLATE utf8_unicode_ci DEFAULT NULL;
-- Query OK, 104100624 rows affected (42 min 19.83 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

UPDATE applt_addr_ifris a
  INNER JOIN patstat2021_lab.addr_ifris b
  ON a.adr_final = b.addr_final AND a.iso_ctry = b.iso_ctry
SET
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = b.iso2
WHERE b.longitude != 0 and b.latitude != 0;
-- Query OK, 101681452 rows affected (2 hours 4 min 8.28 sec)
-- Rows matched: 101681452  Changed: 101681452  Warnings: 0

ALTER TABLE invt_addr_ifris
ADD COLUMN `label` VARCHAR(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `longitude` DOUBLE DEFAULT NULL,
ADD COLUMN `latitude` DOUBLE DEFAULT NULL,
ADD COLUMN `accuracy` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL ,
ADD COLUMN `layer` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `city` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `region` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
ADD COLUMN `iso2` VARCHAR(2) COLLATE utf8_unicode_ci DEFAULT NULL;
-- Query OK, 197449042 rows affected (1 hour 29 min 41.49 sec)
-- Records: 197449042  Duplicates: 0  Warnings: 0

UPDATE invt_addr_ifris a
  INNER JOIN patstat2021_lab.addr_ifris b
  ON a.adr_final = b.addr_final AND a.iso_ctry = b.iso_ctry
SET
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = b.iso2
WHERE b.longitude != 0 and b.latitude != 0;
-- Query OK, 194015238 rows affected (3 hours 28 min 56.33 sec)
-- Rows matched: 194015238  Changed: 194015238  Warnings: 0
