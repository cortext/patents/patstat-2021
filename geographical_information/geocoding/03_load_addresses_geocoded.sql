USE patstat2021_lab;

CREATE TABLE `geocoded_addr_ifris` (
  `file` text COLLATE utf8_unicode_ci,
  `id` int(11) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `parserank` int(11) DEFAULT NULL,
  `address` varchar(550) COLLATE utf8_unicode_ci,
  `label` text COLLATE utf8_unicode_ci,
  `longitude` double COLLATE utf8_unicode_ci,
  `latitude` double COLLATE utf8_unicode_ci,
  `confidence` FLOAT COLLATE utf8_unicode_ci,
  `accuracy` text COLLATE utf8_unicode_ci,
  `layer` text COLLATE utf8_unicode_ci,
  `city` text COLLATE utf8_unicode_ci,
  `region` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `iso2` text COLLATE utf8_unicode_ci,
  `iso3` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`address`(200))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0,07 sec)

LOAD DATA LOCAL INFILE '/home/geocoded_addr_ifris.csv'
INTO TABLE geocoded_addr_ifris
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
-- Query OK, 8033692 rows affected, 65535 warnings (4 min 12,97 sec)                                                                                                                                                                                                                 
-- Records: 8033724  Deleted: 0  Skipped: 32  Warnings: 927099

CREATE INDEX idx_longitude USING BTREE ON patstat2021_lab.geocoded_addr_ifris (longitude);
-- Query OK, 8033692 rows affected (4 min 9,10 sec)
-- Records: 8033692  Duplicates: 0  Warnings: 0

SELECT COUNT(*) FROM patstat2021_lab.geocoded_addr_ifris; -- 8.033.692
SELECT COUNT(*) FROM patstat2021_lab.geocoded_addr_ifris WHERE longitude = 0; -- 317.826
SELECT COUNT(*) FROM patstat2021_lab.geocoded_addr_ifris WHERE longitude != 0; -- 7.715.866

SELECT COUNT(*) FROM patstat2021_lab.addr_ifris; -- 8.711.633
SELECT COUNT(*) FROM patstat2021_lab.addr_ifris WHERE longitude = 0 OR longitude IS NULL; -- 6.711.585
SELECT COUNT(*) FROM patstat2021_lab.addr_ifris WHERE longitude != 0; -- 2.000.048

UPDATE addr_ifris a 
  INNER JOIN geocoded_addr_ifris b 
  ON CONCAT(addr_final, ", ", iso_ctry) = b.address
SET
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.confidence = b.confidence,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = b.iso2
WHERE
  a.longitude IS NULL AND
  b.confidence != 0;
-- Query OK, 5932740 rows affected (8 min 17,45 sec)
-- Rows matched: 5932740  Changed: 5932740  Warnings: 0

SELECT COUNT(*) FROM patstat2021_lab.addr_ifris WHERE longitude = 0 OR longitude IS NULL; -- 787.158
SELECT COUNT(*) FROM patstat2021_lab.addr_ifris WHERE longitude != 0; -- 7.924.475

-- Cases where geocoding results were (longitude, latitude) = (0,0)
SELECT count(*) FROM patstat2021_lab.addr_ifris WHERE longitude = 0 and latitude = 0; -- 7.961
