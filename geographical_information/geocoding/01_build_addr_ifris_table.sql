USE patstat2021_lab;

CREATE INDEX idx_adr_final_iso_ctry USING BTREE 
  ON patstat2021_staging.applt_addr_ifris (adr_final(200),iso_ctry);
-- Query OK, 104100624 rows affected (38 min 31,37 sec)
-- Records: 104100624  Duplicates: 0  Warnings: 0

CREATE INDEX idx_adr_final_iso_ctry USING BTREE 
  ON patstat2021_staging.invt_addr_ifris (adr_final(200),iso_ctry);
-- Query OK, 197449042 rows affected (1 hour 43 min 17,35 sec)
-- Records: 197449042  Duplicates: 0  Warnings: 0

CREATE TABLE `addr_ifris` (
  `addr_final` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `iso_ctry` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `confidence` float DEFAULT NULL,
  `accuracy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `layer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iso2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`addr_final`(200),`iso_ctry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0,02 sec)

INSERT IGNORE INTO addr_ifris (addr_final, iso_ctry)
SELECT DISTINCT adr_final, iso_ctry
FROM patstat2021_staging.applt_addr_ifris;
-- Query OK, 974489 rows affected, 6 warnings (7 min 22,19 sec)
-- Records: 1695511  Duplicates: 721022  Warnings: 6

INSERT IGNORE INTO addr_ifris (addr_final, iso_ctry)
SELECT DISTINCT adr_final, iso_ctry
FROM patstat2021_staging.invt_addr_ifris;
-- Query OK, 7016160 rows affected, 20 warnings (16 min 11,98 sec)
-- Records: 7573513  Duplicates: 557353  Warnings: 20
