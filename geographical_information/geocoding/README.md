# Geocoding

1. Build a dictionary of addresses: `addr_ifris` table. 
    
    [01_build_addr_ifris_table.sql](01_build_addr_ifris_table.sql)
  - Creates a table with the columns: addr_final, iso_ctry, label, longitude, latitude, accuracy, layer, city, region, iso2.
  - Takes all the distinct adr_final, iso_ctry values from applicants and inventors tables, building a dictionary.

2. Fill table with values from previous version.
  
    [02_fill_with_available_data.sql](02_fill_with_available_data.sql) 

3. Geocode addresses with Cortext Manager
  - Build a file with the addresses to geocode.
    ```sh
    $ mysql -u <your_user> -p -h <ssh_host> -P db_port -e 'SELECT addr_final, iso_ctry, CONCAT(addr_final, ", ", iso_ctry) AS addr_to_geoc FROM patstat2021_lab.addr_ifris WHERE longitude IS NULL' > /home/patstat2021_addresses_to_geocode.csv
    ```
  - Upload the file, parse it and run the Geocoding script in Cortext Manager.
  - [03_load_addresses_geocoded.sql](03_load_addresses_geocoded.sql): Once it is finished, take the results and load them into a table, and based on it update `addr_ifris`.

4. Update geolocation values in the applicants and inventors tables
  - [04_update_applt_invt_tables.sql](04_update_applt_invt_tables.sql): the update is performed in **both** staging and main database.

5. Corrections
  The rows with empty value for address and country had a value for longitude, latitude and the rest of the geolocation fields, which was not correct. They were then set to NULL.

  Number of distinct applicants: 17.789.634
  Number of applicants without address: 12.333.507 (69.32%)
  Number of distinct inventors: 47.733.543
  Number of inventors without address: 27.764.323 (58.16%)
