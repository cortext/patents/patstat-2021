USE patstat2021_lab;

CREATE INDEX idx_longitude USING BTREE 
  ON patstatAvr2017.applt_addr_ifris (longitude);
-- Query OK, 94021308 rows affected (1 hour 59 min 3,28 sec)
-- Records: 94021308  Duplicates: 0  Warnings: 0

CREATE INDEX idx_longitude USING BTREE 
  ON patstatAvr2017.invt_addr_ifris (longitude);
-- Query OK, 155090791 rows affected (2 hours 4 min 22,02 sec)
-- Records: 155090791  Duplicates: 0  Warnings: 0

CREATE INDEX idx_iso_alpha3 USING BTREE 
  ON patstatAvr2017.tls801_country (iso_alpha3);
-- Query OK, 236 rows affected (0,54 sec)
-- Records: 236  Duplicates: 0  Warnings: 0

UPDATE addr_ifris a
  INNER JOIN patstatAvr2017.applt_addr_ifris b 
  	ON a.addr_final = b.adr_final AND a.iso_ctry = b.iso_ctry
  INNER JOIN patstatAvr2017.tls801_country c 
  	ON b.iso3 = c.iso_alpha3 
SET 
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = c.ctry_code 
WHERE a.longitude IS NULL AND b.longitude IS NOT NULL;
-- Query OK, 1228358 rows affected (49 min 8,72 sec)
-- Rows matched: 1228358  Changed: 1228358  Warnings: 0

UPDATE addr_ifris a
  INNER JOIN patstatAvr2017.invt_addr_ifris b 
  	ON a.addr_final = b.adr_final AND a.iso_ctry = b.iso_ctry
  INNER JOIN patstatAvr2017.tls801_country c 
  	ON b.iso3 = c.iso_alpha3 
SET 
  a.label = b.label,
  a.longitude = b.longitude,
  a.latitude = b.latitude,
  a.accuracy = b.accuracy,
  a.layer = b.layer,
  a.city = b.city,
  a.region = b.region,
  a.iso2 = c.ctry_code 
WHERE a.longitude IS NULL AND b.longitude IS NOT NULL;
-- Query OK, 771688 rows affected (36 min 9,24 sec)
-- Rows matched: 771688  Changed: 771688  Warnings: 0

SELECT COUNT(*) FROM addr_ifris;
-- 8.711.633

SELECT COUNT(*) FROM addr_ifris WHERE longitude IS NULL;
-- 6.711.587

SELECT COUNT(*) FROM addr_ifris WHERE longitude IS NOT NULL;
-- 2.000.046
