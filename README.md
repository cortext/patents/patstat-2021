# Patstat IFRIS 2021

The official documentation of the raw Patstat data, version April 2021, is in [DataCatalog_Global_v5.17.pdf](DataCatalog_Global_v5.17.pdf).

We treat this database producing a new one called **Patstat IFRIS 2021**.

## Load raw data

See folder: [load_clean_reorganize_data/load_new_raw_data](load_clean_reorganize_data/load_new_raw_data/)

## Apply same procedures as the ones applied in previous treated version

In the previous versions that we have treated, we have applied multiple procedures to add new variables, enrich missing values, add new analytical dimensions, and more. Part of the procedures applied were documented in some files that are available here: [2018-12-20-FS-Fiche_Suivi-PATSTAT_2017.pdf](2018-12-20-FS-Fiche_Suivi-PATSTAT_2017.pdf) and [2018-12-20-ST-Mise_en_base-PATSTAT.pdf](2018-12-20-ST-Mise_en_base-PATSTAT.pdf).

To continue offering this added value, now those procedures need to be applied to Patstat 2021, but as it has changed slightly in terms of data structure and content, we need to analyze and modify the existing scripts so they can be used for this version. 

- [main_changes_by_table_from_2017_to_2021.md](main_changes_by_table_from_2017_to_2021.md): The major changes from the Patstat version 2017 to 2021 are summarized here.
- [scripts_log_patstat_2017.xlsx](scripts_log_patstat_2017.xlsx): The information about the processes we applied in Patstat 2017 was listed here, in the order they were applied.

## Procedures applied in Patstat IFRIS 2021

The processes that will be applied in this Patstat IFRIS version 2021 are going to be stored in this repository as much as possible, organized in folders by topics. So to keep a track of the **the order in which they were applied** they will be listed here: [scripts_log_patstat_2021.xlsx](scripts_log_patstat_2021.xlsx), with information about where to find the files related and some notes if needed.

---
## Reusable procedures
Some reusable procedures inside our scripts, you may find more:
- Unzip and load raw data from a new version
    Location: [load_clean_reorganize_data/load_new_raw_data/unzip_and_load_data/](load_clean_reorganize_data/load_new_raw_data/unzip_and_load_data/)

- Run multiple sql files
    Location: [load_clean_reorganize_data/load_new_raw_data/run_multiple_sql_files.sh](load_clean_reorganize_data/load_new_raw_data/run_multiple_sql_files.sh)

- Convert 'create table' sql files from TSQL syntax to MySQL
    Location: [load_clean_reorganize_data/load_new_raw_data/tsql_to_mysql_create_table.py](load_clean_reorganize_data/load_new_raw_data/tsql_to_mysql_create_table.py)

## Versioning strategy

- Raw data: `patstat2021_raw_data`
- Useful data: `patstat2021_lab`
- Main DB: `patstat2021_main`, stable and latest version of changes.
- Staging DB: `patstat2021_staging`, where all the operations are performed before a big change is included in the Main DB.
- Backups DB: `patstat2021_backups`

### Flow
- Before any table update in `Staging DB`, a backup table must be created in `Backups DB` with this name format: `backupYYYYMMDD_tablename`.
- After the needed changes are performed and tested in `Staging DB`, the table can be moved to `Main DB`, replacing the old one there, if any.
