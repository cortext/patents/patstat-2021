# Check changes in the documentation of Patstat 2021, by table

- TLS214_NPL_PUBL
    - Layout changes, they have consolidated duplicates and created NPL_PUBLN_ID: a MD5 hash based primary key using the NPL_BIBLIO attribute. The XP-Number has been moved to a new attribute XP-NR
    - Some attributes are now populated for more NPL types.
    - Attribute ONLINE_CLASSIFICATION may hold more than one Derwent class.
    - Attribute ONLINE_AVAILABILITY can now hold up to 500 characters.
    - Attribute NPL_AUTHOR now can hold up to 1 000 characters.
    - Duplicates have been consolidated
    - **Key attribute NPL_PUBLN_ID is not a number any more, but the MD5 hash of attribute NPL_BIBLIO**
    - **XP-number – if it exists – is now in a separate attribute XP_NR (was in NPL_PUBLN_ID before)**

- TLS212_CITATION
    - **The domain of the attribute CITED_NPL_PUBLN_ID changed.**
    - **Domain of attribute CITED_NPL_PUBLN_ID has been changed to be consistent with attribute NPL_PUBLN_ID of table TLS214_NPL_PUBLN.**
    - **New attribute CITN_REPLENISHED contains the information whether a citation has been replenished and from which publication the citation originates.**
    
- TLS231_INPADOC_LEGAL_EVENT
    - **New attribute EVENT_ID, which is a stable identifier for legal events.**
    - The attribute FEE_COUNTRY is now also populated for event code VSFP (Validation State Fee Paid). In human language: granted EP applications can be “validated” in non EP-member states which accept a granted EP patent as being a valid patent. Currently this concerns Morocco (MA), Republic of Moldova (MD), Tunisia (TN) and Cambodia (KH). There are about 200 granted EP applications that have been validated in one of the validation states. This attribute allows you to see which patents have been validated in which country.
    - For event code VS25, the attribute LAPSE_COUNTRY has been corrected.
    - **New attribute EVENT_FILING_DATE**
    - New legal event codes for EPO’s validation states have been introduced, notably VSFP (annual fee payment in a validation state) and VS25 (lapse in validation state).
    
- TLS201_APPLN
    - **Attribute APPLN_AUTH has changed its meaning. For international applications it now contains “WO” and not the receiving office anymore. Please adapt your existing script.**
    - **Attribute RECEIVING_OFFICE has been added, which is also part of the alternate key.**
    
- TLS215_CITN_CATEG
    - **New attribute CITN_REPLENISHED contains the information whether a citation has been replenished and from which publication the citation originates.**
    - **New attribute relevant_clain.**
    - Attribute “CITN_CATEG”: “&” is now a valid value.

- TLS206_PERSON
    - NUTS codes (see attribute NUTS) is now conformant to the official NUTS codes in the reference table.
    - **Some NUTS values in TLS206_PERSON are provided by OECD’s REGPAT database, January 2020. These enhanced records have a NUTS_LEVEL with value 4.**
    - Some DOCDB standardised names are wrongly assigned to persons of US patents, because the sequence of persons in the USPTO data source and that in DOCDB sometimes do not match correctly.
    There is no known fix. When working with US patent applicants or inventors, you should avoid using the DOCDB standardised name. Instead, you might consider other harmonised names available in table TLS206_PERSON.
    - Addresses of persons which have been added in the 2019 Spring Edition or later have not been regionalised. That means that for these persons the attributes NUTS and NUTS_LEVEL contain default values only.
    To rectify this situation, some NUTS values in TLS206_PERSON are enhanced by OECD’s REGPAT database, January 2020. These enhanced records have a NUTS_LEVEL with value 4.
    
- TLS904_NUTS
    - **The reference table TLS904_NUTS has been restructured. It contains now NUTS codes from levels 1 to 3.**
    - NUTS codes (see attribute NUTS) is now conformant to the official NUTS codes in the reference table.

- TLS803_LEGAL_EVENT_CODE
    - **Change of name and content of attributes for legal event categories which are now based on WIPO ST.27**
    - Attribute EVENT_IMPACT is deprecated.

- TLS801_COUNTRY
    - The attribute STATE_INDICATOR has been renamed to ORGANISATION_FLAG and its domain changed accordingly.

- TLS216_APPLN_CONT
    - now contains also changes of IPR type (changes from patents to utility model to vice versa).

- TLS226_PERSON_ORIG
    - Because the new attribute is also part of the Primary Key, persons which have been represented in the past by a single record in a person table are now represented by multiple records in case their original language name differs.

- Others
    - Table TLS906_PERSON has been removed. Its content has been merged into table TLS206_PERSON.
    - The mapping of applicants, inventors and assignees of the USPTO data source to applicants and inventors in PATSTAT has been improved.
    - Some flag attributes changed their domain from 0/1 to Y/N:
        • Attribute GRANTED in table TLS201_APPLN
        • Attribute PUBLN_FIRST_GRANT in table TLS211_PAT_PUBLN